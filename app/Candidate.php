<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = [
        'user_id',
        'seeking_work',
        'profile_picture',
        'curriculum',
        'names',
        'maternal_name',
        'paternal_name',
        'country', 
        'state',
        'city',
        'district',
        'street',
        'house_number',
        'home_phone',
        'mobile_phone',
        'email',
        'gender', 
        'curp',
        'rfc',
        'nss' ,
        'birthday_date'
    ];
    public function education_data()
    {
        return $this->hasMany('App\EducationData');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language');
    }

    public function softwares()
    {
        return $this->belongsToMany('App\Software');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function work_experience()
    {
        return $this->hasMany('App\WorkExperience');
    }

    public function vacancies()
    {
        return $this->belongsToMany('App\Vacancy')->withPivot( 'id',
            'status',
            'date_postulate',
            'date_cv_sent',
            'reason_cv_sent',
            'date_interview',
            'reason_interview',
            'date_psychometric',
            'reason_psychometric',
            'date_hired',
            'reason_hired',
            'date_rejected',
            'reason_rejected'
        );
    }
}
