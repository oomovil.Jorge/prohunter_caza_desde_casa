<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateSoftware extends Model
{
    protected $table = 'candidate_software';
    protected $fillable = [
        'candidate_id',
        'software_id',
        'percentage_known',
        'certification_type'
    ];
}
