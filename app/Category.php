<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function preferences()
    {
        return $this->belongsToMany('App\Preference');
    }    

    public function vacancies()
    {
        return $this->hasMany('App\Vacancy');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
