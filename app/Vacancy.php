<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    public function category(){
        return belongsTo('App\Category');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function candidates(){
        return $this->belongsToMany('App\Candidate');   
    }
}
