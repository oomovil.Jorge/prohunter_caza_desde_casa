<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationData extends Model
{
    protected $table = 'education_data';
    protected $fillable = [
        'candidate_id',
        'universidad',
        'start_month',
        'start_year',
        'end_month',
        'end_year',
        'academic_level',
        'degree_obtained',
        'additional_info',
    ];
}
