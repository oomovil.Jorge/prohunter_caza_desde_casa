<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $fillable = ['info_dispositivo'];

    public function preference()
    {
        return $this->hasOne('App\Preference');
    }

    public function vacancies()
    {
        return $this->belongsToMany('App\Vacancy');
    }

    public function candidates()
    {
        return $this->hasMany('App\Candidate');
    }

    public function firebase()
    {
        return $this->hasOne('App\Firebase');
    }
}
