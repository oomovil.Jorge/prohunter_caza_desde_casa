<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateVacancy extends Model
{
    protected $table = 'candidate_vacancy';
    protected $fillable = [
        'candidate_id',
        'vacancy_id',
        'status',
        'date_postulate',
         ];
}
