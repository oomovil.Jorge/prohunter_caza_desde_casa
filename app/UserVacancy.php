<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVacancy extends Model
{
    protected $table = 'user_vacancy';
    protected $fillable = ['user_id', 'vacancy_id'];
}
