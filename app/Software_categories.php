<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Software_categories extends Model
{
    protected $table = "software_categories";
    protected $fillable = ['id_categoria', 'id_software'];
}
