<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firebase extends Model
{
    protected $fillable = ['user_id', 'token'];
    protected $table = 'firebase';


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
