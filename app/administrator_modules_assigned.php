<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class administrator_modules_assigned extends Model
{
    protected $table = 'administrator_modules_assigned';

    protected $fillable = [
        'user_id',
        'modules'
    ];
}
