<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_users extends Model
{
    protected $table = 'payment_users';
    protected $fillable = ['user_id', 'postulate_id','total_to_pay','paid_percentage'];

}
