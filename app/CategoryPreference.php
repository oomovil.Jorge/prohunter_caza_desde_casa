<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPreference extends Model
{
    protected $table = 'category_preference';
    protected $fillable = ['preference_id', 'category_id'];
}
