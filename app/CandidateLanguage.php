<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateLanguage extends Model
{
    protected $table = 'candidate_language';
    protected $fillable = [
        'candidate_id',
        'language_id',  
        'percentage_known',
        'certification_type'
    ];
}
