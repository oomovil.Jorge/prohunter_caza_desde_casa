<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCandidate extends Model
{
    protected $table = 'user_candidate';
}
