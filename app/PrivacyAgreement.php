<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyAgreement extends Model
{
    protected $table = 'privacy_agreement';
}
