<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecuperarPasswordMail extends Mailable{
    use Queueable, SerializesModels;

    public $title;
    public $correo;
    public $hashed_random_password;

    public function __construct($title,$correo,$hashed_random_password)
    {
        $this->title = $title;
        $this->correo = $correo;
        $this->hashed_random_password=$hashed_random_password;
    }

    public function build(){
        return $this->from('no-reply@prohunter.com')
                ->subject($this->title)
                ->view('mail.recuperar_password')
                ->withSwiftMessage(function ($message) {
                    $message->getHeaders()
                            ->addTextHeader($this->title, $this->correo,$this->hashed_random_password);
 });

    }
}
