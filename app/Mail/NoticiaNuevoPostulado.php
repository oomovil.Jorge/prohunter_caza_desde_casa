<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NoticiaNuevoPostulado extends Mailable{
    use Queueable, SerializesModels;

    public $titulo;
    public $correo;
    public $msg;

    public function __construct($titulo,$correo,$msg)
    {
        $this->titulo = $titulo;
        $this->correo = $correo;
        $this->msg = $msg;
    }

    public function build(){
        return $this->from('no-reply@prohunter.com')
                ->subject($this->titulo)
                ->view('mail.Noticia_Nuevo_Postulado')
                ->withSwiftMessage(function ($message) {
                    $message->getHeaders()
                            ->addTextHeader($this->titulo, $this->correo,$this->msg);
 });

    }
}
