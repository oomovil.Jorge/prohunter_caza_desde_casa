<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use App\UserVacancy;
use Illuminate\Support\Facades\DB;

class VacancyResource extends JsonResource
{

    public function toArray($request){

        $vacancy_id = null;
        $user = User::where('correo', $request->email)->first();

        if(!is_null($user)){

            $user_vacancy = UserVacancy::where('user_id', $user->id)
                          ->where('vacancy_id', $vacancy_id = $this->id)
                          ->first();

            if(is_null($user_vacancy))
                $is_favorite = false;
            else $is_favorite = true; 

        }else{
            $is_favorite = false;
        }

        $array = parent::toArray($request);
        $array['total_registred'] = DB::table('candidate_vacancy')->where('vacancy_id', $vacancy_id)->count();
        $array['favorite'] = $is_favorite;

        return $array;
    }

    public function with($request)
    {
        return [
            'exito' => 1,
            'msg' => 'Exito'
        ];
    }
}
