<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource{

    public function toArray($request)
    {
        if(empty($this->imagen_perfil))
            $imagen_perfil = $this->imagen_perfil;
        else
            $imagen_perfil = asset('storage/app/').'/'.$this->imagen_perfil;

        return [
            'nombre' => $this->nombre,
            'correo' => $this->correo,
            'contrasena' => $this->contrasena,
            'celular' => $this->celular,
            'info_dispositivo' => $this->info_dispositivo,
            'tipo' => $this->tipo,
            'imagen_perfil' => $imagen_perfil,
            'fecha_cumpleaños' => $this->fecha_cumpleaños,
            'lugar_nacimiento' => $this->lugar_nacimiento,
            'RFC' => $this->RFC
        ];
    }

    public function with($request){
        return [
            'exito' => 1,
            'msg' => 'Exito'
        ];
    }
}
