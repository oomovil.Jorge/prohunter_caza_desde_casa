<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\CandidateLanguage;
use App\CandidateSoftware;

class CandidateResource extends JsonResource
{

    public function toArray($request)
    {
        $personal_data = array();
        $personal_data['id'] = $this->id;
        $personal_data['seeking_work'] = $this->seeking_work;

        if(!is_null($this->profile_picture)) {
            $personal_data['profile_picture'] = asset('storage/app')."/" . $this->profile_picture;
        }
        else {
            $personal_data['profile_picture'] = $this->profile_picture;
        }
        if(!is_null($this->curriculum))
            $personal_data['curriculum'] = asset('storage/app/')."/" .$this->curriculum;
        else
            $personal_data['curriculum'] = $this->curriculum;

        $personal_data['names'] = $this->names;
        $personal_data['maternal_name'] = $this->maternal_name;
        $personal_data['paternal_name'] = $this->paternal_name;
        $personal_data['country'] = $this->country;
        $personal_data['state'] = $this->state;
        $personal_data['city'] = $this->city;
        $personal_data['district'] = $this->district;
        $personal_data['street'] = $this->street;
        $personal_data['house_number'] = $this->house_number;
        $personal_data['home_phone'] = $this->home_phone;
        $personal_data['mobile_phone'] = $this->mobile_phone;
        $personal_data['email'] = $this->email;
        $personal_data['gender'] = $this->gender;
        $personal_data['curp'] = $this->curp;
        $personal_data['rfc'] = $this->rfc;
        $personal_data['nss'] = $this->nss;
        $personal_data['birthday_date'] = $this->birthday_date;

        $languages_array = array();
        foreach($this->languages as $language){
            $language_item = array();
            $pivot = CandidateLanguage::where('candidate_id', $this->id)->where('language_id', $language->id)->first();
            $language_item['id'] = $language->id;
            $language_item['name'] = $language->name;
            $language_item['percentage_known'] = $pivot->percentage_known;
            $language_item['certification_type'] = $pivot->certification_type;
            $languages_array[] = $language_item;
        }

        $software_array = array();
        foreach($this->softwares as $software){

            $software_item = array();
            $pivot = CandidateSoftware::where('candidate_id', $this->id)->where('software_id', $software->id)->first();

            $software_item['id'] = $software->id;
            $software_item['name'] = $software->name;
            $software_item['percentage_known'] = $pivot->percentage_known;
            $software_item['certification_type'] = $pivot->certification_type;
            $software_array[] = $software_item;
        }
                return  [
            'personal_data' => $personal_data,
            'education_data' => $this->education_data,
            'languages' => $languages_array,
            'softwares' => $software_array,
            'work_experience' => $this->work_experience,
            'postulated_vacancies' => $this->vacancies,
        ];
    }
}