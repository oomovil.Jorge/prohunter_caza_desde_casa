<?php

namespace App\Http\Controllers;

use App\Notice;
use App\User;
use App\Http\Resources\NoticeResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\User as UserResource;

class NoticesController extends Controller{

    public function listado_notificacion(){

        $notices = Notice::all();

        return NoticeResource::collection($notices)->additional([
            'exito' => 1,
            'msg' => 'Exito'
        ]);
    }

    public function registrar_notificacion(Request $request){

        $Notice = new Notice;

        if($request->has(['inputtitulo', 'inputContenido'])) {

            $Notice->titulo = $request->input('inputtitulo');
            $Notice->contenido = $request->input('inputContenido');



            if(!is_null($request->file('inputfile')))
            {
                if(Storage::disk('local')->exists($Notice->imagen))
                    Storage::delete($Notice->imagen);
                $file_name = $request->file('inputfile')->store('Img_Avisos');
                $Notice->imagen = asset('storage/').'/'.$file_name;
            }

            if($Notice->save()){

                $tokens = DB::select("SELECT * FROM firebase;");
                $title = $Notice->titulo  ;
                $subtitle = '';
                $body =  $Notice->contenido ;
                $message = '';
                $icon = $Notice->imagen;
                $this->notificacion($tokens, $title, $subtitle, $body, $message, $icon);

                $msg = "Registrado correctamente.";
                return redirect('/admin/Notificaciones/Nueva_Notificacion?Msg='.$msg);
            }else{
                $msg = "Error al intentar guardar! Intentelo de nuevo mas tarde";
                return redirect('/admin/Notificaciones/?Error=true&Msg=' . $msg)->withInput();
            }



        }else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Notificaciones/?Error=true&Msg=' . $msg)->withInput();
        }


    }

    public function eliminar_notificacion(){

       $notice =  Notice::where('id', '=',  Input::get('id'))->first();

        if(!is_null($notice)) {

            if($notice->delete()){
                $msg = "Eliminado correctamente";
                return redirect('/admin/Notificaciones/?Msg='.$msg)->withInput();
            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Notificaciones/?Error=true&Msg='.$msg)->withInput();
            }
        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Notificaciones/?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function  modificar_notificacion(Request $request){

        $file_name = "";

            if ($request->has(['inputid','inputtitulo', 'inputContenido'])) {

                    $notice = Notice::find($request->input('inputid'));

                    if (!is_null($notice)) {

                        $notice->titulo = $request->input('inputtitulo');
                        $notice->contenido = $request->input('inputContenido');

                    if (!is_null($request->file('inputfile'))) {
                        if (Storage::disk('local')->exists($notice->imagen))
                            Storage::delete($notice->imagen);
                        $file_name = $request->file('inputfile')->store('Img_Avisos');
                        $notice->imagen = asset('storage/app/').'/'.$file_name;
                    }

                    if ($notice->save()) {


                        $tokens = DB::select("SELECT * FROM firebase;");
                        $title = $request->input('inputtitulo') ;
                        $subtitle = '';
                        $body =  $request->input('inputContenido');
                        $message = '';
                        $icon = asset('storage/app/').'/'.$file_name;
                        $this->notificacion($tokens, $title, $subtitle, $body, $message, $icon);



                        $msg = "Modificado correctamente";
                        return redirect('/admin/Notificaciones/Modificar_notificacion?id=' . $notice->id . '&Msg='.$msg);
                    } else {
                        $msg = "Hubo un error al tratar de Modificar";
                        return redirect('/admin/Notificaciones/Modificar_notificacion?id=' . $notice->id . '&Error=true&Msg='.$msg);
                    }

                    } else {
                        $msg = "No se encontro el registro";
                        return redirect('/admin/Notificaciones/Modificar_notificacion?id=' . $notice->id . '&Error=true&Msg='.$msg);
                    }

                } else {
                    $msg = "No se enviaron todos los campos requeridos";
                return redirect('/admin/Notificaciones/Modificar_notificacion?id=' . $request->input('inputid') . '&Error=true&Msg='.$msg);
                }
        }

    private function notificacion($token,$title,$subtitle,$body,$message,$icon){

        // CONFIGURE FIRE BASE DATA
        $token_to_send  = array();
        foreach($token as $item){
            //Se crea el el arreglo de token para 1 o *
            $token_to_send[] = $item->token;
         }
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array
        (
            'registration_ids' =>  $token_to_send,
            'notification' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon
                ],
            'data' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon
                ]
        );
        $headers = array(
            'Authorization:key = AAAAH1ZV3YM:APA91bGv-Nv8c7hr977n2Opoz77oiQR0tXZj8ExxbNIbPO91QUKnkAQwzcdRW1yWAD9tjRl4_nwRtLr9fRtpFpPeOiNK39blyNKj-mouWTglBlsBaM4y96dgx7GdGKJSByE5Il_O0nnq',
            'Content-Type: application/json'
        );

        //EXECUTE FIREBASE NOTIFICATIONS TO EACH TOKEN
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $msg = curl_exec($ch);
        curl_close($ch);

        if ($msg === FALSE) {
            $msg = "Error de Firebase";
            return '{"exito" : 0, "msg" : '.$msg.'}';
        }else{
            return '{"exito" : 0, "msg" : "true"}';
        }
    }



}
