<?php

namespace App\Http\Controllers;


use App\announcements;
use App\Firebase;
use App\Mail\NoticiaNuevoPostulado;
use App\Payment_history;
use App\UserBankData;
use App\UserVacancy;
use App\Vacancy;
use Illuminate\Http\Request;
use App\Candidate;
use App\Language;
use App\User;
use App\Software;
use App\EducationData;
use App\WorkExperience;
use App\CandidateLanguage;
use App\CandidateSoftware;
use App\CandidateVacancy;
use App\payment_users;
use App\Http\Resources\CandidateResource;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;


class CandidateController extends Controller{

    public function agregar_editar_candidato(Request $request){

        $personal_data = json_decode($request->personal_data);


        $user = User::where('correo', $request->email)->first();
        if(is_null($user))
            return '{"exito" : 0, "msg" : "El correo de usuario no es valido"}';
     if(
            is_null($request->seeking_work) or
            is_null($personal_data->names) or
            is_null($personal_data->maternal_name) or
            is_null($personal_data->paternal_name) or
            is_null($personal_data->country) or
            is_null($personal_data->state) or
            is_null($personal_data->city) or
            is_null($personal_data->district) or
            is_null($personal_data->street) or
            is_null($personal_data->house_number) or
            is_null($personal_data->home_phone) or
            is_null($personal_data->mobile_phone) or
            is_null($personal_data->email) or
            is_null($personal_data->gender)
        )
        { return '{"exito" : 0, "msg" : "Por favor llena todos los campos de informacion basica"}'; }

        if(is_null($request->candidate_id))
            return $this->insertarCandidato($request, $user);
        else
            return $this->modificarCandidato($request);
    }

    public function listado_candidatos(Request $request){

        $user = User::where('correo', $request->email)->first();

        if(is_null($user))
            return '{"exito" : 0, "msg" : "No existe un usuario registrado al correo proporcionado"}';


        $candidates = $user->candidates;

        $return_array = array();
        $candidates_array = array();

        $return_array['user_id'] = $user->id;
        foreach ($candidates as $candidate){
            $candidate_object = array();

      // $candidateCheck = CandidateVacancy::where('candidate_id',$candidate->id);

      //  if(is_null($candidateCheck)){
            $candidate_object[] = new CandidateResource($candidate);
            $candidates_array[] = $candidate_object[0];
      //  }



        }
        $return_array['candidates'] = $candidates_array;
        $return_array['exito'] = 1;
        $return_array['msg'] = "Exito";

        return json_encode($return_array);
    }

    public function listado_candidatos_activos(Request $request){

        $user = User::where('correo', $request->email)->first();

        if(is_null($user))
            return '{"exito" : 0, "msg" : "No existe un usuario registrado al correo proporcionado"}';


        $candidates = $user->candidates;

        $return_array = array();
        $candidates_array = array();
        $return_array['user_id'] = $user->id;

        foreach ($candidates as $candidate){
            $candidate_object = array();

           $activos = DB::table('candidate_vacancy')
                ->where('candidate_id',$candidate->id)->where('status','Postulado')
                ->Orwhere('candidate_id',$candidate->id)->where('status','CV Visto')
                ->Orwhere('candidate_id',$candidate->id)->where('status','En entrevista')
                ->Orwhere('candidate_id',$candidate->id)->where('status','Psicométrico')
                ->count();


           if($activos == 0){

            $candidate_object[] = new CandidateResource($candidate);
            $candidates_array[] = $candidate_object[0];

           }

        }
        $return_array['candidates'] = $candidates_array;
        $return_array['exito'] = 1;
        $return_array['msg'] = "Exito";

        return json_encode($return_array);
    }

    public function postular_candidato(Request $request){

        $vacancy_id = $request->vacancy_id;
        $candidate_id = $request->candidate_id;
        $status = $request->status;
        $status = empty($status) ? 'Postulado': $status;

        if (empty($vacancy_id) or is_null($vacancy_id) or empty($candidate_id) or is_null($candidate_id)) {
            return '{"exito" : 0, "msg" : "Por favor proporciona todos los datos de postulacion"}';
        }

        // valida si existe la candidatura
        $Existe = false;
        $Existe0 = false;
         $CandidateVacancy = DB::select("select * from candidate_vacancy where candidate_id = ? and vacancy_id = ?",[$candidate_id,$vacancy_id]);

        //Valida que ya tenga datos vancarios
        $Candidate = Candidate::find($candidate_id);
        $vacante = Vacancy::find($vacancy_id);

        $dataBank = DB::select('select * from user_bank_data where id_user = ?;',[$Candidate->user_id]);
        foreach($dataBank as $item){ $Existe0 = true;}
        if(!$Existe0){
        return '{"exito" : 400, "msg" : "Debes agregar tus datos bancarios para poder empezar a postular candidatos"}';
        }
            $fecha_actual= null;
            $date = DB::select('select now() as fecha_actual;');
            foreach ($date as $item) {$fecha_actual = $item->fecha_actual; }

            CandidateVacancy::create([
                'candidate_id' => $candidate_id,
                'vacancy_id' => $vacancy_id,
                'status' => $status,
                'date_postulate' => $fecha_actual
            ]);

             $nombre_Candidato = $Candidate->names .' '. $Candidate->paternal_name.' '. $Candidate->maternal_name;
             $nombre_Vacante = $vacante->vacancy_name;

             $tokens = DB::select("SELECT * FROM firebase WHERE user_id = $Candidate->user_id");
             $title = '¡Candidato postulado!';
             $subtitle = '';
             $body = '¡Has postulado a '.$nombre_Candidato.' en la vacante '.$nombre_Vacante.'';
             $message = '';
             $icon = '1';

             $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);

             $tokens = DB::select("SELECT * FROM firebase WHERE user_id = $vacante->Created_by");
             $title = '¡Candidato postulado!';
             $subtitle = '';
             $body = '¡Han postulado a '.$nombre_Candidato.' en la vacante '.$nombre_Vacante.'';
             $message = '';
             $icon = '1';

             $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);

             $admin = User::find($vacante->Created_by);
             $msg = "¡Se ha postulado a '$nombre_Candidato' en tu vacante '$nombre_Vacante'!";
             Mail::to($admin->correo)->send(new NoticiaNuevoPostulado('ProHunter: Nueva postulación',$admin->correo,$msg));

             return '{"exito" : 1, "msg" : "Exito"}';
    }

    public function email_administrador_postulacion(Request $request){
     //Email llega al backend cuando alguien es postulado a una vacante que el administador registro
        $correo = $request->input('correo');
        $msg = $request->input('msg');

        Mail::to($correo)->send(new NoticiaNuevoPostulado('ProHunter: Nueva postulación',$correo,$msg));

        return '{"exito" : 1, "msg" : "Exito"}';
    }

    public function listado_postulados(Request $request){

        $user = User::where('correo', $request->email)->first();

        if(is_null($user))
            return '{"exito" : 0, "msg" : "No existe un usuario registrado al correo proporcionado"}';
        $candidates = $user->candidates;
        $return_array = array();
        $candidates_array = array();
        $return_array['user_id'] = $user->id;

        foreach ($candidates as $candidate){

            if(!is_null($candidate->vacancies->first())){

                $candidate_object = array();
                $candidate_object[] = new CandidateResource($candidate);
                $candidates_array[] = $candidate_object[0];
            }
        }

        $return_array['candidates'] = $candidates_array;
        $return_array['exito'] = 1;
        $return_array['msg'] = "Exito";

        return json_encode($return_array);
    }

    public function eliminar_candidato(Request $request){
       $candidate = Candidate::find($request->candidate_id);

       if(is_null($candidate))
           return '{"exito" : 0, "msg" : "No se encontro un candidato con el ID proporcionado"}';

       CandidateLanguage::where('candidate_id', $candidate->id)->delete();
       CandidateSoftware::where('candidate_id', $candidate->id)->delete();
       CandidateVacancy::where('candidate_id', $candidate->id)->delete();
     //  UserCandidate::where('candidate_id', $candidate->id)->delete();
       WorkExperience::where('candidate_id', $candidate->id)->delete();
       EducationData::where('candidate_id', $candidate->id)->delete();
       Candidate::find($request->candidate_id)->delete();

        return '{"exito" : 1, "msg" : "Exito"}';
    }

    public function buscar_candidato(Request $request){

        $search_string = $request->search_string;

        $user = User::where('correo', $request->email)->first();
        if(is_null($user)) {
            return '{"exito" : 0, "msg" : "No existe un usuario registrado al correo proporcionado"}';
        }else {
            $id_user = $user->id;

            $busqueda = Candidate::where('user_id', '=', $id_user)->where(function ($query) use ($search_string) {
                $query->where('names', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('maternal_name', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('paternal_name', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('country', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('state', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('city', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('district', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('house_number', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('home_phone', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('mobile_phone', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('email', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('gender', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('curp', 'LIKE', '%' . $search_string . '%')
                    ->orWhere('nss', 'LIKE', '%' . $search_string . '%');
            })->paginate();

            return CandidateResource::collection($busqueda)->additional([
                'exito' => 1,
                'msg' => 'Exito'
            ]);

        }
    }

    public function buscar_postulacion(Request $request){

        //Recibe email y id_postulacion
        $user = User::where('correo', $request->email)->first();
        if(is_null($user)) {
            return '{"exito" : 0, "msg" : "No existe un usuario registrado al correo proporcionado"}';
        }else {

            $CandidateVacancy = CandidateVacancy::find($request->id_postulacion);
            if(is_null($CandidateVacancy)) {
                return '{"exito" : 0, "msg" : "No encontro la postulacion"}';
            }else {

                $return_array['data'] = $CandidateVacancy;
                $return_array['exito'] = 1;
                $return_array['msg'] = 'Exito';

                  return $return_array;

           }
        }
    }

    public function datos_candidato(Request $request){

        $candidates = Candidate::where('id',$request->id)->first();
            if(!is_null($candidates)){

        $return_array = array();
        $return_array['candidato'] = new CandidateResource($candidates);
        $return_array['exito'] = 1;
        $return_array['msg'] = "Exito";
            return json_encode($return_array);
        }else{
            return '{"exito" : 0, "msg" : "Candidato no encontrado"}';

        }
    }

    //SUBROUTINES
    private function insertarCandidato($request, $user){

        $personal_data = json_decode($request->personal_data);
        $profile_picture = $request->file('profile_picture');
        if (!is_null($profile_picture)) {
            $profile_picture = $profile_picture->store('candidate_profile_pictures');
        }

        $curriculum = $request->file('curriculum');
        if (!is_null($curriculum)) {
            $curriculum = $curriculum->store('curriculums');
        }


        $candidate = Candidate::create([
            'user_id' => $user->id,
            'seeking_work' => $request->seeking_work,
            'profile_picture' => $profile_picture,
            'curriculum' => $curriculum,
            'names' => $personal_data->names,
            'maternal_name' => $personal_data->maternal_name,
            'paternal_name' => $personal_data->paternal_name,
            'country' => $personal_data->country,
            'state' => $personal_data->state,
            'city' => $personal_data->city,
            'district' => $personal_data->district,
            'street' => $personal_data->street,
            'house_number' => $personal_data->house_number,
            'home_phone' => $personal_data->home_phone,
            'mobile_phone' => $personal_data->mobile_phone,
            'email' => $personal_data->email,
            'gender' => $personal_data->gender,
            'curp' => $personal_data->curp,
            'rfc' => $personal_data->rfc,
            'nss' => $personal_data->nss,
            'birthday_date' => $personal_data->birthday_date
        ]);

        $education_data = json_decode($request->education_data);
        if (!is_null($education_data)){
            foreach($education_data as $education_data_entry) {
                EducationData::create
                ([
                    'candidate_id' => $candidate->id,
                    'universidad' => $education_data_entry->universidad,
                    'start_month' => $education_data_entry->start_month,
                    'start_year' => $education_data_entry->start_year,
                    'end_month' => $education_data_entry->end_month,
                    'end_year' => $education_data_entry->end_year,
                    'academic_level' => $education_data_entry->academic_level,
                    'degree_obtained' => $education_data_entry->degree_obtained,
                    'additional_info' => $education_data_entry->additional_info
                ]);
            }
        }

        $languages_array = json_decode($request->languages);
        if (!is_null($languages_array)){
            foreach($languages_array as $language){
                $languageRow = Language::where('id', $language->language_id)->first();
                if(!is_null($languageRow))
                {
                    CandidateLanguage::create
                    ([
                        'candidate_id' => $candidate->id,
                        'language_id' => $language->language_id,
                        'percentage_known' => $language->percentage_known,
                        'certification_type' => $language->certification_type
                    ]);
                }
            }
        }

        $software_array = json_decode($request->software);
        if (!is_null($software_array)){

            foreach($software_array as $software){

                $softwareRow = Software::where('id', $software->software_id)->first();
                if(!is_null($softwareRow)){

                    CandidateSoftware::create([
                        'candidate_id' => $candidate->id,
                        'software_id' => $software->software_id,
                        'percentage_known' => $software->percentage_known,
                        'certification_type' => $software->certification_type
                    ]);
                }
            }

        }

        $experience_array = json_decode($request->experience);
        if (!is_null($experience_array)){
            foreach($experience_array as $experience){

                $portfolio_1 = null;
                $portfolio_2 = null;
                $portfolio_3 = null;
                if(isset($experience->work_portfolio_url_1)){
                    $portfolio_1 = $experience->work_portfolio_url_1;
                }
                if(isset( $experience->work_portfolio_url_2)){
                    $portfolio_2 =  $experience->work_portfolio_url_2;
                }
                if(isset( $experience->work_portfolio_url_3)){
                    $portfolio_3 = $experience->work_portfolio_url_3;
                }

                WorkExperience::create([
                    'candidate_id' => $candidate->id,
                    'position' => $experience->position,
                    'active' => $experience->active,
                    'company_name' => $experience->company_name,
                    'role' => $experience->role,
                    'activities' => $experience->activities,
                    'start_month' => $experience->start_month,
                    'start_year' => $experience->start_year,
                    'end_month' => $experience->end_month,
                    'end_year' => $experience->end_year,
                    'work_portfolio_url_1' => $portfolio_1,
                    'work_portfolio_url_2' => $portfolio_2,
                    'work_portfolio_url_3' => $portfolio_3
                ]);
            }
        }


    $token  = DB::select('select * from Buscar_recomendados where candidate_id = ? group by vacancy_name',[$candidate->id]);
foreach($token as $tokens){
    $full_name = $personal_data->names. ' ' . $personal_data->maternal_name. ' ' . $personal_data->paternal_name;
    $title = '¡Nuevo candidato registrado!';
    $subtitle = '';
    $body = 'Podrías postular al candidato '.$full_name.' en la vacante ' . $tokens->vacancy_name . ' de la empresa ' . $tokens->company_name .'!';
    $message = '';
    $icon =  '8';
    $this->notificacion_recomensaciones($tokens->token,$tokens->user_id,$title, $subtitle, $body, $message, $icon);
}
       return '{"exito" : 1, "msg" : "Exito Candidato insertado" , "id" : "'.$candidate->id.'"}';
    }

    private function modificarCandidato($request){

        $candidate = Candidate::where('id', $request->candidate_id)->first();
        if(is_null($candidate)){
            return '{"exito" : 0, "msg" : "El ID de candidato no es valido"}';
        }
        //FILE OPERATIONS
        $profile_picture = $request->file('profile_picture');
        $curriculum = $request->file('curriculum');

        if(!is_null($profile_picture)){
            if(Storage::disk('local')->exists($candidate->profile_picture)){
                Storage::delete($candidate->profile_picture);
            }
            $profile_picture = $profile_picture->store('candidate_profile_pictures');
            $candidate->profile_picture = $profile_picture;
        }
        if(!is_null($curriculum)) {
            if(Storage::disk('local')->exists($candidate->curriculum)){
                Storage::delete($candidate->curriculum);
            }
            $curriculum = $curriculum->store('curriculums');
            $candidate->curriculum = $curriculum;
        }

        $personal_data = json_decode($request->personal_data);
        if(!is_null($personal_data)) {
                $candidate->names = $personal_data->names;
                $candidate->seeking_work = $request->seeking_work;
                $candidate->maternal_name = $personal_data->maternal_name;
                $candidate->paternal_name = $personal_data->paternal_name;
                $candidate->country = $personal_data->country;
                $candidate->state = $personal_data->state;
                $candidate->city = $personal_data->city;
                $candidate->district = $personal_data->district;
                $candidate->street = $personal_data->street;
                $candidate->house_number = $personal_data->house_number;
                $candidate->home_phone = $personal_data->home_phone;
                $candidate->mobile_phone = $personal_data->mobile_phone;
                $candidate->email = $personal_data->email;
                $candidate->gender = $personal_data->gender;
                $candidate->curp = $personal_data->curp;
                $candidate->rfc = $personal_data->rfc;
                $candidate->nss = $personal_data->nss;
                $candidate->birthday_date = $personal_data->birthday_date;
                $candidate->save();
        }

        $education_data = json_decode($request->education_data);
        if(!is_null($education_data)) {
            //DELETE EDUCATION DATA FOR THE CANDIDATE AND INSERT NEW DATA
            EducationData::where('candidate_id', $candidate->id)->delete();
            foreach($education_data as $education_data_entry){
                EducationData::create([
                    'candidate_id' => $candidate->id,
                    'universidad' => $education_data_entry->universidad,
                    'start_month' => $education_data_entry->start_month,
                    'start_year' => $education_data_entry->start_year,
                    'end_month' => $education_data_entry->end_month,
                    'end_year' => $education_data_entry->end_year,
                    'academic_level' => $education_data_entry->academic_level,
                    'degree_obtained' => $education_data_entry->degree_obtained,
                    'additional_info' => $education_data_entry->additional_info
                ]);
            }
        }

        $languages_array = json_decode($request->languages);
        if(!is_null($languages_array)) {
            CandidateLanguage::where('candidate_id', $candidate->id)->delete();
            foreach($languages_array as $language){
                $languageRow = Language::where('id', $language->language_id)->first();
                if(!is_null($languageRow))
                {
                    CandidateLanguage::create
                    ([
                        'candidate_id' => $candidate->id,
                        'language_id' => $language->language_id,
                        'percentage_known' => $language->percentage_known,
                        'certification_type' => $language->certification_type
                    ]);
                }
            }
        }

        $software_array = json_decode($request->software);
        if(!is_null($software_array)) {
            CandidateSoftware::where('candidate_id', $candidate->id)->delete();
            foreach($software_array as $software){
                $softwareRow = Software::where('id', $software->software_id)->first();
                if(!is_null($softwareRow)){
                    CandidateSoftware::create ([
                        'candidate_id' => $candidate->id,
                        'software_id' => $software->software_id,
                        'percentage_known' => $software->percentage_known,
                        'certification_type' => $software->certification_type
                    ]);
                }
            }
        }

        $experience_array = json_decode($request->experience);
        if(!is_null($experience_array)) {
            WorkExperience::where('candidate_id', $candidate->id)->delete();
            foreach($experience_array as $experience){

                $portfolio_1 = null;
                $portfolio_2 = null;
                $portfolio_3 = null;

                if(isset($experience->work_portfolio_url_1)){
                    $portfolio_1 = $experience->work_portfolio_url_1;
                }
                if(isset( $experience->work_portfolio_url_2)){
                    $portfolio_2 =  $experience->work_portfolio_url_2;
                }
                if(isset( $experience->work_portfolio_url_3)){
                    $portfolio_3 = $experience->work_portfolio_url_3;
                }

                WorkExperience::create([
                    'candidate_id' => $candidate->id,
                    'position' => $experience->position,
                    'active' => $experience->active,
                    'company_name' => $experience->company_name,
                    'role' => $experience->role,
                    'activities' => $experience->activities,
                    'start_month' => $experience->start_month,
                    'start_year' => $experience->start_year,
                    'end_month' => $experience->end_month,
                    'end_year' => $experience->end_year,
                    'work_portfolio_url_1' => $portfolio_1,
                    'work_portfolio_url_2' => $portfolio_2,
                    'work_portfolio_url_3' => $portfolio_3
                ]);
            }
        }

            $token = DB::select('select * from Buscar_recomendados where candidate_id = ? group by vacancy_name', [$candidate->id]);
            foreach ($token as $tokens) {
                $full_name = $personal_data->names . ' ' . $personal_data->maternal_name . ' ' . $personal_data->paternal_name;
                $title = '¡Nuevo candidato registrado!';
                $subtitle = '';
                $body = 'Podrías postular al candidato ' . $full_name . ' en la vacante ' . $tokens->vacancy_name . ' de la empresa ' . $tokens->company_name . '!';
                $message = '';
                $icon = '8';
                $this->notificacion_recomensaciones($tokens->token, $tokens->user_id, $title, $subtitle, $body, $message, $icon);
        }


        return '{"exito" : 1, "msg" : "Exito Modifica Candidato" , "id" : "'.$candidate->id.'"}';
    }

    // Web
    public function eliminar_candidato_web(){

        $id = Input::get('id');
        $id_user = Input::get('user_id');
        $candidate = Candidate::find($id);

        if(is_null($candidate)) {
            $msg = "No se encontro al postulado";
            return redirect('/admin/Usuarios_app/Ver_Candidatos_usuario?Error=true&id='.$id_user.'&Msg='.$msg);
        }
        CandidateLanguage::where('candidate_id', $candidate->id)->delete();
        CandidateSoftware::where('candidate_id', $candidate->id)->delete();
        CandidateVacancy::where('candidate_id', $candidate->id)->delete();
        WorkExperience::where('candidate_id', $candidate->id)->delete();
        EducationData::where('candidate_id', $candidate->id)->delete();
        Candidate::find($id)->delete();

        $msg = "Eliminado correctamente";
        return redirect('/admin/Usuarios_app/Ver_Candidatos_usuario?id='.$id_user.'&Msg='.$msg);
    }

    public function eliminar_postulacion_web(){

        $id = Input::get('id');
        $candidate = CandidateVacancy::find($id);

     if(is_null($candidate)) {
            $msg = "No se encontro al postulado";
            return redirect('/admin/Postulados?Error=true&Msg='.$msg);
           }

        $candidate->delete();

        $msg = "Eliminado correctamente";
        return redirect('/admin/Postulados?Msg='.$msg);
    }

    public function editar_estatus_postulacion(Request $request){



       if($request->has(['id', 'inputEstatus','inputMotivo'])){

           $id_postulacion = $request->id;
           $status = $request->inputEstatus;
           $motivo = $request->inputMotivo;
           $CandidateVacancy = CandidateVacancy::find($id_postulacion);

       if(!is_null($CandidateVacancy)) {

           // se obtiene la fecha actual
            $fecha_actual= null;
            $date = DB::select('select now() as fecha_actual;');
            foreach ($date as $item) {  $fecha_actual = $item->fecha_actual; }

           // obtiene id de la vacante y del candidato
           $Candidate = Candidate::find($CandidateVacancy->candidate_id);
           $vacante = Vacancy::find($CandidateVacancy->vacancy_id);

           $id_usuario = $Candidate->user_id;
           $nombre_Candidato = $Candidate->names .' '. $Candidate->paternal_name.' '. $Candidate->maternal_name;
           $nombre_Vacante = $vacante->vacancy_name;

           $CandidateVacancy->status = $status;

           $tokens = DB::select("SELECT * FROM firebase WHERE user_id = $id_usuario");

           switch ($status) {

                case "CV Visto":
                    $CandidateVacancy->date_cv_sent = $fecha_actual;
                    $CandidateVacancy->date_interview = null;
                    $CandidateVacancy->date_psychometric = null;
                    $CandidateVacancy->date_hired = null;

                    $CandidateVacancy->reason_cv_sent = $motivo;

                    $title = '¡El CV Visto!';
                    $subtitle = '';
                    $body = '¡El curriculum de tu postulado '.$nombre_Candidato.' ha sido revisado!';
                    $message = $motivo;
                    $icon = '2';
                    $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);

                    break;
                case "En entrevista":

                    $CandidateVacancy->date_interview = $fecha_actual;
                    $CandidateVacancy->date_psychometric = null;
                    $CandidateVacancy->date_hired = null;

                    $CandidateVacancy->reason_interview = $motivo;

                    $title = '¡En entrevista!';
                    $subtitle = '';
                    $body = '¡El postulado '.$nombre_Candidato.' fue llamado a entrevista!';
                    $message = $motivo;
                    $icon =  '3';
                    $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);

                    break;
                case "Psicométrico":
                    $CandidateVacancy->date_psychometric = $fecha_actual;
                    $CandidateVacancy->date_hired = null;

                    $CandidateVacancy->reason_psychometric = $motivo;

                    $title = '¡Examenes psicométricos!';
                    $subtitle = '';
                    $body = 'Tu postulado '.$nombre_Candidato.' hara examenes psicométicos';
                    $message = $motivo;
                    $icon = '4';
                    $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);

                    break;
                case "Contratado":// o vacante cubierta automatico

          $CandidateVacancy->date_hired = $fecha_actual;
          $CandidateVacancy->reason_hired = $motivo;
          $CandidateVacancy->Active = false;

      /*Quita la vacante de favoritos ya que ya no esta disponible*/
       UserVacancy::where('vacancy_id', $CandidateVacancy->vacancy_id)->delete();


      /* Ejecuta procedure donde cambia estatus de los demas candidatos*/
       $msg = "";
             if ($Postulados = DB::select(DB::raw('CALL Cambiar_status_postulado(?,?,?)'),[$CandidateVacancy->candidate_id,$CandidateVacancy->vacancy_id,"Contratado"])) {
                     foreach ($Postulados as $Postulado) { $msg = $Postulado->msg; }

             if ($msg === "Contratado") {

                 $this->agregar_ganancia_usuario($CandidateVacancy->candidate_id ,$CandidateVacancy->id);


                 $title = 'Postulado contratado';
                 $subtitle = '';
                 $body = '¡Felicidades! ¡Tu postulado ' . $nombre_Candidato . ' a sido contratado en la vacante ' . $nombre_Vacante . '!';
                 $message = $motivo;
                 $icon = '5';
                 $this->notificacion($tokens, $title, $subtitle, $body, $message, $icon);

                 // Notificacion a los que fueron rechazados
                 $tokens = DB::select('select c.names,c.user_id,f.token as token from firebase f join  users u on f.user_id = u.id
                                       join candidates c on u.id = c.user_id join candidate_vacancy cv on c.id = cv.candidate_id
                                       where cv.vacancy_id = ? and status = "Vacante cubierta";', [$CandidateVacancy->vacancy_id]);
                 if (!is_null($tokens)){
                     foreach ($tokens as $token) {
                         $title = 'Vacante cubierta';
                         $subtitle = '';
                         $body = '¡Lo sentimos! ¡La postulacion de ' . $token->names . ' en la vacante ' . $nombre_Vacante . ' ya ha sido cubierta!';
                         $message = '';
                         $icon = '6';
                         $this->notificacion($tokens, $title, $subtitle, $body, $message, $icon);
                     }
                  }
              }
                    }
             break;
                case "Rechazado":

                    $CandidateVacancy->Active = false;
                    $CandidateVacancy->date_hired = null;
                    $CandidateVacancy->date_rejected = $fecha_actual;
                    $CandidateVacancy->reason_rejected = $motivo;

                    $title = '¡Postulado rechazado!';
                    $subtitle = '';
                    $body = '¡Lo sentimos! Tu postulado '.$nombre_Candidato.' ha sido rechazado';
                    $message = $motivo;
                    $icon = '7';
                    //Envia notificacion de cambio de estatus de postulación
                    $tokens = DB::select("SELECT * FROM firebase WHERE user_id = $id_usuario");
                    $this->notificacion($tokens,$title, $subtitle, $body, $message, $icon);
                    break;
                case "Vacante cubierta":

                    $CandidateVacancy->Active = false;
                    $CandidateVacancy->date_hired = null;
                    $CandidateVacancy->date_rejected = $fecha_actual;
                    $CandidateVacancy->reason_rejected = $motivo;

                   break;

            }

            if($CandidateVacancy->save()) {

                $msg = "Actualizado correctamente";
                return redirect('/admin/Postulados/Ver_postulacion?id=' . $id_postulacion . '&Msg=' . $msg)->withInput();

            }else{
                $msg = "Hubo un error al tratar de actualizar";
                return redirect('/admin/Postulados/Ver_postulacion?id='.$id_postulacion.'&Error=true&Msg='.$msg)->withInput();
            }
         }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Postulados/Ver_postulacion?id='.$id_postulacion.'&Error=true&Msg='.$msg)->withInput();
            }
    }else {
           $msg = "No se enviarion todos los campos";
           return redirect('/admin/Postulados/Ver_vacantes?id='.$request->id.'&Error=true&Msg='.$msg)->withInput();
       }
    }

    public function agregar_archivos_candidato(Request $request){

        if ($request->has(['candidate_id'])) {

            $candidate = Candidate::find($request->candidate_id);

            $profile_picture = $request->file('profile_picture');
        if (!is_null($profile_picture)) {
            $candidate->profile_picture = $profile_picture->store('candidate_profile_pictures');
        }

        $curriculum = $request->file('curriculum');
        if (!is_null($curriculum)) {
            $candidate->curriculum = $curriculum->store('curriculums');
        }

            $candidate->save();

            return '{"exito" : 1, "msg" : "Exito"}';
    }else{
            return '{"exito" : 1, "msg" : "Debes enviar el id del candidato"}';
        }

    }

    // subroutines
    private function agregar_ganancia_usuario($id_candidato,$id_postulacion){
      //  $ganancia = new Payment_users;
        $id_usuario = null;
        //Busca el id del usuario al que le corresponde el candidato contratado
    $Candidatos= DB::select('select * from candidates where id = ?',[$id_candidato]);
        foreach ($Candidatos as $Candidato) {$id_usuario = $Candidato->user_id; }
        $Total=0;

        $Candidatos= DB::select('select cv.id as id_postulacion,u.id,u.nombre,v.salary,v.contract_profit_percentage,((REPLACE(v.salary, ",", \'\') / 100 )* v.contract_profit_percentage) as Ganacia,
         (select (SUM((REPLACE(v.salary, ",", \'\') / 100 ) * v.contract_profit_percentage)) as Total from vacancies v,candidate_vacancy cv, candidates c,users u
         where v.id = cv.vacancy_id AND cv.candidate_id = c.id AND c.user_id = u.id AND status = "Contratado") as Total from vacancies v,candidate_vacancy cv, candidates c,users u
         where v.id = cv.vacancy_id AND cv.candidate_id = c.id AND c.user_id = u.id AND status = "Contratado" AND u.id = ? and cv.id = ?;',[$id_usuario,$id_postulacion]);


        foreach ($Candidatos as $Candidato) { $Total = $Candidato->Ganacia;/*Total  */}
        $ganancia =  payment_users::create([
                'user_id'=> $id_usuario ,
                'postulate_id'=> $id_postulacion ,
                'total_to_pay'=> $Total ,
                'paid_percentage'=> '0%']);

        // agrega historial
        $historial = new Payment_history;
        $historial->payment_id = $ganancia->id;
        $historial->missing = $Total;
        $historial->paid_percentage = '0%';

        $historial->save();

     return true;
    }

    public function generar_excel(){

$id = Input::get('id');
        $user = User::where('id',$id)->first();

           if( $user->tipo = 'Root'){
               $assoc = DB::select('select * from reporte_candidatos_a_apagar;');
             }else{
               $assoc = DB::select('select * from reporte_candidatos_a_apagar where id_usuario = ?;',[$id]);
           }

        $existen = false;
        $array_assoc = null;

        foreach($assoc as $col => $values){
          $pos = 0;
            $edad = $this->Cacular_Edad($values->fecha_cumpleaños);
            foreach($values as $key => $value){

                if($pos == 4){
                    $array_assoc[$col]['Edad'] = $edad ;
                }
                $array_assoc[$col][$key]=$value;
                $existen = true;
                $pos++;
            }
        }


        if(!$existen){
            $msg = "Actualmente no tienes pagos por realizar";
            return redirect('/admin/Postulados?Msg='.$msg);

        }else{
            session_start();
            $_SESSION['assoc']=$array_assoc;
            header("Location: ../app/Http/Controllers/xls_controller.php");
        }
    }

    private function notificacion($token,$title,$subtitle,$body,$message,$icon){

        // CONFIGURE FIRE BASE DATA
        $token_to_send  = array();
        foreach($token as $item){
            //Se crea el el arreglo de token para 1 o *
            $token_to_send[] = $item->token;
            /*Se guarda la notificacion en la bd*/
            $announcements = new announcements;
            $announcements->user_id = $item->user_id;
            $announcements->title = $title;
            $announcements->subtitle = $subtitle;
            $announcements->body = $body;
            $announcements->message = $message;
            $announcements->icon = $icon;
            $announcements->save();

        }
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array
        (
            'registration_ids' =>  $token_to_send,
            'notification' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "watched" => "0",
                    "icon" => $icon
                ],
            'data' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "watched" => "0",
                    "icon" => $icon
                ]
        );
        $headers = array(
            'Authorization:key = AAAAH1ZV3YM:APA91bGv-Nv8c7hr977n2Opoz77oiQR0tXZj8ExxbNIbPO91QUKnkAQwzcdRW1yWAD9tjRl4_nwRtLr9fRtpFpPeOiNK39blyNKj-mouWTglBlsBaM4y96dgx7GdGKJSByE5Il_O0nnq',
            'Content-Type: application/json'
        );

        //EXECUTE FIREBASE NOTIFICATIONS TO EACH TOKEN
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $msg = curl_exec($ch);
        curl_close($ch);


      //  dd($msg);

        if ($msg === FALSE) {
            $msg = "Error de Firebase";
            return '{"exito" : 0, "msg" : '.$msg.'}';
        }else{
            return '{"exito" : 1, "msg" : "true"}';
        }
    }

    private function notificacion_recomensaciones($token,$id_user,$title,$subtitle,$body,$message,$icon){

        // CONFIGURE FIRE BASE DATA
        $token_to_send  = array();
            //Se crea el el arreglo de token para 1 o *
            $token_to_send[] = $token;
            /*Se guarda la notificacion en la bd*/
            $announcements = new announcements;
            $announcements->user_id = $id_user;
            $announcements->title = $title;
            $announcements->subtitle = $subtitle;
            $announcements->body = $body;
            $announcements->message = $message;
            $announcements->icon = $icon;
            $announcements->save();

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array
        (
            'registration_ids' =>  $token_to_send,
            'notification' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon
                ],
            'data' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon
                ]
        );
        $headers = array(
            'Authorization:key = AAAAH1ZV3YM:APA91bGv-Nv8c7hr977n2Opoz77oiQR0tXZj8ExxbNIbPO91QUKnkAQwzcdRW1yWAD9tjRl4_nwRtLr9fRtpFpPeOiNK39blyNKj-mouWTglBlsBaM4y96dgx7GdGKJSByE5Il_O0nnq',
            'Content-Type: application/json'
        );

        //EXECUTE FIREBASE NOTIFICATIONS TO EACH TOKEN
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $msg = curl_exec($ch);
        curl_close($ch);

        if ($msg === FALSE) {
            $msg = "Error de Firebase";
            return '{"exito" : 0, "msg" : '.$msg.'}';
        }else{
            return '{"exito" : 0, "msg" : "true"}';
        }
    }

    private function  Cacular_Edad($Fecha_nacimiento){
        $dia=date("d");
        $mes=date("m");
        $ano=date("Y");
        $dianaz=date("d",strtotime($Fecha_nacimiento));
        $mesnaz=date("m",strtotime($Fecha_nacimiento));
        $anonaz=date("Y",strtotime($Fecha_nacimiento));

        //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
        if (($mesnaz == $mes) && ($dianaz > $dia)) {
            $ano=($ano-1); }
        //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
        if ($mesnaz > $mes) {
            $ano=($ano-1);}
        //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
        $edad=($ano-$anonaz);

        return $edad;
    }


}

