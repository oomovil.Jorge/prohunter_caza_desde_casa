<?php

namespace App\Http\Controllers;

use App\announcements;
use App\City;
use App\State;
use Illuminate\Http\Request;
use App\Vacancy;
use App\User;
use App\Category;
use App\UserVacancy;
use App\Http\Resources\VacancyResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Mail\Mailer;
use App\Mail\RecuperarPasswordMail;


class VacantesController extends Controller
{
  public function listado_vacantes(Request $request){

        //CONSTANT DATA
        $perPage = 15;
        //REQUEST DATA
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $user = User::where('correo', $request->email)->first();

        $vacancy_id = $request->vacancy_id;
        $vacancy_name = $request->vacancy_name;
        $company_name = $request->company_name;
        $country = $request->country;
        $state = $request->state;
        $city = $request->city;
        $schedule_type = $request->schedule_type;
        $salary = $request->salary;
        $contract_profit_percentage = $request->contract_profit_percentage;

        //LOAD SPECIFIC VACANCY
        if(!is_null($vacancy_id) or !empty($vacancy_id)){

            $vacancy = Vacancy::where('id', $vacancy_id)->get();

            if(is_null($vacancy))
                return '{"exito" : 0, "msg" : "No existe un avance con el ID proporcionado"}';
            else {
                return VacancyResource::collection($vacancy)->additional([
                    'exito' => 1,
                    'msg' => 'Exito'
                ]);
                }
        }

      $filtered_vacancies = $this->filtro_vacantes($vacancy_name,$company_name,$country,$state,$city,$schedule_type,$salary,$contract_profit_percentage);

      // si tiene filtro
      if( $vacancy_name != "" || $company_name != "" || $country != "" || $state != "" || $city != "" || $schedule_type != "" || $salary == "" || $contract_profit_percentage != "" ){

          $paginator = $filtered_vacancies->paginate();
          $vacancyCollection =  VacancyResource::collection($paginator)->additional([
              'exito' => 1,
              'msg' => 'Exito'
          ]);
          return $vacancyCollection;
      }

        if(!is_null($user)){//REGISTERED USER

            $user_preference = $user->preference;

            if(!is_null($user_preference)){//USER WITH PREFERENCES

                $preferred_categories = $user_preference->categories;
                $ordered_vacancies_array = array();
                $used_category_ids = array();
                $filtered_vacancies = $filtered_vacancies->get();

                //GO THROUGH EACH PREFFERRED CATEGORY AND ADD ALL VACANCIES FOR THAT CATEGORY TO AN ARRAY
      foreach($preferred_categories as $preferred_category){
                    foreach($filtered_vacancies as $vacancy){
                        if($vacancy->category_id == $preferred_category->id)
                            $ordered_vacancies_array[] = $vacancy;
                        }
                    $used_category_ids[] = $preferred_category->id;
                }

                $categories = Category::all();
                foreach($categories as $category){
                    if(!in_array($category->id, $used_category_ids)){
                        $vacancyCollection = $category->vacancies;

                        foreach($vacancyCollection as $vacancy){
                            $ordered_vacancies_array[] = $vacancy;

                        }
                        $used_category_ids[] = $category->id;
                    }
                }

                $currentVacancies = array_slice($ordered_vacancies_array, $perPage * ($currentPage - 1), $perPage);
                $paginator = new LengthAwarePaginator($currentVacancies, count($ordered_vacancies_array), $perPage, $currentPage);
                $paginator->withPath(url()->current());

   $vacancyCollection =  VacancyResource::collection($paginator)->additional([
                    'exito' => 1,
                    'msg' => 'Exito'
                ]);
                return $vacancyCollection;

            }else{  //USER WITH NO PREFERENCES

                 $paginator = $filtered_vacancies->paginate();
                 $vacancyCollection =  VacancyResource::collection($paginator)->additional([
                    'exito' => 1,
                    'msg' => 'Exito'
                ]);
                return $vacancyCollection;
            }

        } else {//GUEST USER

            $paginator = $filtered_vacancies->paginate();
            $vacancyCollection =  VacancyResource::collection($paginator)->additional([
                'exito' => 1,
                'msg' => 'Exito'
            ]);

            return $vacancyCollection;
        }
    }

  public function listado_vacantes_favoritas(Request $request){
        $email = $request->email;
        $user = User::where('correo', $email)->first();
        if(is_null($user))
            return '{"exito" : 0, "msg" : "El usuario no se encuentra registrado"}';
        $favorites = $user->vacancies;

        $vacancy= VacancyResource::collection($favorites)->additional([
            'exito' => 1,
            'msg' => 'Exito'
        ]);

        return $vacancy;
    }

  public function modificar_status_vacante_favorita(Request $request){
        //REQUEST DATA

        if( is_null($request->vacancy_id)){
            return '{"exito" : 0, "msg" : "Debes enviar ID de vacante"}';
        }

        $email = $request->email;
        $vacancy_id = $request->vacancy_id;
        $status = (boolean) $request->status;


        $user = User::where('correo', $email)->first();
        if(is_null($user))
            return '{"exito" : 0, "msg" : "El usuario no se encuentra registrado"}';
        $user_id = $user->id;
        if($status == false){
            UserVacancy::where('user_id', $user_id)->where('vacancy_id', $vacancy_id)->delete();

            return '{"exito" : 1, "msg" : "Se removio la vacante de las favoritas"}';
        } else{
            $vacancy = Vacancy::find($vacancy_id);
            if(is_null($vacancy)) {
                return '{"exito" : 0, "msg" : "La vacante elegida no existe"}';
            }else{

                $relation = UserVacancy::where('user_id', $user_id)->where('vacancy_id', $vacancy_id)->first();

                if(is_null($relation)){
                    UserVacancy::create([
                        'user_id' => $user_id,
                        'vacancy_id' => $vacancy_id
                    ]);
                    return '{"exito" : 1, "msg" : "Se agrego la vacante a las favoritas"}';
                }else {
                    return '{"exito" : 0, "msg" : "La vacante ya es una favorita para este usuario"}';
                }

            }
        }
    }

  public function buscar_vacante(Request $request){

   $search_string = $request->search_string;
   $paginator = Vacancy::where('vacancy_name', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('company_name', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('country', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('state', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('city', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('schedule_type', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('salary', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->orWhere('contract_profit_percentage', 'LIKE', '%'.$search_string.'%')->where('Active','=','1')
                        ->paginate();

        return VacancyResource::collection($paginator)->additional([
            'exito' => 1,
            'msg' => 'Exito'
        ]);
    }

  public function registrar_vacante(Request $request){

        $Vacancy = new Vacancy;

        if($request->has(['inputCategoria',
            'inputPuesto',
            'inputempresa',
            'inputfile',
            'InputPais',
            'inputEstado',
            'inputCiudad',
            'inputHorario',
            'inputSalario',
            'inputGanacia',
            'inputInfo_Empresa',
            'inputDetalles',
            'input_limite'])){

            $Vacancy->category_id = $request->input('inputCategoria');
            $Vacancy->vacancy_name = $request->input('inputPuesto');
            $Vacancy->company_name = $request->input('inputempresa');
            $Vacancy->Created_by = $request->input('id_session');

                if(Storage::disk('local')->exists($Vacancy->company_logo))
                    Storage::delete($Vacancy->company_logo);
                $file_name = $request->file('inputfile')->store('company_logos');
                $Vacancy->company_logo = asset('storage/').'/'.$file_name;

            $Vacancy->country = $request->input('InputPais');

            $State = State::where('id', $request->input('inputEstado'))->first();
             $Vacancy->state = $State->name;


            $city = City::where('id', $request->input('inputCiudad'))->first();

            $Vacancy->city = $city->name;
            $Vacancy->schedule_type = $request->input('inputHorario');
            $Vacancy->salary = $request->input('inputSalario');
            $Vacancy->contract_profit_percentage = $request->input('inputGanacia');
            $Vacancy->company_info = $request->input('inputInfo_Empresa');
            $Vacancy->details = $request->input('inputDetalles');
            $Vacancy->limit_date = $request->input('input_limite');
            $Vacancy->Active  = 1 ;


            if($Vacancy->save()){

                 $token = DB::select('select * from Buscar_recomendados where vacancy_id = ?', [$Vacancy->id]);
               foreach ($token as $tokens) {
                    $title = '¡Nueva vacante disponible!';
                    $subtitle = '';
                    $body = 'Podrías postular al candidato ' .$tokens->full_name . ' en la vacante ' . $tokens->vacancy_name . ' de la empresa ' . $tokens->company_name . '!';
                    $message = '';
                    $icon = '8';
                    $this->notificacion_recomensaciones($tokens->token, $tokens->user_id, $title, $subtitle, $body, $message, $icon);
                }

    $msg = "Registrado correctamente.";
    return redirect('/admin/Vacantes?Msg='.$msg);

      }else{
            $msg = "A ocurrido un error, lo sentimos";
            return redirect('/Vacantes/Nueva_Vacante?Error=true&Msg='.$msg)->withInput();
      }

      }else{
            $msg = "No se enviaron todos los campos";
            return redirect('/admin/Vacantes/Nueva_Vacante?Error=true&Msg='.$msg)->withInput();
       }
    }

  public function eliminar_vacante(){

      $id = Input::get('id');
      $Vacancy = Vacancy::find($id);

      if(!is_null($Vacancy)) {

          if (Storage::disk('local')->exists($Vacancy->company_logo)) {
              Storage::delete($Vacancy->company_logo);
          }

          if(  DB::select('CALL Eliminar_Vacantes('.$id.')')){

              $msg = "Eliminado correctamente";
              return redirect('/admin/Vacantes/?Msg='.$msg)->withInput();
          }else{
              $msg = "Hubo un error al tratar de eliminar";
              return redirect('/admin/Vacantes/?Error=true&Msg='.$msg)->withInput();
          }

      }else {
          $msg = "No se encontro el registro";
          return redirect('/admin/Vacantes/?Error=true&Msg='.$msg)->withInput();
      }

  }

  public function modificar_vacante(Request $request){

        $Vacancy = Vacancy::where('id', '=', $request->input('inputid'))->first();

        if($request->has(['inputCategoria',
            'inputPuesto',
            'inputempresa',
            'InputPais',
            'inputEstado',
            'inputCiudad',
            'inputHorario',
            'inputSalario',
            'inputGanacia',
            'inputInfo_Empresa',
            'inputDetalles'])){

            $Vacancy->category_id = $request->input('inputCategoria');
            $Vacancy->vacancy_name = $request->input('inputPuesto');
            $Vacancy->company_name = $request->input('inputempresa');

            if(!is_null($request->file('inputfile'))) {
                if (Storage::disk('local')->exists($Vacancy->company_logo))
                    Storage::delete($Vacancy->company_logo);
                $file_name = $request->file('inputfile')->store('company_logos');
                $Vacancy->company_logo = asset('storage/app/').'/'.$file_name;
            }
            $Vacancy->country = $request->input('InputPais');

            $State = State::where('id', $request->input('inputEstado'))->first();
            $Vacancy->state = $State->name;

            $city = City::where('id', $request->input('inputCiudad'))->first();
            $Vacancy->city = $city->name;

            $Vacancy->schedule_type = $request->input('inputHorario');
            $Vacancy->salary = $request->input('inputSalario');
            $Vacancy->contract_profit_percentage = $request->input('inputGanacia');
            $Vacancy->company_info = $request->input('inputInfo_Empresa');
            $Vacancy->details = $request->input('inputDetalles');
            $Vacancy->limit_date = $request->input('input_limite');


            if($Vacancy->save()){
                $msg = "Modificado correctamente.";
                return redirect('content_Contries?id=' . $Vacancy->id . '&Msg='.$msg);
            }else{
                $msg = "A ocurrido un error, lo sentimos";
                return redirect('/admin/Vacantes/Modificar_Vacante?id=' . $Vacancy->id . '&Error=true&Msg='.$msg);
            }
        }else{
            $msg = "No se enviaron todos los campos";
            return redirect('/admin/Vacantes/Modificar_Vacante?id=' . $Vacancy->id. '&Error=true&Msg='.$msg);
        }


    }

  public function listar_postulados_vacante(Request $request){
//Admin app
        $candidato =  DB::select('select c.* from candidate_vacancy cv,candidates c where c.id = cv.candidate_id  and cv.vacancy_id = ?', [$request->input('vacante')] );
        $NumPostulados =  DB::select('select count(*) as contadorPostulados from candidate_vacancy cv,candidates c where c.id = cv.candidate_id  and cv.vacancy_id = ?', [$request->input('vacante')] );

        echo '<h1>Postulados a esta vacante</h1>';
        echo '<br>';
        echo '<h4>Postulados : '.$NumPostulados[0]->contadorPostulados.'</h4>';
        echo '<br>';
        foreach ($candidato as $datos_candiato) {
            $foto = isset($datos_candiato->profile_picture) ? asset('storage/app/'.$datos_candiato->profile_picture) : asset("public/Img/notpictureuser.png");
            $nombre = $datos_candiato->names . ' ' . $datos_candiato->paternal_name . ' ' . $datos_candiato->maternal_name;
            $ubicacion = $datos_candiato->city . ',' . $datos_candiato->state;
            $ruta = route('DetalleCandidato',['id' => $datos_candiato->id]);

            echo '<hr>
           <img src="' . $foto . '" alt="Avatar" class="w3-circle" style="height:30px;float:left">
            <label style="float :left">&nbsp;' . $nombre . '</label>
            <br>
              <a style="float :right" class="badge badge-pill badge-info" href="' . $ruta . '">Ver perfil</a>
            <br>
           <label style="float: left;font-size: 10px"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;'.$ubicacion.'</label>';

        }
        return;
    }

  private function filtro_vacantes($vacancy_name,$company_name,$country,$state,$city,$schedule_type,$salary,$contract_profit_percentage){

        $filtered_vacancies = Vacancy::where('Active','=','1')->orderBy('created_at', 'desc');

            $filtered_vacancies->where('vacancy_name', 'LIKE', "%$vacancy_name%");
            $filtered_vacancies->where('company_name', 'LIKE', "%$company_name%");
            $filtered_vacancies->where('country', 'LIKE', "%$country%");
            $filtered_vacancies->where('state', 'LIKE', "%$state%");
            $filtered_vacancies->where('city', 'LIKE', "%$city%");
            $filtered_vacancies->where('schedule_type', 'LIKE', "%$schedule_type%");
            $filtered_vacancies->where('salary', 'LIKE', "%$salary%");
            $filtered_vacancies->where('contract_profit_percentage', 'LIKE', "%$contract_profit_percentage%");

        return $filtered_vacancies;
  }

  private function notificacion_recomensaciones($token,$id_user,$title,$subtitle,$body,$message,$icon){

        // CONFIGURE FIRE BASE DATA
        $token_to_send  = array();
        //Se crea el el arreglo de token para 1 o *
        $token_to_send[] = $token;
        /*Se guarda la notificacion en la bd*/
        $announcements = new announcements;
        $announcements->user_id = $id_user;
        $announcements->title = $title;
        $announcements->subtitle = $subtitle;
        $announcements->body = $body;
        $announcements->message = $message;
        $announcements->icon = $icon;
        $announcements->save();

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array
        (
            'registration_ids' =>  $token_to_send,
            'notification' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "watched" => "0",
                    "icon" => $icon
                ],
            'data' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "watched" => "0",
                    "icon" => $icon
                ]
        );
        $headers = array(
            'Authorization:key = AAAAH1ZV3YM:APA91bGv-Nv8c7hr977n2Opoz77oiQR0tXZj8ExxbNIbPO91QUKnkAQwzcdRW1yWAD9tjRl4_nwRtLr9fRtpFpPeOiNK39blyNKj-mouWTglBlsBaM4y96dgx7GdGKJSByE5Il_O0nnq',
            'Content-Type: application/json'
        );

        //EXECUTE FIREBASE NOTIFICATIONS TO EACH TOKEN
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $msg = curl_exec($ch);
        curl_close($ch);

        if ($msg === FALSE) {
            $msg = "Error de Firebase";
            return '{"exito" : 0, "msg" : '.$msg.'}';
        }else{
            return '{"exito" : 0, "msg" : "true"}';
        }
    }





}
