<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;

class LocationController extends Controller
{
    public function load_countries()
    {
        $return_array = array();
        $return_array['countries'] = Country::all();
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function load_states(Request $request)
    {
        $country = Country::find($request->country_id);
        if (is_null($country))
            return '{"exito" : 0, "msg" : "El ID de pais es invalido"}';

        $return_array = array();
        $return_array['states'] = $country->states;
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function load_cities(Request $request)
    {
        $state = State::find($request->state_id);
        if (is_null($state))
            return '{"exito" : 0, "msg" : "El ID de estado es invalido"}';

        $return_array = array();
        $return_array['cities'] = $state->cities;
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function load_states_web()
    {
        $id_pais = $_GET['id'];
        $content = DB::select('select * from states where country_id = ' . $id_pais);
        $html = "<option value='' disabled selected>Seleccciona un Estado</option>";
        echo $html;
        foreach ($content as $result) {
            $html = "<option value=$result->id>$result->name</option>";
            echo $html;
        }
        return;
    }

    public function load_cities_web()
    {
        $id_ciudad = $_GET['id'];
        $content = DB::select('select * from cities where state_id = ' . $id_ciudad);
        $html = "<option value='' disabled selected>Seleccione una ciudad</option>";
        echo $html;
        foreach ($content as $result) {
            $html = "<option value=$result->id>$result->name</option>";
            echo $html;
        }
        return;
    }

    public function agregar_pais(Request $request)
    {
        $country = new Country;

        if ($request->has(['inputPais', 'inputNombrecorto'])) {

            $country->name = $request->input('inputPais');
            $country->short_name = $request->input('inputNombrecorto');

            if ($country->save()) {
                $msg = "Registrado correctamente.";
                return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg);
            } else {
                $msg = "Lo sentimos, ha ocurrido un error intenta más tarde";
                return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
            }
        } else {
            $msg = "No se enviarion todos los campos requeridos";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
        }
    }

    public function agregar_estado(Request $request)
    {
        $state = new State;

        if ($request->has(['inputPais', 'inputEstado', 'inputNombrecorto'])) {

            $state->name = $request->input('inputEstado');
            $state->short_name = $request->input('inputNombrecorto');
            $state->country_id = $request->input('inputPais');

            if ($state->save()) {
                $msg = "Registrado correctamente.";
                return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg);
            } else {
                $msg = "Lo sentimos, ha ocurrido un error intenta más tarde";
                return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
            }

        } else {
            $msg = "No se enviarion todos los campos requeridos";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
        }


    }

    public function agregar_ciudad(Request $request)
    {
        $city = new City;
        if ($request->has(['InputPais3', 'inputEstado3', 'inputCiudad3'])) {

            $city->name = $request->input('inputCiudad3');
            $city->state_id = $request->input('inputEstado3');
            $city->number = 0;

            if ($city->save()) {
                $msg = "Registrado correctamente.";
                return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg);
            } else {
                $msg = "Lo sentimos, ha ocurrido un error intenta más tarde";
                return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
            }

        } else {
            $msg = "No se enviarion todos los campos requeridos";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg);
        }
    }

    public function elimina_pais()
    {
        $id = Input::get('id');
        $country = Country::find($id);

        if (!is_null($country)) {


            $msg = "";


            $states = State::where('country_id', $country->id)->get();

            foreach ($states as $state) {
                // Elimina todas las ciudades dentro de un estado
                $cities = City::where('state_id', $state->id)->get();

                foreach ($cities as $city) {
                    $city->delete();
                }

            }

            //Elimina estados
            foreach ($states as $state) {
                $state->delete();
            }


            $country->delete();

            $msg = "Eliminado correctamente";


            if ($msg === "Eliminado correctamente") {
                return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg)->withInput();
            } else {
                return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=Error al tratar de eliminar')->withInput();
            }

        } else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg)->withInput();
        }


    }

    public function elimina_estado()
    {
        $id = Input::get('id');
        $state = State::find($id);
        if (!is_null($state)) {


            $msg = "";

            $cities = City::where('state_id', $state->id)->get();

            foreach ($cities as $city) {
                $city->delete();
            }

            //Elimina estado
            $state->delete();
            $msg = "Eliminado correctamente";


            if ($msg === "Eliminado correctamente") {
                return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg)->withInput();
            } else {

                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg)->withInput();
            }


        } else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function elimina_ciudad()
    {
        $id = Input::get('id');
        $city = City::find($id);
        if (!is_null($city)) {


            $msg = "";
            $city->delete();

                if ($msg === "Eliminado correctamente") {
                    return redirect('/admin/Complementos/Zona_Geografica?Msg=' . $msg)->withInput();
                } else {

                    $msg = "Hubo un error al tratar de eliminar";
                    return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg)->withInput();
                }


        } else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Zona_Geografica?Error=true&Msg=' . $msg)->withInput();
        }
    }


}
