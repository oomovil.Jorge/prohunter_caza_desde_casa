<?php

namespace App\Http\Controllers;

use App\Notice;
use Illuminate\Http\Request;
use App\IntroSlider;
use App\Http\Resources\IntroSliders;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class IntroSlidersController extends Controller
{
    public function introduccion(){
        $introSliders = IntroSlider::all(); 
        if(is_null($introSliders)){
            return '{"exito" : 0, "msg" : "No hay sliders registrados"}'; 
        }
        return IntroSliders::collection($introSliders)->additional([
            'exito' => 1,
            'msg' => 'Exito'
        ]);
    }


    public function  agregar_nuevo_slider(Request $request){

        $IntroSlider = new IntroSlider;

        if($request->has(['inputtitulo', 'inputContenido','inputfile'])) {

            $IntroSlider->titulo = $request->input('inputtitulo');
            $IntroSlider->contenido = $request->input('inputContenido');

            if(Storage::disk('local')->exists($IntroSlider->urlImagen))
                    Storage::delete($IntroSlider->urlImagen);
                $file_name = $request->file('inputfile')->store('sliderss');
            $IntroSlider->urlImagen = asset('storage/').'/'.$file_name;

            $IntroSlider->save();
            $msg = "Registrado correctamente.";
            return redirect('/admin/Complementos/Admin_intro_app?Msg='.$msg);

        }else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Admin_intro_app?Error=true&Msg=' . $msg)->withInput();
        }
    }


  public function eliminar_slider(){
      $intro =  IntroSlider::where('id', '=',  Input::get('id'))->first();

      if(!is_null($intro)) {

          if(Storage::disk('local')->exists($intro->urlImagen)){
              Storage::delete($intro->urlImagen);
          }

          if($intro->delete()){
              $msg = "Eliminado correctamente";
              return redirect('/admin/Complementos/Admin_intro_app?Msg='.$msg)->withInput();
          }else{
              $msg = "Hubo un error al tratar de eliminar";
              return redirect('/admin/Complementos/Admin_intro_app?Error=true&Msg='.$msg)->withInput();
          }
      }else {
          $msg = "No se encontro el registro";
          return redirect('/admin/Complementos/Admin_intro_app?Error=true&Msg='.$msg)->withInput();
      }
  }

    //Cambiar
    public function  modificar_notificacion(Request $request){

        if ($request->has(['inputid','inputtitulo', 'inputContenido'])) {

            $notice = Notice::find($request->input('inputid'));

            if (!is_null($notice)) {

                $notice->titulo = $request->input('inputtitulo');
                $notice->contenido = $request->input('inputContenido');

                if (!is_null($request->file('inputfile'))) {
                    if (Storage::disk('local')->exists($notice->imagen))
                        Storage::delete($notice->imagen);
                    $file_name = $request->file('inputfile')->store('Img_Avisos');
                    $notice->imagen = asset('storage/app/').'/'.$file_name;
                }

                if ($notice->save()) {
                    $msg = "Modificado correctamente";
                    return redirect('/admin/Complementos/Admin_intro_app?id=' . $notice->id . '&Msg='.$msg);
                } else {
                    $msg = "Hubo un error al tratar de Modificar";
                    return redirect('/admin/Complementos/Admin_intro_app?id=' . $notice->id . '&Error=true&Msg='.$msg);
                }

            } else {
                $msg = "No se encontro el registro";
                return redirect('/admin/Complementos/Admin_intro_app?id=' . $notice->id . '&Error=true&Msg='.$msg);
            }

        } else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Admin_intro_app?id=' . $request->input('inputid'). '&Error=true&Msg='.$msg);
        }
    }


}
