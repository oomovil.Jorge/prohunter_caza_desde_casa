<?php

namespace App\Http\Controllers\Backend;

use App\modules_admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdministratorController extends Controller
{

    public function view_login()
    {
        return view('admin.login');
    }


    public function view_table()
    {
        $table = User::where('tipo', 'Administrador')
            ->Orwhere('tipo', 'Root')->get();
        return view('admin.Administrator.Administradores',compact('table'));
    }

    public function view_add()
    {
        $modules = modules_admin::all();
        return view('admin.Administrator.Nuevo_administrador',compact('modules'));
    }


    public function view_edit()
    {



        return view('admin.Administrator.Modificar_admin');
    }
}
