<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriasController extends Controller
{
    public function view_show()
    {


        $categories = Category::all();

        return view('admin.Category.Categorias', compact('categories'));
    }

    public function view_add()
    {
        return view('admin.Category.Nueva_categoria');
    }

    public function view_edit()
    {
        return view('admin.Category.Modificar_categoria');
    }
}
