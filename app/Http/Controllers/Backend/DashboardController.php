<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function view()
    {

      /*  $postulados = DB::select("SELECT * FROM candidate_vacancy cv,candidates c WHERE cv.candidate_id = c.id  AND
            ((status <> 'Total_postulados') AND (cv.created_at <> 0)AND cv.created_at BETWEEN getdate() AND dateadd(day, -30, getdate()));
			");
*/
        $postulados = DB::select("SELECT * FROM candidate_vacancy cv,candidates c WHERE cv.candidate_id = c.id  AND
            ((status <> 'Total_postulados') AND (cv.created_at <> 0) AND (CURDATE() - INTERVAL -(30) DAY));");


/*
        $contratados = DB::select("SELECT c.* FROM candidate_vacancy cv,candidates c WHERE cv.candidate_id = c.id AND ((status = 'Contratado')
            AND (cv.created_at <> 0) AND (cv.created_at <> 0)AND cv.created_at BETWEEN getdate() AND dateadd(day, -30, getdate()));");
        */

        $contratados = DB::select("SELECT c.* FROM candidate_vacancy cv,candidates c WHERE cv.candidate_id = c.id AND ((status = 'Contratado')
            AND (cv.created_at <> 0) AND (CURDATE() - INTERVAL -(30) DAY));");


        $Postulados_vs_contratados_30_dias =  DB::select('select * from Postulados_vs_contratados_30_dias;');

        $Grafica_Preferencias_genero =  DB::select('select * from Grafica_Preferencias_genero;');


        return view('admin.dashboard')
            ->with('postulados',$postulados)
            ->with('Postulados_vs_contratados_30_dias',$Postulados_vs_contratados_30_dias)
            ->with('Grafica_Preferencias_genero',$Grafica_Preferencias_genero)
            ->with('contratados',$contratados);

    }
}
