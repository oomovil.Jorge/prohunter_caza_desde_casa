<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostuladosController extends Controller
{
    public function view_show()
    {
        return view('admin.Postulates.Postulados');
    }

    public function view_add()
    {
        return view('admin.Postulates.Nuevo_Postulado');
    }

    public function view_Ver_postulacion()
    {
        return view('admin.Postulates.Ver_postulacion');
    }

    public function view_Rechazados()
    {
        return view('admin.Postulates.Rechazados');
    }
}
