<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificacionesController extends Controller
{
    public function view_show()
    {
        return view('admin.Notices.Notificaciones');
    }

    public function view_add()
    {
        return view('admin.Notices.Nuevo_Notificacion');
    }

    public function view_edit()
    {
        return view('admin.Notices.Modificar_notificacion');
    }
}
