<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VacantesController extends Controller
{


    public function view_show()
    {
        return view('admin.Vacancies.Vacantes');
    }

    public function view_add()
    {
        $listaCategorias = Category::all();
        $listaPaises =  Country::all();

        return view('admin.Vacancies.Nueva_Vacante',compact('listaCategorias','listaPaises'));
    }

    public function view_edit()
    {
              $content_category =  \DB::select('select * from categories');
              $content_Contries =  \DB::select('select * from countries');

        return view('admin.Vacancies.Modificar_vacante',compact('content_category','content_Contries'));
    }

    public function view_Vacantes_cubiertas()
    {
        return view('admin.Vacancies.vacantes_cubiertas');
    }
}
