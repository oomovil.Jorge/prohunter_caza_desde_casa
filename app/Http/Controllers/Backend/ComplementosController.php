<?php

namespace App\Http\Controllers\Backend;

use App\About;
use App\Blog;
use App\Category;
use App\Country;
use App\IntroSlider;
use App\Http\Controllers\Controller;
use App\PrivacyAgreement;
use Illuminate\Support\Facades\DB;

class ComplementosController extends Controller
{


    public function Admin_intro_app()
    {
        $introsSlider = IntroSlider::all();
        return view('admin.Complements.Admin_intro_APP', compact('introsSlider'));

    }

    public function Blog_Aviso_privacidad()
    {


        $url_blog = Blog::all()->first()->url;
        $Aviso = PrivacyAgreement::all()->first()->text;

        return view('admin.Complements.Blog_Aviso_privacidad')
            ->with('url_blog', $url_blog)
            ->with('Aviso', $Aviso);

    }

    public function Idiomas()
    {
        return view('admin.Complements.Idiomas');

    }

    public function Intro_Slider()
    {
        return view('admin.Complements.IntroSlider');
    }

    public function Software_Categorias()
    {

        $categories = Category::all();
        $software_categories = DB::select('select s.id,s.name as software,c.name as categoria 
                                     from software s join software_categories sc on s.id = sc.id_software 
                                     join categories c on sc.id_categoria = c.id;');


        return view('admin.Complements.Software_Categorias', compact('categories', 'software_categories'));

    }

    public function Zona_Geografica()
    {


        $country = Country::all();
//   DB::select('select id,name as Pais,short_name from countries')


        $Estado_Provincia = DB::select('SELECT s.id,s.name as Estado , s.short_name , c.name as Pais 
                                      FROM states s join countries c on s.country_id = c.id');


        $ciudad = DB::select('select ct.id,c.name as Pais,s.name as Estado,ct.name as Ciudad 
                             from countries c join states s on c.id = s.country_id 
                             join cities ct on ct.state_id = s.id;');


        return view('admin.Complements.Zona_Geografica', compact('country', 'Estado_Provincia', 'ciudad'));

    }

    public function Acerca_de_app()
    {

        $text = "";
        if(!is_null($text)){
            $text = About::all()->first()->text;
            $text = $text != null ? $text : "";
        }

          $img_about = About::where('image_url','!=',null)->get();

        return view('admin.Complements.Acerca_de_App', compact('text','img_about'));

    }

    public function Preguntas_frecuentes()
    {
        return view('admin.Complements.Preguntas_frecuentes');

    }

    public function Bancos()
    {
        return view('admin.Complements.Bancos');

    }


}
