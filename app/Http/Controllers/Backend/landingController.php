<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class landingController extends Controller
{

    public function view_landing()
    {
        return view('web.landing');
    }

    public function view_login()
    {
        return view('web.login');
    }

    public function view_web()
    {
        return view('web.dashboard');
    }

    public function view_recuperar()
    {
        return view('web.recuperar');
    }

    public function view_registro()
    {
        return view('web.registro');
    }

    public function view_perfil()
    {
        return view('web.Perfil.perfil');
    }

    public function view_Candidato()
    {
        return view('web.Candidatos.candidato');
    }

    public function view_editar_Candidato()
    {
        return view('web.Candidatos.Editar_candidato');
    }

    public function view_registro_Candidato()
    {
        return view('web.Candidatos.Registro_candidato');
    }

    public function view_Postulado()
    {
        return view('web.Postulados.postulado');
    }

    public function view_Invitado()
    {
        return view('web.invitado');
    }

    public function view_Movil()
    {
        return view('web.movil');
    }

}
