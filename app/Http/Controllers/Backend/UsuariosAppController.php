<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsuariosAppController extends Controller
{

    public function view_show()
    {

        $table = User::where('tipo', '!=', 'Administrador')
                ->where('tipo', '!=', 'Root')
                ->get();

        return view('admin.Users_APP.Usuarios_app', compact('table'));
    }


    public function view_add()
    {
        return view('admin.Users_APP.Nuevo_Usuario');
    }


    public function view_edit()
    {
        return view('admin.Users_APP.Modificar_usuario');
    }


    public function view_Mi_cuenta()
    {
        return view('admin.Users_APP.Mi_cuenta');
    }


    public function view_Ganancias()
    {
        return view('admin.Users_APP.Ganancias');
    }

    public function view_Estadistica()
    {
        return view('admin.Users_APP.Estadistica');
    }

    public function view_Listar_favoritos()
    {
        return view('admin.Users_APP.Listar_favoritos');
    }


    public function view_candidato_usuario()
    {
        return view('admin.Users_APP.Ver_Candidatos_usuario');
    }

    public function view_Detalle_Candidato()
    {
        return view('admin.Users_APP.Detalle_Candidato');

    }

    public function view_imprimir_candidato()
    {
        return view('admin.Users_APP.Imprimir_Candidato');
    }


}
