<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Preference;
use App\CategoryPreference;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\PreferenceResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class CategoriesController extends Controller
{
    public function listado_categorias()
    {
        $listadoCategorias = Category::all();

        if(is_null($listadoCategorias))
        {
            return '{"exito" : 0, "msg" : "No se encontraron categorias"}'; 
        }
        else
        {
            return CategoryResource::collection($listadoCategorias)->additional([
                'exito' => 1,
                'msg' => 'Exito'
            ]);
        }        
    }

    public function listado_preferencias_usuario(Request $request)
    {
        $email = $request->email;

        if(empty($email) or is_null($email))
        {
            return '{"exito" : 0, "msg" : "Por favor proporciona un correo electronico"}'; 
        }
        else
        {
            $user = User::where('correo', '=', $email)->first();
            if(is_null($user))
            {
                return '{"exito" : 0, "msg" : "No se encontro un usuario registrado a este correo"}'; 
            }
            else
            {
                $preference = $user->preference;
                if(is_null($preference))
                    return '{"data" : [], "exito" : 1, "msg" : "Exito"}'; 
                else
                {
                    $categories = $preference->categories;
                    return CategoryResource::collection($categories)
                                ->additional(['exito' => 1, 'msg' => 'Exito']);
                }
            }
        }
    }   

    public function seleccionar_preferencias(Request $request)
    {
        $email = $request->email;
        $category_ids = array();
        $category_ids = json_decode($request->category_ids)->category_ids;

        if(empty($email) or is_null($email))
        {
            return '{"exito" : 0, "msg" : "Por favor proporciona un correo electronico"}'; 
        }
        else if(empty($category_ids) or is_null($category_ids))
        {
            return '{"exito" : 0, "msg" : "Por favor selecciona al menos una preferencia"}'; 
        }
        else
        {
            $user = User::where('correo', '=', $email)->first();
            if(is_null($user))
            {
                return '{"exito" : 0, "msg" : "No se encontro un usuario con el correo proporcionado"}'; 
            }
            else
            {
                if(is_null($user->preference))//CREATE PREFERENCE FOR THE USER
                {
                    $preference = Preference::create([
                        'user_id' => $user->id,
                    ]);
                    $preference_id = $preference->id;
                    CategoryPreference::where('preference_id', $preference_id)->delete();
                }
                else
                {
                    $preference_id = $user->preference->id;
                }
            
                $oldRelations = CategoryPreference::where('preference_id', $preference_id);
                if(!is_null($oldRelations))
                    $oldRelations->delete();
                
                //CREATE THE RELATIONSHIP FOR THE PREFERENCE TO EACH CATEGORY
                foreach($category_ids as $category_id)
                { 
                    CategoryPreference::create([
                        'preference_id' => $preference_id,
                        'category_id' => $category_id
                    ]);  
                }
                return '{"exito" : 1, "msg" : "Exito"}'; 
            }
        }        
    }

    // web
    public function nueva_categoria(Request $request){

        if ($request->has(['inputCategoria', 'inputfile'])) {
            $Categorias = new Category;

            $Categorias->name = $request->input('inputCategoria');

            if (Storage::disk('local')->exists($Categorias->icon_url))
                Storage::delete($Categorias->icon_url);
            $file_name = $request->file('inputfile')->store('category_icons');
            $Categorias->icon_url = asset('storage/').'/'.$file_name;

            if($Categorias->save()){
                $msg = "Registrado correctamente.";
                return redirect('/admin/Categorias/Nueva_categoria?Msg='.$msg);
            }else{
                $msg = "Lo sentimos, ha ocurrido un error intenta más tarde";
                return redirect('/admin/Categorias/Nueva_categoria?Error=true&Msg='.$msg)->withInput();
            }
        }else{
            $msg = "No se enviarion todos los campos requeridos.";
            return redirect('/admin/Categorias/Nueva_categoria?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function eliminar_categoria(){

        $id = Input::get('id');
        $Categoria = Category::find($id);

        if(!is_null($Categoria)) {
            $msg = "";
            if( $Categorias = DB::select(DB::raw('CALL Eliminar_Categoria('.$id.')'))){
                foreach ($Categorias as $Categoria) {
                    $msg = $Categoria->msg;
                }

                if($msg === "Eliminado correctamente"){
                    return redirect('/admin/Categorias/?Msg='.$msg)->withInput();
                }else{
                    return redirect('/admin/Categorias/?Error=true&Msg='.$msg)->withInput();
                }

            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Categorias/?Error=true&Msg='.$msg)->withInput();
            }
        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Categorias/?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function modificar_categoria(Request $request){
        if ($request->has(['inputid','inputCategoria'])) {

            $Categorias = Category::find( $request->input('inputid'));

            $Categorias->name = $request->input('inputCategoria');

            if (!is_null($request->file('inputfile'))) {
              if (Storage::disk('local')->exists($Categorias->icon_url))
                  Storage::delete($Categorias->icon_url);
              $file_name = $request->file('inputfile')->store('category_icons');
              $Categorias->icon_url = asset('storage/').'/'.$file_name;
          }

            if($Categorias->save()){
                $msg = "Modificado correctamente.";
                return redirect('/admin/Categorias/Modificar_categoria?id=' . $Categorias->id . '&Msg='.$msg);
            }else{
                $msg = "Lo sentimos, ha ocurrido un error intenta más tarde";
                return redirect('/admin/Categorias/Modificar_categoria?id=' . $Categorias->id . '&Error=true&Msg='.$msg)->withInput();
            }

        }else{
            $msg = "No se enviarion todos los campos requeridos.";
            return redirect('/admin/Categorias/Modificar_categoria?id=' . $Categorias->id . '&Error=true&Msg='.$msg)->withInput();
        }

    }
}
