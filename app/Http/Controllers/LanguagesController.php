<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
use App\Software;
use Illuminate\Support\Facades\Input;

class LanguagesController extends Controller
{
    public function load_languages_softwares(){
        $return_array = array();
        $return_array['languages'] = Language::all();
        $return_array['softwares'] = Software::all();
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return json_encode($return_array);   
    }
    //Lenguaje
    public function agregar_lenguaje(Request $request){

         if ($request->has(['inputLengua'])) {

            $lengua = Language::where('name', $request->Input('inputLengua')) -> first();

            if(is_null($lengua)) {

        $lengua  = new Language;

             $lengua->name = $request->Input('inputLengua');
 
             if ($lengua->save()) {
                 $msg = "Registado correctamente";
                 return redirect('/admin/Complementos/Idiomas?Msg='.$msg);
             } else {
                 $msg = "Hubo un error al tratar de registrar";
                 return redirect('/admin/Complementos/Idiomas?&Error=true&Msg='.$msg);
             }

            }else{
                $msg = "Este lenguaje ya existe.";
                return redirect('/admin/Complementos/Idiomas?Error=true&Msg=' . $msg)->withInput();
            }

         }else {
             $msg = "No se enviaron todos los campos requeridos";
             return redirect('/admin/Complementos/Idiomas?Error=true&Msg=' . $msg)->withInput();
         }
 
     }

    public function eliminar_lenguaje(){

        $lenguage =  Language::where('id', '=',  Input::get('id'))->first();

        if(!is_null($lenguage)) {

            if($lenguage->delete()){
                $msg = "Eliminado correctamente";
                return redirect('/admin/Complementos/Idiomas?Msg='.$msg)->withInput();
            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Complementos/Idiomas?Error=true&Msg='.$msg)->withInput();
            }
        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Idiomas?Error=true&Msg='.$msg)->withInput();
        }
    }

     }


