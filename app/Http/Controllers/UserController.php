<?php

namespace App\Http\Controllers;

use App\administrator_modules_assigned;
use App\Available_bank;
use App\Firebase;
use App\Payment_history;
use App\payment_users;
use App\UserBankData;
use App\User_rating;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Mail\Mailer;
use App\Mail\RecuperarPasswordMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Mail;
use App\announcements;

class UserController extends Controller{

    //Movil
    public function registro(Request $request){

        $hasAllFields = false;
        $hasNullValues = false;
        $user = new User;

        if($request->has(['nombre', 'correo', 'contrasena', 'celular', 'info_dispositivo', 'tipo'])){
            $user->nombre = $request->input('nombre');
            $user->correo = $request->input('correo');

            $user->contrasena = Hash::make($request->input('contrasena'));
            $user->celular = $request->input('celular');
            $user->info_dispositivo = $request->input('info_dispositivo');
            $user->tipo = $request->input('tipo');
            $hasAllFields = true;

        }

        if(empty($user->nombre)) $hasNullValues = true;
        if(empty($user->correo)) $hasNullValues = true;
        if(empty($user->contrasena)) $hasNullValues = true;
        if(empty($user->info_dispositivo)) $hasNullValues = true;
        if(empty($user->tipo)) $hasNullValues = true;

        if(!$hasAllFields){
            return '{"exito" : 0, "msg" : "No se enviaron todos los campos"}';
        }
        else if($hasNullValues){
            return '{"exito" : 0, "msg" : "Por favor llena todos los campos"}';

        }else{
            $correoExistente = User::where('correo', '=', $user->correo)->first();

            if(is_null($correoExistente)){

                if(!is_null($request->file('imagen_perfil'))){

                    if(Storage::disk('local')->exists($user->imagen_perfil))
                        Storage::delete($user->imagen_perfil);
                    $file_name = $request->file('imagen_perfil')->store('imagenes_perfil');
                    $user->imagen_perfil = $file_name;
                }

                $user->save();
                return new UserResource($user);
            }else{
                return '{"exito" : 0, "msg" : "El correo ingresado ya existe"}';
            }
        }
    }

    public function login(Request $request){
        $user = null;
        $hasAllFields = false;

        if($request->has('correo', 'contrasena', 'info_dispositivo')){
            $hasAllFields = true;
            $correo = $request->correo;
            $contrasena = $request->contrasena;
            $info_dispositivo = $request->info_dispositivo;
            $user = User::where('correo', $correo)->first();
        }

        if($hasAllFields){
            if(is_null($user))
                return '{"exito" : 0, "msg" : "El usuario no se encuentra registrado"}';
            else{
                if(Hash::check($contrasena, $user->contrasena) == false)

                    return '{"exito" : 0, "msg" : "La contraseña ingresada es incorrecta"}';
                else
                {
                    $user->update(array('info_dispositivo' => $info_dispositivo));
                    return new UserResource($user);
                }
            }
        }else{
            return '{"exito" : 0, "msg" : "No se enviaron los campos correctos"}';
        }
    }

    public function recuperar_password(Request $request, Mailer $mailer){

        $correo = $request->correo;

        if(empty($correo) or is_null($correo)){
            return '{"exito" : 0, "msg" : "Por favor proporciona tu correo electronico"}';
        }else{

       $user = User::where('correo', $request->correo)->first();

            if(is_null($user)){
                return '{"exito" : 0, "msg" : "El correo ingresado no esta registrado"}';
            }else if($user->tipo != "normal"){
                return '{"exito" : 0, "msg" : "El correo ingresado está registrado con Facebook o Google"}';
            }else{

                $newpass = str_random(8);
                $hashed_random_password = Hash::make($newpass);
                $user->contrasena =  $hashed_random_password;
                $user->save();

                $mailer ->to($correo)->send(new RecuperarPasswordMail('ProHunter: Recuperar Contraseña',$correo,$newpass));
                return '{"exito" : 1, "msg" : "Exito"}';
            }
        }
    }

    public function actualizar_perfil(Request $request){
        $user = User::where('correo', $request->correo)->first();

        if(!is_null($user)){
            if(!empty($request->input('nombre')))
                $user->nombre = $request->input('nombre');
            if(!empty($request->input('contrasena')))
                $user->contrasena = Hash::make($request->input('contrasena'));
            if(!empty($request->input('celular')))
                $user->celular = $request->input('celular');
            if(!empty($request->input('fecha_cumpleaños')))
                $user->fecha_cumpleaños = $request->input('fecha_cumpleaños');
            if(!empty($request->input('lugar_nacimiento')))
                $user->lugar_nacimiento = $request->input('lugar_nacimiento');
            if(!empty($request->input('RFC')))
                $user->RFC = $request->input('RFC');
            $user->save();
            return new UserResource($user);
        }
        else
        {
            return '{"exito" : 0, "msg" : "El correo no se encuentra registrado"}';
        }
    }

    public function actualizar_imagen_perfil(Request $request){
        $user = User::where('correo', $request->correo)->first();
        if(!is_null($user))
        {
            if(!is_null($request->file('imagen_perfil')))
            {
                if(Storage::disk('local')->exists($user->imagen_perfil))
                    Storage::delete($user->imagen_perfil);
                $user->imagen_perfil = $request->file('imagen_perfil')->store('imagenes_perfil');
                $user->save();
                return new UserResource($user);
        }
            else
                return '{"exito" : 0, "msg" : "Por favor elige una imagen"}';
        }
        else
        {
            return '{"exito" : 0, "msg" : "El correo no se encuentra registrado"}';
        }
    }

    public function agregar_token_firebase(Request $request){
      // Agrega y actualiza el token si existe  de furebase

        if($request->has('email','token')){


   if(is_null($request->token)){
                return '{"exito" : 0, "msg" : "Debes enviar un token"}';
    }

    $user = User::where('correo', $request->email)->first();
    if(is_null($user)) {
        return '{"exito" : 0, "msg" : "El correo proporcionado no se encuentra registrado"}';
    }else{

        $firebase = $user->firebase;
        if(!is_null($firebase)){
            $firebase->token = $request->token;

            $firebase->save();
            return '{"exito" : 1, "msg" : "sobreescrito"}';
        }else{

          Firebase::create([
          'user_id' => $user->id,
          'token' => $request->token
          ]);

     return '{"exito" : 1, "msg" : "nuevo"}';
        }
    }
        }else{
            return '{"exito" : 0, "msg" : "Debes enviar todos los campos"}';
        }
    }

    public function listar_datos_bancarios(Request $request){

        if($request->has('correo')) {
            $user = User::where('correo', $request->correo)->first();

            if (!is_null($user)) {
                $return_array['data'] = UserBankData::where('id_user', $user->id)->first();
                $return_array['exito'] = 1;
                $return_array['msg'] = 'Exito';
                return $return_array;
        }else{
                return '{"exito" : 0, "msg" : "Sin resultados"}';
            }
        }else{
            return '{"exito" : 0, "msg" : "No enviaste un correo"}';
        }
    }

    public function listar_ganancias(Request $request){

        if($request->has('correo')){
            $user = User::where('correo', $request->correo)->first();

            if (!is_null($user)){

                $return_array['data'] = DB::select('call Ganacias_usuario('.$user->id.')');
                $return_array['exito'] = 1;
                $return_array['msg'] = 'Exito';
                return $return_array;

            }else{
                return '{"exito" : 0, "msg" : "Sin resultados"}';
            }
        }else{
            return '{"exito" : 0, "msg" : "No enviaste un correo"}';
        }
    }

    public function listar__historial_ganancias(Request $request){

        if($request->has('payment_id')){
            $id = $request->input('payment_id');
   $return_array['data'] = DB::select("SELECT ph.id,pu.total_to_pay,ph.payment_id,ph.missing,ph.paid_percentage,
                                       CASE
                                       WHEN ph.paid_percentage = '0%' THEN 0
                                       WHEN ph.paid_percentage = '50%' THEN ph.missing
                                       WHEN ph.paid_percentage = '100%' THEN (pu.total_to_pay / 2)
                                       END AS paid,
                                       ph.created_at,ph.updated_at FROM payment_users pu,payment_history ph
                                       WHERE  pu.id = ph.payment_id AND ph.payment_id = ?;" ,[$id]);

         $return_array['exito'] = 1;
         $return_array['msg'] = 'Exito';

         return $return_array;

        }else{
            return '{"exito" : 0, "msg" : "No enviaste un id"}';
        }
    }

    public function mostrar_calificacion(Request $request){

        if($request->has('correo')) {
            $user = User::where('correo', $request->correo)->first();

            if (!is_null($user)) {
   $star_rating = DB::select('select round(avg(qualification),1) as star_rating  from user_rating where user_id = ?',[$user->id]);
                foreach ($star_rating as $star){$star_rating = $star->star_rating;}

                $return_array['data'] = isset($star_rating) ? $star_rating : 0;
                $return_array['exito'] = 1;
                $return_array['msg'] = 'Exito';
                return $return_array;

            }else return '{"exito" : 0, "msg" : "Correo no encontrado"}';

        }else return '{"exito" : 0, "msg" : "No enviaste un correo"}';
    }

    public function agregar_modificar_datos_bancarios(Request $request){

   if(!empty($request->input('correo')) and !empty($request->input('cuenta'))and !empty($request->input('banco' )) and !empty($request->input('beneficiario' ))){

            $user = User::where('correo', $request->correo)->first();
            if(!is_null($user)){

               $UserBankData  = UserBankData::where('id_user',  $user->id )->first();

                if(is_null($UserBankData)){
                    $UserBankData = new UserBankData;
                    $UserBankData->id_user =  $user->id;
                }

                $UserBankData->cuenta_clabe = $request->input('cuenta');
                $UserBankData->banco_destino  = $request->input('banco' );
                $UserBankData->beneficiario = $request->input('beneficiario' );
                $UserBankData->save();

                return '{"exito" : 1, "msg" : "Datos guardados"}';

            }else {
                return '{"exito" : 0, "msg" : "Correo no encontrado"}';
            }
   }else{
     return '{"exito" : 0, "msg" : "Por favor llena todos los campos"}';
   }
    }

    public function listar_avisos_usuarios(Request $request){

        $user = User::where('correo', '=', $request->email)->first();
        if(!is_null($user)){
//
            $return_array = array();
            $return_array['count'] = DB::select('SELECT count(*) as notWatched FROM announcements WHERE user_id = ? AND watched = 0 AND created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY)',[$user->id])[0]->notWatched;
            $return_array['data'] = DB::select('SELECT * FROM announcements WHERE user_id = ? AND created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY) order by created_at desc',[$user->id]);
            $return_array['exito'] = 1;
            $return_array['msg'] = 'Exito';

            return $return_array;

        }else{
            return '{"exito" : 0, "msg" : "El correo no se encuentra registrado"}';
        }
    }

    public function marcar_avisos_usuarios(Request $request){

        $user = User::where('correo', '=', $request->email)->first();
        if(!is_null($user)){
            $ids_por_wathcear = DB::select('SELECT id FROM announcements WHERE user_id = ? AND watched = 0 AND created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY)',[$user->id]);

            foreach($ids_por_wathcear as $id){
                $announcements = announcements::find($id->id);
                $announcements->watched = 1;
                $announcements->save();
            }
            $return_array = array();
            $return_array['exito'] = 1;
            $return_array['msg'] = 'Exito';

            return $return_array;

        }else{
            return '{"exito" : 0, "msg" : "El correo no se encuentra registrado"}';
        }
    }

    public function listar_bancos(){
        $return_array = array();
        $return_array['data'] = Available_bank::all();
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function estadistica(Request $request){

        $user = User::where('correo', $request->email)->first();

        if(is_null($user)) {
            return '{"exito" : 0, "msg" : "El correo de usuario no es valido"}';
        }else {

            // Asi es este codigo paso por distintas modificaciónes en distintos tiempos


            // Manda la calificiación
            $star_rating = DB::select('select round(avg(qualification),1) as star_rating  from user_rating where user_id = ?',[$user->id]);
            foreach ($star_rating as $star){$star_rating = $star->star_rating;}
            $return_array['Estrellas'] = isset($star_rating) ? $star_rating : '0';

            //Envia las fechas de actividad
            try {
                $Grafica = DB::select('CALL Grafica_Actividad_Usuarios(' . $user->id . ')');
                $return_array['grafica_fechas'] = $Grafica;
            }catch (\Exception $e){
                $return_array['grafica_fechas'] = 0;
            }

            //Envia las datos de grafica de preferencias de genero y de contratados vs postulados
            try {
            $Grafica2 = DB::select('CALL Estadistica_Candidatos_ultimos_30_dias(' . $user->id . ')');
            $return_array['grafica_ultimos_30_dias'] = $Grafica2;
            }catch (\Exception $e){
                $return_array['grafica_ultimos_30_dias'] = 0;
            }

            // Envia detalle de preferencias de genero
            $query_detalle_grafica_genero = "select *  from candidates WHERE gender = ? AND user_id = ?  and created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY);";
            $return_array['detalle_grafica_genero']['maculino'] = DB::select($query_detalle_grafica_genero,['Hombre',$user->id]);
            $return_array['detalle_grafica_genero']['femenino'] = DB::select($query_detalle_grafica_genero,['Mujer',$user->id]);

            // Envia detalle de preferencias de contratados vs postulados
            $return_array['detalle_grafica_vacantes_vs_postulados']['postulados'] =

             DB::select("select * from
                         candidates c,candidate_vacancy cv,vacancies v where c.id = cv.candidate_id AND cv.vacancy_id = v.id AND user_id = ?
                         and cv.created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY);",[$user->id]);

            $return_array['detalle_grafica_vacantes_vs_postulados']['contratados'] =
             DB::select("select * from candidates c,candidate_vacancy cv where c.id = cv.candidate_id AND status = 'Contratado'
                         AND user_id = ?  and cv.created_at AND DATE_SUB(CURDATE(),INTERVAL -30 DAY);",[$user->id]);

            $return_array['exito'] = 1;
            $return_array['msg'] = "Exito";

            return $return_array;
        }
    }

    //web
    public function registrar_administrador(Request $request){

        $hasAllFields = false;
        $hasNullValues = false;
        $user = new User;

        if($request->has(['inputNombre', 'inputCorreo', 'inputContrasena','inputConfirmar', 'inputCel','inputPrivilegio'])){

            $user->nombre = $request->input('inputNombre');
            $user->correo = $request->input('inputCorreo');

            $temContrase1 = $request->input('inputContrasena');
            $temContrase2 = $request->input('inputConfirmar');

            $privilegio = $request->input('inputPrivilegio');

            if($temContrase1 === $temContrase2){
                $user->contrasena = Hash::make($request->input('inputContrasena'));
            }else {

                $msg = "Las contraseñas no coinciden";
                return redirect('/admin/Administradores/Nuevo_administrador?Error=true&Msg='.$msg);
        }

            $user->celular = $request->input('inputCel');
            $user->info_dispositivo = 'Web';
            $user->tipo = $privilegio;
            $hasAllFields = true;
        }
        if(empty($user->nombre)) $hasNullValues = true;
        if(empty($user->correo)) $hasNullValues = true;
        if(empty($user->contrasena)) $hasNullValues = true;

        if(!$hasAllFields){

            $msg = "No se enviaron todos los campos";
            return redirect('/admin/Administradores/Nuevo_administrador?Error=true&Msg='.$msg)->withInput();

        }  else if($hasNullValues) {

            $msg = "Por favor llena todos los campos";
            return redirect('/admin/Administradores/Nuevo_administrador?Error=true&Msg='.$msg)->withInput();

        }  else if($hasNullValues) {
        } else {

           $correoExistente = User::where('correo', '=', $user->correo)->first();

            if(is_null($correoExistente)){

                //agrega la imagen si no es null
                if(!is_null($request->file('inputfile'))){

                    if(Storage::disk('local')->exists($user->imagen_perfil))
                        Storage::delete($user->imagen_perfil);
                    $file_name = $request->file('inputfile')->store('imagenes_perfil');

                    $user->imagen_perfil = $file_name;

                }

                $user->save();
                //Modulos
                if($user->tipo == 'Administrador'){

                    $Modulos[] =  Input::get('Administradores');
                    $Modulos[] =  Input::get('UsuariosAPP');
                    $Modulos[] =  Input::get('Categorías');
                    $Modulos[] =  Input::get('Vacantes');
                    $Modulos[] =  Input::get('Postulados');
                    $Modulos[] =  Input::get('Notificaciones');
                    $Modulos[] =  Input::get('IntroAPP');
                    $Modulos[] =  Input::get('Zonageografica');
                    $Modulos[] =  Input::get('PreguntasFrecuentes');
                    $Modulos[] =  Input::get('Idiomas');
                    $Modulos[] =  Input::get('SoftwareCategorías');
                    $Modulos[] =  Input::get('BlogyAvisodeprivacidad');
                    $Modulos[] =  Input::get('AcercadeAPP');
                    $Modulos[] =  Input::get('Bancos');


                    foreach($Modulos as $clave=>$valor){
                        if(empty($valor)) unset($Modulos[$clave]);
                    }
                    $Modulos = json_encode(array_merge($Modulos));

                    administrator_modules_assigned::updateOrCreate(
                        ['user_id' => $user->id],
                        ['modules' => $Modulos]
                    );

                }else{

                    //Root tiene todos los privilegios
                    $root = '["1","2","3","4","5","6","7","8","9","10","12","13","14","15"]';
                    administrator_modules_assigned::updateOrCreate(
                        ['user_id' => $user->id],
                        ['modules' => $root ]
                    );

                }



                $msg = "Registrado correctamente.";
                return redirect('/admin/Administradores/Nuevo_administrador?Msg='.$msg);

            } else {
                $msg = "El correo ingresado ya existe!";
                return redirect('/admin/Administradores/Nuevo_administrador?Error=true&Msg='.$msg);
            }
        }
    }

    public function registrar_usuario_en_Web(Request $request){

        $hasAllFields = false;
        $hasNullValues = false;
        $user = new User;

        if($request->has(['inputNombre', 'inputCorreo', 'inputContrasena','inputConfirmar', 'inputCel'])){

            $user->nombre = $request->input('inputNombre');
            $user->correo = $request->input('inputCorreo');

            $temContrase1 = $request->input('inputContrasena');
            $temContrase2 = $request->input('inputConfirmar');

            if(!empty($request->input('input_fecha_nacimiento'))) {
                $date = date_create($request->input('input_fecha_nacimiento'));
                $user->fecha_cumpleaños = date_format($date, 'Y-m-d');
            }

            if(!empty($request->input('input_Lugar_nacimiento')))
                $user->lugar_nacimiento = $request->input('input_Lugar_nacimiento');

            if(!empty($request->input('inputRFC')))
                $user->RFC = $request->input('inputRFC');

            if($temContrase1 === $temContrase2){
                $user->contrasena = Hash::make($request->input('inputContrasena'));
            }else {

                $msg = "las contraseñas no coinciden";
                return redirect('/admin/Usuarios_app/Nuevo_usuario?Error=true&Msg='.$msg)->withInput();
            }

            $user->celular = $request->input('inputCel');
            $user->info_dispositivo = 'Web';
            $user->tipo = 'normal';
            $hasAllFields = true;
        }
        if(empty($user->nombre)) $hasNullValues = true;
        if(empty($user->correo)) $hasNullValues = true;
        if(empty($user->contrasena)) $hasNullValues = true;

        if(!$hasAllFields){

            $msg = "No se enviaron todos los campos";
            return redirect('/admin/Usuarios_app/Nuevo_usuario?Error=true&Msg='.$msg)->withInput();

        }  else if($hasNullValues) {

            $msg = "Por favor llena todos los campos";
            return redirect('/admin/Usuarios_app/Nuevo_usuario?Error=true&Msg='.$msg)->withInput();

        }  else if($hasNullValues) {
        } else {

            $correoExistente = User::where('correo', '=', $user->correo)->first();

            if(is_null($correoExistente)){

                //agrega la imagen si no es null
                if(!is_null($request->file('inputfile'))){

                    if(Storage::disk('local')->exists($user->imagen_perfil))
                        Storage::delete($user->imagen_perfil);
                    $file_name = $request->file('inputfile')->store('imagenes_perfil');

                    $user->imagen_perfil = $file_name;

                }

                $user->save();
                $msg = "Registrado correctamente.";
                return redirect('/admin/Usuarios_app/Nuevo_usuario?Msg='.$msg);

            } else {
                $msg = "El correo ingresado ya existe!";
                return redirect('/admin/Usuarios_app/Nuevo_usuario?Error=true&Msg='.$msg)->withInput();
            }
        }
    }

    public function login_web(Request $request){

        $user = null;
        $hasAllFields = false;

        if($request->has('correo', 'contrasena')){
            $hasAllFields = true;
            $correo = $request->correo;
            $contrasena = $request->contrasena;

            $user = User::where('correo', $correo)->first();
        }
        if($hasAllFields){
            if(is_null($user)) {
                $msg = 'El usuario no se encuentra registrado';
                return redirect('login?Error=true&Msg='.$msg)->withInput();

            }else{
                if(Hash::check($contrasena, $user->contrasena) == false) {

                    $msg = 'La contraseña ingresada es incorrecta';
                    return redirect('login?Error=true&Msg='.$msg)->withInput();
                }else{



                    $_SESSION['Oomovil'] = $user->id;
                    $_SESSION['privilegios'] = $user->tipo;

                    if($user->tipo == 'Administrador' or $user->tipo == 'Root') {
                        $_SESSION['isAdmin'] = true;
                        return redirect()->route('Dashboard');

                    }else{
                        $_SESSION['isAdmin'] = false;
                        return redirect()->route('Web');
                    }
                }
            }
        }else{
            $msg = 'No se enviaron los campos correctos';
            return redirect('login?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function cerrar_sesion_admin(){


        if(isset($_SESSION['Oomovil'])){
            unset($_SESSION['Oomovil']);
            unset($_SESSION['privilegios']);
            unset($_SESSION['isAdmin']);
            session_destroy();
        }
        return redirect()->route('Login');
    }

    public function cerrar_sesion_user(){


        if(isset($_SESSION['Oomovil'])){
            unset($_SESSION['Oomovil']);
            unset($_SESSION['privilegios']);
            session_destroy();
        }
        return redirect()->route('Login_web');
    }

    public function modificar_Admin(Request $request){




        $user = User::where('correo',$request->input('inputCorreo') )
            ->where('tipo','=' ,'Administrador')
            ->orWhere('tipo','=' ,'Root')
            ->where('correo','=', $request->input('inputCorreo'))->first();


            if ($request->has(['inputNombre', 'inputCel'])) {
            try {
                    $user->nombre = $request->input('inputNombre');
                    $user->celular = $request->input('inputCel');
                    $user->info_dispositivo = 'Web';
                    $user->tipo = $request->input('inputPrivilegio');

              }catch(\Exception $e){
              $msg = $e;
              return redirect('/admin/Administradores/Modificar_administrador?correo=' . $user->correo . '&Error=true&Msg=' . $msg)->withInput();
                }



            } else {
                $msg = "No se enviaron todos los campos";
                return redirect('/admin/Administradores/Modificar_administrador?correo=' . $user->correo . '&Error=true&Msg=' . $msg)->withInput();
            }
            $temContrase1 = $request->input('inputContrasena');
            $temContrase2 = $request->input('inputConfirmar');

            if ($temContrase1 != null and $temContrase2 != null) {

                if ($temContrase1 === $temContrase2) {
                    $user->contrasena = Hash::make($request->input('inputContrasena'));
                } else {
                    $msg = "las contraseñas no coinciden";
                    return redirect('/admin/Administradores/Modificar_administrador?correo=' . $user->correo . '&Error=true&Msg=' . $msg)->withInput();
                }
            }

            if (!is_null($request->file('inputfile'))) {
                if (Storage::disk('local')->exists($user->imagen_perfil))
                    Storage::delete($user->imagen_perfil);
                $file_name = $request->file('inputfile')->store('imagenes_perfil');
                $user->imagen_perfil = $file_name;
            }

        //Modulos
       if($user->tipo == 'Administrador'){

           $Modulos[] =  Input::get('Administradores');
           $Modulos[] =  Input::get('UsuariosAPP');
           $Modulos[] =  Input::get('Categorías');
           $Modulos[] =  Input::get('Vacantes');
           $Modulos[] =  Input::get('Postulados');
           $Modulos[] =  Input::get('Notificaciones');
           $Modulos[] =  Input::get('IntroAPP');
           $Modulos[] =  Input::get('Zonageografica');
           $Modulos[] =  Input::get('PreguntasFrecuentes');
           $Modulos[] =  Input::get('Idiomas');
           $Modulos[] =  Input::get('SoftwareCategorías');
           $Modulos[] =  Input::get('BlogyAvisodeprivacidad');
           $Modulos[] =  Input::get('AcercadeAPP');
           $Modulos[] =  Input::get('Bancos');


           foreach($Modulos as $clave=>$valor){
               if(empty($valor)) unset($Modulos[$clave]);
           }
           $Modulos = json_encode(array_merge($Modulos));

           administrator_modules_assigned::updateOrCreate(
                ['user_id' => $user->id],
                ['modules' => $Modulos]
            );

       }else{

           //Root tiene todos los privilegios
           $root = '["1","2","3","4","5","6","7","8","9","10","12","13","14","15"]';
           administrator_modules_assigned::updateOrCreate(
               ['user_id' => $user->id],
               ['modules' => $root ]
           );

       }

            $user->save();
            $msg = "Modificado correctamente.";
            return redirect('/admin/Administradores/Modificar_administrador?correo=' . $user->correo . '&Msg=' . $msg);
        }

    public function modificar_Usuario_APP_Web(Request $request){

        $user = User::where('correo', '=', $request->input('inputCorreo'))->where('tipo', '!=', 'Administrador')->first();

        if ($request->has(['inputNombre', 'inputCel'])) {
            try {
                $user->nombre = $request->input('inputNombre');
                $user->celular = $request->input('inputCel');
                $user->info_dispositivo = 'Web';

                      if(!empty($request->input('input_fecha_nacimiento'))) {
                    $date = date_create($request->input('input_fecha_nacimiento'));
                    $user->fecha_cumpleaños = date_format($date, 'Y-m-d');
                }

               if(!empty($request->input('input_Lugar_nacimiento')))
                    $user->lugar_nacimiento = $request->input('input_Lugar_nacimiento');

               if(!empty($request->input('inputRFC')))
                    $user->RFC = $request->input('inputRFC');

            }catch(\Exception $e){
                return redirect()->back();
            }
        } else {
            $msg = "No se enviaron todos los campos";
            return redirect('/admin/Administradores/Modificar_administrador?correo=' . $user->correo . '&Error=true&Msg=' . $msg)->withInput();
        }
        $temContrase1 = $request->input('inputContrasena');
        $temContrase2 = $request->input('inputConfirmar');

        if ($temContrase1 != null and $temContrase2 != null) {

            if ($temContrase1 === $temContrase2) {
                $user->contrasena = Hash::make($request->input('inputContrasena'));
            } else {
                $msg = "las contraseñas no coinciden";
                return redirect('/admin/Usuarios_app/Modificar_usuario?correo=' . $user->correo . '&Error=true&Msg=' . $msg)->withInput();
            }
        }

        if (!is_null($request->file('inputfile'))) {

            if (Storage::disk('local')->exists($user->imagen_perfil))
                Storage::delete($user->imagen_perfil);
            $file_name = $request->file('inputfile')->store('imagenes_perfil');
            $user->imagen_perfil = $file_name;
        }

        $user->save();
        $msg = "Modificado correctamente.";
        return redirect('/admin/Usuarios_app/Modificar_usuario?correo=' . $user->correo . '&Msg=' . $msg);
    }

    public function eliminar_usuario(){

        $user = User::find(Input::get('id'));

        if(!is_null($user)) {


            if($user->delete()){

                $msg = "Eliminado correctamente";
                return redirect('/admin/Usuarios_app/?Msg='.$msg)->withInput();
            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Usuarios_app/?Error=true&Msg='.$msg)->withInput();
            }
        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Usuarios_app/?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function eliminar_Admin(){

        $user = User::find(Input::get('id'));

        if(!is_null($user)) {


            if($user->delete()){

                $msg = "Eliminado correctamente";
                return redirect('/admin/Administradores/?Msg='.$msg)->withInput();
            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Administradores/?Error=true&Msg='.$msg)->withInput();
            }

        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Administradores/?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function actualizar_ganacias_usuario(Request $request){

        if($request->has('input_pago', 'input_id','input_id_ganancia')){

            $pago_porcentaje = $request->input_pago;
            if($pago_porcentaje == -1){
                $msg = "Selecciona un valor permitido";
                return redirect('/admin/Usuarios_app/Ganacias?id=' . $request->input_id_ganancia . '&Error=true&Msg=' . $msg)->withInput();
            }

            $id_ganancia = $request->input_id;
            $Pago = payment_users::find($id_ganancia);
            $Pago->paid_percentage = $pago_porcentaje;

            $Pago->save();

            // agrega historial
            $historial = new Payment_history;

            $historial->payment_id = $id_ganancia;

            $faltantenew = 0;
            $faltante =  DB::select("select ROUND((total_to_pay / 2),2) as faltante from payment_users where id = ?",[$id_ganancia]);
            foreach($faltante as $faltantes){ $faltantenew =$faltantes->faltante;}

            $historial->missing = $faltantenew;
            $historial->paid_percentage = $pago_porcentaje;

            $historial->save();

            $msg = "Actualizado correctamente";
            return redirect('/admin/Usuarios_app/Ganacias?id=' . $request->input_id_ganancia . '&Msg=' . $msg)->withInput();

        }else{
            $msg = "No se enviarion todos los campos";
            return redirect('/admin/Usuarios_app/Ganacias?id=' . $request->input_id_ganancia . '&Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function calificar_usuario(Request $request){

        if($request->has(['user_id','starnew','admin_id'])){

            $User_rating =  User_rating::where('user_id','=', $request->input('user_id'))
               ->where('admin_id','=', $request->input('admin_id'))->first();;

           if(is_null($User_rating)){
               $User_rating = new User_rating;
           }
            $User_rating->user_id = $request->input('user_id');
            $User_rating->qualification = $request->input('starnew');
            $User_rating->admin_id = $request->input('admin_id');

            $User_rating->save();

            echo 'Calificación Guardada';
        }else{
            echo 'No se pudo guardar tu calificación , intentalo mas tarde';
        }
    }

    public function eliminar_bancos(){
        $id = Input::get('id');
        if(Available_bank::destroy($id)){
            $msg = "Eliminado correctamente";
            return redirect('/admin/Complementos/Bancos?Msg='.$msg)->withInput();
        }else{
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Bancos?Error=true&Msg='.$msg)->withInput();
        }
    }

    public function agregar_bancos(Request $request){

    if($request->has('inputbanco')){

           $banco = new Available_bank;
           $banco->name = $request->input('inputbanco');

           if($banco->save()){
                   $msg = "Guardado correctamente";
                   return redirect('/admin/Complementos/Bancos?Msg='.$msg)->withInput();
           }else{
                   $msg = "Lo sentimos, hubo un error intentalo mas tarde";
                   return redirect('/admin/Complementos/Bancos?Error=true&Msg='.$msg)->withInput();
           }

      }else{
        return '{"exito" : 0, "msg" : "Debes enviar todos los campos"}';
      }
    }

    /*public function notificacion_manual(Request $request){

        $token =  $request->input('token');
        $title =  $request->input('title');
        $subtitle =  $request->input('subtitle');
        $body =  $request->input('body');
        $message = $request->input('message');
        $icon = $request->input('icon');


        $token  = DB::select("select token from firebase where user_id = 2") ;

        $token_to_send = [];
        foreach($token as $tk){

            $token_to_send[] = $tk->token;

        }

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array
        (
            'registration_ids' =>  $token_to_send,
            'notification' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon,
                ],
            'data' =>
                [
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "body" => $body,
                    "message" => $message,
                    "icon" => $icon
                ]
        );
        $headers = array(
            'Authorization:key = AAAAH1ZV3YM:APA91bGv-Nv8c7hr977n2Opoz77oiQR0tXZj8ExxbNIbPO91QUKnkAQwzcdRW1yWAD9tjRl4_nwRtLr9fRtpFpPeOiNK39blyNKj-mouWTglBlsBaM4y96dgx7GdGKJSByE5Il_O0nnq',
            'Content-Type: application/json'
        );

        //EXECUTE FIREBASE NOTIFICATIONS TO EACH TOKEN
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $msg = curl_exec($ch);
        curl_close($ch);

        if ($msg === FALSE) {
            $msg = "Error de Firebase";
            return '{"exito" : 0, "msg" : '.$msg.'}';
        }else{
            return '{"exito" : 1, "msg" : '.$msg.'}';
        }
    }*/


}
