<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\PrivacyAgreement;
use App\About;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{


    public function blog()
    {
        $return_array = array();
        $return_array['url'] = Blog::first()->url;
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function aviso_privacidad()
    {
        $return_array = array();
        $return_array['text'] = PrivacyAgreement::first()->text;
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function about()
    {
        $return_array['text'] = About::first()->text;
        $urls = array();
        foreach (About::whereNotNull('image_url')->get() as $item) {
            $urls[] = $item->image_url;
        }
        $return_array['image_urls'] = $urls;
        $return_array['exito'] = 1;
        $return_array['msg'] = 'Exito';
        return $return_array;
    }

    public function actualizar_about_texto(Request $request)
    {
        if ($request->has(['inputtext'])) {
            $about = About::find(1);

            if (is_null($about)) {
                $about = new About();
            }

            $about->text = $request->input('inputtext');
            $about->save();
            $msg = "Registrado correctamente.";
            return redirect('/admin/Complementos/Acerca_de_app?Msg=' . $msg);
        } else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Acerca_de_app?Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function actualizar_about_imgs(Request $request)
    {

        $about = new About;
        if (!is_null($request->file('inputfile'))) {

            $about->text = null;
            if (Storage::disk('local')->exists($about->image_url))
                Storage::delete($about->image_url);
            $file_name = $request->file('inputfile')->store('about_images');
            $about->image_url = asset('storage/') . '/' . $file_name;

        }

        $about->save();
        $msg = "Registrado correctamente.";
        return redirect('/admin/Complementos/Acerca_de_app?Msg=' . $msg);

    }

    public function eliminar_img_about()
    {

        $about = About::where('id', '=', Input::get('id'))->first();
        if (!is_null($about)) {

            if ($about->delete()) {
                $msg = "Eliminado correctamente";
                return redirect('/admin/Complementos/Acerca_de_app?Msg=' . $msg)->withInput();
            } else {
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Complementos/Acerca_de_app?Error=true&Msg=' . $msg)->withInput();
            }
        } else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Acerca_de_app?Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function Cambiar_blog_Aviso(Request $request)
    {

        if ($request->has(['inputblog', 'inputAviso'])) {


            $blog = new Blog();
            $blog->url = $request->Input('inputblog');
            $blog->save();


            $aviso = new PrivacyAgreement();
            $aviso->text = $request->Input('inputAviso');
            $aviso->save();
            $msg = "Registado correctamente";


            if ($msg === "Registado correctamente") {

                return redirect('/admin/Complementos/Blog_Aviso_privacidad?Msg=' . $msg)->withInput();
            } else {
                $msg = "No se enviaron todos los campos requeridos";
                return redirect('/admin/Complementos/Blog_Aviso_privacidad?Error=true&Msg=' . $msg)->withInput();
            }

        } else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Blog_Aviso_privacidad?Error=true&Msg=' . $msg)->withInput();
        }


    }
}
