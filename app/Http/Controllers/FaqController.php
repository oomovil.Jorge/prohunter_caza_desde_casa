<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Http\Resources\FaqResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FaqController extends Controller
{
    public function listado_preguntas_frecuentes(){
        return FaqResource::collection(Faq::all())->additional([
            'exito' => 1,
            'msg' => "Exito"
        ]);
    }

    //Pregumtas grecuentes

    public function  agregar_pregunta(Request $request){

        $Faq = new Faq;

        if($request->has(['inputPregunta', 'inputRespuesta'])) {

            $Faq->question   = $request->input('inputPregunta');
            $Faq->answer     = $request->input('inputRespuesta');

            $Faq->save();
            $msg = "Registrado correctamente.";
            return redirect('/admin/Complementos/Preguntas_frecuentes?Msg='.$msg);

        }else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Preguntas_frecuentes?Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function eliminar_preguntas(){

        $Faq =  Faq::where('id', '=',  Input::get('id'))->first();

        if(!is_null($Faq)) {

            if($Faq->delete()){
                $msg = "Eliminado correctamente";
                return redirect('/admin/Complementos/Preguntas_frecuentes?Msg='.$msg)->withInput();
            }else{
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Complementos/Preguntas_frecuentes?Error=true&Msg='.$msg)->withInput();
            }
        }else {
            $msg = "No se encontro el registro";
            return redirect('/admin/Complementos/Preguntas_frecuentes?Error=true&Msg='.$msg)->withInput();
        }

    }



}
