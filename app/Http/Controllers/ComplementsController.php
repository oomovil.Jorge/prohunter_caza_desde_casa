<?php

namespace App\Http\Controllers;

use App\Software_categories;
use Illuminate\Http\Request;
use App\Language;
use App\Software;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class ComplementsController extends Controller
{

//Software categoria

    public function agregar_software_categoria(Request $request)
    {

        if ($request->has(['inputSoftware', 'inputcat'])) {


            $software = Software::create(['name' => $request->input('inputSoftware')]);

            $soft_cat = Software_categories::create(
                [
                    'id_categoria' => $request->input('inputcat'),
                    'id_software' => $software->id
                ]
            );


            if ($soft_cat) {
                $msg = "Registado correctamente";
                return redirect('/admin/Complementos/Software_Categorias?Msg=' . $msg)->withInput();
            } else {
                $msg = "Hubo un error al tratar de eliminar";
                return redirect('/admin/Complementos/Software_Categorias?Error=true&Msg=' . $msg)->withInput();
            }

        } else {
            $msg = "No se enviaron todos los campos requeridos";
            return redirect('/admin/Complementos/Idiomas?Error=true&Msg=' . $msg)->withInput();
        }
    }

    public function eliminar_software()
    {

        $id = Input::get('id');

        $msg = "";

        $software = Software::find($id);

        if (!is_null($software)) {
            $soft_cat = Software_categories::where('id_software', $id)->first();

            $soft_cat->delete();
            $software->delete();
            $msg = "Eliminado correctamente";

        }

        if ($msg === "Eliminado correctamente") {
            return redirect('/admin/Complementos/Software_Categorias?Msg=' . $msg)->withInput();
        } else {
            $msg = "Hubo un error al tratar de eliminar";
            return redirect('/admin/Complementos/Software_Categorias?Error=true&Msg=' . $msg)->withInput();
        }


    }


}

