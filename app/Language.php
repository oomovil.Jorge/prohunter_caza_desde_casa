<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function candidates()
    {
        return $this->belongsToMany('App\Candidate');
    }
}
