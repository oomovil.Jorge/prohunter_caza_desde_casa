<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modules_admin extends Model
{
    protected $table = 'modules_admin';

    protected $fillable = [ 'name'];

}
