<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    protected $table = 'work_experience';
    protected $fillable = [
        'candidate_id',
        'position',
        'active',
        'company_name',
        'role',
        'activities',
        'start_month',
        'start_year',
        'end_month',
        'end_year',
        'work_portfolio_url_1',
        'work_portfolio_url_2',
        'work_portfolio_url_3'
    ];

    public function candidate()
    {
        return $this->belongsTo('App\Candidate');
    }
}
