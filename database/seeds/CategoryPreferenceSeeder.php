<?php

use Illuminate\Database\Seeder;
use App\CategoryPreference;

class CategoryPreferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryPreference::create([
            'preference_id' => 1,
            'category_id' => 1
        ]);

        CategoryPreference::create([
            'preference_id' => 1,
            'category_id' => 2
        ]);
        
        CategoryPreference::create([
            'preference_id' => 1,
            'category_id' => 3
        ]);

        CategoryPreference::create([
            'preference_id' => 2,
            'category_id' => 1
        ]);

        CategoryPreference::create([
            'preference_id' => 3,
            'category_id' => 3
        ]);
    }
}
