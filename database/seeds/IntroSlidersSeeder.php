<?php

use Illuminate\Database\Seeder;
use App\IntroSlider;

class IntroSlidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IntroSlider::create([
            'titulo' => 'Intro 1',
            'contenido' => 'Contenido 1',
            'urlImagen' => 'http://porvenirapps.com/prohunter/storage/app/sliders/1.jpg'
        ]);

        IntroSlider::create([
            'titulo' => 'Intro 2',
            'contenido' => 'Contenido 2',
            'urlImagen' => 'http://porvenirapps.com/prohunter/storage/app/sliders/2.jpg'
        ]);

        IntroSlider::create([
            'titulo' => 'Intro 3',
            'contenido' => 'Contenido 3',
            'urlImagen' => 'http://porvenirapps.com/prohunter/storage/app/sliders/3.jpg'
        ]);
    }
}
