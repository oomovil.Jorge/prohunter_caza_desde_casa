<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        //Related
        $this->call(IntroSlidersSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(VacanciesSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(SoftwareSeeder::class);

        //Users
        $this->call(UsersSeeder::class);
        $this->call(UserVacancySeeder::class);
        $this->call(UserCandidateSeeder::class);
        $this->call(PreferencesSeeder::class);
        $this->call(CategoryPreferenceSeeder::class);

        //Candidates
        $this->call(CandidatesSeeder::class);
        $this->call(CandidateLanguageSeeder::class);
        $this->call(CandidateSoftwareSeeder::class);
        $this->call(CandidateVacancySeeder::class);
        $this->call(EducationDataSeeder::class);
        $this->call(WorkExperienceSeeder::class);

        //Locations
        $this->call(CountriesSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(Cities::class);

        //Independant
        $this->call(FirebaseSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(NoticesSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(PrivacyAgreementSeeder::class);
        $this->call(AboutSeeder::class);
    }
}
