<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
        Category::create([
            'name' => 'Categoria 1',
            'icon_url' => 'http://porvenirapps.com/prohunter/storage/app/category_icons/category_icon_1.png'
        ]);

        Category::create([
            'name' => 'Categoria 2',
            'icon_url' => 'http://porvenirapps.com/prohunter/storage/app/category_icons/category_icon_2.png'
        ]);

        Category::create([
            'name' => 'Categoria 3',
            'icon_url' => 'http://porvenirapps.com/prohunter/storage/app/category_icons/category_icon_3.png'
        ]);
    }
}
