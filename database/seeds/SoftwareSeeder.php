<?php

use Illuminate\Database\Seeder;
use App\Software;

class SoftwareSeeder extends Seeder
{
    public function run()
    {
        Software::create([
            'name' => 'Adobe Illustrator'
        ]);

        Software::create([
            'name' => 'Office Excel'
        ]);

        Software::create([
            'name' => 'Office Word'
        ]);

        Software::create([
            'name' => 'Office PowerPoint'
        ]);

        Software::create([
            'name' => 'Adobe PhotoShop'
        ]);
        
    }
}
