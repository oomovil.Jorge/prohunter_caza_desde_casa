<?php

use Illuminate\Database\Seeder;

class ModulesAdminTableSeeder extends Seeder
{

    public function run()

    {


        $dt = \Carbon\Carbon::now();

        DB::table('modules_admin')->insert(
            ['name' => 'Administradores', 'created_at' => $dt, 'updated_at' => $dt]
             );
        DB::table('modules_admin')->insert(
            ['name' => 'Usuarios APP', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Categorías', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Vacantes', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Postulados', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Notificaciones', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Intro APP', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Zona geográfica	', 'created_at' => $dt, 'updated_at' => $dt]
           );
        DB::table('modules_admin')->insert(
            ['name' => 'Preguntas Frecuentes	', 'created_at' => $dt, 'updated_at' => $dt]
        );


        DB::table('modules_admin')->insert(
            ['name' => 'Idiomas', 'created_at' => $dt, 'updated_at' => $dt]
        );


        DB::table('modules_admin')->insert(
            ['name' => 'borrar este', 'created_at' => $dt, 'updated_at' => $dt] //  id  = 11
        );


        DB::table('modules_admin')->insert(
            ['name' => 'Software Categorías', 'created_at' => $dt, 'updated_at' => $dt]
           );
        DB::table('modules_admin')->insert(
            ['name' => 'Blog y Aviso de privacidad', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Acerca de APP', 'created_at' => $dt, 'updated_at' => $dt]
        );
        DB::table('modules_admin')->insert(
            ['name' => 'Bancos', 'created_at' => $dt, 'updated_at' => $dt]
        );


     DB::delete('delete modules_admin where id = ?', ['11']);

    }




}
