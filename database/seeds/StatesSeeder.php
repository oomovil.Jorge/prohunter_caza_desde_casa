<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesSeeder extends Seeder
{
    public function run()
    {
        State::create([
			'id' => '01',
			'name' =>  'Aguascalientes',
			'short_name' =>  'Ags.',
			'country_id' =>  '1'
		]);
		State::create([
			'id' => '02',
			'name' =>  'Baja California',
			'short_name' =>  'BC',
			'country_id' =>  '1'
			
		]);
		State::create([
			'id' => '03',
			'name' =>  'Baja California Sur',
			'short_name' =>  'BCS',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '04',
			'name' =>  'Campeche',
			'short_name' =>  'Camp.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '05',
			'name' =>  'Coahuila de Zaragoza',
			'short_name' =>  'Coah.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '06',
			'name' =>  'Colima',
			'short_name' =>  'Col.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '07',
			'name' =>  'Chiapas',
			'short_name' =>  'Chis.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '08',
			'name' =>  'Chihuahua',
			'short_name' =>  'Chih.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '09',
			'name' =>  'Distrito Federal',
			'short_name' =>  'DF',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '10',
			'name' =>  'Durango',
			'short_name' =>  'Dgo.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '11',
			'name' =>  'Guanajuato',
			'short_name' =>  'Gto.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '12',
			'name' =>  'Guerrero',
			'short_name' =>  'Gro.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '13',
			'name' =>  'Hidalgo',
			'short_name' =>  'Hgo.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '14',
			'name' =>  'Jalisco',
			'short_name' =>  'Jal.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '15',
			'name' =>  'México',
			'short_name' =>  'Mex.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '16',
			'name' =>  'Michoacán de Ocampo',
			'short_name' =>  'Mich.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '17',
			'name' =>  'Morelos',
			'short_name' =>  'Mor.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '18',
			'name' =>  'Nayarit',
			'short_name' =>  'Nay.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '19',
			'name' =>  'Nuevo León',
			'short_name' =>  'NL',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '20',
			'name' =>  'Oaxaca',
			'short_name' =>  'Oax.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '21',
			'name' =>  'Puebla',
			'short_name' =>  'Pue.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '22',
			'name' =>  'Querétaro',
			'short_name' =>  'Qro.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '23',
			'name' =>  'Quintana Roo',
			'short_name' =>  'Q. Roo',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '24',
			'name' =>  'San Luis Potosí',
			'short_name' =>  'SLP',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '25',
			'name' =>  'Sinaloa',
			'short_name' =>  'Sin.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '26',
			'name' =>  'Sonora',
			'short_name' =>  'Son.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '27',
			'name' =>  'Tabasco',
			'short_name' =>  'Tab.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '28',
			'name' =>  'Tamaulipas',
			'short_name' =>  'Tamps.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '29',
			'name' =>  'Tlaxcala',
			'short_name' =>  'Tlax.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '30',
			'name' =>  'Veracruz de Ignacio de la Llave',
			'short_name' =>  'Ver.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '31',
			'name' =>  'Yucatán',
			'short_name' =>  'Yuc.',
			'country_id' =>  '1'

		]);
		State::create([
			'id' => '32',
			'name' =>  'Zacatecas',
			'short_name' =>  'Zac.',
			'country_id' =>  '1'
		]);
    }
}
