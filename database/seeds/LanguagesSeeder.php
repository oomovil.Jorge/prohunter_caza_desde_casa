<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguagesSeeder extends Seeder
{

    public function run()
    {
        Language::create([
            'name' => 'English'
        ]);

        Language::create([
            'name' => 'Spanish'
        ]);

        Language::create([
            'name' => 'Cantonese'
        ]);
    }
}
