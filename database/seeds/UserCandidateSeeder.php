<?php

use Illuminate\Database\Seeder;
use App\UserCandidate;

class UserCandidateSeeder extends Seeder
{

    public function run()
    {
        UserCandidate::create([
            'user_id' => '1',
            'candidate_id' => '1'
        ]);

        UserCandidate::create([
            'user_id' => '1',
            'candidate_id' => '2'
        ]);

        UserCandidate::create([
            'user_id' => '2',
            'candidate_id' => '1'
        ]);
    }
}
