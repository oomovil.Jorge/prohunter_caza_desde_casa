<?php

use Illuminate\Database\Seeder;
use App\EducationData;

class EducationDataSeeder extends Seeder
{

    public function run()
    {
        EducationData::create([
            'candidate_id' => '1',
            'universidad' => 'CUCEI',
            'start_month' => '08',
            'start_year' => '14',
            'end_month' => '11',
            'end_year' => '18',
            'academic_level' => 'Licenciatura',
            'degree_obtained' => 'Ing. en Computacion',
            'additional_info' => 'U de G',            
        ]);

        EducationData::create([
            'candidate_id' => '2',
            'universidad' => 'CETI',
            'start_month' => '08',
            'start_year' => '14',
            'end_month' => '11',
            'end_year' => '18',
            'academic_level' => 'Licenciatura',
            'degree_obtained' => 'Ing. en Computacion',
            'additional_info' => 'U de G',            
        ]);

        EducationData::create([
            'candidate_id' => '3',
            'universidad' => 'ITESO',
            'start_month' => '08',
            'start_year' => '14',
            'end_month' => '11',
            'end_year' => '18',
            'academic_level' => 'Licenciatura',
            'degree_obtained' => 'Ing. en Computacion',
            'additional_info' => 'U de G',            
        ]);
    }
}
