<?php

use Illuminate\Database\Seeder;
use App\PrivacyAgreement;

class PrivacyAgreementSeeder extends Seeder
{
    public function run()
    {
        PrivacyAgreement::create([
            'text' => 'Lorem ipsum dolor sit amet, odio modus suscipit id nec, te pri veniam civibus. Est eu assum graece equidem, in civibus scaevola has. An quem cibo lorem est, cum semper dolores in. Pro no utamur periculis, ei omnium pericula intellegat quo. Ei recusabo convenire conceptam pro, ea nam accumsan reprimique temporibus. Ea sea essent phaedrum, ea cum atomorum antiopam.

            Per justo reprehendunt ea. Dictas vituperatoribus eos cu, eos ad vivendum delicata salutandi. Elitr repudiare vituperata ut vel, qualisque instructior mel et. Autem ullum sea cu, ex debet soleat definitiones usu.
            
            Pri in atqui vulputate, cum at dico novum, ad quo tota conclusionemque. Ocurreret prodesset tincidunt pri at. Eam et dolore gubergren mnesarchum. Vero sonet nam ut. Sed at tota impedit nostrum.
            
            Ne sea liber quaeque offendit. Vis tractatos gloriatur eu. Ut probatus iracundia vel, an per commodo ullamcorper. Nec no enim mundi, tollit deseruisse ea duo, an scribentur referrentur ullamcorper vim.
            
            Eam et alia viris legimus. Ex usu ferri congue soleat. Nec tritani omittantur eu, te usu rebum postea deserunt. Doctus admodum accumsan duo id, nam te sale congue graece. Hinc option vivendo per ei, an virtute evertitur deseruisse est.'
        ]);
    }
}
