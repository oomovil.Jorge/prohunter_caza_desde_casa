<?php

use Illuminate\Database\Seeder;
use App\UserVacancy;

class UserVacancySeeder extends Seeder
{
    public function run()
    {
        UserVacancy::create([
            'user_id' => '1',
            'vacancy_id' => '1'
        ]);
        
        UserVacancy::create([
            'user_id' => '2',
            'vacancy_id' => '1'
        ]);

        UserVacancy::create([
            'user_id' => '2',
            'vacancy_id' => '2'
        ]);

        UserVacancy::create([
            'user_id' => '3',
            'vacancy_id' => '1'
        ]);

        UserVacancy::create([
            'user_id' => '3',
            'vacancy_id' => '2'
        ]);

        UserVacancy::create([
            'user_id' => '3',
            'vacancy_id' => '3'
        ]);
    }
}
