 <?php

use Illuminate\Database\Seeder;
use App\City;

class Cities extends Seeder
{
    public function run()
    {
		City::create([
            'name' =>  'Aguascalientes',
            'state_id' => '1',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Asientos',
            'state_id' => '1',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Calvillo',
            'state_id' => '1',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Cosío',
            'state_id' => '1',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Jesús María',
            'state_id' => '1',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Pabellón de Arteaga',
            'state_id' => '1',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Rincón de Romos',
            'state_id' => '1',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'San José de Gracia',
            'state_id' => '1',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Tepezalá',
            'state_id' => '1',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'El Llano',
            'state_id' => '1',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'San Francisco de los Romo',
            'state_id' => '1',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Ensenada',
            'state_id' => '2',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Mexicali',
            'state_id' => '2',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Tecate',
            'state_id' => '2',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Tijuana',
            'state_id' => '2',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Playas de Rosarito',
            'state_id' => '2',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Comondú',
            'state_id' => '3',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Mulegé',
            'state_id' => '3',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'La Paz',
            'state_id' => '3',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Los Cabos',
            'state_id' => '3',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Loreto',
            'state_id' => '3',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Calkiní',
            'state_id' => '4',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Campeche',
            'state_id' => '4',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Carmen',
            'state_id' => '4',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Champotón',
            'state_id' => '4',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Hecelchakán',
            'state_id' => '4',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Hopelchén',
            'state_id' => '4',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Palizada',
            'state_id' => '4',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Tenabo',
            'state_id' => '4',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Escárcega',
            'state_id' => '4',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Calakmul',
            'state_id' => '4',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Candelaria',
            'state_id' => '4',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Abasolo',
            'state_id' => '5',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acuña',
            'state_id' => '5',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Allende',
            'state_id' => '5',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Arteaga',
            'state_id' => '5',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Candela',
            'state_id' => '5',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Castaños',
            'state_id' => '5',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Cuatro Ciénegas',
            'state_id' => '5',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Escobedo',
            'state_id' => '5',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Francisco I. Madero',
            'state_id' => '5',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Frontera',
            'state_id' => '5',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'General Cepeda',
            'state_id' => '5',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Guerrero',
            'state_id' => '5',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Hidalgo',
            'state_id' => '5',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Jiménez',
            'state_id' => '5',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Juárez',
            'state_id' => '5',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Lamadrid',
            'state_id' => '5',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Matamoros',
            'state_id' => '5',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Monclova',
            'state_id' => '5',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Morelos',
            'state_id' => '5',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Múzquiz',
            'state_id' => '5',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Nadadores',
            'state_id' => '5',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Nava',
            'state_id' => '5',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '5',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Parras',
            'state_id' => '5',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Piedras Negras',
            'state_id' => '5',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Progreso',
            'state_id' => '5',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Ramos Arizpe',
            'state_id' => '5',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Sabinas',
            'state_id' => '5',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Sacramento',
            'state_id' => '5',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Saltillo',
            'state_id' => '5',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'San Buenaventura',
            'state_id' => '5',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'San Juan de Sabinas',
            'state_id' => '5',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'San Pedro',
            'state_id' => '5',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Sierra Mojada',
            'state_id' => '5',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Torreón',
            'state_id' => '5',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Viesca',
            'state_id' => '5',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Villa Unión',
            'state_id' => '5',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Zaragoza',
            'state_id' => '5',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Armería',
            'state_id' => '6',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Colima',
            'state_id' => '6',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Comala',
            'state_id' => '6',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Coquimatlán',
            'state_id' => '6',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Cuauhtémoc',
            'state_id' => '6',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Ixtlahuacán',
            'state_id' => '6',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Manzanillo',
            'state_id' => '6',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Minatitlán',
            'state_id' => '6',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Tecomán',
            'state_id' => '6',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Villa de Álvarez',
            'state_id' => '6',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Acacoyagua',
            'state_id' => '7',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acala',
            'state_id' => '7',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Acapetahua',
            'state_id' => '7',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Altamirano',
            'state_id' => '7',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Amatán',
            'state_id' => '7',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Amatenango de la Frontera',
            'state_id' => '7',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Amatenango del Valle',
            'state_id' => '7',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Angel Albino Corzo',
            'state_id' => '7',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Arriaga',
            'state_id' => '7',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Bejucal de Ocampo',
            'state_id' => '7',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Bella Vista',
            'state_id' => '7',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Berriozábal',
            'state_id' => '7',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Bochil',
            'state_id' => '7',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'El Bosque',
            'state_id' => '7',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Cacahoatán',
            'state_id' => '7',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Catazajá',
            'state_id' => '7',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Cintalapa',
            'state_id' => '7',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Coapilla',
            'state_id' => '7',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Comitán de Domínguez',
            'state_id' => '7',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'La Concordia',
            'state_id' => '7',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Copainalá',
            'state_id' => '7',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Chalchihuitán',
            'state_id' => '7',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Chamula',
            'state_id' => '7',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Chanal',
            'state_id' => '7',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Chapultenango',
            'state_id' => '7',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Chenalhó',
            'state_id' => '7',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Chiapa de Corzo',
            'state_id' => '7',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Chiapilla',
            'state_id' => '7',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Chicoasén',
            'state_id' => '7',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Chicomuselo',
            'state_id' => '7',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Chilón',
            'state_id' => '7',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Escuintla',
            'state_id' => '7',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Francisco León',
            'state_id' => '7',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Frontera Comalapa',
            'state_id' => '7',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Frontera Hidalgo',
            'state_id' => '7',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'La Grandeza',
            'state_id' => '7',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Huehuetán',
            'state_id' => '7',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Huixtán',
            'state_id' => '7',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Huitiupán',
            'state_id' => '7',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Huixtla',
            'state_id' => '7',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'La Independencia',
            'state_id' => '7',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Ixhuatán',
            'state_id' => '7',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Ixtacomitán',
            'state_id' => '7',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Ixtapa',
            'state_id' => '7',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Ixtapangajoya',
            'state_id' => '7',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Jiquipilas',
            'state_id' => '7',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Jitotol',
            'state_id' => '7',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Juárez',
            'state_id' => '7',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Larráinzar',
            'state_id' => '7',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'La Libertad',
            'state_id' => '7',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Mapastepec',
            'state_id' => '7',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Las Margaritas',
            'state_id' => '7',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Mazapa de Madero',
            'state_id' => '7',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Mazatán',
            'state_id' => '7',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Metapa',
            'state_id' => '7',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Mitontic',
            'state_id' => '7',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Motozintla',
            'state_id' => '7',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Nicolás Ruíz',
            'state_id' => '7',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Ocosingo',
            'state_id' => '7',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Ocotepec',
            'state_id' => '7',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Ocozocoautla de Espinosa',
            'state_id' => '7',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Ostuacán',
            'state_id' => '7',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Osumacinta',
            'state_id' => '7',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Oxchuc',
            'state_id' => '7',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Palenque',
            'state_id' => '7',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Pantelhó',
            'state_id' => '7',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Pantepec',
            'state_id' => '7',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Pichucalco',
            'state_id' => '7',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Pijijiapan',
            'state_id' => '7',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'El Porvenir',
            'state_id' => '7',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Villa Comaltitlán',
            'state_id' => '7',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Pueblo Nuevo Solistahuacán',
            'state_id' => '7',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Rayón',
            'state_id' => '7',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Reforma',
            'state_id' => '7',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Las Rosas',
            'state_id' => '7',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Sabanilla',
            'state_id' => '7',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Salto de Agua',
            'state_id' => '7',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'San Cristóbal de las Casas',
            'state_id' => '7',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'San Fernando',
            'state_id' => '7',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Siltepec',
            'state_id' => '7',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Simojovel',
            'state_id' => '7',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Sitalá',
            'state_id' => '7',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Socoltenango',
            'state_id' => '7',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Solosuchiapa',
            'state_id' => '7',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Soyaló',
            'state_id' => '7',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Suchiapa',
            'state_id' => '7',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Suchiate',
            'state_id' => '7',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Sunuapa',
            'state_id' => '7',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Tapachula',
            'state_id' => '7',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Tapalapa',
            'state_id' => '7',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Tapilula',
            'state_id' => '7',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Tecpatán',
            'state_id' => '7',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Tenejapa',
            'state_id' => '7',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Teopisca',
            'state_id' => '7',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Tila',
            'state_id' => '7',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Tonalá',
            'state_id' => '7',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Totolapa',
            'state_id' => '7',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'La Trinitaria',
            'state_id' => '7',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Tumbalá',
            'state_id' => '7',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Tuxtla Gutiérrez',
            'state_id' => '7',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Tuxtla Chico',
            'state_id' => '7',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Tuzantán',
            'state_id' => '7',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Tzimol',
            'state_id' => '7',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Unión Juárez',
            'state_id' => '7',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Venustiano Carranza',
            'state_id' => '7',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Villa Corzo',
            'state_id' => '7',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Villaflores',
            'state_id' => '7',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Yajalón',
            'state_id' => '7',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'San Lucas',
            'state_id' => '7',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Zinacantán',
            'state_id' => '7',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'San Juan Cancuc',
            'state_id' => '7',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'Aldama',
            'state_id' => '7',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Benemérito de las Américas',
            'state_id' => '7',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'Maravilla Tenejapa',
            'state_id' => '7',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'Marqués de Comillas',
            'state_id' => '7',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'Montecristo de Guerrero',
            'state_id' => '7',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'San Andrés Duraznal',
            'state_id' => '7',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'Santiago el Pinar',
            'state_id' => '7',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'Ahumada',
            'state_id' => '8',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Aldama',
            'state_id' => '8',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Allende',
            'state_id' => '8',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Aquiles Serdán',
            'state_id' => '8',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Ascensión',
            'state_id' => '8',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Bachíniva',
            'state_id' => '8',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Balleza',
            'state_id' => '8',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Batopilas',
            'state_id' => '8',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Bocoyna',
            'state_id' => '8',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Buenaventura',
            'state_id' => '8',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Camargo',
            'state_id' => '8',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Carichí',
            'state_id' => '8',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Casas Grandes',
            'state_id' => '8',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Coronado',
            'state_id' => '8',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Coyame del Sotol',
            'state_id' => '8',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'La Cruz',
            'state_id' => '8',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Cuauhtémoc',
            'state_id' => '8',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Cusihuiriachi',
            'state_id' => '8',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Chihuahua',
            'state_id' => '8',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Chínipas',
            'state_id' => '8',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Delicias',
            'state_id' => '8',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Dr. Belisario Domínguez',
            'state_id' => '8',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Galeana',
            'state_id' => '8',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Santa Isabel',
            'state_id' => '8',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Gómez Farías',
            'state_id' => '8',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Gran Morelos',
            'state_id' => '8',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Guachochi',
            'state_id' => '8',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Guadalupe',
            'state_id' => '8',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Guadalupe y Calvo',
            'state_id' => '8',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Guazapares',
            'state_id' => '8',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Guerrero',
            'state_id' => '8',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Hidalgo del Parral',
            'state_id' => '8',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Huejotitán',
            'state_id' => '8',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Ignacio Zaragoza',
            'state_id' => '8',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Janos',
            'state_id' => '8',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Jiménez',
            'state_id' => '8',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Juárez',
            'state_id' => '8',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Julimes',
            'state_id' => '8',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'López',
            'state_id' => '8',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Madera',
            'state_id' => '8',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Maguarichi',
            'state_id' => '8',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Manuel Benavides',
            'state_id' => '8',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Matachí',
            'state_id' => '8',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Matamoros',
            'state_id' => '8',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Meoqui',
            'state_id' => '8',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Morelos',
            'state_id' => '8',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Moris',
            'state_id' => '8',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Namiquipa',
            'state_id' => '8',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Nonoava',
            'state_id' => '8',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Nuevo Casas Grandes',
            'state_id' => '8',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '8',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Ojinaga',
            'state_id' => '8',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Praxedis G. Guerrero',
            'state_id' => '8',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Riva Palacio',
            'state_id' => '8',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Rosales',
            'state_id' => '8',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Rosario',
            'state_id' => '8',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'San Francisco de Borja',
            'state_id' => '8',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'San Francisco de Conchos',
            'state_id' => '8',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'San Francisco del Oro',
            'state_id' => '8',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Santa Bárbara',
            'state_id' => '8',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Satevó',
            'state_id' => '8',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Saucillo',
            'state_id' => '8',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Temósachic',
            'state_id' => '8',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'El Tule',
            'state_id' => '8',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Urique',
            'state_id' => '8',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Uruachi',
            'state_id' => '8',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Valle de Zaragoza',
            'state_id' => '8',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Azcapotzalco',
            'state_id' => '9',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Coyoacán',
            'state_id' => '9',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Cuajimalpa de Morelos',
            'state_id' => '9',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Gustavo A. Madero',
            'state_id' => '9',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Iztacalco',
            'state_id' => '9',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Iztapalapa',
            'state_id' => '9',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'La Magdalena Contreras',
            'state_id' => '9',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Milpa Alta',
            'state_id' => '9',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Álvaro Obregón',
            'state_id' => '9',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Tláhuac',
            'state_id' => '9',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Tlalpan',
            'state_id' => '9',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Xochimilco',
            'state_id' => '9',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '9',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Cuauhtémoc',
            'state_id' => '9',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Miguel Hidalgo',
            'state_id' => '9',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Venustiano Carranza',
            'state_id' => '9',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Canatlán',
            'state_id' => '10',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Canelas',
            'state_id' => '10',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Coneto de Comonfort',
            'state_id' => '10',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Cuencamé',
            'state_id' => '10',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Durango',
            'state_id' => '10',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'General Simón Bolívar',
            'state_id' => '10',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Gómez Palacio',
            'state_id' => '10',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Guadalupe Victoria',
            'state_id' => '10',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Guanaceví',
            'state_id' => '10',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Hidalgo',
            'state_id' => '10',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Indé',
            'state_id' => '10',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Lerdo',
            'state_id' => '10',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Mapimí',
            'state_id' => '10',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Mezquital',
            'state_id' => '10',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Nazas',
            'state_id' => '10',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Nombre de Dios',
            'state_id' => '10',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '10',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'El Oro',
            'state_id' => '10',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Otáez',
            'state_id' => '10',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Pánuco de Coronado',
            'state_id' => '10',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Peñón Blanco',
            'state_id' => '10',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Poanas',
            'state_id' => '10',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Pueblo Nuevo',
            'state_id' => '10',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Rodeo',
            'state_id' => '10',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'San Bernardo',
            'state_id' => '10',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'San Dimas',
            'state_id' => '10',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'San Juan de Guadalupe',
            'state_id' => '10',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'San Juan del Río',
            'state_id' => '10',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'San Luis del Cordero',
            'state_id' => '10',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'San Pedro del Gallo',
            'state_id' => '10',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Santa Clara',
            'state_id' => '10',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Santiago Papasquiaro',
            'state_id' => '10',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Súchil',
            'state_id' => '10',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Tamazula',
            'state_id' => '10',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Tepehuanes',
            'state_id' => '10',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Tlahualilo',
            'state_id' => '10',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Topia',
            'state_id' => '10',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Vicente Guerrero',
            'state_id' => '10',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Nuevo Ideal',
            'state_id' => '10',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Abasolo',
            'state_id' => '11',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acámbaro',
            'state_id' => '11',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'San Miguel de Allende',
            'state_id' => '11',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Apaseo el Alto',
            'state_id' => '11',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Apaseo el Grande',
            'state_id' => '11',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Atarjea',
            'state_id' => '11',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Celaya',
            'state_id' => '11',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Manuel Doblado',
            'state_id' => '11',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Comonfort',
            'state_id' => '11',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Coroneo',
            'state_id' => '11',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Cortazar',
            'state_id' => '11',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Cuerámaro',
            'state_id' => '11',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Doctor Mora',
            'state_id' => '11',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Dolores Hidalgo Cuna de la Independencia Nacional',
            'state_id' => '11',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Guanajuato',
            'state_id' => '11',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Huanímaro',
            'state_id' => '11',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Irapuato',
            'state_id' => '11',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Jaral del Progreso',
            'state_id' => '11',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Jerécuaro',
            'state_id' => '11',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'León',
            'state_id' => '11',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Moroleón',
            'state_id' => '11',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '11',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Pénjamo',
            'state_id' => '11',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Pueblo Nuevo',
            'state_id' => '11',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Purísima del Rincón',
            'state_id' => '11',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Romita',
            'state_id' => '11',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Salamanca',
            'state_id' => '11',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Salvatierra',
            'state_id' => '11',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'San Diego de la Unión',
            'state_id' => '11',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'San Felipe',
            'state_id' => '11',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'San Francisco del Rincón',
            'state_id' => '11',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'San José Iturbide',
            'state_id' => '11',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'San Luis de la Paz',
            'state_id' => '11',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Santa Catarina',
            'state_id' => '11',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Santa Cruz de Juventino Rosas',
            'state_id' => '11',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Santiago Maravatío',
            'state_id' => '11',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Silao de la Victoria',
            'state_id' => '11',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Tarandacuao',
            'state_id' => '11',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Tarimoro',
            'state_id' => '11',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Tierra Blanca',
            'state_id' => '11',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Uriangato',
            'state_id' => '11',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Valle de Santiago',
            'state_id' => '11',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Victoria',
            'state_id' => '11',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Villagrán',
            'state_id' => '11',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Xichú',
            'state_id' => '11',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Yuriria',
            'state_id' => '11',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Acapulco de Juárez',
            'state_id' => '12',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Ahuacuotzingo',
            'state_id' => '12',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Ajuchitlán del Progreso',
            'state_id' => '12',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Alcozauca de Guerrero',
            'state_id' => '12',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Alpoyeca',
            'state_id' => '12',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Apaxtla',
            'state_id' => '12',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Arcelia',
            'state_id' => '12',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Atenango del Río',
            'state_id' => '12',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Atlamajalcingo del Monte',
            'state_id' => '12',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Atlixtac',
            'state_id' => '12',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Atoyac de Álvarez',
            'state_id' => '12',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Ayutla de los Libres',
            'state_id' => '12',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Azoyú',
            'state_id' => '12',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '12',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Buenavista de Cuéllar',
            'state_id' => '12',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Coahuayutla de José María Izazaga',
            'state_id' => '12',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Cocula',
            'state_id' => '12',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Copala',
            'state_id' => '12',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Copalillo',
            'state_id' => '12',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Copanatoyac',
            'state_id' => '12',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Coyuca de Benítez',
            'state_id' => '12',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Coyuca de Catalán',
            'state_id' => '12',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Cuajinicuilapa',
            'state_id' => '12',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Cualác',
            'state_id' => '12',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Cuautepec',
            'state_id' => '12',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Cuetzala del Progreso',
            'state_id' => '12',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Cutzamala de Pinzón',
            'state_id' => '12',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Chilapa de Álvarez',
            'state_id' => '12',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Chilpancingo de los Bravo',
            'state_id' => '12',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Florencio Villarreal',
            'state_id' => '12',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'General Canuto A. Neri',
            'state_id' => '12',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'General Heliodoro Castillo',
            'state_id' => '12',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Huamuxtitlán',
            'state_id' => '12',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Huitzuco de los Figueroa',
            'state_id' => '12',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Iguala de la Independencia',
            'state_id' => '12',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Igualapa',
            'state_id' => '12',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Ixcateopan de Cuauhtémoc',
            'state_id' => '12',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Zihuatanejo de Azueta',
            'state_id' => '12',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Juan R. Escudero',
            'state_id' => '12',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Leonardo Bravo',
            'state_id' => '12',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Malinaltepec',
            'state_id' => '12',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Mártir de Cuilapan',
            'state_id' => '12',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Metlatónoc',
            'state_id' => '12',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Mochitlán',
            'state_id' => '12',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Olinalá',
            'state_id' => '12',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Ometepec',
            'state_id' => '12',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Pedro Ascencio Alquisiras',
            'state_id' => '12',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Petatlán',
            'state_id' => '12',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Pilcaya',
            'state_id' => '12',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Pungarabato',
            'state_id' => '12',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Quechultenango',
            'state_id' => '12',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'San Luis Acatlán',
            'state_id' => '12',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'San Marcos',
            'state_id' => '12',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'San Miguel Totolapan',
            'state_id' => '12',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Taxco de Alarcón',
            'state_id' => '12',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Tecoanapa',
            'state_id' => '12',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Técpan de Galeana',
            'state_id' => '12',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Teloloapan',
            'state_id' => '12',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Tepecoacuilco de Trujano',
            'state_id' => '12',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Tetipac',
            'state_id' => '12',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Tixtla de Guerrero',
            'state_id' => '12',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Tlacoachistlahuaca',
            'state_id' => '12',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Tlacoapa',
            'state_id' => '12',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Tlalchapa',
            'state_id' => '12',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Tlalixtaquilla de Maldonado',
            'state_id' => '12',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Tlapa de Comonfort',
            'state_id' => '12',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Tlapehuala',
            'state_id' => '12',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'La Unión de Isidoro Montes de Oca',
            'state_id' => '12',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Xalpatláhuac',
            'state_id' => '12',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Xochihuehuetlán',
            'state_id' => '12',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Xochistlahuaca',
            'state_id' => '12',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Zapotitlán Tablas',
            'state_id' => '12',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Zirándaro',
            'state_id' => '12',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Zitlala',
            'state_id' => '12',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Eduardo Neri',
            'state_id' => '12',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Acatepec',
            'state_id' => '12',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Marquelia',
            'state_id' => '12',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Cochoapa el Grande',
            'state_id' => '12',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'José Joaquín de Herrera',
            'state_id' => '12',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Juchitán',
            'state_id' => '12',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Iliatenco',
            'state_id' => '12',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Acatlán',
            'state_id' => '13',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acaxochitlán',
            'state_id' => '13',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Actopan',
            'state_id' => '13',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Agua Blanca de Iturbide',
            'state_id' => '13',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Ajacuba',
            'state_id' => '13',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Alfajayucan',
            'state_id' => '13',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Almoloya',
            'state_id' => '13',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Apan',
            'state_id' => '13',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'El Arenal',
            'state_id' => '13',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Atitalaquia',
            'state_id' => '13',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Atlapexco',
            'state_id' => '13',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Atotonilco el Grande',
            'state_id' => '13',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Atotonilco de Tula',
            'state_id' => '13',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Calnali',
            'state_id' => '13',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Cardonal',
            'state_id' => '13',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Cuautepec de Hinojosa',
            'state_id' => '13',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Chapantongo',
            'state_id' => '13',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Chapulhuacán',
            'state_id' => '13',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Chilcuautla',
            'state_id' => '13',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Eloxochitlán',
            'state_id' => '13',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Emiliano Zapata',
            'state_id' => '13',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Epazoyucan',
            'state_id' => '13',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Francisco I. Madero',
            'state_id' => '13',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Huasca de Ocampo',
            'state_id' => '13',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Huautla',
            'state_id' => '13',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Huazalingo',
            'state_id' => '13',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Huehuetla',
            'state_id' => '13',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Huejutla de Reyes',
            'state_id' => '13',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Huichapan',
            'state_id' => '13',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Ixmiquilpan',
            'state_id' => '13',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Jacala de Ledezma',
            'state_id' => '13',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Jaltocán',
            'state_id' => '13',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Juárez Hidalgo',
            'state_id' => '13',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Lolotla',
            'state_id' => '13',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Metepec',
            'state_id' => '13',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'San Agustín Metzquititlán',
            'state_id' => '13',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Metztitlán',
            'state_id' => '13',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Mineral del Chico',
            'state_id' => '13',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Mineral del Monte',
            'state_id' => '13',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'La Misión',
            'state_id' => '13',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Mixquiahuala de Juárez',
            'state_id' => '13',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Molango de Escamilla',
            'state_id' => '13',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Nicolás Flores',
            'state_id' => '13',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Nopala de Villagrán',
            'state_id' => '13',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Omitlán de Juárez',
            'state_id' => '13',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'San Felipe Orizatlán',
            'state_id' => '13',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Pacula',
            'state_id' => '13',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Pachuca de Soto',
            'state_id' => '13',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Pisaflores',
            'state_id' => '13',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Progreso de Obregón',
            'state_id' => '13',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Mineral de la Reforma',
            'state_id' => '13',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'San Agustín Tlaxiaca',
            'state_id' => '13',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'San Bartolo Tutotepec',
            'state_id' => '13',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'San Salvador',
            'state_id' => '13',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Santiago de Anaya',
            'state_id' => '13',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Santiago Tulantepec de Lugo Guerrero',
            'state_id' => '13',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Singuilucan',
            'state_id' => '13',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Tasquillo',
            'state_id' => '13',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Tecozautla',
            'state_id' => '13',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Tenango de Doria',
            'state_id' => '13',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Tepeapulco',
            'state_id' => '13',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Tepehuacán de Guerrero',
            'state_id' => '13',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Tepeji del Río de Ocampo',
            'state_id' => '13',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Tepetitlán',
            'state_id' => '13',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Tetepango',
            'state_id' => '13',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Villa de Tezontepec',
            'state_id' => '13',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Tezontepec de Aldama',
            'state_id' => '13',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Tianguistengo',
            'state_id' => '13',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Tizayuca',
            'state_id' => '13',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Tlahuelilpan',
            'state_id' => '13',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Tlahuiltepa',
            'state_id' => '13',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Tlanalapa',
            'state_id' => '13',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Tlanchinol',
            'state_id' => '13',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Tlaxcoapan',
            'state_id' => '13',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Tolcayuca',
            'state_id' => '13',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Tula de Allende',
            'state_id' => '13',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Tulancingo de Bravo',
            'state_id' => '13',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Xochiatipan',
            'state_id' => '13',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Xochicoatlán',
            'state_id' => '13',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Yahualica',
            'state_id' => '13',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Zacualtipán de Ángeles',
            'state_id' => '13',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Zapotlán de Juárez',
            'state_id' => '13',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Zempoala',
            'state_id' => '13',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Zimapán',
            'state_id' => '13',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Acatic',
            'state_id' => '14',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acatlán de Juárez',
            'state_id' => '14',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Ahualulco de Mercado',
            'state_id' => '14',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Amacueca',
            'state_id' => '14',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Amatitán',
            'state_id' => '14',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Ameca',
            'state_id' => '14',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'San Juanito de Escobedo',
            'state_id' => '14',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Arandas',
            'state_id' => '14',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'El Arenal',
            'state_id' => '14',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Atemajac de Brizuela',
            'state_id' => '14',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Atengo',
            'state_id' => '14',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Atenguillo',
            'state_id' => '14',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Atotonilco el Alto',
            'state_id' => '14',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Atoyac',
            'state_id' => '14',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Autlán de Navarro',
            'state_id' => '14',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Ayotlán',
            'state_id' => '14',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Ayutla',
            'state_id' => '14',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'La Barca',
            'state_id' => '14',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Bolaños',
            'state_id' => '14',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Cabo Corrientes',
            'state_id' => '14',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Casimiro Castillo',
            'state_id' => '14',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Cihuatlán',
            'state_id' => '14',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Zapotlán el Grande',
            'state_id' => '14',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Cocula',
            'state_id' => '14',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Colotlán',
            'state_id' => '14',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Concepción de Buenos Aires',
            'state_id' => '14',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Cuautitlán de García Barragán',
            'state_id' => '14',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Cuautla',
            'state_id' => '14',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Cuquío',
            'state_id' => '14',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Chapala',
            'state_id' => '14',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Chimaltitán',
            'state_id' => '14',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Chiquilistlán',
            'state_id' => '14',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Degollado',
            'state_id' => '14',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Ejutla',
            'state_id' => '14',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Encarnación de Díaz',
            'state_id' => '14',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Etzatlán',
            'state_id' => '14',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'El Grullo',
            'state_id' => '14',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Guachinango',
            'state_id' => '14',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Guadalajara',
            'state_id' => '14',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Hostotipaquillo',
            'state_id' => '14',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Huejúcar',
            'state_id' => '14',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Huejuquilla el Alto',
            'state_id' => '14',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'La Huerta',
            'state_id' => '14',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Ixtlahuacán de los Membrillos',
            'state_id' => '14',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Ixtlahuacán del Río',
            'state_id' => '14',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Jalostotitlán',
            'state_id' => '14',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Jamay',
            'state_id' => '14',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Jesús María',
            'state_id' => '14',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Jilotlán de los Dolores',
            'state_id' => '14',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Jocotepec',
            'state_id' => '14',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Juanacatlán',
            'state_id' => '14',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Juchitlán',
            'state_id' => '14',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Lagos de Moreno',
            'state_id' => '14',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'El Limón',
            'state_id' => '14',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Magdalena',
            'state_id' => '14',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Santa María del Oro',
            'state_id' => '14',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'La Manzanilla de la Paz',
            'state_id' => '14',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Mascota',
            'state_id' => '14',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Mazamitla',
            'state_id' => '14',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Mexticacán',
            'state_id' => '14',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Mezquitic',
            'state_id' => '14',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Mixtlán',
            'state_id' => '14',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Ocotlán',
            'state_id' => '14',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Ojuelos de Jalisco',
            'state_id' => '14',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Pihuamo',
            'state_id' => '14',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Poncitlán',
            'state_id' => '14',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Puerto Vallarta',
            'state_id' => '14',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Villa Purificación',
            'state_id' => '14',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Quitupan',
            'state_id' => '14',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'El Salto',
            'state_id' => '14',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'San Cristóbal de la Barranca',
            'state_id' => '14',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'San Diego de Alejandría',
            'state_id' => '14',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'San Juan de los Lagos',
            'state_id' => '14',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'San Julián',
            'state_id' => '14',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'San Marcos',
            'state_id' => '14',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'San Martín de Bolaños',
            'state_id' => '14',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'San Martín Hidalgo',
            'state_id' => '14',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'San Miguel el Alto',
            'state_id' => '14',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Gómez Farías',
            'state_id' => '14',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'San Sebastián del Oeste',
            'state_id' => '14',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Santa María de los Ángeles',
            'state_id' => '14',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Sayula',
            'state_id' => '14',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Tala',
            'state_id' => '14',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Talpa de Allende',
            'state_id' => '14',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Tamazula de Gordiano',
            'state_id' => '14',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Tapalpa',
            'state_id' => '14',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Tecalitlán',
            'state_id' => '14',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Tecolotlán',
            'state_id' => '14',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Techaluta de Montenegro',
            'state_id' => '14',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Tenamaxtlán',
            'state_id' => '14',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Teocaltiche',
            'state_id' => '14',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Teocuitatlán de Corona',
            'state_id' => '14',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Tepatitlán de Morelos',
            'state_id' => '14',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Tequila',
            'state_id' => '14',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Teuchitlán',
            'state_id' => '14',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Tizapán el Alto',
            'state_id' => '14',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Tlajomulco de Zúñiga',
            'state_id' => '14',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'San Pedro Tlaquepaque',
            'state_id' => '14',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Tolimán',
            'state_id' => '14',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Tomatlán',
            'state_id' => '14',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Tonalá',
            'state_id' => '14',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Tonaya',
            'state_id' => '14',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Tonila',
            'state_id' => '14',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Totatiche',
            'state_id' => '14',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Tototlán',
            'state_id' => '14',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Tuxcacuesco',
            'state_id' => '14',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Tuxcueca',
            'state_id' => '14',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Tuxpan',
            'state_id' => '14',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Unión de San Antonio',
            'state_id' => '14',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'Unión de Tula',
            'state_id' => '14',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Valle de Guadalupe',
            'state_id' => '14',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'Valle de Juárez',
            'state_id' => '14',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'San Gabriel',
            'state_id' => '14',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Villa Corona',
            'state_id' => '14',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'Villa Guerrero',
            'state_id' => '14',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'Villa Hidalgo',
            'state_id' => '14',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'Cañadas de Obregón',
            'state_id' => '14',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'Yahualica de González Gallo',
            'state_id' => '14',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'Zacoalco de Torres',
            'state_id' => '14',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'Zapopan',
            'state_id' => '14',
            'number' =>  '120',
        ]);
        City::create([
            'name' =>  'Zapotiltic',
            'state_id' => '14',
            'number' =>  '121',
        ]);
        City::create([
            'name' =>  'Zapotitlán de Vadillo',
            'state_id' => '14',
            'number' =>  '122',
        ]);
        City::create([
            'name' =>  'Zapotlán del Rey',
            'state_id' => '14',
            'number' =>  '123',
        ]);
        City::create([
            'name' =>  'Zapotlanejo',
            'state_id' => '14',
            'number' =>  '124',
        ]);
        City::create([
            'name' =>  'San Ignacio Cerro Gordo',
            'state_id' => '14',
            'number' =>  '125',
        ]);
        City::create([
            'name' =>  'Acambay de Ruíz Castañeda',
            'state_id' => '15',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acolman',
            'state_id' => '15',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Aculco',
            'state_id' => '15',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Almoloya de Alquisiras',
            'state_id' => '15',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Almoloya de Juárez',
            'state_id' => '15',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Almoloya del Río',
            'state_id' => '15',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Amanalco',
            'state_id' => '15',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Amatepec',
            'state_id' => '15',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Amecameca',
            'state_id' => '15',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Apaxco',
            'state_id' => '15',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Atenco',
            'state_id' => '15',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Atizapán',
            'state_id' => '15',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Atizapán de Zaragoza',
            'state_id' => '15',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Atlacomulco',
            'state_id' => '15',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Atlautla',
            'state_id' => '15',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Axapusco',
            'state_id' => '15',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Ayapango',
            'state_id' => '15',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Calimaya',
            'state_id' => '15',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Capulhuac',
            'state_id' => '15',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Coacalco de Berriozábal',
            'state_id' => '15',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Coatepec Harinas',
            'state_id' => '15',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Cocotitlán',
            'state_id' => '15',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Coyotepec',
            'state_id' => '15',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Cuautitlán',
            'state_id' => '15',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Chalco',
            'state_id' => '15',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Chapa de Mota',
            'state_id' => '15',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Chapultepec',
            'state_id' => '15',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Chiautla',
            'state_id' => '15',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Chicoloapan',
            'state_id' => '15',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Chiconcuac',
            'state_id' => '15',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Chimalhuacán',
            'state_id' => '15',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Donato Guerra',
            'state_id' => '15',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Ecatepec de Morelos',
            'state_id' => '15',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Ecatzingo',
            'state_id' => '15',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Huehuetoca',
            'state_id' => '15',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Hueypoxtla',
            'state_id' => '15',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Huixquilucan',
            'state_id' => '15',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Isidro Fabela',
            'state_id' => '15',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Ixtapaluca',
            'state_id' => '15',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Ixtapan de la Sal',
            'state_id' => '15',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Ixtapan del Oro',
            'state_id' => '15',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Ixtlahuaca',
            'state_id' => '15',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Xalatlaco',
            'state_id' => '15',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Jaltenco',
            'state_id' => '15',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Jilotepec',
            'state_id' => '15',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Jilotzingo',
            'state_id' => '15',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Jiquipilco',
            'state_id' => '15',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Jocotitlán',
            'state_id' => '15',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Joquicingo',
            'state_id' => '15',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Juchitepec',
            'state_id' => '15',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Lerma',
            'state_id' => '15',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Malinalco',
            'state_id' => '15',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Melchor Ocampo',
            'state_id' => '15',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Metepec',
            'state_id' => '15',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Mexicaltzingo',
            'state_id' => '15',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Morelos',
            'state_id' => '15',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Naucalpan de Juárez',
            'state_id' => '15',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Nezahualcóyotl',
            'state_id' => '15',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Nextlalpan',
            'state_id' => '15',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Nicolás Romero',
            'state_id' => '15',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Nopaltepec',
            'state_id' => '15',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Ocoyoacac',
            'state_id' => '15',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Ocuilan',
            'state_id' => '15',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'El Oro',
            'state_id' => '15',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Otumba',
            'state_id' => '15',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Otzoloapan',
            'state_id' => '15',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Otzolotepec',
            'state_id' => '15',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Ozumba',
            'state_id' => '15',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Papalotla',
            'state_id' => '15',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'La Paz',
            'state_id' => '15',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Polotitlán',
            'state_id' => '15',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Rayón',
            'state_id' => '15',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'San Antonio la Isla',
            'state_id' => '15',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'San Felipe del Progreso',
            'state_id' => '15',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'San Martín de las Pirámides',
            'state_id' => '15',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'San Mateo Atenco',
            'state_id' => '15',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'San Simón de Guerrero',
            'state_id' => '15',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Santo Tomás',
            'state_id' => '15',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Soyaniquilpan de Juárez',
            'state_id' => '15',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Sultepec',
            'state_id' => '15',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Tecámac',
            'state_id' => '15',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Tejupilco',
            'state_id' => '15',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Temamatla',
            'state_id' => '15',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Temascalapa',
            'state_id' => '15',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Temascalcingo',
            'state_id' => '15',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Temascaltepec',
            'state_id' => '15',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Temoaya',
            'state_id' => '15',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Tenancingo',
            'state_id' => '15',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Tenango del Aire',
            'state_id' => '15',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Tenango del Valle',
            'state_id' => '15',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Teoloyucan',
            'state_id' => '15',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Teotihuacán',
            'state_id' => '15',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Tepetlaoxtoc',
            'state_id' => '15',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Tepetlixpa',
            'state_id' => '15',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Tepotzotlán',
            'state_id' => '15',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Tequixquiac',
            'state_id' => '15',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Texcaltitlán',
            'state_id' => '15',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Texcalyacac',
            'state_id' => '15',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Texcoco',
            'state_id' => '15',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Tezoyuca',
            'state_id' => '15',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Tianguistenco',
            'state_id' => '15',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Timilpan',
            'state_id' => '15',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Tlalmanalco',
            'state_id' => '15',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Tlalnepantla de Baz',
            'state_id' => '15',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Tlatlaya',
            'state_id' => '15',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Toluca',
            'state_id' => '15',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Tonatico',
            'state_id' => '15',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Tultepec',
            'state_id' => '15',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Tultitlán',
            'state_id' => '15',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'Valle de Bravo',
            'state_id' => '15',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Villa de Allende',
            'state_id' => '15',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'Villa del Carbón',
            'state_id' => '15',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'Villa Guerrero',
            'state_id' => '15',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Villa Victoria',
            'state_id' => '15',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'Xonacatlán',
            'state_id' => '15',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'Zacazonapan',
            'state_id' => '15',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'Zacualpan',
            'state_id' => '15',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'Zinacantepec',
            'state_id' => '15',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'Zumpahuacán',
            'state_id' => '15',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'Zumpango',
            'state_id' => '15',
            'number' =>  '120',
        ]);
        City::create([
            'name' =>  'Cuautitlán Izcalli',
            'state_id' => '15',
            'number' =>  '121',
        ]);
        City::create([
            'name' =>  'Valle de Chalco Solidaridad',
            'state_id' => '15',
            'number' =>  '122',
        ]);
        City::create([
            'name' =>  'Luvianos',
            'state_id' => '15',
            'number' =>  '123',
        ]);
        City::create([
            'name' =>  'San José del Rincón',
            'state_id' => '15',
            'number' =>  '124',
        ]);
        City::create([
            'name' =>  'Tonanitla',
            'state_id' => '15',
            'number' =>  '125',
        ]);
        City::create([
            'name' =>  'Acuitzio',
            'state_id' => '16',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Aguililla',
            'state_id' => '16',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Álvaro Obregón',
            'state_id' => '16',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Angamacutiro',
            'state_id' => '16',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Angangueo',
            'state_id' => '16',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Apatzingán',
            'state_id' => '16',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Aporo',
            'state_id' => '16',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Aquila',
            'state_id' => '16',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Ario',
            'state_id' => '16',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Arteaga',
            'state_id' => '16',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Briseñas',
            'state_id' => '16',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Buenavista',
            'state_id' => '16',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Carácuaro',
            'state_id' => '16',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Coahuayana',
            'state_id' => '16',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Coalcomán de Vázquez Pallares',
            'state_id' => '16',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Coeneo',
            'state_id' => '16',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Contepec',
            'state_id' => '16',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Copándaro',
            'state_id' => '16',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Cotija',
            'state_id' => '16',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Cuitzeo',
            'state_id' => '16',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Charapan',
            'state_id' => '16',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Charo',
            'state_id' => '16',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Chavinda',
            'state_id' => '16',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Cherán',
            'state_id' => '16',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Chilchota',
            'state_id' => '16',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Chinicuila',
            'state_id' => '16',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Chucándiro',
            'state_id' => '16',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Churintzio',
            'state_id' => '16',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Churumuco',
            'state_id' => '16',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Ecuandureo',
            'state_id' => '16',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Epitacio Huerta',
            'state_id' => '16',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Erongarícuaro',
            'state_id' => '16',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Gabriel Zamora',
            'state_id' => '16',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Hidalgo',
            'state_id' => '16',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'La Huacana',
            'state_id' => '16',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Huandacareo',
            'state_id' => '16',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Huaniqueo',
            'state_id' => '16',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Huetamo',
            'state_id' => '16',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Huiramba',
            'state_id' => '16',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Indaparapeo',
            'state_id' => '16',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Irimbo',
            'state_id' => '16',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Ixtlán',
            'state_id' => '16',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Jacona',
            'state_id' => '16',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Jiménez',
            'state_id' => '16',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Jiquilpan',
            'state_id' => '16',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Juárez',
            'state_id' => '16',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Jungapeo',
            'state_id' => '16',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Lagunillas',
            'state_id' => '16',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Madero',
            'state_id' => '16',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Maravatío',
            'state_id' => '16',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Marcos Castellanos',
            'state_id' => '16',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Lázaro Cárdenas',
            'state_id' => '16',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Morelia',
            'state_id' => '16',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Morelos',
            'state_id' => '16',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Múgica',
            'state_id' => '16',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Nahuatzen',
            'state_id' => '16',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Nocupétaro',
            'state_id' => '16',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Nuevo Parangaricutiro',
            'state_id' => '16',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Nuevo Urecho',
            'state_id' => '16',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Numarán',
            'state_id' => '16',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '16',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Pajacuarán',
            'state_id' => '16',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Panindícuaro',
            'state_id' => '16',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Parácuaro',
            'state_id' => '16',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Paracho',
            'state_id' => '16',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Pátzcuaro',
            'state_id' => '16',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Penjamillo',
            'state_id' => '16',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Peribán',
            'state_id' => '16',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'La Piedad',
            'state_id' => '16',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Purépero',
            'state_id' => '16',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Puruándiro',
            'state_id' => '16',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Queréndaro',
            'state_id' => '16',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Quiroga',
            'state_id' => '16',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Cojumatlán de Régules',
            'state_id' => '16',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Los Reyes',
            'state_id' => '16',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Sahuayo',
            'state_id' => '16',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'San Lucas',
            'state_id' => '16',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Santa Ana Maya',
            'state_id' => '16',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Salvador Escalante',
            'state_id' => '16',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Senguio',
            'state_id' => '16',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Susupuato',
            'state_id' => '16',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Tacámbaro',
            'state_id' => '16',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Tancítaro',
            'state_id' => '16',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Tangamandapio',
            'state_id' => '16',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Tangancícuaro',
            'state_id' => '16',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Tanhuato',
            'state_id' => '16',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Taretan',
            'state_id' => '16',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Tarímbaro',
            'state_id' => '16',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Tepalcatepec',
            'state_id' => '16',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Tingambato',
            'state_id' => '16',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Tingüindín',
            'state_id' => '16',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Tiquicheo de Nicolás Romero',
            'state_id' => '16',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Tlalpujahua',
            'state_id' => '16',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Tlazazalca',
            'state_id' => '16',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Tocumbo',
            'state_id' => '16',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Tumbiscatío',
            'state_id' => '16',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Turicato',
            'state_id' => '16',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Tuxpan',
            'state_id' => '16',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Tuzantla',
            'state_id' => '16',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Tzintzuntzan',
            'state_id' => '16',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Tzitzio',
            'state_id' => '16',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Uruapan',
            'state_id' => '16',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Venustiano Carranza',
            'state_id' => '16',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Villamar',
            'state_id' => '16',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Vista Hermosa',
            'state_id' => '16',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Yurécuaro',
            'state_id' => '16',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Zacapu',
            'state_id' => '16',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Zamora',
            'state_id' => '16',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Zináparo',
            'state_id' => '16',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'Zinapécuaro',
            'state_id' => '16',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Ziracuaretiro',
            'state_id' => '16',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'Zitácuaro',
            'state_id' => '16',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'José Sixto Verduzco',
            'state_id' => '16',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Amacuzac',
            'state_id' => '17',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Atlatlahucan',
            'state_id' => '17',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Axochiapan',
            'state_id' => '17',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Ayala',
            'state_id' => '17',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Coatlán del Río',
            'state_id' => '17',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Cuautla',
            'state_id' => '17',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Cuernavaca',
            'state_id' => '17',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Emiliano Zapata',
            'state_id' => '17',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Huitzilac',
            'state_id' => '17',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Jantetelco',
            'state_id' => '17',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Jiutepec',
            'state_id' => '17',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Jojutla',
            'state_id' => '17',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Jonacatepec',
            'state_id' => '17',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Mazatepec',
            'state_id' => '17',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Miacatlán',
            'state_id' => '17',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Ocuituco',
            'state_id' => '17',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Puente de Ixtla',
            'state_id' => '17',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Temixco',
            'state_id' => '17',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Tepalcingo',
            'state_id' => '17',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Tepoztlán',
            'state_id' => '17',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Tetecala',
            'state_id' => '17',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Tetela del Volcán',
            'state_id' => '17',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Tlalnepantla',
            'state_id' => '17',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Tlaltizapán de Zapata',
            'state_id' => '17',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Tlaquiltenango',
            'state_id' => '17',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Tlayacapan',
            'state_id' => '17',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Totolapan',
            'state_id' => '17',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Xochitepec',
            'state_id' => '17',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Yautepec',
            'state_id' => '17',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Yecapixtla',
            'state_id' => '17',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Zacatepec',
            'state_id' => '17',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Zacualpan de Amilpas',
            'state_id' => '17',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Temoac',
            'state_id' => '17',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Acaponeta',
            'state_id' => '18',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Ahuacatlán',
            'state_id' => '18',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Amatlán de Cañas',
            'state_id' => '18',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Compostela',
            'state_id' => '18',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Huajicori',
            'state_id' => '18',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Ixtlán del Río',
            'state_id' => '18',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Jala',
            'state_id' => '18',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Xalisco',
            'state_id' => '18',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Del Nayar',
            'state_id' => '18',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Rosamorada',
            'state_id' => '18',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Ruíz',
            'state_id' => '18',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'San Blas',
            'state_id' => '18',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'San Pedro Lagunillas',
            'state_id' => '18',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Santa María del Oro',
            'state_id' => '18',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Santiago Ixcuintla',
            'state_id' => '18',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Tecuala',
            'state_id' => '18',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Tepic',
            'state_id' => '18',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Tuxpan',
            'state_id' => '18',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'La Yesca',
            'state_id' => '18',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Bahía de Banderas',
            'state_id' => '18',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Abasolo',
            'state_id' => '19',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Agualeguas',
            'state_id' => '19',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Los Aldamas',
            'state_id' => '19',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Allende',
            'state_id' => '19',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Anáhuac',
            'state_id' => '19',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Apodaca',
            'state_id' => '19',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Aramberri',
            'state_id' => '19',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Bustamante',
            'state_id' => '19',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Cadereyta Jiménez',
            'state_id' => '19',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'El Carmen',
            'state_id' => '19',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Cerralvo',
            'state_id' => '19',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Ciénega de Flores',
            'state_id' => '19',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'China',
            'state_id' => '19',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Doctor Arroyo',
            'state_id' => '19',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Doctor Coss',
            'state_id' => '19',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Doctor González',
            'state_id' => '19',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Galeana',
            'state_id' => '19',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'García',
            'state_id' => '19',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'San Pedro Garza García',
            'state_id' => '19',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'General Bravo',
            'state_id' => '19',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'General Escobedo',
            'state_id' => '19',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'General Terán',
            'state_id' => '19',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'General Treviño',
            'state_id' => '19',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'General Zaragoza',
            'state_id' => '19',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'General Zuazua',
            'state_id' => '19',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Guadalupe',
            'state_id' => '19',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Los Herreras',
            'state_id' => '19',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Higueras',
            'state_id' => '19',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Hualahuises',
            'state_id' => '19',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Iturbide',
            'state_id' => '19',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Juárez',
            'state_id' => '19',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Lampazos de Naranjo',
            'state_id' => '19',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Linares',
            'state_id' => '19',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Marín',
            'state_id' => '19',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Melchor Ocampo',
            'state_id' => '19',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Mier y Noriega',
            'state_id' => '19',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Mina',
            'state_id' => '19',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Montemorelos',
            'state_id' => '19',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Monterrey',
            'state_id' => '19',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Parás',
            'state_id' => '19',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Pesquería',
            'state_id' => '19',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Los Ramones',
            'state_id' => '19',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Rayones',
            'state_id' => '19',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Sabinas Hidalgo',
            'state_id' => '19',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Salinas Victoria',
            'state_id' => '19',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'San Nicolás de los Garza',
            'state_id' => '19',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Hidalgo',
            'state_id' => '19',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Santa Catarina',
            'state_id' => '19',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Santiago',
            'state_id' => '19',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Vallecillo',
            'state_id' => '19',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Villaldama',
            'state_id' => '19',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Abejones',
            'state_id' => '20',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acatlán de Pérez Figueroa',
            'state_id' => '20',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Asunción Cacalotepec',
            'state_id' => '20',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Asunción Cuyotepeji',
            'state_id' => '20',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Asunción Ixtaltepec',
            'state_id' => '20',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Asunción Nochixtlán',
            'state_id' => '20',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Asunción Ocotlán',
            'state_id' => '20',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Asunción Tlacolulita',
            'state_id' => '20',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Ayotzintepec',
            'state_id' => '20',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'El Barrio de la Soledad',
            'state_id' => '20',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Calihualá',
            'state_id' => '20',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Candelaria Loxicha',
            'state_id' => '20',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Ciénega de Zimatlán',
            'state_id' => '20',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Ciudad Ixtepec',
            'state_id' => '20',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Coatecas Altas',
            'state_id' => '20',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Coicoyán de las Flores',
            'state_id' => '20',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'La Compañía',
            'state_id' => '20',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Concepción Buenavista',
            'state_id' => '20',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Concepción Pápalo',
            'state_id' => '20',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Constancia del Rosario',
            'state_id' => '20',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Cosolapa',
            'state_id' => '20',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Cosoltepec',
            'state_id' => '20',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Cuilápam de Guerrero',
            'state_id' => '20',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Cuyamecalco Villa de Zaragoza',
            'state_id' => '20',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Chahuites',
            'state_id' => '20',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Chalcatongo de Hidalgo',
            'state_id' => '20',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Chiquihuitlán de Benito Juárez',
            'state_id' => '20',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Heroica Ciudad de Ejutla de Crespo',
            'state_id' => '20',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Eloxochitlán de Flores Magón',
            'state_id' => '20',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'El Espinal',
            'state_id' => '20',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Tamazulápam del Espíritu Santo',
            'state_id' => '20',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Fresnillo de Trujano',
            'state_id' => '20',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Guadalupe Etla',
            'state_id' => '20',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Guadalupe de Ramírez',
            'state_id' => '20',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Guelatao de Juárez',
            'state_id' => '20',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Guevea de Humboldt',
            'state_id' => '20',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Mesones Hidalgo',
            'state_id' => '20',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Villa Hidalgo',
            'state_id' => '20',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Heroica Ciudad de Huajuapan de León',
            'state_id' => '20',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Huautepec',
            'state_id' => '20',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Huautla de Jiménez',
            'state_id' => '20',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Ixtlán de Juárez',
            'state_id' => '20',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Heroica Ciudad de Juchitán de Zaragoza',
            'state_id' => '20',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Loma Bonita',
            'state_id' => '20',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Magdalena Apasco',
            'state_id' => '20',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Magdalena Jaltepec',
            'state_id' => '20',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Santa Magdalena Jicotlán',
            'state_id' => '20',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Magdalena Mixtepec',
            'state_id' => '20',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Magdalena Ocotlán',
            'state_id' => '20',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Magdalena Peñasco',
            'state_id' => '20',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Magdalena Teitipac',
            'state_id' => '20',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Magdalena Tequisistlán',
            'state_id' => '20',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Magdalena Tlacotepec',
            'state_id' => '20',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Magdalena Zahuatlán',
            'state_id' => '20',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Mariscala de Juárez',
            'state_id' => '20',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Mártires de Tacubaya',
            'state_id' => '20',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Matías Romero Avendaño',
            'state_id' => '20',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Mazatlán Villa de Flores',
            'state_id' => '20',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Miahuatlán de Porfirio Díaz',
            'state_id' => '20',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Mixistlán de la Reforma',
            'state_id' => '20',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Monjas',
            'state_id' => '20',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Natividad',
            'state_id' => '20',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Nazareno Etla',
            'state_id' => '20',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Nejapa de Madero',
            'state_id' => '20',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Ixpantepec Nieves',
            'state_id' => '20',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Santiago Niltepec',
            'state_id' => '20',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Oaxaca de Juárez',
            'state_id' => '20',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Ocotlán de Morelos',
            'state_id' => '20',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'La Pe',
            'state_id' => '20',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Pinotepa de Don Luis',
            'state_id' => '20',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Pluma Hidalgo',
            'state_id' => '20',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'San José del Progreso',
            'state_id' => '20',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Putla Villa de Guerrero',
            'state_id' => '20',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Santa Catarina Quioquitani',
            'state_id' => '20',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Reforma de Pineda',
            'state_id' => '20',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'La Reforma',
            'state_id' => '20',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Reyes Etla',
            'state_id' => '20',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Rojas de Cuauhtémoc',
            'state_id' => '20',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Salina Cruz',
            'state_id' => '20',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'San Agustín Amatengo',
            'state_id' => '20',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'San Agustín Atenango',
            'state_id' => '20',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'San Agustín Chayuco',
            'state_id' => '20',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'San Agustín de las Juntas',
            'state_id' => '20',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'San Agustín Etla',
            'state_id' => '20',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'San Agustín Loxicha',
            'state_id' => '20',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'San Agustín Tlacotepec',
            'state_id' => '20',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'San Agustín Yatareni',
            'state_id' => '20',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'San Andrés Cabecera Nueva',
            'state_id' => '20',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'San Andrés Dinicuiti',
            'state_id' => '20',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'San Andrés Huaxpaltepec',
            'state_id' => '20',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'San Andrés Huayápam',
            'state_id' => '20',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'San Andrés Ixtlahuaca',
            'state_id' => '20',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'San Andrés Lagunas',
            'state_id' => '20',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'San Andrés Nuxiño',
            'state_id' => '20',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'San Andrés Paxtlán',
            'state_id' => '20',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'San Andrés Sinaxtla',
            'state_id' => '20',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'San Andrés Solaga',
            'state_id' => '20',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'San Andrés Teotilálpam',
            'state_id' => '20',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'San Andrés Tepetlapa',
            'state_id' => '20',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'San Andrés Yaá',
            'state_id' => '20',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'San Andrés Zabache',
            'state_id' => '20',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'San Andrés Zautla',
            'state_id' => '20',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'San Antonino Castillo Velasco',
            'state_id' => '20',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'San Antonino el Alto',
            'state_id' => '20',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'San Antonino Monte Verde',
            'state_id' => '20',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'San Antonio Acutla',
            'state_id' => '20',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'San Antonio de la Cal',
            'state_id' => '20',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'San Antonio Huitepec',
            'state_id' => '20',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'San Antonio Nanahuatípam',
            'state_id' => '20',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'San Antonio Sinicahua',
            'state_id' => '20',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'San Antonio Tepetlapa',
            'state_id' => '20',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'San Baltazar Chichicápam',
            'state_id' => '20',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'San Baltazar Loxicha',
            'state_id' => '20',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'San Baltazar Yatzachi el Bajo',
            'state_id' => '20',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'San Bartolo Coyotepec',
            'state_id' => '20',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'San Bartolomé Ayautla',
            'state_id' => '20',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'San Bartolomé Loxicha',
            'state_id' => '20',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'San Bartolomé Quialana',
            'state_id' => '20',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'San Bartolomé Yucuañe',
            'state_id' => '20',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'San Bartolomé Zoogocho',
            'state_id' => '20',
            'number' =>  '120',
        ]);
        City::create([
            'name' =>  'San Bartolo Soyaltepec',
            'state_id' => '20',
            'number' =>  '121',
        ]);
        City::create([
            'name' =>  'San Bartolo Yautepec',
            'state_id' => '20',
            'number' =>  '122',
        ]);
        City::create([
            'name' =>  'San Bernardo Mixtepec',
            'state_id' => '20',
            'number' =>  '123',
        ]);
        City::create([
            'name' =>  'San Blas Atempa',
            'state_id' => '20',
            'number' =>  '124',
        ]);
        City::create([
            'name' =>  'San Carlos Yautepec',
            'state_id' => '20',
            'number' =>  '125',
        ]);
        City::create([
            'name' =>  'San Cristóbal Amatlán',
            'state_id' => '20',
            'number' =>  '126',
        ]);
        City::create([
            'name' =>  'San Cristóbal Amoltepec',
            'state_id' => '20',
            'number' =>  '127',
        ]);
        City::create([
            'name' =>  'San Cristóbal Lachirioag',
            'state_id' => '20',
            'number' =>  '128',
        ]);
        City::create([
            'name' =>  'San Cristóbal Suchixtlahuaca',
            'state_id' => '20',
            'number' =>  '129',
        ]);
        City::create([
            'name' =>  'San Dionisio del Mar',
            'state_id' => '20',
            'number' =>  '130',
        ]);
        City::create([
            'name' =>  'San Dionisio Ocotepec',
            'state_id' => '20',
            'number' =>  '131',
        ]);
        City::create([
            'name' =>  'San Dionisio Ocotlán',
            'state_id' => '20',
            'number' =>  '132',
        ]);
        City::create([
            'name' =>  'San Esteban Atatlahuca',
            'state_id' => '20',
            'number' =>  '133',
        ]);
        City::create([
            'name' =>  'San Felipe Jalapa de Díaz',
            'state_id' => '20',
            'number' =>  '134',
        ]);
        City::create([
            'name' =>  'San Felipe Tejalápam',
            'state_id' => '20',
            'number' =>  '135',
        ]);
        City::create([
            'name' =>  'San Felipe Usila',
            'state_id' => '20',
            'number' =>  '136',
        ]);
        City::create([
            'name' =>  'San Francisco Cahuacuá',
            'state_id' => '20',
            'number' =>  '137',
        ]);
        City::create([
            'name' =>  'San Francisco Cajonos',
            'state_id' => '20',
            'number' =>  '138',
        ]);
        City::create([
            'name' =>  'San Francisco Chapulapa',
            'state_id' => '20',
            'number' =>  '139',
        ]);
        City::create([
            'name' =>  'San Francisco Chindúa',
            'state_id' => '20',
            'number' =>  '140',
        ]);
        City::create([
            'name' =>  'San Francisco del Mar',
            'state_id' => '20',
            'number' =>  '141',
        ]);
        City::create([
            'name' =>  'San Francisco Huehuetlán',
            'state_id' => '20',
            'number' =>  '142',
        ]);
        City::create([
            'name' =>  'San Francisco Ixhuatán',
            'state_id' => '20',
            'number' =>  '143',
        ]);
        City::create([
            'name' =>  'San Francisco Jaltepetongo',
            'state_id' => '20',
            'number' =>  '144',
        ]);
        City::create([
            'name' =>  'San Francisco Lachigoló',
            'state_id' => '20',
            'number' =>  '145',
        ]);
        City::create([
            'name' =>  'San Francisco Logueche',
            'state_id' => '20',
            'number' =>  '146',
        ]);
        City::create([
            'name' =>  'San Francisco Nuxaño',
            'state_id' => '20',
            'number' =>  '147',
        ]);
        City::create([
            'name' =>  'San Francisco Ozolotepec',
            'state_id' => '20',
            'number' =>  '148',
        ]);
        City::create([
            'name' =>  'San Francisco Sola',
            'state_id' => '20',
            'number' =>  '149',
        ]);
        City::create([
            'name' =>  'San Francisco Telixtlahuaca',
            'state_id' => '20',
            'number' =>  '150',
        ]);
        City::create([
            'name' =>  'San Francisco Teopan',
            'state_id' => '20',
            'number' =>  '151',
        ]);
        City::create([
            'name' =>  'San Francisco Tlapancingo',
            'state_id' => '20',
            'number' =>  '152',
        ]);
        City::create([
            'name' =>  'San Gabriel Mixtepec',
            'state_id' => '20',
            'number' =>  '153',
        ]);
        City::create([
            'name' =>  'San Ildefonso Amatlán',
            'state_id' => '20',
            'number' =>  '154',
        ]);
        City::create([
            'name' =>  'San Ildefonso Sola',
            'state_id' => '20',
            'number' =>  '155',
        ]);
        City::create([
            'name' =>  'San Ildefonso Villa Alta',
            'state_id' => '20',
            'number' =>  '156',
        ]);
        City::create([
            'name' =>  'San Jacinto Amilpas',
            'state_id' => '20',
            'number' =>  '157',
        ]);
        City::create([
            'name' =>  'San Jacinto Tlacotepec',
            'state_id' => '20',
            'number' =>  '158',
        ]);
        City::create([
            'name' =>  'San Jerónimo Coatlán',
            'state_id' => '20',
            'number' =>  '159',
        ]);
        City::create([
            'name' =>  'San Jerónimo Silacayoapilla',
            'state_id' => '20',
            'number' =>  '160',
        ]);
        City::create([
            'name' =>  'San Jerónimo Sosola',
            'state_id' => '20',
            'number' =>  '161',
        ]);
        City::create([
            'name' =>  'San Jerónimo Taviche',
            'state_id' => '20',
            'number' =>  '162',
        ]);
        City::create([
            'name' =>  'San Jerónimo Tecóatl',
            'state_id' => '20',
            'number' =>  '163',
        ]);
        City::create([
            'name' =>  'San Jorge Nuchita',
            'state_id' => '20',
            'number' =>  '164',
        ]);
        City::create([
            'name' =>  'San José Ayuquila',
            'state_id' => '20',
            'number' =>  '165',
        ]);
        City::create([
            'name' =>  'San José Chiltepec',
            'state_id' => '20',
            'number' =>  '166',
        ]);
        City::create([
            'name' =>  'San José del Peñasco',
            'state_id' => '20',
            'number' =>  '167',
        ]);
        City::create([
            'name' =>  'San José Estancia Grande',
            'state_id' => '20',
            'number' =>  '168',
        ]);
        City::create([
            'name' =>  'San José Independencia',
            'state_id' => '20',
            'number' =>  '169',
        ]);
        City::create([
            'name' =>  'San José Lachiguiri',
            'state_id' => '20',
            'number' =>  '170',
        ]);
        City::create([
            'name' =>  'San José Tenango',
            'state_id' => '20',
            'number' =>  '171',
        ]);
        City::create([
            'name' =>  'San Juan Achiutla',
            'state_id' => '20',
            'number' =>  '172',
        ]);
        City::create([
            'name' =>  'San Juan Atepec',
            'state_id' => '20',
            'number' =>  '173',
        ]);
        City::create([
            'name' =>  'Ánimas Trujano',
            'state_id' => '20',
            'number' =>  '174',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Atatlahuca',
            'state_id' => '20',
            'number' =>  '175',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Coixtlahuaca',
            'state_id' => '20',
            'number' =>  '176',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Cuicatlán',
            'state_id' => '20',
            'number' =>  '177',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Guelache',
            'state_id' => '20',
            'number' =>  '178',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Jayacatlán',
            'state_id' => '20',
            'number' =>  '179',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Lo de Soto',
            'state_id' => '20',
            'number' =>  '180',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Suchitepec',
            'state_id' => '20',
            'number' =>  '181',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Tlacoatzintepec',
            'state_id' => '20',
            'number' =>  '182',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Tlachichilco',
            'state_id' => '20',
            'number' =>  '183',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Tuxtepec',
            'state_id' => '20',
            'number' =>  '184',
        ]);
        City::create([
            'name' =>  'San Juan Cacahuatepec',
            'state_id' => '20',
            'number' =>  '185',
        ]);
        City::create([
            'name' =>  'San Juan Cieneguilla',
            'state_id' => '20',
            'number' =>  '186',
        ]);
        City::create([
            'name' =>  'San Juan Coatzóspam',
            'state_id' => '20',
            'number' =>  '187',
        ]);
        City::create([
            'name' =>  'San Juan Colorado',
            'state_id' => '20',
            'number' =>  '188',
        ]);
        City::create([
            'name' =>  'San Juan Comaltepec',
            'state_id' => '20',
            'number' =>  '189',
        ]);
        City::create([
            'name' =>  'San Juan Cotzocón',
            'state_id' => '20',
            'number' =>  '190',
        ]);
        City::create([
            'name' =>  'San Juan Chicomezúchil',
            'state_id' => '20',
            'number' =>  '191',
        ]);
        City::create([
            'name' =>  'San Juan Chilateca',
            'state_id' => '20',
            'number' =>  '192',
        ]);
        City::create([
            'name' =>  'San Juan del Estado',
            'state_id' => '20',
            'number' =>  '193',
        ]);
        City::create([
            'name' =>  'San Juan del Río',
            'state_id' => '20',
            'number' =>  '194',
        ]);
        City::create([
            'name' =>  'San Juan Diuxi',
            'state_id' => '20',
            'number' =>  '195',
        ]);
        City::create([
            'name' =>  'San Juan Evangelista Analco',
            'state_id' => '20',
            'number' =>  '196',
        ]);
        City::create([
            'name' =>  'San Juan Guelavía',
            'state_id' => '20',
            'number' =>  '197',
        ]);
        City::create([
            'name' =>  'San Juan Guichicovi',
            'state_id' => '20',
            'number' =>  '198',
        ]);
        City::create([
            'name' =>  'San Juan Ihualtepec',
            'state_id' => '20',
            'number' =>  '199',
        ]);
        City::create([
            'name' =>  'San Juan Juquila Mixes',
            'state_id' => '20',
            'number' =>  '200',
        ]);
        City::create([
            'name' =>  'San Juan Juquila Vijanos',
            'state_id' => '20',
            'number' =>  '201',
        ]);
        City::create([
            'name' =>  'San Juan Lachao',
            'state_id' => '20',
            'number' =>  '202',
        ]);
        City::create([
            'name' =>  'San Juan Lachigalla',
            'state_id' => '20',
            'number' =>  '203',
        ]);
        City::create([
            'name' =>  'San Juan Lajarcia',
            'state_id' => '20',
            'number' =>  '204',
        ]);
        City::create([
            'name' =>  'San Juan Lalana',
            'state_id' => '20',
            'number' =>  '205',
        ]);
        City::create([
            'name' =>  'San Juan de los Cués',
            'state_id' => '20',
            'number' =>  '206',
        ]);
        City::create([
            'name' =>  'San Juan Mazatlán',
            'state_id' => '20',
            'number' =>  '207',
        ]);
        City::create([
            'name' =>  'San Juan Mixtepec',
            'state_id' => '20',
            'number' =>  '208',
        ]);
        City::create([
            'name' =>  'San Juan Mixtepec',
            'state_id' => '20',
            'number' =>  '209',
        ]);
        City::create([
            'name' =>  'San Juan Ñumí',
            'state_id' => '20',
            'number' =>  '210',
        ]);
        City::create([
            'name' =>  'San Juan Ozolotepec',
            'state_id' => '20',
            'number' =>  '211',
        ]);
        City::create([
            'name' =>  'San Juan Petlapa',
            'state_id' => '20',
            'number' =>  '212',
        ]);
        City::create([
            'name' =>  'San Juan Quiahije',
            'state_id' => '20',
            'number' =>  '213',
        ]);
        City::create([
            'name' =>  'San Juan Quiotepec',
            'state_id' => '20',
            'number' =>  '214',
        ]);
        City::create([
            'name' =>  'San Juan Sayultepec',
            'state_id' => '20',
            'number' =>  '215',
        ]);
        City::create([
            'name' =>  'San Juan Tabaá',
            'state_id' => '20',
            'number' =>  '216',
        ]);
        City::create([
            'name' =>  'San Juan Tamazola',
            'state_id' => '20',
            'number' =>  '217',
        ]);
        City::create([
            'name' =>  'San Juan Teita',
            'state_id' => '20',
            'number' =>  '218',
        ]);
        City::create([
            'name' =>  'San Juan Teitipac',
            'state_id' => '20',
            'number' =>  '219',
        ]);
        City::create([
            'name' =>  'San Juan Tepeuxila',
            'state_id' => '20',
            'number' =>  '220',
        ]);
        City::create([
            'name' =>  'San Juan Teposcolula',
            'state_id' => '20',
            'number' =>  '221',
        ]);
        City::create([
            'name' =>  'San Juan Yaeé',
            'state_id' => '20',
            'number' =>  '222',
        ]);
        City::create([
            'name' =>  'San Juan Yatzona',
            'state_id' => '20',
            'number' =>  '223',
        ]);
        City::create([
            'name' =>  'San Juan Yucuita',
            'state_id' => '20',
            'number' =>  '224',
        ]);
        City::create([
            'name' =>  'San Lorenzo',
            'state_id' => '20',
            'number' =>  '225',
        ]);
        City::create([
            'name' =>  'San Lorenzo Albarradas',
            'state_id' => '20',
            'number' =>  '226',
        ]);
        City::create([
            'name' =>  'San Lorenzo Cacaotepec',
            'state_id' => '20',
            'number' =>  '227',
        ]);
        City::create([
            'name' =>  'San Lorenzo Cuaunecuiltitla',
            'state_id' => '20',
            'number' =>  '228',
        ]);
        City::create([
            'name' =>  'San Lorenzo Texmelúcan',
            'state_id' => '20',
            'number' =>  '229',
        ]);
        City::create([
            'name' =>  'San Lorenzo Victoria',
            'state_id' => '20',
            'number' =>  '230',
        ]);
        City::create([
            'name' =>  'San Lucas Camotlán',
            'state_id' => '20',
            'number' =>  '231',
        ]);
        City::create([
            'name' =>  'San Lucas Ojitlán',
            'state_id' => '20',
            'number' =>  '232',
        ]);
        City::create([
            'name' =>  'San Lucas Quiaviní',
            'state_id' => '20',
            'number' =>  '233',
        ]);
        City::create([
            'name' =>  'San Lucas Zoquiápam',
            'state_id' => '20',
            'number' =>  '234',
        ]);
        City::create([
            'name' =>  'San Luis Amatlán',
            'state_id' => '20',
            'number' =>  '235',
        ]);
        City::create([
            'name' =>  'San Marcial Ozolotepec',
            'state_id' => '20',
            'number' =>  '236',
        ]);
        City::create([
            'name' =>  'San Marcos Arteaga',
            'state_id' => '20',
            'number' =>  '237',
        ]);
        City::create([
            'name' =>  'San Martín de los Cansecos',
            'state_id' => '20',
            'number' =>  '238',
        ]);
        City::create([
            'name' =>  'San Martín Huamelúlpam',
            'state_id' => '20',
            'number' =>  '239',
        ]);
        City::create([
            'name' =>  'San Martín Itunyoso',
            'state_id' => '20',
            'number' =>  '240',
        ]);
        City::create([
            'name' =>  'San Martín Lachilá',
            'state_id' => '20',
            'number' =>  '241',
        ]);
        City::create([
            'name' =>  'San Martín Peras',
            'state_id' => '20',
            'number' =>  '242',
        ]);
        City::create([
            'name' =>  'San Martín Tilcajete',
            'state_id' => '20',
            'number' =>  '243',
        ]);
        City::create([
            'name' =>  'San Martín Toxpalan',
            'state_id' => '20',
            'number' =>  '244',
        ]);
        City::create([
            'name' =>  'San Martín Zacatepec',
            'state_id' => '20',
            'number' =>  '245',
        ]);
        City::create([
            'name' =>  'San Mateo Cajonos',
            'state_id' => '20',
            'number' =>  '246',
        ]);
        City::create([
            'name' =>  'Capulálpam de Méndez',
            'state_id' => '20',
            'number' =>  '247',
        ]);
        City::create([
            'name' =>  'San Mateo del Mar',
            'state_id' => '20',
            'number' =>  '248',
        ]);
        City::create([
            'name' =>  'San Mateo Yoloxochitlán',
            'state_id' => '20',
            'number' =>  '249',
        ]);
        City::create([
            'name' =>  'San Mateo Etlatongo',
            'state_id' => '20',
            'number' =>  '250',
        ]);
        City::create([
            'name' =>  'San Mateo Nejápam',
            'state_id' => '20',
            'number' =>  '251',
        ]);
        City::create([
            'name' =>  'San Mateo Peñasco',
            'state_id' => '20',
            'number' =>  '252',
        ]);
        City::create([
            'name' =>  'San Mateo Piñas',
            'state_id' => '20',
            'number' =>  '253',
        ]);
        City::create([
            'name' =>  'San Mateo Río Hondo',
            'state_id' => '20',
            'number' =>  '254',
        ]);
        City::create([
            'name' =>  'San Mateo Sindihui',
            'state_id' => '20',
            'number' =>  '255',
        ]);
        City::create([
            'name' =>  'San Mateo Tlapiltepec',
            'state_id' => '20',
            'number' =>  '256',
        ]);
        City::create([
            'name' =>  'San Melchor Betaza',
            'state_id' => '20',
            'number' =>  '257',
        ]);
        City::create([
            'name' =>  'San Miguel Achiutla',
            'state_id' => '20',
            'number' =>  '258',
        ]);
        City::create([
            'name' =>  'San Miguel Ahuehuetitlán',
            'state_id' => '20',
            'number' =>  '259',
        ]);
        City::create([
            'name' =>  'San Miguel Aloápam',
            'state_id' => '20',
            'number' =>  '260',
        ]);
        City::create([
            'name' =>  'San Miguel Amatitlán',
            'state_id' => '20',
            'number' =>  '261',
        ]);
        City::create([
            'name' =>  'San Miguel Amatlán',
            'state_id' => '20',
            'number' =>  '262',
        ]);
        City::create([
            'name' =>  'San Miguel Coatlán',
            'state_id' => '20',
            'number' =>  '263',
        ]);
        City::create([
            'name' =>  'San Miguel Chicahua',
            'state_id' => '20',
            'number' =>  '264',
        ]);
        City::create([
            'name' =>  'San Miguel Chimalapa',
            'state_id' => '20',
            'number' =>  '265',
        ]);
        City::create([
            'name' =>  'San Miguel del Puerto',
            'state_id' => '20',
            'number' =>  '266',
        ]);
        City::create([
            'name' =>  'San Miguel del Río',
            'state_id' => '20',
            'number' =>  '267',
        ]);
        City::create([
            'name' =>  'San Miguel Ejutla',
            'state_id' => '20',
            'number' =>  '268',
        ]);
        City::create([
            'name' =>  'San Miguel el Grande',
            'state_id' => '20',
            'number' =>  '269',
        ]);
        City::create([
            'name' =>  'San Miguel Huautla',
            'state_id' => '20',
            'number' =>  '270',
        ]);
        City::create([
            'name' =>  'San Miguel Mixtepec',
            'state_id' => '20',
            'number' =>  '271',
        ]);
        City::create([
            'name' =>  'San Miguel Panixtlahuaca',
            'state_id' => '20',
            'number' =>  '272',
        ]);
        City::create([
            'name' =>  'San Miguel Peras',
            'state_id' => '20',
            'number' =>  '273',
        ]);
        City::create([
            'name' =>  'San Miguel Piedras',
            'state_id' => '20',
            'number' =>  '274',
        ]);
        City::create([
            'name' =>  'San Miguel Quetzaltepec',
            'state_id' => '20',
            'number' =>  '275',
        ]);
        City::create([
            'name' =>  'San Miguel Santa Flor',
            'state_id' => '20',
            'number' =>  '276',
        ]);
        City::create([
            'name' =>  'Villa Sola de Vega',
            'state_id' => '20',
            'number' =>  '277',
        ]);
        City::create([
            'name' =>  'San Miguel Soyaltepec',
            'state_id' => '20',
            'number' =>  '278',
        ]);
        City::create([
            'name' =>  'San Miguel Suchixtepec',
            'state_id' => '20',
            'number' =>  '279',
        ]);
        City::create([
            'name' =>  'Villa Talea de Castro',
            'state_id' => '20',
            'number' =>  '280',
        ]);
        City::create([
            'name' =>  'San Miguel Tecomatlán',
            'state_id' => '20',
            'number' =>  '281',
        ]);
        City::create([
            'name' =>  'San Miguel Tenango',
            'state_id' => '20',
            'number' =>  '282',
        ]);
        City::create([
            'name' =>  'San Miguel Tequixtepec',
            'state_id' => '20',
            'number' =>  '283',
        ]);
        City::create([
            'name' =>  'San Miguel Tilquiápam',
            'state_id' => '20',
            'number' =>  '284',
        ]);
        City::create([
            'name' =>  'San Miguel Tlacamama',
            'state_id' => '20',
            'number' =>  '285',
        ]);
        City::create([
            'name' =>  'San Miguel Tlacotepec',
            'state_id' => '20',
            'number' =>  '286',
        ]);
        City::create([
            'name' =>  'San Miguel Tulancingo',
            'state_id' => '20',
            'number' =>  '287',
        ]);
        City::create([
            'name' =>  'San Miguel Yotao',
            'state_id' => '20',
            'number' =>  '288',
        ]);
        City::create([
            'name' =>  'San Nicolás',
            'state_id' => '20',
            'number' =>  '289',
        ]);
        City::create([
            'name' =>  'San Nicolás Hidalgo',
            'state_id' => '20',
            'number' =>  '290',
        ]);
        City::create([
            'name' =>  'San Pablo Coatlán',
            'state_id' => '20',
            'number' =>  '291',
        ]);
        City::create([
            'name' =>  'San Pablo Cuatro Venados',
            'state_id' => '20',
            'number' =>  '292',
        ]);
        City::create([
            'name' =>  'San Pablo Etla',
            'state_id' => '20',
            'number' =>  '293',
        ]);
        City::create([
            'name' =>  'San Pablo Huitzo',
            'state_id' => '20',
            'number' =>  '294',
        ]);
        City::create([
            'name' =>  'San Pablo Huixtepec',
            'state_id' => '20',
            'number' =>  '295',
        ]);
        City::create([
            'name' =>  'San Pablo Macuiltianguis',
            'state_id' => '20',
            'number' =>  '296',
        ]);
        City::create([
            'name' =>  'San Pablo Tijaltepec',
            'state_id' => '20',
            'number' =>  '297',
        ]);
        City::create([
            'name' =>  'San Pablo Villa de Mitla',
            'state_id' => '20',
            'number' =>  '298',
        ]);
        City::create([
            'name' =>  'San Pablo Yaganiza',
            'state_id' => '20',
            'number' =>  '299',
        ]);
        City::create([
            'name' =>  'San Pedro Amuzgos',
            'state_id' => '20',
            'number' =>  '300',
        ]);
        City::create([
            'name' =>  'San Pedro Apóstol',
            'state_id' => '20',
            'number' =>  '301',
        ]);
        City::create([
            'name' =>  'San Pedro Atoyac',
            'state_id' => '20',
            'number' =>  '302',
        ]);
        City::create([
            'name' =>  'San Pedro Cajonos',
            'state_id' => '20',
            'number' =>  '303',
        ]);
        City::create([
            'name' =>  'San Pedro Coxcaltepec Cántaros',
            'state_id' => '20',
            'number' =>  '304',
        ]);
        City::create([
            'name' =>  'San Pedro Comitancillo',
            'state_id' => '20',
            'number' =>  '305',
        ]);
        City::create([
            'name' =>  'San Pedro el Alto',
            'state_id' => '20',
            'number' =>  '306',
        ]);
        City::create([
            'name' =>  'San Pedro Huamelula',
            'state_id' => '20',
            'number' =>  '307',
        ]);
        City::create([
            'name' =>  'San Pedro Huilotepec',
            'state_id' => '20',
            'number' =>  '308',
        ]);
        City::create([
            'name' =>  'San Pedro Ixcatlán',
            'state_id' => '20',
            'number' =>  '309',
        ]);
        City::create([
            'name' =>  'San Pedro Ixtlahuaca',
            'state_id' => '20',
            'number' =>  '310',
        ]);
        City::create([
            'name' =>  'San Pedro Jaltepetongo',
            'state_id' => '20',
            'number' =>  '311',
        ]);
        City::create([
            'name' =>  'San Pedro Jicayán',
            'state_id' => '20',
            'number' =>  '312',
        ]);
        City::create([
            'name' =>  'San Pedro Jocotipac',
            'state_id' => '20',
            'number' =>  '313',
        ]);
        City::create([
            'name' =>  'San Pedro Juchatengo',
            'state_id' => '20',
            'number' =>  '314',
        ]);
        City::create([
            'name' =>  'San Pedro Mártir',
            'state_id' => '20',
            'number' =>  '315',
        ]);
        City::create([
            'name' =>  'San Pedro Mártir Quiechapa',
            'state_id' => '20',
            'number' =>  '316',
        ]);
        City::create([
            'name' =>  'San Pedro Mártir Yucuxaco',
            'state_id' => '20',
            'number' =>  '317',
        ]);
        City::create([
            'name' =>  'San Pedro Mixtepec',
            'state_id' => '20',
            'number' =>  '318',
        ]);
        City::create([
            'name' =>  'San Pedro Mixtepec',
            'state_id' => '20',
            'number' =>  '319',
        ]);
        City::create([
            'name' =>  'San Pedro Molinos',
            'state_id' => '20',
            'number' =>  '320',
        ]);
        City::create([
            'name' =>  'San Pedro Nopala',
            'state_id' => '20',
            'number' =>  '321',
        ]);
        City::create([
            'name' =>  'San Pedro Ocopetatillo',
            'state_id' => '20',
            'number' =>  '322',
        ]);
        City::create([
            'name' =>  'San Pedro Ocotepec',
            'state_id' => '20',
            'number' =>  '323',
        ]);
        City::create([
            'name' =>  'San Pedro Pochutla',
            'state_id' => '20',
            'number' =>  '324',
        ]);
        City::create([
            'name' =>  'San Pedro Quiatoni',
            'state_id' => '20',
            'number' =>  '325',
        ]);
        City::create([
            'name' =>  'San Pedro Sochiápam',
            'state_id' => '20',
            'number' =>  '326',
        ]);
        City::create([
            'name' =>  'San Pedro Tapanatepec',
            'state_id' => '20',
            'number' =>  '327',
        ]);
        City::create([
            'name' =>  'San Pedro Taviche',
            'state_id' => '20',
            'number' =>  '328',
        ]);
        City::create([
            'name' =>  'San Pedro Teozacoalco',
            'state_id' => '20',
            'number' =>  '329',
        ]);
        City::create([
            'name' =>  'San Pedro Teutila',
            'state_id' => '20',
            'number' =>  '330',
        ]);
        City::create([
            'name' =>  'San Pedro Tidaá',
            'state_id' => '20',
            'number' =>  '331',
        ]);
        City::create([
            'name' =>  'San Pedro Topiltepec',
            'state_id' => '20',
            'number' =>  '332',
        ]);
        City::create([
            'name' =>  'San Pedro Totolápam',
            'state_id' => '20',
            'number' =>  '333',
        ]);
        City::create([
            'name' =>  'Villa de Tututepec de Melchor Ocampo',
            'state_id' => '20',
            'number' =>  '334',
        ]);
        City::create([
            'name' =>  'San Pedro Yaneri',
            'state_id' => '20',
            'number' =>  '335',
        ]);
        City::create([
            'name' =>  'San Pedro Yólox',
            'state_id' => '20',
            'number' =>  '336',
        ]);
        City::create([
            'name' =>  'San Pedro y San Pablo Ayutla',
            'state_id' => '20',
            'number' =>  '337',
        ]);
        City::create([
            'name' =>  'Villa de Etla',
            'state_id' => '20',
            'number' =>  '338',
        ]);
        City::create([
            'name' =>  'San Pedro y San Pablo Teposcolula',
            'state_id' => '20',
            'number' =>  '339',
        ]);
        City::create([
            'name' =>  'San Pedro y San Pablo Tequixtepec',
            'state_id' => '20',
            'number' =>  '340',
        ]);
        City::create([
            'name' =>  'San Pedro Yucunama',
            'state_id' => '20',
            'number' =>  '341',
        ]);
        City::create([
            'name' =>  'San Raymundo Jalpan',
            'state_id' => '20',
            'number' =>  '342',
        ]);
        City::create([
            'name' =>  'San Sebastián Abasolo',
            'state_id' => '20',
            'number' =>  '343',
        ]);
        City::create([
            'name' =>  'San Sebastián Coatlán',
            'state_id' => '20',
            'number' =>  '344',
        ]);
        City::create([
            'name' =>  'San Sebastián Ixcapa',
            'state_id' => '20',
            'number' =>  '345',
        ]);
        City::create([
            'name' =>  'San Sebastián Nicananduta',
            'state_id' => '20',
            'number' =>  '346',
        ]);
        City::create([
            'name' =>  'San Sebastián Río Hondo',
            'state_id' => '20',
            'number' =>  '347',
        ]);
        City::create([
            'name' =>  'San Sebastián Tecomaxtlahuaca',
            'state_id' => '20',
            'number' =>  '348',
        ]);
        City::create([
            'name' =>  'San Sebastián Teitipac',
            'state_id' => '20',
            'number' =>  '349',
        ]);
        City::create([
            'name' =>  'San Sebastián Tutla',
            'state_id' => '20',
            'number' =>  '350',
        ]);
        City::create([
            'name' =>  'San Simón Almolongas',
            'state_id' => '20',
            'number' =>  '351',
        ]);
        City::create([
            'name' =>  'San Simón Zahuatlán',
            'state_id' => '20',
            'number' =>  '352',
        ]);
        City::create([
            'name' =>  'Santa Ana',
            'state_id' => '20',
            'number' =>  '353',
        ]);
        City::create([
            'name' =>  'Santa Ana Ateixtlahuaca',
            'state_id' => '20',
            'number' =>  '354',
        ]);
        City::create([
            'name' =>  'Santa Ana Cuauhtémoc',
            'state_id' => '20',
            'number' =>  '355',
        ]);
        City::create([
            'name' =>  'Santa Ana del Valle',
            'state_id' => '20',
            'number' =>  '356',
        ]);
        City::create([
            'name' =>  'Santa Ana Tavela',
            'state_id' => '20',
            'number' =>  '357',
        ]);
        City::create([
            'name' =>  'Santa Ana Tlapacoyan',
            'state_id' => '20',
            'number' =>  '358',
        ]);
        City::create([
            'name' =>  'Santa Ana Yareni',
            'state_id' => '20',
            'number' =>  '359',
        ]);
        City::create([
            'name' =>  'Santa Ana Zegache',
            'state_id' => '20',
            'number' =>  '360',
        ]);
        City::create([
            'name' =>  'Santa Catalina Quierí',
            'state_id' => '20',
            'number' =>  '361',
        ]);
        City::create([
            'name' =>  'Santa Catarina Cuixtla',
            'state_id' => '20',
            'number' =>  '362',
        ]);
        City::create([
            'name' =>  'Santa Catarina Ixtepeji',
            'state_id' => '20',
            'number' =>  '363',
        ]);
        City::create([
            'name' =>  'Santa Catarina Juquila',
            'state_id' => '20',
            'number' =>  '364',
        ]);
        City::create([
            'name' =>  'Santa Catarina Lachatao',
            'state_id' => '20',
            'number' =>  '365',
        ]);
        City::create([
            'name' =>  'Santa Catarina Loxicha',
            'state_id' => '20',
            'number' =>  '366',
        ]);
        City::create([
            'name' =>  'Santa Catarina Mechoacán',
            'state_id' => '20',
            'number' =>  '367',
        ]);
        City::create([
            'name' =>  'Santa Catarina Minas',
            'state_id' => '20',
            'number' =>  '368',
        ]);
        City::create([
            'name' =>  'Santa Catarina Quiané',
            'state_id' => '20',
            'number' =>  '369',
        ]);
        City::create([
            'name' =>  'Santa Catarina Tayata',
            'state_id' => '20',
            'number' =>  '370',
        ]);
        City::create([
            'name' =>  'Santa Catarina Ticuá',
            'state_id' => '20',
            'number' =>  '371',
        ]);
        City::create([
            'name' =>  'Santa Catarina Yosonotú',
            'state_id' => '20',
            'number' =>  '372',
        ]);
        City::create([
            'name' =>  'Santa Catarina Zapoquila',
            'state_id' => '20',
            'number' =>  '373',
        ]);
        City::create([
            'name' =>  'Santa Cruz Acatepec',
            'state_id' => '20',
            'number' =>  '374',
        ]);
        City::create([
            'name' =>  'Santa Cruz Amilpas',
            'state_id' => '20',
            'number' =>  '375',
        ]);
        City::create([
            'name' =>  'Santa Cruz de Bravo',
            'state_id' => '20',
            'number' =>  '376',
        ]);
        City::create([
            'name' =>  'Santa Cruz Itundujia',
            'state_id' => '20',
            'number' =>  '377',
        ]);
        City::create([
            'name' =>  'Santa Cruz Mixtepec',
            'state_id' => '20',
            'number' =>  '378',
        ]);
        City::create([
            'name' =>  'Santa Cruz Nundaco',
            'state_id' => '20',
            'number' =>  '379',
        ]);
        City::create([
            'name' =>  'Santa Cruz Papalutla',
            'state_id' => '20',
            'number' =>  '380',
        ]);
        City::create([
            'name' =>  'Santa Cruz Tacache de Mina',
            'state_id' => '20',
            'number' =>  '381',
        ]);
        City::create([
            'name' =>  'Santa Cruz Tacahua',
            'state_id' => '20',
            'number' =>  '382',
        ]);
        City::create([
            'name' =>  'Santa Cruz Tayata',
            'state_id' => '20',
            'number' =>  '383',
        ]);
        City::create([
            'name' =>  'Santa Cruz Xitla',
            'state_id' => '20',
            'number' =>  '384',
        ]);
        City::create([
            'name' =>  'Santa Cruz Xoxocotlán',
            'state_id' => '20',
            'number' =>  '385',
        ]);
        City::create([
            'name' =>  'Santa Cruz Zenzontepec',
            'state_id' => '20',
            'number' =>  '386',
        ]);
        City::create([
            'name' =>  'Santa Gertrudis',
            'state_id' => '20',
            'number' =>  '387',
        ]);
        City::create([
            'name' =>  'Santa Inés del Monte',
            'state_id' => '20',
            'number' =>  '388',
        ]);
        City::create([
            'name' =>  'Santa Inés Yatzeche',
            'state_id' => '20',
            'number' =>  '389',
        ]);
        City::create([
            'name' =>  'Santa Lucía del Camino',
            'state_id' => '20',
            'number' =>  '390',
        ]);
        City::create([
            'name' =>  'Santa Lucía Miahuatlán',
            'state_id' => '20',
            'number' =>  '391',
        ]);
        City::create([
            'name' =>  'Santa Lucía Monteverde',
            'state_id' => '20',
            'number' =>  '392',
        ]);
        City::create([
            'name' =>  'Santa Lucía Ocotlán',
            'state_id' => '20',
            'number' =>  '393',
        ]);
        City::create([
            'name' =>  'Santa María Alotepec',
            'state_id' => '20',
            'number' =>  '394',
        ]);
        City::create([
            'name' =>  'Santa María Apazco',
            'state_id' => '20',
            'number' =>  '395',
        ]);
        City::create([
            'name' =>  'Santa María la Asunción',
            'state_id' => '20',
            'number' =>  '396',
        ]);
        City::create([
            'name' =>  'Heroica Ciudad de Tlaxiaco',
            'state_id' => '20',
            'number' =>  '397',
        ]);
        City::create([
            'name' =>  'Ayoquezco de Aldama',
            'state_id' => '20',
            'number' =>  '398',
        ]);
        City::create([
            'name' =>  'Santa María Atzompa',
            'state_id' => '20',
            'number' =>  '399',
        ]);
        City::create([
            'name' =>  'Santa María Camotlán',
            'state_id' => '20',
            'number' =>  '400',
        ]);
        City::create([
            'name' =>  'Santa María Colotepec',
            'state_id' => '20',
            'number' =>  '401',
        ]);
        City::create([
            'name' =>  'Santa María Cortijo',
            'state_id' => '20',
            'number' =>  '402',
        ]);
        City::create([
            'name' =>  'Santa María Coyotepec',
            'state_id' => '20',
            'number' =>  '403',
        ]);
        City::create([
            'name' =>  'Santa María Chachoápam',
            'state_id' => '20',
            'number' =>  '404',
        ]);
        City::create([
            'name' =>  'Villa de Chilapa de Díaz',
            'state_id' => '20',
            'number' =>  '405',
        ]);
        City::create([
            'name' =>  'Santa María Chilchotla',
            'state_id' => '20',
            'number' =>  '406',
        ]);
        City::create([
            'name' =>  'Santa María Chimalapa',
            'state_id' => '20',
            'number' =>  '407',
        ]);
        City::create([
            'name' =>  'Santa María del Rosario',
            'state_id' => '20',
            'number' =>  '408',
        ]);
        City::create([
            'name' =>  'Santa María del Tule',
            'state_id' => '20',
            'number' =>  '409',
        ]);
        City::create([
            'name' =>  'Santa María Ecatepec',
            'state_id' => '20',
            'number' =>  '410',
        ]);
        City::create([
            'name' =>  'Santa María Guelacé',
            'state_id' => '20',
            'number' =>  '411',
        ]);
        City::create([
            'name' =>  'Santa María Guienagati',
            'state_id' => '20',
            'number' =>  '412',
        ]);
        City::create([
            'name' =>  'Santa María Huatulco',
            'state_id' => '20',
            'number' =>  '413',
        ]);
        City::create([
            'name' =>  'Santa María Huazolotitlán',
            'state_id' => '20',
            'number' =>  '414',
        ]);
        City::create([
            'name' =>  'Santa María Ipalapa',
            'state_id' => '20',
            'number' =>  '415',
        ]);
        City::create([
            'name' =>  'Santa María Ixcatlán',
            'state_id' => '20',
            'number' =>  '416',
        ]);
        City::create([
            'name' =>  'Santa María Jacatepec',
            'state_id' => '20',
            'number' =>  '417',
        ]);
        City::create([
            'name' =>  'Santa María Jalapa del Marqués',
            'state_id' => '20',
            'number' =>  '418',
        ]);
        City::create([
            'name' =>  'Santa María Jaltianguis',
            'state_id' => '20',
            'number' =>  '419',
        ]);
        City::create([
            'name' =>  'Santa María Lachixío',
            'state_id' => '20',
            'number' =>  '420',
        ]);
        City::create([
            'name' =>  'Santa María Mixtequilla',
            'state_id' => '20',
            'number' =>  '421',
        ]);
        City::create([
            'name' =>  'Santa María Nativitas',
            'state_id' => '20',
            'number' =>  '422',
        ]);
        City::create([
            'name' =>  'Santa María Nduayaco',
            'state_id' => '20',
            'number' =>  '423',
        ]);
        City::create([
            'name' =>  'Santa María Ozolotepec',
            'state_id' => '20',
            'number' =>  '424',
        ]);
        City::create([
            'name' =>  'Santa María Pápalo',
            'state_id' => '20',
            'number' =>  '425',
        ]);
        City::create([
            'name' =>  'Santa María Peñoles',
            'state_id' => '20',
            'number' =>  '426',
        ]);
        City::create([
            'name' =>  'Santa María Petapa',
            'state_id' => '20',
            'number' =>  '427',
        ]);
        City::create([
            'name' =>  'Santa María Quiegolani',
            'state_id' => '20',
            'number' =>  '428',
        ]);
        City::create([
            'name' =>  'Santa María Sola',
            'state_id' => '20',
            'number' =>  '429',
        ]);
        City::create([
            'name' =>  'Santa María Tataltepec',
            'state_id' => '20',
            'number' =>  '430',
        ]);
        City::create([
            'name' =>  'Santa María Tecomavaca',
            'state_id' => '20',
            'number' =>  '431',
        ]);
        City::create([
            'name' =>  'Santa María Temaxcalapa',
            'state_id' => '20',
            'number' =>  '432',
        ]);
        City::create([
            'name' =>  'Santa María Temaxcaltepec',
            'state_id' => '20',
            'number' =>  '433',
        ]);
        City::create([
            'name' =>  'Santa María Teopoxco',
            'state_id' => '20',
            'number' =>  '434',
        ]);
        City::create([
            'name' =>  'Santa María Tepantlali',
            'state_id' => '20',
            'number' =>  '435',
        ]);
        City::create([
            'name' =>  'Santa María Texcatitlán',
            'state_id' => '20',
            'number' =>  '436',
        ]);
        City::create([
            'name' =>  'Santa María Tlahuitoltepec',
            'state_id' => '20',
            'number' =>  '437',
        ]);
        City::create([
            'name' =>  'Santa María Tlalixtac',
            'state_id' => '20',
            'number' =>  '438',
        ]);
        City::create([
            'name' =>  'Santa María Tonameca',
            'state_id' => '20',
            'number' =>  '439',
        ]);
        City::create([
            'name' =>  'Santa María Totolapilla',
            'state_id' => '20',
            'number' =>  '440',
        ]);
        City::create([
            'name' =>  'Santa María Xadani',
            'state_id' => '20',
            'number' =>  '441',
        ]);
        City::create([
            'name' =>  'Santa María Yalina',
            'state_id' => '20',
            'number' =>  '442',
        ]);
        City::create([
            'name' =>  'Santa María Yavesía',
            'state_id' => '20',
            'number' =>  '443',
        ]);
        City::create([
            'name' =>  'Santa María Yolotepec',
            'state_id' => '20',
            'number' =>  '444',
        ]);
        City::create([
            'name' =>  'Santa María Yosoyúa',
            'state_id' => '20',
            'number' =>  '445',
        ]);
        City::create([
            'name' =>  'Santa María Yucuhiti',
            'state_id' => '20',
            'number' =>  '446',
        ]);
        City::create([
            'name' =>  'Santa María Zacatepec',
            'state_id' => '20',
            'number' =>  '447',
        ]);
        City::create([
            'name' =>  'Santa María Zaniza',
            'state_id' => '20',
            'number' =>  '448',
        ]);
        City::create([
            'name' =>  'Santa María Zoquitlán',
            'state_id' => '20',
            'number' =>  '449',
        ]);
        City::create([
            'name' =>  'Santiago Amoltepec',
            'state_id' => '20',
            'number' =>  '450',
        ]);
        City::create([
            'name' =>  'Santiago Apoala',
            'state_id' => '20',
            'number' =>  '451',
        ]);
        City::create([
            'name' =>  'Santiago Apóstol',
            'state_id' => '20',
            'number' =>  '452',
        ]);
        City::create([
            'name' =>  'Santiago Astata',
            'state_id' => '20',
            'number' =>  '453',
        ]);
        City::create([
            'name' =>  'Santiago Atitlán',
            'state_id' => '20',
            'number' =>  '454',
        ]);
        City::create([
            'name' =>  'Santiago Ayuquililla',
            'state_id' => '20',
            'number' =>  '455',
        ]);
        City::create([
            'name' =>  'Santiago Cacaloxtepec',
            'state_id' => '20',
            'number' =>  '456',
        ]);
        City::create([
            'name' =>  'Santiago Camotlán',
            'state_id' => '20',
            'number' =>  '457',
        ]);
        City::create([
            'name' =>  'Santiago Comaltepec',
            'state_id' => '20',
            'number' =>  '458',
        ]);
        City::create([
            'name' =>  'Santiago Chazumba',
            'state_id' => '20',
            'number' =>  '459',
        ]);
        City::create([
            'name' =>  'Santiago Choápam',
            'state_id' => '20',
            'number' =>  '460',
        ]);
        City::create([
            'name' =>  'Santiago del Río',
            'state_id' => '20',
            'number' =>  '461',
        ]);
        City::create([
            'name' =>  'Santiago Huajolotitlán',
            'state_id' => '20',
            'number' =>  '462',
        ]);
        City::create([
            'name' =>  'Santiago Huauclilla',
            'state_id' => '20',
            'number' =>  '463',
        ]);
        City::create([
            'name' =>  'Santiago Ihuitlán Plumas',
            'state_id' => '20',
            'number' =>  '464',
        ]);
        City::create([
            'name' =>  'Santiago Ixcuintepec',
            'state_id' => '20',
            'number' =>  '465',
        ]);
        City::create([
            'name' =>  'Santiago Ixtayutla',
            'state_id' => '20',
            'number' =>  '466',
        ]);
        City::create([
            'name' =>  'Santiago Jamiltepec',
            'state_id' => '20',
            'number' =>  '467',
        ]);
        City::create([
            'name' =>  'Santiago Jocotepec',
            'state_id' => '20',
            'number' =>  '468',
        ]);
        City::create([
            'name' =>  'Santiago Juxtlahuaca',
            'state_id' => '20',
            'number' =>  '469',
        ]);
        City::create([
            'name' =>  'Santiago Lachiguiri',
            'state_id' => '20',
            'number' =>  '470',
        ]);
        City::create([
            'name' =>  'Santiago Lalopa',
            'state_id' => '20',
            'number' =>  '471',
        ]);
        City::create([
            'name' =>  'Santiago Laollaga',
            'state_id' => '20',
            'number' =>  '472',
        ]);
        City::create([
            'name' =>  'Santiago Laxopa',
            'state_id' => '20',
            'number' =>  '473',
        ]);
        City::create([
            'name' =>  'Santiago Llano Grande',
            'state_id' => '20',
            'number' =>  '474',
        ]);
        City::create([
            'name' =>  'Santiago Matatlán',
            'state_id' => '20',
            'number' =>  '475',
        ]);
        City::create([
            'name' =>  'Santiago Miltepec',
            'state_id' => '20',
            'number' =>  '476',
        ]);
        City::create([
            'name' =>  'Santiago Minas',
            'state_id' => '20',
            'number' =>  '477',
        ]);
        City::create([
            'name' =>  'Santiago Nacaltepec',
            'state_id' => '20',
            'number' =>  '478',
        ]);
        City::create([
            'name' =>  'Santiago Nejapilla',
            'state_id' => '20',
            'number' =>  '479',
        ]);
        City::create([
            'name' =>  'Santiago Nundiche',
            'state_id' => '20',
            'number' =>  '480',
        ]);
        City::create([
            'name' =>  'Santiago Nuyoó',
            'state_id' => '20',
            'number' =>  '481',
        ]);
        City::create([
            'name' =>  'Santiago Pinotepa Nacional',
            'state_id' => '20',
            'number' =>  '482',
        ]);
        City::create([
            'name' =>  'Santiago Suchilquitongo',
            'state_id' => '20',
            'number' =>  '483',
        ]);
        City::create([
            'name' =>  'Santiago Tamazola',
            'state_id' => '20',
            'number' =>  '484',
        ]);
        City::create([
            'name' =>  'Santiago Tapextla',
            'state_id' => '20',
            'number' =>  '485',
        ]);
        City::create([
            'name' =>  'Villa Tejúpam de la Unión',
            'state_id' => '20',
            'number' =>  '486',
        ]);
        City::create([
            'name' =>  'Santiago Tenango',
            'state_id' => '20',
            'number' =>  '487',
        ]);
        City::create([
            'name' =>  'Santiago Tepetlapa',
            'state_id' => '20',
            'number' =>  '488',
        ]);
        City::create([
            'name' =>  'Santiago Tetepec',
            'state_id' => '20',
            'number' =>  '489',
        ]);
        City::create([
            'name' =>  'Santiago Texcalcingo',
            'state_id' => '20',
            'number' =>  '490',
        ]);
        City::create([
            'name' =>  'Santiago Textitlán',
            'state_id' => '20',
            'number' =>  '491',
        ]);
        City::create([
            'name' =>  'Santiago Tilantongo',
            'state_id' => '20',
            'number' =>  '492',
        ]);
        City::create([
            'name' =>  'Santiago Tillo',
            'state_id' => '20',
            'number' =>  '493',
        ]);
        City::create([
            'name' =>  'Santiago Tlazoyaltepec',
            'state_id' => '20',
            'number' =>  '494',
        ]);
        City::create([
            'name' =>  'Santiago Xanica',
            'state_id' => '20',
            'number' =>  '495',
        ]);
        City::create([
            'name' =>  'Santiago Xiacuí',
            'state_id' => '20',
            'number' =>  '496',
        ]);
        City::create([
            'name' =>  'Santiago Yaitepec',
            'state_id' => '20',
            'number' =>  '497',
        ]);
        City::create([
            'name' =>  'Santiago Yaveo',
            'state_id' => '20',
            'number' =>  '498',
        ]);
        City::create([
            'name' =>  'Santiago Yolomécatl',
            'state_id' => '20',
            'number' =>  '499',
        ]);
        City::create([
            'name' =>  'Santiago Yosondúa',
            'state_id' => '20',
            'number' =>  '500',
        ]);
        City::create([
            'name' =>  'Santiago Yucuyachi',
            'state_id' => '20',
            'number' =>  '501',
        ]);
        City::create([
            'name' =>  'Santiago Zacatepec',
            'state_id' => '20',
            'number' =>  '502',
        ]);
        City::create([
            'name' =>  'Santiago Zoochila',
            'state_id' => '20',
            'number' =>  '503',
        ]);
        City::create([
            'name' =>  'Nuevo Zoquiápam',
            'state_id' => '20',
            'number' =>  '504',
        ]);
        City::create([
            'name' =>  'Santo Domingo Ingenio',
            'state_id' => '20',
            'number' =>  '505',
        ]);
        City::create([
            'name' =>  'Santo Domingo Albarradas',
            'state_id' => '20',
            'number' =>  '506',
        ]);
        City::create([
            'name' =>  'Santo Domingo Armenta',
            'state_id' => '20',
            'number' =>  '507',
        ]);
        City::create([
            'name' =>  'Santo Domingo Chihuitán',
            'state_id' => '20',
            'number' =>  '508',
        ]);
        City::create([
            'name' =>  'Santo Domingo de Morelos',
            'state_id' => '20',
            'number' =>  '509',
        ]);
        City::create([
            'name' =>  'Santo Domingo Ixcatlán',
            'state_id' => '20',
            'number' =>  '510',
        ]);
        City::create([
            'name' =>  'Santo Domingo Nuxaá',
            'state_id' => '20',
            'number' =>  '511',
        ]);
        City::create([
            'name' =>  'Santo Domingo Ozolotepec',
            'state_id' => '20',
            'number' =>  '512',
        ]);
        City::create([
            'name' =>  'Santo Domingo Petapa',
            'state_id' => '20',
            'number' =>  '513',
        ]);
        City::create([
            'name' =>  'Santo Domingo Roayaga',
            'state_id' => '20',
            'number' =>  '514',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tehuantepec',
            'state_id' => '20',
            'number' =>  '515',
        ]);
        City::create([
            'name' =>  'Santo Domingo Teojomulco',
            'state_id' => '20',
            'number' =>  '516',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tepuxtepec',
            'state_id' => '20',
            'number' =>  '517',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tlatayápam',
            'state_id' => '20',
            'number' =>  '518',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tomaltepec',
            'state_id' => '20',
            'number' =>  '519',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tonalá',
            'state_id' => '20',
            'number' =>  '520',
        ]);
        City::create([
            'name' =>  'Santo Domingo Tonaltepec',
            'state_id' => '20',
            'number' =>  '521',
        ]);
        City::create([
            'name' =>  'Santo Domingo Xagacía',
            'state_id' => '20',
            'number' =>  '522',
        ]);
        City::create([
            'name' =>  'Santo Domingo Yanhuitlán',
            'state_id' => '20',
            'number' =>  '523',
        ]);
        City::create([
            'name' =>  'Santo Domingo Yodohino',
            'state_id' => '20',
            'number' =>  '524',
        ]);
        City::create([
            'name' =>  'Santo Domingo Zanatepec',
            'state_id' => '20',
            'number' =>  '525',
        ]);
        City::create([
            'name' =>  'Santos Reyes Nopala',
            'state_id' => '20',
            'number' =>  '526',
        ]);
        City::create([
            'name' =>  'Santos Reyes Pápalo',
            'state_id' => '20',
            'number' =>  '527',
        ]);
        City::create([
            'name' =>  'Santos Reyes Tepejillo',
            'state_id' => '20',
            'number' =>  '528',
        ]);
        City::create([
            'name' =>  'Santos Reyes Yucuná',
            'state_id' => '20',
            'number' =>  '529',
        ]);
        City::create([
            'name' =>  'Santo Tomás Jalieza',
            'state_id' => '20',
            'number' =>  '530',
        ]);
        City::create([
            'name' =>  'Santo Tomás Mazaltepec',
            'state_id' => '20',
            'number' =>  '531',
        ]);
        City::create([
            'name' =>  'Santo Tomás Ocotepec',
            'state_id' => '20',
            'number' =>  '532',
        ]);
        City::create([
            'name' =>  'Santo Tomás Tamazulapan',
            'state_id' => '20',
            'number' =>  '533',
        ]);
        City::create([
            'name' =>  'San Vicente Coatlán',
            'state_id' => '20',
            'number' =>  '534',
        ]);
        City::create([
            'name' =>  'San Vicente Lachixío',
            'state_id' => '20',
            'number' =>  '535',
        ]);
        City::create([
            'name' =>  'San Vicente Nuñú',
            'state_id' => '20',
            'number' =>  '536',
        ]);
        City::create([
            'name' =>  'Silacayoápam',
            'state_id' => '20',
            'number' =>  '537',
        ]);
        City::create([
            'name' =>  'Sitio de Xitlapehua',
            'state_id' => '20',
            'number' =>  '538',
        ]);
        City::create([
            'name' =>  'Soledad Etla',
            'state_id' => '20',
            'number' =>  '539',
        ]);
        City::create([
            'name' =>  'Villa de Tamazulápam del Progreso',
            'state_id' => '20',
            'number' =>  '540',
        ]);
        City::create([
            'name' =>  'Tanetze de Zaragoza',
            'state_id' => '20',
            'number' =>  '541',
        ]);
        City::create([
            'name' =>  'Taniche',
            'state_id' => '20',
            'number' =>  '542',
        ]);
        City::create([
            'name' =>  'Tataltepec de Valdés',
            'state_id' => '20',
            'number' =>  '543',
        ]);
        City::create([
            'name' =>  'Teococuilco de Marcos Pérez',
            'state_id' => '20',
            'number' =>  '544',
        ]);
        City::create([
            'name' =>  'Teotitlán de Flores Magón',
            'state_id' => '20',
            'number' =>  '545',
        ]);
        City::create([
            'name' =>  'Teotitlán del Valle',
            'state_id' => '20',
            'number' =>  '546',
        ]);
        City::create([
            'name' =>  'Teotongo',
            'state_id' => '20',
            'number' =>  '547',
        ]);
        City::create([
            'name' =>  'Tepelmeme Villa de Morelos',
            'state_id' => '20',
            'number' =>  '548',
        ]);
        City::create([
            'name' =>  'Heroica Villa Tezoatlán de Segura y Luna. Cuna de la Independencia de Oaxaca',
            'state_id' => '20',
            'number' =>  '549',
        ]);
        City::create([
            'name' =>  'San Jerónimo Tlacochahuaya',
            'state_id' => '20',
            'number' =>  '550',
        ]);
        City::create([
            'name' =>  'Tlacolula de Matamoros',
            'state_id' => '20',
            'number' =>  '551',
        ]);
        City::create([
            'name' =>  'Tlacotepec Plumas',
            'state_id' => '20',
            'number' =>  '552',
        ]);
        City::create([
            'name' =>  'Tlalixtac de Cabrera',
            'state_id' => '20',
            'number' =>  '553',
        ]);
        City::create([
            'name' =>  'Totontepec Villa de Morelos',
            'state_id' => '20',
            'number' =>  '554',
        ]);
        City::create([
            'name' =>  'Trinidad Zaachila',
            'state_id' => '20',
            'number' =>  '555',
        ]);
        City::create([
            'name' =>  'La Trinidad Vista Hermosa',
            'state_id' => '20',
            'number' =>  '556',
        ]);
        City::create([
            'name' =>  'Unión Hidalgo',
            'state_id' => '20',
            'number' =>  '557',
        ]);
        City::create([
            'name' =>  'Valerio Trujano',
            'state_id' => '20',
            'number' =>  '558',
        ]);
        City::create([
            'name' =>  'San Juan Bautista Valle Nacional',
            'state_id' => '20',
            'number' =>  '559',
        ]);
        City::create([
            'name' =>  'Villa Díaz Ordaz',
            'state_id' => '20',
            'number' =>  '560',
        ]);
        City::create([
            'name' =>  'Yaxe',
            'state_id' => '20',
            'number' =>  '561',
        ]);
        City::create([
            'name' =>  'Magdalena Yodocono de Porfirio Díaz',
            'state_id' => '20',
            'number' =>  '562',
        ]);
        City::create([
            'name' =>  'Yogana',
            'state_id' => '20',
            'number' =>  '563',
        ]);
        City::create([
            'name' =>  'Yutanduchi de Guerrero',
            'state_id' => '20',
            'number' =>  '564',
        ]);
        City::create([
            'name' =>  'Villa de Zaachila',
            'state_id' => '20',
            'number' =>  '565',
        ]);
        City::create([
            'name' =>  'San Mateo Yucutindoo',
            'state_id' => '20',
            'number' =>  '566',
        ]);
        City::create([
            'name' =>  'Zapotitlán Lagunas',
            'state_id' => '20',
            'number' =>  '567',
        ]);
        City::create([
            'name' =>  'Zapotitlán Palmas',
            'state_id' => '20',
            'number' =>  '568',
        ]);
        City::create([
            'name' =>  'Santa Inés de Zaragoza',
            'state_id' => '20',
            'number' =>  '569',
        ]);
        City::create([
            'name' =>  'Zimatlán de Álvarez',
            'state_id' => '20',
            'number' =>  '570',
        ]);
        City::create([
            'name' =>  'Acajete',
            'state_id' => '21',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acateno',
            'state_id' => '21',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Acatlán',
            'state_id' => '21',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Acatzingo',
            'state_id' => '21',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Acteopan',
            'state_id' => '21',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Ahuacatlán',
            'state_id' => '21',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Ahuatlán',
            'state_id' => '21',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Ahuazotepec',
            'state_id' => '21',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Ahuehuetitla',
            'state_id' => '21',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Ajalpan',
            'state_id' => '21',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Albino Zertuche',
            'state_id' => '21',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Aljojuca',
            'state_id' => '21',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Altepexi',
            'state_id' => '21',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Amixtlán',
            'state_id' => '21',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Amozoc',
            'state_id' => '21',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Aquixtla',
            'state_id' => '21',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Atempan',
            'state_id' => '21',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Atexcal',
            'state_id' => '21',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Atlixco',
            'state_id' => '21',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Atoyatempan',
            'state_id' => '21',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Atzala',
            'state_id' => '21',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Atzitzihuacán',
            'state_id' => '21',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Atzitzintla',
            'state_id' => '21',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Axutla',
            'state_id' => '21',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Ayotoxco de Guerrero',
            'state_id' => '21',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Calpan',
            'state_id' => '21',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Caltepec',
            'state_id' => '21',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Camocuautla',
            'state_id' => '21',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Caxhuacan',
            'state_id' => '21',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Coatepec',
            'state_id' => '21',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Coatzingo',
            'state_id' => '21',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Cohetzala',
            'state_id' => '21',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Cohuecan',
            'state_id' => '21',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Coronango',
            'state_id' => '21',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Coxcatlán',
            'state_id' => '21',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Coyomeapan',
            'state_id' => '21',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Coyotepec',
            'state_id' => '21',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Cuapiaxtla de Madero',
            'state_id' => '21',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Cuautempan',
            'state_id' => '21',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Cuautinchán',
            'state_id' => '21',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Cuautlancingo',
            'state_id' => '21',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Cuayuca de Andrade',
            'state_id' => '21',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Cuetzalan del Progreso',
            'state_id' => '21',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Cuyoaco',
            'state_id' => '21',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Chalchicomula de Sesma',
            'state_id' => '21',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Chapulco',
            'state_id' => '21',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Chiautla',
            'state_id' => '21',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Chiautzingo',
            'state_id' => '21',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Chiconcuautla',
            'state_id' => '21',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Chichiquila',
            'state_id' => '21',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Chietla',
            'state_id' => '21',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Chigmecatitlán',
            'state_id' => '21',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Chignahuapan',
            'state_id' => '21',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Chignautla',
            'state_id' => '21',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Chila',
            'state_id' => '21',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Chila de la Sal',
            'state_id' => '21',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Honey',
            'state_id' => '21',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Chilchotla',
            'state_id' => '21',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Chinantla',
            'state_id' => '21',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Domingo Arenas',
            'state_id' => '21',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Eloxochitlán',
            'state_id' => '21',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Epatlán',
            'state_id' => '21',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Esperanza',
            'state_id' => '21',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Francisco Z. Mena',
            'state_id' => '21',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'General Felipe Ángeles',
            'state_id' => '21',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Guadalupe',
            'state_id' => '21',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Guadalupe Victoria',
            'state_id' => '21',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Hermenegildo Galeana',
            'state_id' => '21',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Huaquechula',
            'state_id' => '21',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Huatlatlauca',
            'state_id' => '21',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Huauchinango',
            'state_id' => '21',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Huehuetla',
            'state_id' => '21',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Huehuetlán el Chico',
            'state_id' => '21',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Huejotzingo',
            'state_id' => '21',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Hueyapan',
            'state_id' => '21',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Hueytamalco',
            'state_id' => '21',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Hueytlalpan',
            'state_id' => '21',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Huitzilan de Serdán',
            'state_id' => '21',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Huitziltepec',
            'state_id' => '21',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Atlequizayan',
            'state_id' => '21',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Ixcamilpa de Guerrero',
            'state_id' => '21',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Ixcaquixtla',
            'state_id' => '21',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Ixtacamaxtitlán',
            'state_id' => '21',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Ixtepec',
            'state_id' => '21',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Izúcar de Matamoros',
            'state_id' => '21',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Jalpan',
            'state_id' => '21',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Jolalpan',
            'state_id' => '21',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Jonotla',
            'state_id' => '21',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Jopala',
            'state_id' => '21',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Juan C. Bonilla',
            'state_id' => '21',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Juan Galindo',
            'state_id' => '21',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Juan N. Méndez',
            'state_id' => '21',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Lafragua',
            'state_id' => '21',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Libres',
            'state_id' => '21',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'La Magdalena Tlatlauquitepec',
            'state_id' => '21',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Mazapiltepec de Juárez',
            'state_id' => '21',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Mixtla',
            'state_id' => '21',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Molcaxac',
            'state_id' => '21',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Cañada Morelos',
            'state_id' => '21',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Naupan',
            'state_id' => '21',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Nauzontla',
            'state_id' => '21',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Nealtican',
            'state_id' => '21',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Nicolás Bravo',
            'state_id' => '21',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Nopalucan',
            'state_id' => '21',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Ocotepec',
            'state_id' => '21',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Ocoyucan',
            'state_id' => '21',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Olintla',
            'state_id' => '21',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Oriental',
            'state_id' => '21',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Pahuatlán',
            'state_id' => '21',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'Palmar de Bravo',
            'state_id' => '21',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Pantepec',
            'state_id' => '21',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'Petlalcingo',
            'state_id' => '21',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'Piaxtla',
            'state_id' => '21',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Puebla',
            'state_id' => '21',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'Quecholac',
            'state_id' => '21',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'Quimixtlán',
            'state_id' => '21',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'Rafael Lara Grajales',
            'state_id' => '21',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'Los Reyes de Juárez',
            'state_id' => '21',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'San Andrés Cholula',
            'state_id' => '21',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'San Antonio Cañada',
            'state_id' => '21',
            'number' =>  '120',
        ]);
        City::create([
            'name' =>  'San Diego la Mesa Tochimiltzingo',
            'state_id' => '21',
            'number' =>  '121',
        ]);
        City::create([
            'name' =>  'San Felipe Teotlalcingo',
            'state_id' => '21',
            'number' =>  '122',
        ]);
        City::create([
            'name' =>  'San Felipe Tepatlán',
            'state_id' => '21',
            'number' =>  '123',
        ]);
        City::create([
            'name' =>  'San Gabriel Chilac',
            'state_id' => '21',
            'number' =>  '124',
        ]);
        City::create([
            'name' =>  'San Gregorio Atzompa',
            'state_id' => '21',
            'number' =>  '125',
        ]);
        City::create([
            'name' =>  'San Jerónimo Tecuanipan',
            'state_id' => '21',
            'number' =>  '126',
        ]);
        City::create([
            'name' =>  'San Jerónimo Xayacatlán',
            'state_id' => '21',
            'number' =>  '127',
        ]);
        City::create([
            'name' =>  'San José Chiapa',
            'state_id' => '21',
            'number' =>  '128',
        ]);
        City::create([
            'name' =>  'San José Miahuatlán',
            'state_id' => '21',
            'number' =>  '129',
        ]);
        City::create([
            'name' =>  'San Juan Atenco',
            'state_id' => '21',
            'number' =>  '130',
        ]);
        City::create([
            'name' =>  'San Juan Atzompa',
            'state_id' => '21',
            'number' =>  '131',
        ]);
        City::create([
            'name' =>  'San Martín Texmelucan',
            'state_id' => '21',
            'number' =>  '132',
        ]);
        City::create([
            'name' =>  'San Martín Totoltepec',
            'state_id' => '21',
            'number' =>  '133',
        ]);
        City::create([
            'name' =>  'San Matías Tlalancaleca',
            'state_id' => '21',
            'number' =>  '134',
        ]);
        City::create([
            'name' =>  'San Miguel Ixitlán',
            'state_id' => '21',
            'number' =>  '135',
        ]);
        City::create([
            'name' =>  'San Miguel Xoxtla',
            'state_id' => '21',
            'number' =>  '136',
        ]);
        City::create([
            'name' =>  'San Nicolás Buenos Aires',
            'state_id' => '21',
            'number' =>  '137',
        ]);
        City::create([
            'name' =>  'San Nicolás de los Ranchos',
            'state_id' => '21',
            'number' =>  '138',
        ]);
        City::create([
            'name' =>  'San Pablo Anicano',
            'state_id' => '21',
            'number' =>  '139',
        ]);
        City::create([
            'name' =>  'San Pedro Cholula',
            'state_id' => '21',
            'number' =>  '140',
        ]);
        City::create([
            'name' =>  'San Pedro Yeloixtlahuaca',
            'state_id' => '21',
            'number' =>  '141',
        ]);
        City::create([
            'name' =>  'San Salvador el Seco',
            'state_id' => '21',
            'number' =>  '142',
        ]);
        City::create([
            'name' =>  'San Salvador el Verde',
            'state_id' => '21',
            'number' =>  '143',
        ]);
        City::create([
            'name' =>  'San Salvador Huixcolotla',
            'state_id' => '21',
            'number' =>  '144',
        ]);
        City::create([
            'name' =>  'San Sebastián Tlacotepec',
            'state_id' => '21',
            'number' =>  '145',
        ]);
        City::create([
            'name' =>  'Santa Catarina Tlaltempan',
            'state_id' => '21',
            'number' =>  '146',
        ]);
        City::create([
            'name' =>  'Santa Inés Ahuatempan',
            'state_id' => '21',
            'number' =>  '147',
        ]);
        City::create([
            'name' =>  'Santa Isabel Cholula',
            'state_id' => '21',
            'number' =>  '148',
        ]);
        City::create([
            'name' =>  'Santiago Miahuatlán',
            'state_id' => '21',
            'number' =>  '149',
        ]);
        City::create([
            'name' =>  'Huehuetlán el Grande',
            'state_id' => '21',
            'number' =>  '150',
        ]);
        City::create([
            'name' =>  'Santo Tomás Hueyotlipan',
            'state_id' => '21',
            'number' =>  '151',
        ]);
        City::create([
            'name' =>  'Soltepec',
            'state_id' => '21',
            'number' =>  '152',
        ]);
        City::create([
            'name' =>  'Tecali de Herrera',
            'state_id' => '21',
            'number' =>  '153',
        ]);
        City::create([
            'name' =>  'Tecamachalco',
            'state_id' => '21',
            'number' =>  '154',
        ]);
        City::create([
            'name' =>  'Tecomatlán',
            'state_id' => '21',
            'number' =>  '155',
        ]);
        City::create([
            'name' =>  'Tehuacán',
            'state_id' => '21',
            'number' =>  '156',
        ]);
        City::create([
            'name' =>  'Tehuitzingo',
            'state_id' => '21',
            'number' =>  '157',
        ]);
        City::create([
            'name' =>  'Tenampulco',
            'state_id' => '21',
            'number' =>  '158',
        ]);
        City::create([
            'name' =>  'Teopantlán',
            'state_id' => '21',
            'number' =>  '159',
        ]);
        City::create([
            'name' =>  'Teotlalco',
            'state_id' => '21',
            'number' =>  '160',
        ]);
        City::create([
            'name' =>  'Tepanco de López',
            'state_id' => '21',
            'number' =>  '161',
        ]);
        City::create([
            'name' =>  'Tepango de Rodríguez',
            'state_id' => '21',
            'number' =>  '162',
        ]);
        City::create([
            'name' =>  'Tepatlaxco de Hidalgo',
            'state_id' => '21',
            'number' =>  '163',
        ]);
        City::create([
            'name' =>  'Tepeaca',
            'state_id' => '21',
            'number' =>  '164',
        ]);
        City::create([
            'name' =>  'Tepemaxalco',
            'state_id' => '21',
            'number' =>  '165',
        ]);
        City::create([
            'name' =>  'Tepeojuma',
            'state_id' => '21',
            'number' =>  '166',
        ]);
        City::create([
            'name' =>  'Tepetzintla',
            'state_id' => '21',
            'number' =>  '167',
        ]);
        City::create([
            'name' =>  'Tepexco',
            'state_id' => '21',
            'number' =>  '168',
        ]);
        City::create([
            'name' =>  'Tepexi de Rodríguez',
            'state_id' => '21',
            'number' =>  '169',
        ]);
        City::create([
            'name' =>  'Tepeyahualco',
            'state_id' => '21',
            'number' =>  '170',
        ]);
        City::create([
            'name' =>  'Tepeyahualco de Cuauhtémoc',
            'state_id' => '21',
            'number' =>  '171',
        ]);
        City::create([
            'name' =>  'Tetela de Ocampo',
            'state_id' => '21',
            'number' =>  '172',
        ]);
        City::create([
            'name' =>  'Teteles de Avila Castillo',
            'state_id' => '21',
            'number' =>  '173',
        ]);
        City::create([
            'name' =>  'Teziutlán',
            'state_id' => '21',
            'number' =>  '174',
        ]);
        City::create([
            'name' =>  'Tianguismanalco',
            'state_id' => '21',
            'number' =>  '175',
        ]);
        City::create([
            'name' =>  'Tilapa',
            'state_id' => '21',
            'number' =>  '176',
        ]);
        City::create([
            'name' =>  'Tlacotepec de Benito Juárez',
            'state_id' => '21',
            'number' =>  '177',
        ]);
        City::create([
            'name' =>  'Tlacuilotepec',
            'state_id' => '21',
            'number' =>  '178',
        ]);
        City::create([
            'name' =>  'Tlachichuca',
            'state_id' => '21',
            'number' =>  '179',
        ]);
        City::create([
            'name' =>  'Tlahuapan',
            'state_id' => '21',
            'number' =>  '180',
        ]);
        City::create([
            'name' =>  'Tlaltenango',
            'state_id' => '21',
            'number' =>  '181',
        ]);
        City::create([
            'name' =>  'Tlanepantla',
            'state_id' => '21',
            'number' =>  '182',
        ]);
        City::create([
            'name' =>  'Tlaola',
            'state_id' => '21',
            'number' =>  '183',
        ]);
        City::create([
            'name' =>  'Tlapacoya',
            'state_id' => '21',
            'number' =>  '184',
        ]);
        City::create([
            'name' =>  'Tlapanalá',
            'state_id' => '21',
            'number' =>  '185',
        ]);
        City::create([
            'name' =>  'Tlatlauquitepec',
            'state_id' => '21',
            'number' =>  '186',
        ]);
        City::create([
            'name' =>  'Tlaxco',
            'state_id' => '21',
            'number' =>  '187',
        ]);
        City::create([
            'name' =>  'Tochimilco',
            'state_id' => '21',
            'number' =>  '188',
        ]);
        City::create([
            'name' =>  'Tochtepec',
            'state_id' => '21',
            'number' =>  '189',
        ]);
        City::create([
            'name' =>  'Totoltepec de Guerrero',
            'state_id' => '21',
            'number' =>  '190',
        ]);
        City::create([
            'name' =>  'Tulcingo',
            'state_id' => '21',
            'number' =>  '191',
        ]);
        City::create([
            'name' =>  'Tuzamapan de Galeana',
            'state_id' => '21',
            'number' =>  '192',
        ]);
        City::create([
            'name' =>  'Tzicatlacoyan',
            'state_id' => '21',
            'number' =>  '193',
        ]);
        City::create([
            'name' =>  'Venustiano Carranza',
            'state_id' => '21',
            'number' =>  '194',
        ]);
        City::create([
            'name' =>  'Vicente Guerrero',
            'state_id' => '21',
            'number' =>  '195',
        ]);
        City::create([
            'name' =>  'Xayacatlán de Bravo',
            'state_id' => '21',
            'number' =>  '196',
        ]);
        City::create([
            'name' =>  'Xicotepec',
            'state_id' => '21',
            'number' =>  '197',
        ]);
        City::create([
            'name' =>  'Xicotlán',
            'state_id' => '21',
            'number' =>  '198',
        ]);
        City::create([
            'name' =>  'Xiutetelco',
            'state_id' => '21',
            'number' =>  '199',
        ]);
        City::create([
            'name' =>  'Xochiapulco',
            'state_id' => '21',
            'number' =>  '200',
        ]);
        City::create([
            'name' =>  'Xochiltepec',
            'state_id' => '21',
            'number' =>  '201',
        ]);
        City::create([
            'name' =>  'Xochitlán de Vicente Suárez',
            'state_id' => '21',
            'number' =>  '202',
        ]);
        City::create([
            'name' =>  'Xochitlán Todos Santos',
            'state_id' => '21',
            'number' =>  '203',
        ]);
        City::create([
            'name' =>  'Yaonáhuac',
            'state_id' => '21',
            'number' =>  '204',
        ]);
        City::create([
            'name' =>  'Yehualtepec',
            'state_id' => '21',
            'number' =>  '205',
        ]);
        City::create([
            'name' =>  'Zacapala',
            'state_id' => '21',
            'number' =>  '206',
        ]);
        City::create([
            'name' =>  'Zacapoaxtla',
            'state_id' => '21',
            'number' =>  '207',
        ]);
        City::create([
            'name' =>  'Zacatlán',
            'state_id' => '21',
            'number' =>  '208',
        ]);
        City::create([
            'name' =>  'Zapotitlán',
            'state_id' => '21',
            'number' =>  '209',
        ]);
        City::create([
            'name' =>  'Zapotitlán de Méndez',
            'state_id' => '21',
            'number' =>  '210',
        ]);
        City::create([
            'name' =>  'Zaragoza',
            'state_id' => '21',
            'number' =>  '211',
        ]);
        City::create([
            'name' =>  'Zautla',
            'state_id' => '21',
            'number' =>  '212',
        ]);
        City::create([
            'name' =>  'Zihuateutla',
            'state_id' => '21',
            'number' =>  '213',
        ]);
        City::create([
            'name' =>  'Zinacatepec',
            'state_id' => '21',
            'number' =>  '214',
        ]);
        City::create([
            'name' =>  'Zongozotla',
            'state_id' => '21',
            'number' =>  '215',
        ]);
        City::create([
            'name' =>  'Zoquiapan',
            'state_id' => '21',
            'number' =>  '216',
        ]);
        City::create([
            'name' =>  'Zoquitlán',
            'state_id' => '21',
            'number' =>  '217',
        ]);
        City::create([
            'name' =>  'Amealco de Bonfil',
            'state_id' => '22',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Pinal de Amoles',
            'state_id' => '22',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Arroyo Seco',
            'state_id' => '22',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Cadereyta de Montes',
            'state_id' => '22',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Colón',
            'state_id' => '22',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Corregidora',
            'state_id' => '22',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Ezequiel Montes',
            'state_id' => '22',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Huimilpan',
            'state_id' => '22',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Jalpan de Serra',
            'state_id' => '22',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Landa de Matamoros',
            'state_id' => '22',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'El Marqués',
            'state_id' => '22',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Pedro Escobedo',
            'state_id' => '22',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Peñamiller',
            'state_id' => '22',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Querétaro',
            'state_id' => '22',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'San Joaquín',
            'state_id' => '22',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'San Juan del Río',
            'state_id' => '22',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Tequisquiapan',
            'state_id' => '22',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Tolimán',
            'state_id' => '22',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Cozumel',
            'state_id' => '23',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Felipe Carrillo Puerto',
            'state_id' => '23',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Isla Mujeres',
            'state_id' => '23',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Othón P. Blanco',
            'state_id' => '23',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '23',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'José María Morelos',
            'state_id' => '23',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Lázaro Cárdenas',
            'state_id' => '23',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Solidaridad',
            'state_id' => '23',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Tulum',
            'state_id' => '23',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Bacalar',
            'state_id' => '23',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Ahualulco',
            'state_id' => '24',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Alaquines',
            'state_id' => '24',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Aquismón',
            'state_id' => '24',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Armadillo de los Infante',
            'state_id' => '24',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Cárdenas',
            'state_id' => '24',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Catorce',
            'state_id' => '24',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Cedral',
            'state_id' => '24',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Cerritos',
            'state_id' => '24',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Cerro de San Pedro',
            'state_id' => '24',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Ciudad del Maíz',
            'state_id' => '24',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Ciudad Fernández',
            'state_id' => '24',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Tancanhuitz',
            'state_id' => '24',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Ciudad Valles',
            'state_id' => '24',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Coxcatlán',
            'state_id' => '24',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Charcas',
            'state_id' => '24',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Ebano',
            'state_id' => '24',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Guadalcázar',
            'state_id' => '24',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Huehuetlán',
            'state_id' => '24',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Lagunillas',
            'state_id' => '24',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Matehuala',
            'state_id' => '24',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Mexquitic de Carmona',
            'state_id' => '24',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Moctezuma',
            'state_id' => '24',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Rayón',
            'state_id' => '24',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Rioverde',
            'state_id' => '24',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Salinas',
            'state_id' => '24',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'San Antonio',
            'state_id' => '24',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'San Ciro de Acosta',
            'state_id' => '24',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'San Luis Potosí',
            'state_id' => '24',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'San Martín Chalchicuautla',
            'state_id' => '24',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'San Nicolás Tolentino',
            'state_id' => '24',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Santa Catarina',
            'state_id' => '24',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Santa María del Río',
            'state_id' => '24',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Santo Domingo',
            'state_id' => '24',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'San Vicente Tancuayalab',
            'state_id' => '24',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Soledad de Graciano Sánchez',
            'state_id' => '24',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Tamasopo',
            'state_id' => '24',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Tamazunchale',
            'state_id' => '24',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Tampacán',
            'state_id' => '24',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Tampamolón Corona',
            'state_id' => '24',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Tamuín',
            'state_id' => '24',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Tanlajás',
            'state_id' => '24',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Tanquián de Escobedo',
            'state_id' => '24',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Tierra Nueva',
            'state_id' => '24',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Vanegas',
            'state_id' => '24',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Venado',
            'state_id' => '24',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Villa de Arriaga',
            'state_id' => '24',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Villa de Guadalupe',
            'state_id' => '24',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Villa de la Paz',
            'state_id' => '24',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Villa de Ramos',
            'state_id' => '24',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Villa de Reyes',
            'state_id' => '24',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Villa Hidalgo',
            'state_id' => '24',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Villa Juárez',
            'state_id' => '24',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Axtla de Terrazas',
            'state_id' => '24',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Xilitla',
            'state_id' => '24',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Zaragoza',
            'state_id' => '24',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Villa de Arista',
            'state_id' => '24',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Matlapa',
            'state_id' => '24',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'El Naranjo',
            'state_id' => '24',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Ahome',
            'state_id' => '25',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Angostura',
            'state_id' => '25',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Badiraguato',
            'state_id' => '25',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Concordia',
            'state_id' => '25',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Cosalá',
            'state_id' => '25',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Culiacán',
            'state_id' => '25',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Choix',
            'state_id' => '25',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Elota',
            'state_id' => '25',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Escuinapa',
            'state_id' => '25',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'El Fuerte',
            'state_id' => '25',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Guasave',
            'state_id' => '25',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Mazatlán',
            'state_id' => '25',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Mocorito',
            'state_id' => '25',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Rosario',
            'state_id' => '25',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Salvador Alvarado',
            'state_id' => '25',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'San Ignacio',
            'state_id' => '25',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Sinaloa',
            'state_id' => '25',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Navolato',
            'state_id' => '25',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Aconchi',
            'state_id' => '26',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Agua Prieta',
            'state_id' => '26',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Alamos',
            'state_id' => '26',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Altar',
            'state_id' => '26',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Arivechi',
            'state_id' => '26',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Arizpe',
            'state_id' => '26',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Atil',
            'state_id' => '26',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Bacadéhuachi',
            'state_id' => '26',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Bacanora',
            'state_id' => '26',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Bacerac',
            'state_id' => '26',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Bacoachi',
            'state_id' => '26',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Bácum',
            'state_id' => '26',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Banámichi',
            'state_id' => '26',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Baviácora',
            'state_id' => '26',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Bavispe',
            'state_id' => '26',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Benjamín Hill',
            'state_id' => '26',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Caborca',
            'state_id' => '26',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Cajeme',
            'state_id' => '26',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Cananea',
            'state_id' => '26',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Carbó',
            'state_id' => '26',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'La Colorada',
            'state_id' => '26',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Cucurpe',
            'state_id' => '26',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Cumpas',
            'state_id' => '26',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Divisaderos',
            'state_id' => '26',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Empalme',
            'state_id' => '26',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Etchojoa',
            'state_id' => '26',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Fronteras',
            'state_id' => '26',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Granados',
            'state_id' => '26',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Guaymas',
            'state_id' => '26',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Hermosillo',
            'state_id' => '26',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Huachinera',
            'state_id' => '26',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Huásabas',
            'state_id' => '26',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Huatabampo',
            'state_id' => '26',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Huépac',
            'state_id' => '26',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Imuris',
            'state_id' => '26',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Magdalena',
            'state_id' => '26',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Mazatán',
            'state_id' => '26',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Moctezuma',
            'state_id' => '26',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Naco',
            'state_id' => '26',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Nácori Chico',
            'state_id' => '26',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Nacozari de García',
            'state_id' => '26',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Navojoa',
            'state_id' => '26',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Nogales',
            'state_id' => '26',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Onavas',
            'state_id' => '26',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Opodepe',
            'state_id' => '26',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Oquitoa',
            'state_id' => '26',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Pitiquito',
            'state_id' => '26',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Puerto Peñasco',
            'state_id' => '26',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Quiriego',
            'state_id' => '26',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Rayón',
            'state_id' => '26',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Rosario',
            'state_id' => '26',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Sahuaripa',
            'state_id' => '26',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'San Felipe de Jesús',
            'state_id' => '26',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'San Javier',
            'state_id' => '26',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'San Luis Río Colorado',
            'state_id' => '26',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'San Miguel de Horcasitas',
            'state_id' => '26',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'San Pedro de la Cueva',
            'state_id' => '26',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Santa Ana',
            'state_id' => '26',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Santa Cruz',
            'state_id' => '26',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Sáric',
            'state_id' => '26',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Soyopa',
            'state_id' => '26',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Suaqui Grande',
            'state_id' => '26',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Tepache',
            'state_id' => '26',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Trincheras',
            'state_id' => '26',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Tubutama',
            'state_id' => '26',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Ures',
            'state_id' => '26',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Villa Hidalgo',
            'state_id' => '26',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Villa Pesqueira',
            'state_id' => '26',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Yécora',
            'state_id' => '26',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'General Plutarco Elías Calles',
            'state_id' => '26',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '26',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'San Ignacio Río Muerto',
            'state_id' => '26',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Balancán',
            'state_id' => '27',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Cárdenas',
            'state_id' => '27',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Centla',
            'state_id' => '27',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Centro',
            'state_id' => '27',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Comalcalco',
            'state_id' => '27',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Cunduacán',
            'state_id' => '27',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Emiliano Zapata',
            'state_id' => '27',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Huimanguillo',
            'state_id' => '27',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Jalapa',
            'state_id' => '27',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Jalpa de Méndez',
            'state_id' => '27',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Jonuta',
            'state_id' => '27',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Macuspana',
            'state_id' => '27',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Nacajuca',
            'state_id' => '27',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Paraíso',
            'state_id' => '27',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Tacotalpa',
            'state_id' => '27',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Teapa',
            'state_id' => '27',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Tenosique',
            'state_id' => '27',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Abasolo',
            'state_id' => '28',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Aldama',
            'state_id' => '28',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Altamira',
            'state_id' => '28',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Antiguo Morelos',
            'state_id' => '28',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Burgos',
            'state_id' => '28',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Bustamante',
            'state_id' => '28',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Camargo',
            'state_id' => '28',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Casas',
            'state_id' => '28',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Ciudad Madero',
            'state_id' => '28',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Cruillas',
            'state_id' => '28',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Gómez Farías',
            'state_id' => '28',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'González',
            'state_id' => '28',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Güémez',
            'state_id' => '28',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Guerrero',
            'state_id' => '28',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Gustavo Díaz Ordaz',
            'state_id' => '28',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Hidalgo',
            'state_id' => '28',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Jaumave',
            'state_id' => '28',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Jiménez',
            'state_id' => '28',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Llera',
            'state_id' => '28',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Mainero',
            'state_id' => '28',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'El Mante',
            'state_id' => '28',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Matamoros',
            'state_id' => '28',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Méndez',
            'state_id' => '28',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Mier',
            'state_id' => '28',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Miguel Alemán',
            'state_id' => '28',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Miquihuana',
            'state_id' => '28',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Nuevo Laredo',
            'state_id' => '28',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Nuevo Morelos',
            'state_id' => '28',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Ocampo',
            'state_id' => '28',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Padilla',
            'state_id' => '28',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Palmillas',
            'state_id' => '28',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Reynosa',
            'state_id' => '28',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Río Bravo',
            'state_id' => '28',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'San Carlos',
            'state_id' => '28',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'San Fernando',
            'state_id' => '28',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'San Nicolás',
            'state_id' => '28',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Soto la Marina',
            'state_id' => '28',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Tampico',
            'state_id' => '28',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Tula',
            'state_id' => '28',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Valle Hermoso',
            'state_id' => '28',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Victoria',
            'state_id' => '28',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Villagrán',
            'state_id' => '28',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Xicoténcatl',
            'state_id' => '28',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Amaxac de Guerrero',
            'state_id' => '29',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Apetatitlán de Antonio Carvajal',
            'state_id' => '29',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Atlangatepec',
            'state_id' => '29',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Atltzayanca',
            'state_id' => '29',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Apizaco',
            'state_id' => '29',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Calpulalpan',
            'state_id' => '29',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'El Carmen Tequexquitla',
            'state_id' => '29',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Cuapiaxtla',
            'state_id' => '29',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Cuaxomulco',
            'state_id' => '29',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Chiautempan',
            'state_id' => '29',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Muñoz de Domingo Arenas',
            'state_id' => '29',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Españita',
            'state_id' => '29',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Huamantla',
            'state_id' => '29',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Hueyotlipan',
            'state_id' => '29',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Ixtacuixtla de Mariano Matamoros',
            'state_id' => '29',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Ixtenco',
            'state_id' => '29',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Mazatecochco de José María Morelos',
            'state_id' => '29',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Contla de Juan Cuamatzi',
            'state_id' => '29',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Tepetitla de Lardizábal',
            'state_id' => '29',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Sanctórum de Lázaro Cárdenas',
            'state_id' => '29',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Nanacamilpa de Mariano Arista',
            'state_id' => '29',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Acuamanala de Miguel Hidalgo',
            'state_id' => '29',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Natívitas',
            'state_id' => '29',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Panotla',
            'state_id' => '29',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'San Pablo del Monte',
            'state_id' => '29',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Santa Cruz Tlaxcala',
            'state_id' => '29',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Tenancingo',
            'state_id' => '29',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Teolocholco',
            'state_id' => '29',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Tepeyanco',
            'state_id' => '29',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Terrenate',
            'state_id' => '29',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Tetla de la Solidaridad',
            'state_id' => '29',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Tetlatlahuca',
            'state_id' => '29',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Tlaxcala',
            'state_id' => '29',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Tlaxco',
            'state_id' => '29',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Tocatlán',
            'state_id' => '29',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Totolac',
            'state_id' => '29',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Ziltlaltépec de Trinidad Sánchez Santos',
            'state_id' => '29',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Tzompantepec',
            'state_id' => '29',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Xaloztoc',
            'state_id' => '29',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Xaltocan',
            'state_id' => '29',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Papalotla de Xicohténcatl',
            'state_id' => '29',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Xicohtzinco',
            'state_id' => '29',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Yauhquemehcan',
            'state_id' => '29',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Zacatelco',
            'state_id' => '29',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '29',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Emiliano Zapata',
            'state_id' => '29',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Lázaro Cárdenas',
            'state_id' => '29',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'La Magdalena Tlaltelulco',
            'state_id' => '29',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'San Damián Texóloc',
            'state_id' => '29',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'San Francisco Tetlanohcan',
            'state_id' => '29',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'San Jerónimo Zacualpan',
            'state_id' => '29',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'San José Teacalco',
            'state_id' => '29',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'San Juan Huactzinco',
            'state_id' => '29',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'San Lorenzo Axocomanitla',
            'state_id' => '29',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'San Lucas Tecopilco',
            'state_id' => '29',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Santa Ana Nopalucan',
            'state_id' => '29',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Santa Apolonia Teacalco',
            'state_id' => '29',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Santa Catarina Ayometla',
            'state_id' => '29',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Santa Cruz Quilehtla',
            'state_id' => '29',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Santa Isabel Xiloxoxtla',
            'state_id' => '29',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Acajete',
            'state_id' => '30',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acatlán',
            'state_id' => '30',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Acayucan',
            'state_id' => '30',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Actopan',
            'state_id' => '30',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Acula',
            'state_id' => '30',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Acultzingo',
            'state_id' => '30',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Camarón de Tejeda',
            'state_id' => '30',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Alpatláhuac',
            'state_id' => '30',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Alto Lucero de Gutiérrez Barrios',
            'state_id' => '30',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Altotonga',
            'state_id' => '30',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Alvarado',
            'state_id' => '30',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Amatitlán',
            'state_id' => '30',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Naranjos Amatlán',
            'state_id' => '30',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Amatlán de los Reyes',
            'state_id' => '30',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Angel R. Cabada',
            'state_id' => '30',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'La Antigua',
            'state_id' => '30',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Apazapan',
            'state_id' => '30',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Aquila',
            'state_id' => '30',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Astacinga',
            'state_id' => '30',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Atlahuilco',
            'state_id' => '30',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Atoyac',
            'state_id' => '30',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Atzacan',
            'state_id' => '30',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Atzalan',
            'state_id' => '30',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Tlaltetela',
            'state_id' => '30',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Ayahualulco',
            'state_id' => '30',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Banderilla',
            'state_id' => '30',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '30',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Boca del Río',
            'state_id' => '30',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Calcahualco',
            'state_id' => '30',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Camerino Z. Mendoza',
            'state_id' => '30',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Carrillo Puerto',
            'state_id' => '30',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Catemaco',
            'state_id' => '30',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Cazones de Herrera',
            'state_id' => '30',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Cerro Azul',
            'state_id' => '30',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Citlaltépetl',
            'state_id' => '30',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Coacoatzintla',
            'state_id' => '30',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Coahuitlán',
            'state_id' => '30',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Coatepec',
            'state_id' => '30',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Coatzacoalcos',
            'state_id' => '30',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Coatzintla',
            'state_id' => '30',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Coetzala',
            'state_id' => '30',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Colipa',
            'state_id' => '30',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Comapa',
            'state_id' => '30',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Córdoba',
            'state_id' => '30',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Cosamaloapan de Carpio',
            'state_id' => '30',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Cosautlán de Carvajal',
            'state_id' => '30',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Coscomatepec',
            'state_id' => '30',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Cosoleacaque',
            'state_id' => '30',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Cotaxtla',
            'state_id' => '30',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Coxquihui',
            'state_id' => '30',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Coyutla',
            'state_id' => '30',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Cuichapa',
            'state_id' => '30',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Cuitláhuac',
            'state_id' => '30',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Chacaltianguis',
            'state_id' => '30',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Chalma',
            'state_id' => '30',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Chiconamel',
            'state_id' => '30',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Chiconquiaco',
            'state_id' => '30',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Chicontepec',
            'state_id' => '30',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Chinameca',
            'state_id' => '30',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Chinampa de Gorostiza',
            'state_id' => '30',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Las Choapas',
            'state_id' => '30',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Chocamán',
            'state_id' => '30',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Chontla',
            'state_id' => '30',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Chumatlán',
            'state_id' => '30',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'Emiliano Zapata',
            'state_id' => '30',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Espinal',
            'state_id' => '30',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Filomeno Mata',
            'state_id' => '30',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Fortín',
            'state_id' => '30',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Gutiérrez Zamora',
            'state_id' => '30',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Hidalgotitlán',
            'state_id' => '30',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Huatusco',
            'state_id' => '30',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Huayacocotla',
            'state_id' => '30',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Hueyapan de Ocampo',
            'state_id' => '30',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Huiloapan de Cuauhtémoc',
            'state_id' => '30',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Ignacio de la Llave',
            'state_id' => '30',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Ilamatlán',
            'state_id' => '30',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Isla',
            'state_id' => '30',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Ixcatepec',
            'state_id' => '30',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Ixhuacán de los Reyes',
            'state_id' => '30',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Ixhuatlán del Café',
            'state_id' => '30',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Ixhuatlancillo',
            'state_id' => '30',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Ixhuatlán del Sureste',
            'state_id' => '30',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Ixhuatlán de Madero',
            'state_id' => '30',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Ixmatlahuacan',
            'state_id' => '30',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Ixtaczoquitlán',
            'state_id' => '30',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Jalacingo',
            'state_id' => '30',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Xalapa',
            'state_id' => '30',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Jalcomulco',
            'state_id' => '30',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Jáltipan',
            'state_id' => '30',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Jamapa',
            'state_id' => '30',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Jesús Carranza',
            'state_id' => '30',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Xico',
            'state_id' => '30',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Jilotepec',
            'state_id' => '30',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Juan Rodríguez Clara',
            'state_id' => '30',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Juchique de Ferrer',
            'state_id' => '30',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Landero y Coss',
            'state_id' => '30',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Lerdo de Tejada',
            'state_id' => '30',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Magdalena',
            'state_id' => '30',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Maltrata',
            'state_id' => '30',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Manlio Fabio Altamirano',
            'state_id' => '30',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Mariano Escobedo',
            'state_id' => '30',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Martínez de la Torre',
            'state_id' => '30',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Mecatlán',
            'state_id' => '30',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Mecayapan',
            'state_id' => '30',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Medellín de Bravo',
            'state_id' => '30',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Miahuatlán',
            'state_id' => '30',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Las Minas',
            'state_id' => '30',
            'number' =>  '107',
        ]);
        City::create([
            'name' =>  'Minatitlán',
            'state_id' => '30',
            'number' =>  '108',
        ]);
        City::create([
            'name' =>  'Misantla',
            'state_id' => '30',
            'number' =>  '109',
        ]);
        City::create([
            'name' =>  'Mixtla de Altamirano',
            'state_id' => '30',
            'number' =>  '110',
        ]);
        City::create([
            'name' =>  'Moloacán',
            'state_id' => '30',
            'number' =>  '111',
        ]);
        City::create([
            'name' =>  'Naolinco',
            'state_id' => '30',
            'number' =>  '112',
        ]);
        City::create([
            'name' =>  'Naranjal',
            'state_id' => '30',
            'number' =>  '113',
        ]);
        City::create([
            'name' =>  'Nautla',
            'state_id' => '30',
            'number' =>  '114',
        ]);
        City::create([
            'name' =>  'Nogales',
            'state_id' => '30',
            'number' =>  '115',
        ]);
        City::create([
            'name' =>  'Oluta',
            'state_id' => '30',
            'number' =>  '116',
        ]);
        City::create([
            'name' =>  'Omealca',
            'state_id' => '30',
            'number' =>  '117',
        ]);
        City::create([
            'name' =>  'Orizaba',
            'state_id' => '30',
            'number' =>  '118',
        ]);
        City::create([
            'name' =>  'Otatitlán',
            'state_id' => '30',
            'number' =>  '119',
        ]);
        City::create([
            'name' =>  'Oteapan',
            'state_id' => '30',
            'number' =>  '120',
        ]);
        City::create([
            'name' =>  'Ozuluama de Mascareñas',
            'state_id' => '30',
            'number' =>  '121',
        ]);
        City::create([
            'name' =>  'Pajapan',
            'state_id' => '30',
            'number' =>  '122',
        ]);
        City::create([
            'name' =>  'Pánuco',
            'state_id' => '30',
            'number' =>  '123',
        ]);
        City::create([
            'name' =>  'Papantla',
            'state_id' => '30',
            'number' =>  '124',
        ]);
        City::create([
            'name' =>  'Paso del Macho',
            'state_id' => '30',
            'number' =>  '125',
        ]);
        City::create([
            'name' =>  'Paso de Ovejas',
            'state_id' => '30',
            'number' =>  '126',
        ]);
        City::create([
            'name' =>  'La Perla',
            'state_id' => '30',
            'number' =>  '127',
        ]);
        City::create([
            'name' =>  'Perote',
            'state_id' => '30',
            'number' =>  '128',
        ]);
        City::create([
            'name' =>  'Platón Sánchez',
            'state_id' => '30',
            'number' =>  '129',
        ]);
        City::create([
            'name' =>  'Playa Vicente',
            'state_id' => '30',
            'number' =>  '130',
        ]);
        City::create([
            'name' =>  'Poza Rica de Hidalgo',
            'state_id' => '30',
            'number' =>  '131',
        ]);
        City::create([
            'name' =>  'Las Vigas de Ramírez',
            'state_id' => '30',
            'number' =>  '132',
        ]);
        City::create([
            'name' =>  'Pueblo Viejo',
            'state_id' => '30',
            'number' =>  '133',
        ]);
        City::create([
            'name' =>  'Puente Nacional',
            'state_id' => '30',
            'number' =>  '134',
        ]);
        City::create([
            'name' =>  'Rafael Delgado',
            'state_id' => '30',
            'number' =>  '135',
        ]);
        City::create([
            'name' =>  'Rafael Lucio',
            'state_id' => '30',
            'number' =>  '136',
        ]);
        City::create([
            'name' =>  'Los Reyes',
            'state_id' => '30',
            'number' =>  '137',
        ]);
        City::create([
            'name' =>  'Río Blanco',
            'state_id' => '30',
            'number' =>  '138',
        ]);
        City::create([
            'name' =>  'Saltabarranca',
            'state_id' => '30',
            'number' =>  '139',
        ]);
        City::create([
            'name' =>  'San Andrés Tenejapan',
            'state_id' => '30',
            'number' =>  '140',
        ]);
        City::create([
            'name' =>  'San Andrés Tuxtla',
            'state_id' => '30',
            'number' =>  '141',
        ]);
        City::create([
            'name' =>  'San Juan Evangelista',
            'state_id' => '30',
            'number' =>  '142',
        ]);
        City::create([
            'name' =>  'Santiago Tuxtla',
            'state_id' => '30',
            'number' =>  '143',
        ]);
        City::create([
            'name' =>  'Sayula de Alemán',
            'state_id' => '30',
            'number' =>  '144',
        ]);
        City::create([
            'name' =>  'Soconusco',
            'state_id' => '30',
            'number' =>  '145',
        ]);
        City::create([
            'name' =>  'Sochiapa',
            'state_id' => '30',
            'number' =>  '146',
        ]);
        City::create([
            'name' =>  'Soledad Atzompa',
            'state_id' => '30',
            'number' =>  '147',
        ]);
        City::create([
            'name' =>  'Soledad de Doblado',
            'state_id' => '30',
            'number' =>  '148',
        ]);
        City::create([
            'name' =>  'Soteapan',
            'state_id' => '30',
            'number' =>  '149',
        ]);
        City::create([
            'name' =>  'Tamalín',
            'state_id' => '30',
            'number' =>  '150',
        ]);
        City::create([
            'name' =>  'Tamiahua',
            'state_id' => '30',
            'number' =>  '151',
        ]);
        City::create([
            'name' =>  'Tampico Alto',
            'state_id' => '30',
            'number' =>  '152',
        ]);
        City::create([
            'name' =>  'Tancoco',
            'state_id' => '30',
            'number' =>  '153',
        ]);
        City::create([
            'name' =>  'Tantima',
            'state_id' => '30',
            'number' =>  '154',
        ]);
        City::create([
            'name' =>  'Tantoyuca',
            'state_id' => '30',
            'number' =>  '155',
        ]);
        City::create([
            'name' =>  'Tatatila',
            'state_id' => '30',
            'number' =>  '156',
        ]);
        City::create([
            'name' =>  'Castillo de Teayo',
            'state_id' => '30',
            'number' =>  '157',
        ]);
        City::create([
            'name' =>  'Tecolutla',
            'state_id' => '30',
            'number' =>  '158',
        ]);
        City::create([
            'name' =>  'Tehuipango',
            'state_id' => '30',
            'number' =>  '159',
        ]);
        City::create([
            'name' =>  'Álamo Temapache',
            'state_id' => '30',
            'number' =>  '160',
        ]);
        City::create([
            'name' =>  'Tempoal',
            'state_id' => '30',
            'number' =>  '161',
        ]);
        City::create([
            'name' =>  'Tenampa',
            'state_id' => '30',
            'number' =>  '162',
        ]);
        City::create([
            'name' =>  'Tenochtitlán',
            'state_id' => '30',
            'number' =>  '163',
        ]);
        City::create([
            'name' =>  'Teocelo',
            'state_id' => '30',
            'number' =>  '164',
        ]);
        City::create([
            'name' =>  'Tepatlaxco',
            'state_id' => '30',
            'number' =>  '165',
        ]);
        City::create([
            'name' =>  'Tepetlán',
            'state_id' => '30',
            'number' =>  '166',
        ]);
        City::create([
            'name' =>  'Tepetzintla',
            'state_id' => '30',
            'number' =>  '167',
        ]);
        City::create([
            'name' =>  'Tequila',
            'state_id' => '30',
            'number' =>  '168',
        ]);
        City::create([
            'name' =>  'José Azueta',
            'state_id' => '30',
            'number' =>  '169',
        ]);
        City::create([
            'name' =>  'Texcatepec',
            'state_id' => '30',
            'number' =>  '170',
        ]);
        City::create([
            'name' =>  'Texhuacán',
            'state_id' => '30',
            'number' =>  '171',
        ]);
        City::create([
            'name' =>  'Texistepec',
            'state_id' => '30',
            'number' =>  '172',
        ]);
        City::create([
            'name' =>  'Tezonapa',
            'state_id' => '30',
            'number' =>  '173',
        ]);
        City::create([
            'name' =>  'Tierra Blanca',
            'state_id' => '30',
            'number' =>  '174',
        ]);
        City::create([
            'name' =>  'Tihuatlán',
            'state_id' => '30',
            'number' =>  '175',
        ]);
        City::create([
            'name' =>  'Tlacojalpan',
            'state_id' => '30',
            'number' =>  '176',
        ]);
        City::create([
            'name' =>  'Tlacolulan',
            'state_id' => '30',
            'number' =>  '177',
        ]);
        City::create([
            'name' =>  'Tlacotalpan',
            'state_id' => '30',
            'number' =>  '178',
        ]);
        City::create([
            'name' =>  'Tlacotepec de Mejía',
            'state_id' => '30',
            'number' =>  '179',
        ]);
        City::create([
            'name' =>  'Tlachichilco',
            'state_id' => '30',
            'number' =>  '180',
        ]);
        City::create([
            'name' =>  'Tlalixcoyan',
            'state_id' => '30',
            'number' =>  '181',
        ]);
        City::create([
            'name' =>  'Tlalnelhuayocan',
            'state_id' => '30',
            'number' =>  '182',
        ]);
        City::create([
            'name' =>  'Tlapacoyan',
            'state_id' => '30',
            'number' =>  '183',
        ]);
        City::create([
            'name' =>  'Tlaquilpa',
            'state_id' => '30',
            'number' =>  '184',
        ]);
        City::create([
            'name' =>  'Tlilapan',
            'state_id' => '30',
            'number' =>  '185',
        ]);
        City::create([
            'name' =>  'Tomatlán',
            'state_id' => '30',
            'number' =>  '186',
        ]);
        City::create([
            'name' =>  'Tonayán',
            'state_id' => '30',
            'number' =>  '187',
        ]);
        City::create([
            'name' =>  'Totutla',
            'state_id' => '30',
            'number' =>  '188',
        ]);
        City::create([
            'name' =>  'Tuxpan',
            'state_id' => '30',
            'number' =>  '189',
        ]);
        City::create([
            'name' =>  'Tuxtilla',
            'state_id' => '30',
            'number' =>  '190',
        ]);
        City::create([
            'name' =>  'Ursulo Galván',
            'state_id' => '30',
            'number' =>  '191',
        ]);
        City::create([
            'name' =>  'Vega de Alatorre',
            'state_id' => '30',
            'number' =>  '192',
        ]);
        City::create([
            'name' =>  'Veracruz',
            'state_id' => '30',
            'number' =>  '193',
        ]);
        City::create([
            'name' =>  'Villa Aldama',
            'state_id' => '30',
            'number' =>  '194',
        ]);
        City::create([
            'name' =>  'Xoxocotla',
            'state_id' => '30',
            'number' =>  '195',
        ]);
        City::create([
            'name' =>  'Yanga',
            'state_id' => '30',
            'number' =>  '196',
        ]);
        City::create([
            'name' =>  'Yecuatla',
            'state_id' => '30',
            'number' =>  '197',
        ]);
        City::create([
            'name' =>  'Zacualpan',
            'state_id' => '30',
            'number' =>  '198',
        ]);
        City::create([
            'name' =>  'Zaragoza',
            'state_id' => '30',
            'number' =>  '199',
        ]);
        City::create([
            'name' =>  'Zentla',
            'state_id' => '30',
            'number' =>  '200',
        ]);
        City::create([
            'name' =>  'Zongolica',
            'state_id' => '30',
            'number' =>  '201',
        ]);
        City::create([
            'name' =>  'Zontecomatlán de López y Fuentes',
            'state_id' => '30',
            'number' =>  '202',
        ]);
        City::create([
            'name' =>  'Zozocolco de Hidalgo',
            'state_id' => '30',
            'number' =>  '203',
        ]);
        City::create([
            'name' =>  'Agua Dulce',
            'state_id' => '30',
            'number' =>  '204',
        ]);
        City::create([
            'name' =>  'El Higo',
            'state_id' => '30',
            'number' =>  '205',
        ]);
        City::create([
            'name' =>  'Nanchital de Lázaro Cárdenas del Río',
            'state_id' => '30',
            'number' =>  '206',
        ]);
        City::create([
            'name' =>  'Tres Valles',
            'state_id' => '30',
            'number' =>  '207',
        ]);
        City::create([
            'name' =>  'Carlos A. Carrillo',
            'state_id' => '30',
            'number' =>  '208',
        ]);
        City::create([
            'name' =>  'Tatahuicapan de Juárez',
            'state_id' => '30',
            'number' =>  '209',
        ]);
        City::create([
            'name' =>  'Uxpanapa',
            'state_id' => '30',
            'number' =>  '210',
        ]);
        City::create([
            'name' =>  'San Rafael',
            'state_id' => '30',
            'number' =>  '211',
        ]);
        City::create([
            'name' =>  'Santiago Sochiapan',
            'state_id' => '30',
            'number' =>  '212',
        ]);
        City::create([
            'name' =>  'Abalá',
            'state_id' => '31',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Acanceh',
            'state_id' => '31',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Akil',
            'state_id' => '31',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Baca',
            'state_id' => '31',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Bokobá',
            'state_id' => '31',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Buctzotz',
            'state_id' => '31',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Cacalchén',
            'state_id' => '31',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Calotmul',
            'state_id' => '31',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Cansahcab',
            'state_id' => '31',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Cantamayec',
            'state_id' => '31',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Celestún',
            'state_id' => '31',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Cenotillo',
            'state_id' => '31',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'Conkal',
            'state_id' => '31',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'Cuncunul',
            'state_id' => '31',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'Cuzamá',
            'state_id' => '31',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'Chacsinkín',
            'state_id' => '31',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Chankom',
            'state_id' => '31',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Chapab',
            'state_id' => '31',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Chemax',
            'state_id' => '31',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Chicxulub Pueblo',
            'state_id' => '31',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Chichimilá',
            'state_id' => '31',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Chikindzonot',
            'state_id' => '31',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Chocholá',
            'state_id' => '31',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Chumayel',
            'state_id' => '31',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Dzán',
            'state_id' => '31',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Dzemul',
            'state_id' => '31',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Dzidzantún',
            'state_id' => '31',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Dzilam de Bravo',
            'state_id' => '31',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Dzilam González',
            'state_id' => '31',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Dzitás',
            'state_id' => '31',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Dzoncauich',
            'state_id' => '31',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Espita',
            'state_id' => '31',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Halachó',
            'state_id' => '31',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Hocabá',
            'state_id' => '31',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Hoctún',
            'state_id' => '31',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Homún',
            'state_id' => '31',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Huhí',
            'state_id' => '31',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Hunucmá',
            'state_id' => '31',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Ixil',
            'state_id' => '31',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Izamal',
            'state_id' => '31',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'Kanasín',
            'state_id' => '31',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Kantunil',
            'state_id' => '31',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Kaua',
            'state_id' => '31',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Kinchil',
            'state_id' => '31',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Kopomá',
            'state_id' => '31',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Mama',
            'state_id' => '31',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Maní',
            'state_id' => '31',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Maxcanú',
            'state_id' => '31',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Mayapán',
            'state_id' => '31',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Mérida',
            'state_id' => '31',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Mocochá',
            'state_id' => '31',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Motul',
            'state_id' => '31',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Muna',
            'state_id' => '31',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Muxupip',
            'state_id' => '31',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Opichén',
            'state_id' => '31',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Oxkutzcab',
            'state_id' => '31',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Panabá',
            'state_id' => '31',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Peto',
            'state_id' => '31',
            'number' =>  '058',
        ]);
        City::create([
            'name' =>  'Progreso',
            'state_id' => '31',
            'number' =>  '059',
        ]);
        City::create([
            'name' =>  'Quintana Roo',
            'state_id' => '31',
            'number' =>  '060',
        ]);
        City::create([
            'name' =>  'Río Lagartos',
            'state_id' => '31',
            'number' =>  '061',
        ]);
        City::create([
            'name' =>  'Sacalum',
            'state_id' => '31',
            'number' =>  '062',
        ]);
        City::create([
            'name' =>  'Samahil',
            'state_id' => '31',
            'number' =>  '063',
        ]);
        City::create([
            'name' =>  'Sanahcat',
            'state_id' => '31',
            'number' =>  '064',
        ]);
        City::create([
            'name' =>  'San Felipe',
            'state_id' => '31',
            'number' =>  '065',
        ]);
        City::create([
            'name' =>  'Santa Elena',
            'state_id' => '31',
            'number' =>  '066',
        ]);
        City::create([
            'name' =>  'Seyé',
            'state_id' => '31',
            'number' =>  '067',
        ]);
        City::create([
            'name' =>  'Sinanché',
            'state_id' => '31',
            'number' =>  '068',
        ]);
        City::create([
            'name' =>  'Sotuta',
            'state_id' => '31',
            'number' =>  '069',
        ]);
        City::create([
            'name' =>  'Sucilá',
            'state_id' => '31',
            'number' =>  '070',
        ]);
        City::create([
            'name' =>  'Sudzal',
            'state_id' => '31',
            'number' =>  '071',
        ]);
        City::create([
            'name' =>  'Suma',
            'state_id' => '31',
            'number' =>  '072',
        ]);
        City::create([
            'name' =>  'Tahdziú',
            'state_id' => '31',
            'number' =>  '073',
        ]);
        City::create([
            'name' =>  'Tahmek',
            'state_id' => '31',
            'number' =>  '074',
        ]);
        City::create([
            'name' =>  'Teabo',
            'state_id' => '31',
            'number' =>  '075',
        ]);
        City::create([
            'name' =>  'Tecoh',
            'state_id' => '31',
            'number' =>  '076',
        ]);
        City::create([
            'name' =>  'Tekal de Venegas',
            'state_id' => '31',
            'number' =>  '077',
        ]);
        City::create([
            'name' =>  'Tekantó',
            'state_id' => '31',
            'number' =>  '078',
        ]);
        City::create([
            'name' =>  'Tekax',
            'state_id' => '31',
            'number' =>  '079',
        ]);
        City::create([
            'name' =>  'Tekit',
            'state_id' => '31',
            'number' =>  '080',
        ]);
        City::create([
            'name' =>  'Tekom',
            'state_id' => '31',
            'number' =>  '081',
        ]);
        City::create([
            'name' =>  'Telchac Pueblo',
            'state_id' => '31',
            'number' =>  '082',
        ]);
        City::create([
            'name' =>  'Telchac Puerto',
            'state_id' => '31',
            'number' =>  '083',
        ]);
        City::create([
            'name' =>  'Temax',
            'state_id' => '31',
            'number' =>  '084',
        ]);
        City::create([
            'name' =>  'Temozón',
            'state_id' => '31',
            'number' =>  '085',
        ]);
        City::create([
            'name' =>  'Tepakán',
            'state_id' => '31',
            'number' =>  '086',
        ]);
        City::create([
            'name' =>  'Tetiz',
            'state_id' => '31',
            'number' =>  '087',
        ]);
        City::create([
            'name' =>  'Teya',
            'state_id' => '31',
            'number' =>  '088',
        ]);
        City::create([
            'name' =>  'Ticul',
            'state_id' => '31',
            'number' =>  '089',
        ]);
        City::create([
            'name' =>  'Timucuy',
            'state_id' => '31',
            'number' =>  '090',
        ]);
        City::create([
            'name' =>  'Tinum',
            'state_id' => '31',
            'number' =>  '091',
        ]);
        City::create([
            'name' =>  'Tixcacalcupul',
            'state_id' => '31',
            'number' =>  '092',
        ]);
        City::create([
            'name' =>  'Tixkokob',
            'state_id' => '31',
            'number' =>  '093',
        ]);
        City::create([
            'name' =>  'Tixmehuac',
            'state_id' => '31',
            'number' =>  '094',
        ]);
        City::create([
            'name' =>  'Tixpéhual',
            'state_id' => '31',
            'number' =>  '095',
        ]);
        City::create([
            'name' =>  'Tizimín',
            'state_id' => '31',
            'number' =>  '096',
        ]);
        City::create([
            'name' =>  'Tunkás',
            'state_id' => '31',
            'number' =>  '097',
        ]);
        City::create([
            'name' =>  'Tzucacab',
            'state_id' => '31',
            'number' =>  '098',
        ]);
        City::create([
            'name' =>  'Uayma',
            'state_id' => '31',
            'number' =>  '099',
        ]);
        City::create([
            'name' =>  'Ucú',
            'state_id' => '31',
            'number' =>  '100',
        ]);
        City::create([
            'name' =>  'Umán',
            'state_id' => '31',
            'number' =>  '101',
        ]);
        City::create([
            'name' =>  'Valladolid',
            'state_id' => '31',
            'number' =>  '102',
        ]);
        City::create([
            'name' =>  'Xocchel',
            'state_id' => '31',
            'number' =>  '103',
        ]);
        City::create([
            'name' =>  'Yaxcabá',
            'state_id' => '31',
            'number' =>  '104',
        ]);
        City::create([
            'name' =>  'Yaxkukul',
            'state_id' => '31',
            'number' =>  '105',
        ]);
        City::create([
            'name' =>  'Yobaín',
            'state_id' => '31',
            'number' =>  '106',
        ]);
        City::create([
            'name' =>  'Apozol',
            'state_id' => '32',
            'number' =>  '001',
        ]);
        City::create([
            'name' =>  'Apulco',
            'state_id' => '32',
            'number' =>  '002',
        ]);
        City::create([
            'name' =>  'Atolinga',
            'state_id' => '32',
            'number' =>  '003',
        ]);
        City::create([
            'name' =>  'Benito Juárez',
            'state_id' => '32',
            'number' =>  '004',
        ]);
        City::create([
            'name' =>  'Calera',
            'state_id' => '32',
            'number' =>  '005',
        ]);
        City::create([
            'name' =>  'Cañitas de Felipe Pescador',
            'state_id' => '32',
            'number' =>  '006',
        ]);
        City::create([
            'name' =>  'Concepción del Oro',
            'state_id' => '32',
            'number' =>  '007',
        ]);
        City::create([
            'name' =>  'Cuauhtémoc',
            'state_id' => '32',
            'number' =>  '008',
        ]);
        City::create([
            'name' =>  'Chalchihuites',
            'state_id' => '32',
            'number' =>  '009',
        ]);
        City::create([
            'name' =>  'Fresnillo',
            'state_id' => '32',
            'number' =>  '010',
        ]);
        City::create([
            'name' =>  'Trinidad García de la Cadena',
            'state_id' => '32',
            'number' =>  '011',
        ]);
        City::create([
            'name' =>  'Genaro Codina',
            'state_id' => '32',
            'number' =>  '012',
        ]);
        City::create([
            'name' =>  'General Enrique Estrada',
            'state_id' => '32',
            'number' =>  '013',
        ]);
        City::create([
            'name' =>  'General Francisco R. Murguía',
            'state_id' => '32',
            'number' =>  '014',
        ]);
        City::create([
            'name' =>  'El Plateado de Joaquín Amaro',
            'state_id' => '32',
            'number' =>  '015',
        ]);
        City::create([
            'name' =>  'General Pánfilo Natera',
            'state_id' => '32',
            'number' =>  '016',
        ]);
        City::create([
            'name' =>  'Guadalupe',
            'state_id' => '32',
            'number' =>  '017',
        ]);
        City::create([
            'name' =>  'Huanusco',
            'state_id' => '32',
            'number' =>  '018',
        ]);
        City::create([
            'name' =>  'Jalpa',
            'state_id' => '32',
            'number' =>  '019',
        ]);
        City::create([
            'name' =>  'Jerez',
            'state_id' => '32',
            'number' =>  '020',
        ]);
        City::create([
            'name' =>  'Jiménez del Teul',
            'state_id' => '32',
            'number' =>  '021',
        ]);
        City::create([
            'name' =>  'Juan Aldama',
            'state_id' => '32',
            'number' =>  '022',
        ]);
        City::create([
            'name' =>  'Juchipila',
            'state_id' => '32',
            'number' =>  '023',
        ]);
        City::create([
            'name' =>  'Loreto',
            'state_id' => '32',
            'number' =>  '024',
        ]);
        City::create([
            'name' =>  'Luis Moya',
            'state_id' => '32',
            'number' =>  '025',
        ]);
        City::create([
            'name' =>  'Mazapil',
            'state_id' => '32',
            'number' =>  '026',
        ]);
        City::create([
            'name' =>  'Melchor Ocampo',
            'state_id' => '32',
            'number' =>  '027',
        ]);
        City::create([
            'name' =>  'Mezquital del Oro',
            'state_id' => '32',
            'number' =>  '028',
        ]);
        City::create([
            'name' =>  'Miguel Auza',
            'state_id' => '32',
            'number' =>  '029',
        ]);
        City::create([
            'name' =>  'Momax',
            'state_id' => '32',
            'number' =>  '030',
        ]);
        City::create([
            'name' =>  'Monte Escobedo',
            'state_id' => '32',
            'number' =>  '031',
        ]);
        City::create([
            'name' =>  'Morelos',
            'state_id' => '32',
            'number' =>  '032',
        ]);
        City::create([
            'name' =>  'Moyahua de Estrada',
            'state_id' => '32',
            'number' =>  '033',
        ]);
        City::create([
            'name' =>  'Nochistlán de Mejía',
            'state_id' => '32',
            'number' =>  '034',
        ]);
        City::create([
            'name' =>  'Noria de Ángeles',
            'state_id' => '32',
            'number' =>  '035',
        ]);
        City::create([
            'name' =>  'Ojocaliente',
            'state_id' => '32',
            'number' =>  '036',
        ]);
        City::create([
            'name' =>  'Pánuco',
            'state_id' => '32',
            'number' =>  '037',
        ]);
        City::create([
            'name' =>  'Pinos',
            'state_id' => '32',
            'number' =>  '038',
        ]);
        City::create([
            'name' =>  'Río Grande',
            'state_id' => '32',
            'number' =>  '039',
        ]);
        City::create([
            'name' =>  'Sain Alto',
            'state_id' => '32',
            'number' =>  '040',
        ]);
        City::create([
            'name' =>  'El Salvador',
            'state_id' => '32',
            'number' =>  '041',
        ]);
        City::create([
            'name' =>  'Sombrerete',
            'state_id' => '32',
            'number' =>  '042',
        ]);
        City::create([
            'name' =>  'Susticacán',
            'state_id' => '32',
            'number' =>  '043',
        ]);
        City::create([
            'name' =>  'Tabasco',
            'state_id' => '32',
            'number' =>  '044',
        ]);
        City::create([
            'name' =>  'Tepechitlán',
            'state_id' => '32',
            'number' =>  '045',
        ]);
        City::create([
            'name' =>  'Tepetongo',
            'state_id' => '32',
            'number' =>  '046',
        ]);
        City::create([
            'name' =>  'Teúl de González Ortega',
            'state_id' => '32',
            'number' =>  '047',
        ]);
        City::create([
            'name' =>  'Tlaltenango de Sánchez Román',
            'state_id' => '32',
            'number' =>  '048',
        ]);
        City::create([
            'name' =>  'Valparaíso',
            'state_id' => '32',
            'number' =>  '049',
        ]);
        City::create([
            'name' =>  'Vetagrande',
            'state_id' => '32',
            'number' =>  '050',
        ]);
        City::create([
            'name' =>  'Villa de Cos',
            'state_id' => '32',
            'number' =>  '051',
        ]);
        City::create([
            'name' =>  'Villa García',
            'state_id' => '32',
            'number' =>  '052',
        ]);
        City::create([
            'name' =>  'Villa González Ortega',
            'state_id' => '32',
            'number' =>  '053',
        ]);
        City::create([
            'name' =>  'Villa Hidalgo',
            'state_id' => '32',
            'number' =>  '054',
        ]);
        City::create([
            'name' =>  'Villanueva',
            'state_id' => '32',
            'number' =>  '055',
        ]);
        City::create([
            'name' =>  'Zacatecas',
            'state_id' => '32',
            'number' =>  '056',
        ]);
        City::create([
            'name' =>  'Trancoso',
            'state_id' => '32',
            'number' =>  '057',
        ]);
        City::create([
            'name' =>  'Santa María de la Paz',
            'state_id' => '32',
            'number' =>  '058',
        ]);

    }
}
