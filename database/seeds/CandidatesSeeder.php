<?php

use Illuminate\Database\Seeder;
use App\Candidate;

class CandidatesSeeder extends Seeder
{
    public function run()
    {
        Candidate::create([
            'user_id' => 1,
            'profile_picture' => null,
            'curriculum' => null,
            'seeking_work' => '0',
            'names' => 'Nombre Nombre 1',
            'maternal_name' => 'Apellido Maternal 1',
            'paternal_name' => 'Apellido Paternal 1',
            'country' => 'Pais 1',
            'state' => 'Estado 1',
            'city' => 'Ciudad 1',
            'district' => 'Colonia 1',
            'street' => 'Calle 1',
            'house_number' => 'Numero de Casa 1',
            'home_phone' => '11111111',
            'mobile_phone' => '3311111111',
            'email' => 'correocandidato1@correo.com',
            'gender' => 'Hombre',
            'curp' => 'CANANSQAJDHG1',
            'rfc' => 'SAUBSAN839HDUISBIB',
            'nss' => null
        ]);
        
        Candidate::create([
            'user_id' => 1,
            'profile_picture' => null,
            'curriculum' => null,
            'seeking_work' => '0',
            'names' => 'Nombre Nombre 2',
            'maternal_name' => 'Apellido Maternal 2',
            'paternal_name' => 'Apellido Paternal 2',
            'country' => 'Pais 2',
            'state' => 'Estado 2',
            'city' => 'Ciudad 2',
            'district' => 'Colonia 2',
            'street' => 'Calle 2',
            'house_number' => 'Numero de Casa 2',
            'home_phone' => '22222222',
            'mobile_phone' => '33222222',
            'email' => 'correocandidato2@correo.com',
            'gender' => 'Hombre',
            'curp' => 'CANANSQAJDHG2',
            'rfc' => 'SAUBSAN829HDUISBIB',
            'nss' => null
        ]);

        Candidate::create([
            'user_id' => 1,
            'profile_picture' => null,
            'curriculum' => null,
            'seeking_work' => '0',
            'names' => 'Nombre Nombre 3',
            'maternal_name' => 'Apellido Maternal 3',
            'paternal_name' => 'Apellido Paternal 3',
            'country' => 'Pais 3',
            'state' => 'Estado 3',
            'city' => 'Ciudad 3',
            'district' => 'Colonia 3',
            'street' => 'Calle 3',
            'house_number' => 'Numero de Casa 3',
            'home_phone' => '33333333',
            'mobile_phone' => '3333333333',
            'email' => 'correocandidato3@correo.com',
            'gender' => 'Hombre',
            'curp' => 'CANANSQAJDHG3',
            'rfc' => 'SAUBSAN839HDUISBIB',
            'nss' => null
        ]);
    }
}
