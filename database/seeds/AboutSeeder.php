<?php

use Illuminate\Database\Seeder;
use App\About;

class AboutSeeder extends Seeder
{
    public function run()
    {
        About::create([
            'text' => 'Lorem ipsum dolor sit amet, probo dicant disputationi pri ut. Melius complectitur ex vis, no cum aeterno volumus epicuri. Eam dicunt vidisse corrumpit ea. Nec probo partem ea, ad mel alia adversarium, eos eu putent recteque molestiae. Mea simul causae an. Modus civibus expetendis eu per, ut vis eros duis vitae.

            Fierent deterruisset vix an, ei usu habeo deseruisse, eu mea tota reprehendunt. Ut eam solet putent, cum salutatus definiebas ei. Insolens erroribus te duo, te homero percipit sadipscing eam. Paulo probatus reprehendunt duo ad, pro id aeque postea accusata. Dicit legendos mea id, sit ad dicit fabulas dignissim. Mea doming consetetur an.'
        ]);

        About::create([
            'image_url' => 'about_images/category_icon_1.png'
        ]);

        About::create([
            'image_url' => 'about_images/category_icon_2.png'
        ]);

        About::create([
            'image_url' => 'about_images/category_icon_3.png'
        ]);
    }
}
