<?php

use Illuminate\Database\Seeder;
use App\Notice;

class NoticesSeeder extends Seeder
{
    public function run()
    {
        Notice::create([
            'titulo' => 'Aviso 1',
            'contenido' => 'Contenido 1',
            'imagen' => 'Aviso 1'
        ]);

        Notice::create([
            'titulo' => 'Aviso 2',
            'contenido' => 'Contenido 2',
            'imagen' => 'Aviso 2'
        ]);

        Notice::create([
            'titulo' => 'Aviso 3',
            'contenido' => 'Contenido 3',
            'imagen' => 'Aviso 3'
        ]);

        Notice::create([
            'titulo' => 'Aviso 4',
            'contenido' => 'Contenido 4',
            'imagen' => 'Aviso 4'
        ]);
    }
}
