<?php

use Illuminate\Database\Seeder;
use App\CandidateSoftware;

class CandidateSoftwareSeeder extends Seeder
{
    public function run()
    {
        CandidateSoftware::create([
            'candidate_id' => '1',
            'software_id' => '1',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateSoftware::create([
            'candidate_id' => '2',
            'software_id' => '1',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateSoftware::create([
            'candidate_id' => '3',
            'software_id' => '1',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);
    }
}
