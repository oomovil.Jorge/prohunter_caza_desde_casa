<?php

use Illuminate\Database\Seeder;
use App\WorkExperience;

class WorkExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkExperience::create([
            'candidate_id' => '1',
            'active' => 0,
            'position' => 'Programmer',
            'company_name' => 'Company A',
            'role' => 'Role A',
            'activities' => 'Programming computer software',
            'start_month' => '08',
            'start_year' => '2018',
            'end_month' => '12',
            'end_year' => '2022',
            'work_portfolio_url_1' => null,
            'work_portfolio_url_2' => null,
            'work_portfolio_url_3' => null
        ]);

        WorkExperience::create([
            'candidate_id' => '1',
            'active' => 0,
            'position' => 'Manager',
            'company_name' => 'Company A',
            'role' => 'Role A',
            'activities' => 'Managing people',
            'start_month' => '08',
            'start_year' => '2018',
            'end_month' => '12',
            'end_year' => '2022',
            'work_portfolio_url_1' => null,
            'work_portfolio_url_2' => null,
            'work_portfolio_url_3' => null
        ]);

        WorkExperience::create([
            'candidate_id' => '2',
            'active' => 0,
            'position' => 'Customer Service',
            'company_name' => 'Company A',
            'role' => 'Role A',
            'activities' => 'Helping people',
            'start_month' => '08',
            'start_year' => '2018',
            'end_month' => '12',
            'end_year' => '2022',
            'work_portfolio_url_1' => null,
            'work_portfolio_url_2' => null,
            'work_portfolio_url_3' => null
        ]);
    }
}
