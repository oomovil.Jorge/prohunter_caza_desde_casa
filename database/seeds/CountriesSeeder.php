<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesSeeder extends Seeder
{

    public function run()
    {
        Country::create([
            'name' => 'Mexico',
            'short_name' => 'MX'
        ]);
    }
}
