<?php

use Illuminate\Database\Seeder;

class StatusCandidateTableSeeder extends Seeder
{

    public function run()
    {
        $dt = \Carbon\Carbon::now();

        DB::table('status_candidate')->insert(
            ['name_status' => 'Postulado', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'CV Visto', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'En entrevista', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'Psicométrico', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'Contratado', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'Vacante cubierta', 'created_at' => $dt, 'updated_at' => $dt]
        );

        DB::table('status_candidate')->insert(
            ['name_status' => 'Rechazado', 'created_at' => $dt, 'updated_at' => $dt]
        );
    }
}
