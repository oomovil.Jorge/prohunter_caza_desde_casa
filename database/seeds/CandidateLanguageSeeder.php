<?php

use Illuminate\Database\Seeder;
use App\CandidateLanguage;

class CandidateLanguageSeeder extends Seeder
{
    public function run()
    {
        CandidateLanguage::create([
            'candidate_id' => '1',
            'language_id' => '1',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateLanguage::create([
            'candidate_id' => '1',
            'language_id' => '2',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateLanguage::create([
            'candidate_id' => '2',
            'language_id' => '1',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateLanguage::create([
            'candidate_id' => '2',
            'language_id' => '3',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);

        CandidateLanguage::create([
            'candidate_id' => '3',
            'language_id' => '3',
            'percentage_known' => 80,
            'certification_type' => 'TOEFL'
        ]);
    }
}
