<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'nombre' => 'root',
            'correo' => 'admin@oomovil.com',
            'contrasena' => '$2y$10$P74.MPEbxPSUS7fVz6YI0./513jBopebyRkNTQJ3Y0sXNXX.v04c.', // 123456
            'celular' => '(52) 1122334455',
            'info_dispositivo' => 'seeder',
            'tipo' => 'Root'
        ]);

        $dt = \Carbon\Carbon::now();

        DB::table('administrator_modules_assigned')->insert(
            [    'user_id' => 1,
                'modules' => '["1","2","3","4","5","6","7","8","9","10","12","13","14","15"]',
                'created_at' => $dt,
                'updated_at' => $dt]
        );


    }
}
