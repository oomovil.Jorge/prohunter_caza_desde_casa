<?php

use Illuminate\Database\Seeder;
use App\Preference;
use App\User;

class PreferencesSeeder extends Seeder
{
    public function run()
    {
        Preference::create([
            'user_id' => 1,
        ]);

        Preference::create([
            'user_id' => 2,
        ]);

        Preference::create([
            'user_id' => 3,
        ]);
    }
}