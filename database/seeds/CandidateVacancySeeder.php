<?php

use Illuminate\Database\Seeder;
use App\CandidateVacancy;

class CandidateVacancySeeder extends Seeder
{
    public function run()
    {
        CandidateVacancy::create([
            'candidate_id' => '1',
            'vacancy_id' => '1',
            'status' => 'Postulado'
        ]);

        CandidateVacancy::create([
            'candidate_id' => '2',
            'vacancy_id' => '10',
            'status' => 'Postulado'
        ]);

        CandidateVacancy::create([
            'candidate_id' => '3',
            'vacancy_id' => '4',
            'status' => 'Postulado'
        ]);
    }
}
