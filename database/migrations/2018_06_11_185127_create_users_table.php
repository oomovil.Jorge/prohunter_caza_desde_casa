<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('correo')->unique();
            $table->string('contrasena');
            $table->string('celular')->nullable();
            $table->string('info_dispositivo');
            $table->string('imagen_perfil')->nullable();
            $table->string('tipo');
            $table->string('fecha_cumpleaños')->nullable();
            $table->string('lugar_nacimiento')->nullable();
            $table->string('RFC')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
