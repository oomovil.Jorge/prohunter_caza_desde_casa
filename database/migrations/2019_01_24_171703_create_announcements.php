<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncements extends Migration
{

    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('subtitle');
            $table->text('body');
            $table->text('message');
            $table->text('icon');
            $table->integer('watched');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
