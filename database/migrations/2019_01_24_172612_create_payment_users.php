<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentUsers extends Migration
{

    public function up()
    {
        Schema::create('payment_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('postulate_id');
            $table->float('total_to_pay');
            $table->string('paid_percentage');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('payment_users');
    }
}
