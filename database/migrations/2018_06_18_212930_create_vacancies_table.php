<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{

    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_id');
            $table->string('vacancy_name');
            $table->string('company_name');
            $table->string('company_logo');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('schedule_type');
            $table->string('salary');
            $table->integer('contract_profit_percentage');
            $table->text('company_info');
            $table->string('details');
            $table->integer('Active')->default(1);
            $table->integer('Created_by');
            $table->string('limit_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
