<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkExperienceTable extends Migration
{

    public function up()
    {
        Schema::create('work_experience', function (Blueprint $table) {
            $table->increments('id');
            $table->string('candidate_id');
            $table->string('active');
            $table->string('position');
            $table->string('company_name');
            $table->string('role');
            $table->text('activities');
            $table->integer('start_month');
            $table->integer('start_year');
            $table->integer('end_month');
            $table->integer('end_year');
            $table->string('work_portfolio_url_1')->nullable();
            $table->string('work_portfolio_url_2')->nullable();
            $table->string('work_portfolio_url_3')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('work_experience');
    }
}
