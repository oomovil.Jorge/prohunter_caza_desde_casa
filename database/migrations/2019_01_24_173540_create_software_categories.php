<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareCategories extends Migration
{

    public function up()
    {
        Schema::create('software_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categoria');
            $table->integer('id_software');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('software_categories');

    }
}
