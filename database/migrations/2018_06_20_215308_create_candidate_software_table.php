<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateSoftwareTable extends Migration
{

    public function up()
    {
        Schema::create('candidate_software', function (Blueprint $table) {
            $table->increments('id');
            $table->string('candidate_id');
            $table->string('software_id');
            $table->string('percentage_known');
            $table->string('certification_type')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('candidate_software');
    }
}
