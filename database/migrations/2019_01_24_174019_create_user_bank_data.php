<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBankData extends Migration
{

    public function up()
    {
        Schema::create('user_bank_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('cuenta_clabe');
            $table->string('banco_destino');
            $table->string('beneficiario');
            $table->timestamps();
        });

    }


    public function down()
    {
        Schema::dropIfExists('user_bank_data');
    }
}
