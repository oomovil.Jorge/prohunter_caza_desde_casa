<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistory extends Migration
{

    public function up()
    {
        Schema::create('payment_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->float('missing');
            $table->string('paid_percentage');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_history');
    }
}
