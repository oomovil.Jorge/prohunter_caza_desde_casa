<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorModulesAssigned extends Migration
{

    public function up()
    {
        Schema::create('administrator_modules_assigned', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('modules');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('administrator_modules_assigned');
    }
}
