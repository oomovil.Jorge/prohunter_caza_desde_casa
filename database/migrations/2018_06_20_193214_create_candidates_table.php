<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{

    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->boolean('seeking_work');
            $table->string('profile_picture')->nullable();
            $table->string('curriculum')->nullable();
            $table->string('names');
            $table->string('maternal_name');
            $table->string('paternal_name');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('district');
            $table->string('street');
            $table->string('house_number');
            $table->string('home_phone');
            $table->string('mobile_phone');
            $table->string('email');
            $table->string('gender');
            $table->string('curp')->nullable();
            $table->string('rfc')->nullable();
            $table->string('nss')->nullable();
            $table->dateTime('birthday_date')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
