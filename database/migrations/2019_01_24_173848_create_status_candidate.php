<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusCandidate extends Migration
{

    public function up()
    {
        Schema::create('status_candidate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('status_candidate');
    }
}
