<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_candidate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('candidate_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_candidate');
    }
}