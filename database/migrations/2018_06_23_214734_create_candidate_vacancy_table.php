<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateVacancyTable extends Migration
{

    public function up()
    {
        Schema::create('candidate_vacancy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('candidate_id');
            $table->string('vacancy_id');

            $table->dateTime('date_postulate');

            $table->dateTime('date_cv_sent');
            $table->string('reason_cv_sent');

            $table->dateTime('date_interview');
            $table->string('reason_interview');

            $table->dateTime('date_psychometric');
            $table->string('reason_psychometric');

            $table->dateTime('date_hired');
            $table->string('reason_hired');

            $table->dateTime('date_rejected');
            $table->string('reason_rejected');

            $table->string('status');
            $table->boolean('Active');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('candidate_vacancy');
    }
}
