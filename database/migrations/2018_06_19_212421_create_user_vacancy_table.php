<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVacancyTable extends Migration
{
    public function up()
    {
        Schema::create('user_vacancy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('vacancy_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_vacancy');
    }
}
