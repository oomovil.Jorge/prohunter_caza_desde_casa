<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivacyAgreementTable extends Migration
{

    public function up()
    {
        Schema::create('privacy_agreement', function (Blueprint $table) {
            $table->text('text');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('privacy_agreement');
    }
}
