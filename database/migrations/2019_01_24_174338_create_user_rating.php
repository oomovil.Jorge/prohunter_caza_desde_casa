<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRating extends Migration
{

    public function up()
    {
        Schema::create('user_rating', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('admin_id');
            $table->float('qualification');
            $table->text('commentary');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('user_rating');
    }
}
