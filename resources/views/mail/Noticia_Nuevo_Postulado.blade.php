<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $titulo }}</title>

           <!-- Fonts -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>

            html, body {
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .FondoProhunter-usuario  {
                background: url("{{asset('Img/usuario_prohunterweb.jpg')}}") no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            @font-face {
                font-family: 'Harabara';
                src:  url("{{asset('Fonts/Harabara.ttf')}}");
            }
          label{
              color: white;
              font-size: 16px;
              font-family: 'Harabara';
           }

            .Boton_Login {
                border-radius: 42px 42px 42px 42px;
                -moz-border-radius: 42px 42px 42px 42px;
                -webkit-border-radius: 42px 42px 42px 42px;
                border: 8px solid #00B2A6;
                background-color: #00B2A6;
                filter: alpha(opacity=50);
                color: white;
                font-family: 'Harabara';
                font-size: 100%;
                outline-style:none;
                border-color:transparent;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body class="FondoProhunter-usuario">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <img src="{{url('Img/logo-prohunter-login.png')}}"/>
                </div>

                <label style="margin-bottom: 30px">{{$msg}}</label>
                <br>
                <a href="{{route('Dashboard')}}" class="Boton_Login" style="width: 35%;;height: 45px;cursor: pointer;letter-spacing: 0.1em; ">Dar seguimiento</a>

                </div>
        </div>
    </body>
</html>


