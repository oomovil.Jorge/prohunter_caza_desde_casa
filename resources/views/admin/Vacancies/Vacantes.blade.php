@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 4;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>

        <!-- Contenido Principal  -->
<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{asset('jquery-3.2.1.min.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">


<link rel="stylesheet" href="{{asset('css/w3.css')}}">

<script>
    $(document).ready(function() {

        $('#Tabla').DataTable({
            "scrollX": true,
            "order": [[ 0, "desc" ]],
            columnDefs: [ {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
            "language": {
                "lengthMenu": "Mostrar _MENU_ vacantes ",
                "zeroRecords": "Sin resultados encontrados..",
                "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "No resultados disponibles",
                "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "processing":  "Buscando...",
                "paginate": {
                    "first":    "Primero",
                    "last":     "Ultimo",
                    "next":     "Siguiente",
                    "previous": "Anterior"

                } } });
    });

    function Confirmacion(id){
        var ids = id;
        alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar esta vacante ? ," +
                " si contiene postulados o esta en las preferencias de alguien  ,esto se eliminará también",
                function() {
                    window.location.href = '{{url('/api/eliminar_vacante')}}?id= '+ ids;
                },
                function() {/*Cancel*/ });
   }
</script>
<style>

.btnModal{
        border-radius: 90px 90px 90px 90px; !important;
        -moz-border-radius: 90px 90px 90px 90px;!important;
        -webkit-border-radius: 90px 90px 90px 90px; !important;
    }

.thumbnail {position: relative; z-index: 0; }
    .thumbnail:hover{ background-color: transparent; z-index: 50; }
    .thumbnail span{ /* Estilos para la imagen agrandada */
        position: absolute;
        background-color: black;
        padding: 5px;
        left: -100px;
        border: 5px double gray;
        visibility: hidden;
        color: #ffffff;
        text-decoration: none;
    }
    .thumbnail span img{ border-width: 0; padding: 2px; }
    .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

</style>

<body style="overflow-x: hidden;background: #F3F7FA;">
<div  style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-vacantes.png')}}">&nbsp;&nbsp;Vacantes</label>
    <br>
    <br>
    <button class="btn btn-info" style="color: white" onClick="top.location.href= '{{ url('/admin/Vacantes/Nueva_Vacante') }}'">Nuevo</button>
    <button class="btn btn-info" onclick="window.location.href='{{ route('vacantes_cubiertas')}}'">Vacantes cubiertas</button>

    <br>


        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente'  or isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.')
           <br>
           <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $_GET['Msg'] }}</strong>
           </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @endif


    <br>
    <table id="Tabla" class="table" width="100%">
        <thead>
        <tr>
            <th></th>
          <!--  <th>Logo</th> -->
            <th style="text-align: center;vertical-align: middle">Compañía</th>
            <th style="text-align: center;vertical-align: middle">Vacante</th>
            <th style="text-align: center;vertical-align: middle">Ciudad</th>
            <th style="text-align: center;vertical-align: middle">Salario</th>
            <th style="text-align: center;vertical-align: middle">Fecha creación</th>
            <th style="text-align: center;vertical-align: middle">Cantidad postulados</th>
            <th style="text-align: center;vertical-align: middle">Postulados</th>
            <th width="25px"></th>
            <th width="25px"></th>
        </tr>
        </thead>
        <tbody>

        <?php
        $sesion =  \Illuminate\Support\Facades\Session::get('session');
        $privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');
                if($privilegios == 'Root'){
                    $content =  \Illuminate\Support\Facades\DB::select('select * from vacancies where Active = 1');
                }else{
                    $content =  \Illuminate\Support\Facades\DB::select('select * from vacancies where Active = 1 AND created_by = ?;',[$sesion]);
                }
        ?>
        @foreach($content as $result)
            <tr>
                <td hidden><!--  fecha oculta para ordenar por fecha  -->
                    {{ $result->created_at }}
                </td>
                <td>{{ $result->company_name }}</td>
                <td>{{ $result->vacancy_name }}</td>
                <td>{{ $result->city }}</td>
                <td> ${{ $result->salary }}</td>
                <td>{{ date("d/m/Y", strtotime($result->created_at )) }}</td>

          <td  style="text-align: center;vertical-align: middle">{{DB::select('select count(*) as contadorPostulados from candidate_vacancy cv,candidates c where c.id = cv.candidate_id  and cv.vacancy_id = ?', [$result->id] )[0]->contadorPostulados}}</td>

                <td  style="text-align: center;vertical-align: middle"><!--  Postulados   -->
                    <button class="btn" style="background-color: #00B2A6;width: 40px;height: 20px" onclick="Modal({{$result->id}})">
                        <img src="{{url('Img/icn-postulados.png')}}">
                    </button>
                </td>

                <td  style="text-align: center;vertical-align: middle"> <!--  Eliminar   -->
                    <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Confirmacion({{$result->id }})">
                        <img src="{{url('Img/icn-eliminar.png')}}">
                    </button>
                </td>

                <td  style="text-align: center;vertical-align: middle"> <!--  Modificar   -->
                    <button class="btn" style="background-color: #00B2A6;width: 40px;height: 20px"  onClick="top.location.href= '{{ url('/admin/Vacantes/Modificar_Vacante?id='. $result->id ) }}'">
                        <img src="{{url('Img/icn-editar.png')}}">
                    </button>
                </td>
            </tr>
        @endforeach
    </table>
</div>

<script>
    function Modal(id){

      $.post("{{url('/api/listar_postulados_vacante')}}",{"vacante":id},function(respuesta){
          $('#postulados').html(respuesta);
        });
        UIkit.modal('#modal-detalle_postulados').show();
    }
</script>

<div id="modal-detalle_postulados" uk-modal>
    <div class="uk-modal-dialog uk-modal-body" align="center">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div id="postulados" name="postulados" class="w3-hide w3-container  w3-show" style="max-height: 500px;overflow-y: scroll"></div>
    </div>
    </div>
</body>
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Vacantes");
</script>

<!-- Fin Contenido Principal  -->
@endsection
