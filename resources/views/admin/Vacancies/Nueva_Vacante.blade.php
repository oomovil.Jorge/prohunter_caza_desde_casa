@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 4;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <script>
        $(document).ready(function () {
            $('#inputEstado').attr('disabled', true);
            $('#inputCiudad').attr('disabled', true);

            //Pais
            $('#InputPais').change(function () {
                $('#InputPais option:selected').each(function () {
                    var id_Pais = $(this).val();
                    $.get("{{url('/api/load_states_web')}}?id=" + id_Pais,
                        function (data) {
                            $('#inputEstado').attr('disabled', false);
                            $('#inputEstado').html(data);
                        });
                });
            });
            //Estado
            $('#inputEstado').change(function () {
                $('#inputEstado option:selected').each(function () {
                    var id_Pais = $(this).val();
                    $.get("{{url('/api/load_cities_web')}}?id=" + id_Pais,
                        function (data) {
                            $('#inputCiudad').attr('disabled', false);
                            $('#inputCiudad').html(data);
                        });
                });
            });

        });
    </script>

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img
                src="{{url('Img/icn-seccion-vacantes.png')}}">&nbsp;&nbsp; Nueva Vacante</label>
        <br>
        <br>
        <?php
        $session = \Illuminate\Support\Facades\Session::get('session'); // session activa

        if (isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.') {
            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>' . $_GET['Msg'] . '</strong>';
            echo '</div>';
        } else if (isset($_GET['Error']) && isset($_GET['Msg'])) {
            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>' . $_GET['Msg'] . '</strong>';
            echo '</div>';
        } ?>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/registrar_vacante')}}">

            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="inputPuesto">Puesto *</label>
                    <input type="text" class="form-control" id="inputPuesto" name="inputPuesto"
                           placeholder="Nombre vacante" minlength="3" required>
                </div>

                <div class="form-group col-md-5">
                    <label for="inputempresa">Nombre empresa *</label>
                    <input type="text" class="form-control" id="inputempresa" name="inputempresa"
                           placeholder="Nombre empresa" minlength="3" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputfile">Logo *</label>
                    <input type="file" class="form-control" id="inputfile" name="inputfile" required accept="image/*">
                </div>

                <div class="form-group col-md-4">
                    <label for="inputCategoria">Categoría</label>
                    <select name="inputCategoria" class="form-control" id="inputCategoria" required>

                        <option value="">Seleccciona una categoría</option>
                        @foreach($listaCategorias as $result)
                            <option value="{{$result->id}}">{{$result->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="InputPais">País</label>
                    <select class="form-control" id="InputPais" name="InputPais" required>
                        <option value="">Selecccionar Pais</option>

                        @foreach($listaPaises as $result)
                            <option value="{{$result->id}}">{{$result->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3">
                    <label for="inputEstado">Estado</label>
                    <select class="form-control" id="inputEstado" name="inputEstado" required></select>
                </div>

                <div class="form-group col-md-3">
                    <label for="inputCiudad">Ciudad *</label>
                    <select class="form-control" id="inputCiudad" name="inputCiudad" required></select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputHorario">Horario *</label>
                    <input type="text" class="form-control" id="inputHorario" minlength="3" name="inputHorario"
                           placeholder="Horario" required>
                </div>

                <div class="form-group col-md-3">
                    <label for="inputSalario">Salario $ *</label>
                    <input type="text" class="form-control txtMoney" id="inputSalario" name="inputSalario"
                           placeholder="Salario" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputInfo_Empresa">Información de la empresa *</label>
                <textarea class="form-control" id="inputInfo_Empresa" name="inputInfo_Empresa" rows="3"
                          placeholder="..." minlength="3" required></textarea>
            </div>

            <div class="form-group">
                <label for="inputDetalles">Detalles *</label>
                <textarea class="form-control" id="inputDetalles" name="inputDetalles" rows="3" placeholder="..."
                          minlength="3" required></textarea>
            </div>


            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="input_limite">Fecha límite : *</label>
                    <input type="date" class="form-control" id="input_limite" name="input_limite" required/>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputGanacia">Porcentaje de ganancias % *</label>
                    <select class="form-control" id="inputGanacia" name="inputGanacia" required>
                        @for($i=0 ; $i <= 100;$i++)
                            <option value='{{$i}}'>{{$i}}</option>@endfor
                    </select>
                </div>
            </div>

            <br>
            <input id="id_session" name="id_session" value="{{$session}}" hidden>
            <button type="submit" class="btn btn-info">Guardar</button>
            <a class="btn btn-secondary" style="color: white;" href="{{url('/admin/Vacantes')}}">Volver</a>
        </form>

        <script src="https://unpkg.com/imask"></script>
        <script>

            Array.from(document.getElementsByClassName('txtSolo')).forEach(function (item) {
                new IMask(item, {
                    mask: /^[a-zA-Z_áéíóúñ\s]*$/
                })
            });

            Array.from(document.getElementsByClassName('txtSoloMayus')).forEach(function (item) {
                new IMask(item, {
                    mask: /^\w+$/,
                    prepare: function (str) {
                        return str.toUpperCase();
                    }, commit: function (value, masked) {
                        masked._value = value.toLowerCase();
                    }
                })
            });

            Array.from(document.getElementsByClassName('txtCel')).forEach(function (item) {
                new IMask(item, {
                    mask: '+(00)0000-0000'
                })
            });

            //Money
            Array.from(document.getElementsByClassName('txtMoney')).forEach(function (item) {
                new IMask(item, {
                    mask: Number,
                    scale: 2,
                    radix: '.',
                    mapToRadix: ['.'],
                    signed: true,
                    padFractionalZeros: true,
                    normalizeZeros: false,
                    min: 0,
                    thousandsSeparator: ','
                })
            });

        </script>

    </div>
    </body>
@endsection
