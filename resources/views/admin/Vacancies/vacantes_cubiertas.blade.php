    @extends('admin.layouts.app')
    @section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 5;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>
            <!-- Contenido Principal  -->
    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('jquery-3.2.1.min.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">


    <link rel="stylesheet" href="{{asset('css/w3.css')}}">

    <script>
        $(document).ready(function() {

            $('#Tabla').DataTable({
                "scrollX": true,

                "language": {
                    "lengthMenu": "Mostrar _MENU_ vacantes ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } });
        });

    </script>
    <style>
        .btnModal{
            border-radius: 90px 90px 90px 90px; !important;
            -moz-border-radius: 90px 90px 90px 90px;!important;
            -webkit-border-radius: 90px 90px 90px 90px; !important;
        }

    </style>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <div  style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-vacantes.png')}}">&nbsp;&nbsp;Vacantes cubiertas</label>
        <br>
        <br>
        <br>
         <table id="Tabla" class="table" width="100%">
            <thead>
            <tr>
                <th style="text-align: center;vertical-align: middle">Usuario</th>
                <th style="text-align: center;vertical-align: middle">Compañia</th>
                <th style="text-align: center;vertical-align: middle">Vacante</th>
                <th style="text-align: center;vertical-align: middle">Contratado</th>
                <th style="text-align: center;vertical-align: middle">Vacante creada por</th>
                <th style="text-align: center;vertical-align: middle"></th>
            </tr>
            </thead>
            <tbody>

            <?php
            $sesion =  \Illuminate\Support\Facades\Session::get('session');
            $privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');
            if($privilegios == 'Root'){

           $content =  \Illuminate\Support\Facades\DB::select("select * from vacantes_cubiertas group by vacante_id;");
            }else{
                $content =  \Illuminate\Support\Facades\DB::select('select  * from vacantes_cubiertas where created_by = ?;',[$sesion]);
            }
            ?>

                @foreach($content as $result)
                <tr>

                    <td> {{ $result->Nombre }}</td>
                    <td> {{ $result->company_name }}</td>
                    <td> {{ $result->vacancy_name }}</td>
                    <td> {{ $result->Contratado }}</td>
                    <td> {{ $result->vacante_creada_por }}</td>

                    <td  style="text-align: center;vertical-align: middle"><!--  Postulados   -->
                        <button class="btn" style="background-color: #00B2A6;width: 40px;height: 20px" onClick="top.location.href= '{{ route('ver_Postulacion',['id' =>$result->id]) }}'">
                            <img  width="50%" src="{{url('Img/open-eye-with-shine.png')}}">
                        </button>
                    </td>

                </tr>
            @endforeach
        </table>
        <br>
        <a class="btn btn-secondary" style="color: white;float: right;margin-right: 10px" href="{{ route('vacantes') }}">Volver</a>
    </div>

    <div id="modal-detalle_postulados" uk-modal>
        <div class="uk-modal-dialog uk-modal-body" align="center">
            <div  id="postulados" name="postulados" class="w3-hide w3-container  w3-show"></div>
            <button class="btn-secondary uk-modal-close btnModal" type="button">cerrar</button>
        </div>
    </div>
    </body>
    <script>
        window.onload = function() {
            if (document.getElementsByClassName('dataTables_empty').length) {
                document.getElementById('Tabla_paginate').style.visibility = "hidden";
            }
        };
        history.pushState(null, "", "/admin/Vacantes/Vacantes_cubiertas");
    </script>

    <!-- Fin Contenido Principal  -->
    @endsection
