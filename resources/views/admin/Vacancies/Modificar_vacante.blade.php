@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 4;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>


    <script>
        $(document).ready(function(){
            //Pais
            $('#InputPais').change(function(){
                $('#InputPais option:selected').each(function () {
                    var id_Pais =  $(this).val();
                    $.get("{{url('/api/load_states_web')}}?id=" + id_Pais,
                            function(data){
                                $('#inputEstado').html(data);
                            });
                });
            });
            //Estado
            $('#inputEstado').change(function(){
                $('#inputEstado option:selected').each(function () {
                    var id_Pais =  $(this).val();
                    $.get("{{url('/api/load_cities_web')}}?id=" + id_Pais,
                            function(data){
                                $('#inputCiudad').html(data);
                            });
                });
            });

        });
    </script>
    <link rel="stylesheet" href="{{asset('css/w3.css')}}">

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-vacantes.png')}}">&nbsp;&nbsp; Modificar vacante</label>
        <br>
        <br>
        <?php

        if(isset($_GET['Msg']) && $_GET['Msg'] === 'Modificado correctamente.' ){

            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';

        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){

            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }

        if(!isset($_GET['id'])){
            echo '<label><strong>Vacante no encontrado</strong></label>';
        }else{

        $result= \App\Vacancy::where('id',  $_GET['id'] )->first();

        if(is_null($result) ){
            echo '<label><strong>Vacante no encontrado</strong></label>';
        }else{
        ?>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/modificar_vacante')}}">


            <div class="form-row"  style="float: right;padding-right: 100px">
                    <img class="w3-circle" height="150px" width="200px" src="{{ $result->company_logo }}">
           </div>



                <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="inputPuesto">Puesto *</label>
                    <input type="text" class="form-control" id="inputPuesto" name="inputPuesto" placeholder="Nombre vacante" minlength="3" required value="{{$result->vacancy_name}}">
                </div>


                <div class="form-group col-md-5">
                    <label for="inputempresa">Nombre empresa *</label>
                    <input type="text" class="form-control" id="inputempresa" name="inputempresa" placeholder="Nombre empresa" minlength="3" required value="{{$result->company_name}}">
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputfile">Logo *</label>
                    <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*" >
                </div>


                <div class="form-group col-md-4">
                    <label for="inputCategoria">Categoría</label>
                    <select name="inputCategoria"  class="form-control" id="inputCategoria" required>
                        <option value="">Seleccciona una categoría</option>

                        @foreach($content_category as $result_category)
                            @if($result_category->id ==  $result->category_id)
                                    <option selected value="{{$result_category->id}}">{{$result_category->name}}</option>
                                @else
                                    <option value="{{$result_category->id}}">{{$result_category->name}}</option>
                                @endif

                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="InputPais">País</label>
                    <select class="form-control" id="InputPais" name="InputPais">
                        <option value="0">Selecccionar Pais</option>
                        @foreach($content_Contries as $result_Contries)

                            @if($result_Contries->id === $result->country)
                                <option selected value="{{$result_Contries->id}}">{{$result_Contries->name}}</option>
                                @else
                                <option value="{{$result_Contries->id}}">{{$result_Contries->name}}</option>
                                @endif

                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3">
                    <label for="inputEstado">Estado</label>
                    <select class="form-control" id="inputEstado" name="inputEstado">
                        <option value='0'>Seleccione un Estado</option>

                        <?php
      $content_states =  \Illuminate\Support\Facades\DB::select('select  * from states where name = ? union select  * from states where country_id = ? and name != ?',[$result->state, $result->country,$result->state]);
                        $pos = 0;
                        ?>
                        @foreach($content_states as $result_states)
                            @if($pos == 0)
                                <option selected value="{{$result_states->id}}">{{$result_states->name}}</option>
                            @else
                                <option value="{{$result_states->id}}">{{$result_states->name}}</option>
                            @endif
                                <?php  $pos++; ?>
                        @endforeach
                        <?php  $pos = 0; ?>
                    </select>

                </div>

                <div class="form-group col-md-3">
                    <label for="inputCiudad">Ciudad *</label>
                    <select class="form-control" id="inputCiudad" name="inputCiudad">
                        <option value='0'>Seleccione una ciudad</option>

                        <?php
   $content_city =  \Illuminate\Support\Facades\DB::select('select  * from cities where name = ? union select  * from cities where state_id = ? and name != ?',[$result->city, $result_states->id,$result->city]);
                        $pos = 0;
                        ?>
                        @foreach($content_city as $result_city)
                            @if($pos == 0)
                                <option selected value="{{$result_city->id}}">{{$result_city->name}}</option>
                            @else
                                <option value="{{$result_city->id}}">{{$result_city->name}}</option>
                            @endif
                            <?php  $pos++; ?>
                        @endforeach
                        <?php  $pos = 0; ?>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputHorario">Horario *</label>
                    <input type="text" class="form-control" id="inputHorario" name="inputHorario" placeholder="Horario"  minlength="3"required value="{{$result->schedule_type}}">
                </div>

                <div class="form-group col-md-3">
                    <label for="inputSalario">Salario $ *</label>
                    <input type="number" class="form-control" id="inputSalario" name="inputSalario" placeholder="Salario" required value="{{$result->salary}}">
                </div>

            </div>

        <div class="form-group">
                <label for="inputInfo_Empresa">Información de la empresa *</label>
                <textarea class="form-control" id="inputInfo_Empresa" name="inputInfo_Empresa" rows="3" placeholder="..."  minlength="3" required>{{$result->company_info}}</textarea>
            </div>
            <div class="form-group">
                <label for="inputDetalles">Detalles *</label>
                <textarea class="form-control" id="inputDetalles" name="inputDetalles" rows="3" placeholder="..." minlength="3" required>{{$result->details}}</textarea>
            </div>
            <div class="form-row">

                    <div class="form-group col-md-3">
                        <label for="input_limite">Fecha limite : *</label>
                        <input type="date" class="form-control" id="input_limite" name="input_limite" value="{{$result->limit_date}}" required/>
                    </div>

                <div class="form-group col-md-3">
                        <label for="inputGanacia">Porcentaje de ganancias % *</label>
                            <select class="form-control" id="inputGanacia" name="inputGanacia">

                                @for($i=0 ; $i <= 100;$i++)
                                    @if($i ==  $result->contract_profit_percentage)
                                        <option selected value='{{$i}}'>{{$i}}</option>
                                    @else
                                        <option value='{{$i}}'>{{$i}}</option>
                                    @endif
                                @endfor
                            </select>
                     </div>
               </div>
            <br>
            <input hidden id="inputid" name="inputid" value="{{$result->id}}">
            <button type="submit" class="btn btn-info">Guardar</button>
            <a class="btn btn-secondary" style="color: white;" href="{{url('/admin/Vacantes')}}">Volver</a>
        </form>
        <?php  }}?>
    </div>
    </body>
    <!-- Fin Contenido Principal  -->
    <script>
        history.pushState(null, "", "/admin/Vacantes/Modificar_Vacante?id={{$_GET['id']}}");
    </script>
@endsection
