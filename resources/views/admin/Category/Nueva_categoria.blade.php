@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 3;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-usuarios.png')}}">&nbsp;&nbsp; Nueva categoría</label>
        <br>
        <br>


    <?php if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.' ){
            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
                echo '<div class="alert alert-warning">';
                echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>'.$_GET['Msg'].'</strong>';
                echo '</div>';
            } ?>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/nueva_categoria')}}">
            <div class="form-group col-md-6">

                <label for="inputCategoria">Nombre Categoria *</label>
                <input type="text" class="form-control" id="inputCategoria" name="inputCategoria" minlength="3" placeholder="Nombre Categoria" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputfile">Icono de la categoria (recomendado 128px * 128px)</label>
                <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*" requiered >
            </div>

            <button type="submit" class="btn btn-info">Guardar</button>
            <a class="btn btn-secondary" style="color: white" href="{{route('categoria')}}">Volver</a>

        </form>

    </div>
    </body>

    <script>
        history.pushState(null, "", "/admin/Categorias/Nueva_categoria");
    </script>
@endsection