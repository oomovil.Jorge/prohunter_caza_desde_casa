<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ProHunter">
    <meta name="Jorge Manzano">

    <title>Pro Hunter</title>
    <link rel="shortcut icon" href="{{ asset('/Img/icn-favicon.png') }}">
    <link href="{{ url('/css/login.css') }}" rel="stylesheet">
    <link href="{{ url('/css/Bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{asset('/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('/css/Bootstrap/js/bootstrap.min.js')}}"></script>
</head>

<body class="FondoProhunter">
<form method="POST" action="{{url('/api/login_web')}}">
    <div class="centrar" align="Center">

        @if(isset($msg))
                <input type="text" class='form-control'><span>{{ $msg }}</span>
        @endif

        <img src="{{ url('/Img/logo-prohunter-login.png') }}"/>
               <br>
               <br>
        <label class="LabeLogin">Inicio de sesión</label>
               <br>
        <input class="BordesRedondosInput" style="width: 100%" type="text" name="correo" placeholder="Correo"/>
               <br>
               <br>
        <input class="BordesRedondosInput" style="width: 100%" type="Password" name="contrasena" placeholder="Contraseña"/>
              <br>
              <br>
        <input type="submit" class="Boton_Login" style="width: 100%;cursor: pointer"  value="INICIAR SESIÓN" />

            <?php
            if(isset($_GET['Error']) && isset($_GET['Msg'])){
                echo '<br>';
                echo '<br>';
                echo '<div class="alert alert-danger">';
                echo '<button  type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>'.$_GET['Msg'].'</strong>';
                echo '</div>';
            }
            ?>
            <br>
            <br>
            <a  style="color: white" href="{{url('/')}}">¡Iniciar como usuario!</a>
            <br>
        </div>
</form>
<script>
    /*if (window.location.protocol != "https:") {
        window.location.href='https://'+window.location.host+window.location.pathname;
    }*/
    if(!!localStorage.getItem('prohunter')){
        window.location.href =  window.location.href+"web";
    }
    if(!!localStorage.getItem('admprohunter')){
        window.location.href =  window.location.href+"admin";
    }
    localStorage.clear();

    history.pushState(null, "", "/admin");</script>
    </body>
</html>
