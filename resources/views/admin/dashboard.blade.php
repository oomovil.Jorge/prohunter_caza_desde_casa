@extends('admin.layouts.app')
<meta charset="utf-8">

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/w3.css')}}">



<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        let data = google.visualization.arrayToDataTable([
            ['Usuario', 'Porcentaje'],
            <?php
            $cargaG1 = false;
            foreach ($Postulados_vs_contratados_30_dias as $result) {
                $cargaG1 = true;

                echo '["Postulados",'.$result->Total_postulados.'],';
                echo '["Contratados",'.$result->Total_contratados.']';
            }
            ?>
        ]);

        let options = {
            title: 'Postulados VS Contratados \n  (Últimos 30 días)',
            pieStartAngle: 100,
            pieHole: 0.3,
            'height':300,
            backgroundColor: { fill:'transparent' },
            slices: {
                0: { color: '#00B9AE' },
                1: { color: '#2E3D56' }
            }
        };

        let chart = new google.visualization.PieChart(document.getElementById('Grafica_1'));
        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['sexo', 'Porcentaje'],
            <?php

            $cargaG2 = false;
            foreach ($Grafica_Preferencias_genero as $result) {
                $cargaG2 = true;
                echo '["Masculino",'.$result->Masculino.'],';
                echo '["Femenino",'.$result->Femenino.']';
            }
            ?>
        ]);

        var options = {
            title: 'Género más registrado',
            pieStartAngle: 100,
            'height':300,
            backgroundColor: { fill:'transparent' },
            slices: {
                0: { color: '#00B9AE' },
                1: { color: '#2E3D56' }
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('Grafica_2'));
        chart.draw(data, options);
    }
</script>

<?php
use \Illuminate\Support\Facades\DB;
  use \Illuminate\Support\Facades\Session;

$session =  Session::get('session');
$privilegios =  Session::get('privilegios');

$tarjeta1 =  DB::select('select count(*) as count from users;');
if($privilegios == 'Root'){

    $tarjeta2 =  DB::select('select count(*) as count from vacancies where Active = 1;');
    $tarjeta3 = DB::select('select count(*) as count from tabla_postulados;');

}else{
    $tarjeta2 = DB::select('select count(*) as count from vacancies where Active = 1 AND created_by = ?;',[$session]);
    $tarjeta3 =  DB::select('select count(*) as count from tabla_postulados where created_by = ?;',[$session]);
}
?>

@section('Contenido')

 <!-- Contenido Principal  -->

<div class="card-deck">
    <div class="card" onclick="top.location.href= '{{route('usuariosApp')}}'">
        <div class="card-body" style="background-color: #00B9AE;color: white;cursor: pointer">
            <img src="{{url('Img/icn-estadistica-usuario.png')}}" align="right">
            <h1 class="card-title" style="color: white;font-family: OpenSans-Bold">{{$tarjeta1[0]->count}}</h1>
            <p class="card-text">Total de reclutadores activos</p>
        </div>
    </div>

    <div class="card" onclick="top.location.href= '{{route('vacantes')}}'">
        <div class="card-body" style="background-color: #2E3D56;color: white;cursor: pointer">
            <img src="{{url('Img/icn-vacantes-estadisticas.png')}}" align="right">
            <h1 class="card-title" style="color: white;font-family: OpenSans-Bold"><label id="t1" name="t1">{{  $tarjeta2[0]->count }}</label></h1>
            <p class="card-text">Total de vacantes disponibles</p>
        </div>
    </div>

    <div class="card" onclick="top.location.href= '{{route('postulados')}}'">
        <div class="card-body" style="background-color: #C77531;color: white;cursor: pointer">
            <img src="{{url('Img/icn-estadistica-usuario.png')}}" align="right">
            <h1 id="t2" name="t2" class="card-title" style="color: white;font-family: OpenSans-Bold">{{$tarjeta3[0]->count}}</h1>
            <p class="card-text">Total de postulados activos</p>
        </div>
    </div>
    </div>

       <br>
          <br>
             <br>
                <br>

<div align="container">
    <div class="form-row">
        <div class="col" align="center">
              <div  id="Grafica_1" ></div>
            <a href="#" uk-toggle="target: #modal-G1" style="position: relative;top: -35px">Detalle</a>
        </div>
        <div class="col" align="center">
              <div  id="Grafica_2"  ></div>
            <a href="#" uk-toggle="target: #modal-G2" style="position: relative;top: -25px">Detalle</a>
        </div>
    </div>
</div>

<div id="modal-G1" uk-modal>
    <div class="uk-modal-dialog uk-modal-body" align="center">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div style="width: 350px" id="g1" name="g1" class="w3-hide w3-container  w3-show"></div>
         <h2>Usuarios vs postulados</h2>
        <br>
        <div id="Demo1" class="w3-hide w3-container  w3-show" style="width: 100%;max-height: 500px;overflow: scroll;">

            <h3>Postulados</h3>
            <hr>


    <?php
            foreach ($postulados as $clave => $datos_candiato) {

       $picture = isset($datos_candiato->profile_picture) ? asset('storage/app/'.$datos_candiato->profile_picture) : asset('Img/notpictureuser.png');
       $name = $nombre_completo =$datos_candiato->names . ' ' . $datos_candiato->paternal_name.' ' . $datos_candiato->maternal_name;
       $location = $datos_candiato->city .','. $datos_candiato->state;

            ?>
            <div style="float: left;height: 100px;width: 100%">
            <img style="float: left;height:30px" src="{{ $picture }}" alt="Avatar" class="w3-circle">
                <label style="float: left;width: 300px;text-align: left">&nbsp;{{ $name }}</label>
                <br>
                <a style="float: right" class="badge badge-pill badge-info"  href="{{ route('DetalleCandidato',['id' => $datos_candiato->id]) }}">Ver perfil</a>
                <br>
                <label style="float: left;font-size: 10px"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{ $location }}</label>
                <hr></div>

       <?php   }  ?>

            <br>
            <br>

            <div style="clear: both"></div>

            <h3>Contratados</h3>
            <br>
            <br>


            <?php
            foreach ($contratados as $clave => $datos_candiato) {
                ?>

            <div style="float: left;height: 100px;width: 100%">
            <img style="float: left;height:30px" src="{{isset($datos_candiato->profile_picture) ?  asset('storage/app/'. $datos_candiato->profile_picture) : asset('Img/notpictureuser.png')}}" alt="Avatar" class="w3-circle">
            <label style="float: left;width: 300px;text-align: left">&nbsp;{{$nombre_completo =$datos_candiato->names . ' ' . $datos_candiato->paternal_name.' ' . $datos_candiato->maternal_name}}</label>
            <br>
                <a style="float: right" class="badge badge-pill badge-info"  href="{{route('DetalleCandidato',['id' => $datos_candiato->id])}}">Ver perfil</a>

                <br>
                <label style="float: left;font-size: 10px"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{$datos_candiato->city .','. $datos_candiato->state}}</label>
            <hr>
            </div>
            <?php   }  ?>
    <br>
        </div>
    </div>
</div>

<div id="modal-G2" uk-modal>
    <div class="uk-modal-dialog uk-modal-body" align="center">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

        <div style="width: 350px" id="g2" name="g2" class="w3-hide w3-container  w3-show"></div>
        <h2>Género más registrado</h2>
        <br>
        <div id="Demo1" class="w3-hide w3-container  w3-show" style="width: 100%;max-height: 500px;overflow: scroll;">

            <h3>Masculino</h3>
            <hr>
            @php($masculino = DB::select("SELECT * FROM candidates WHERE gender  = 'Hombre';"))
            @foreach($masculino as $datos_candiato)
            <div style="float: left;height: 100px;width: 100%">
                <img style="float: left;height:30px" src="{{isset($datos_candiato->profile_picture) ? asset('storage/app/'.$datos_candiato->profile_picture) : asset('Img/notpictureuser.png')}}" alt="Avatar" class="w3-circle">
                <label style="float: left;">&nbsp;{{$nombre_completo =$datos_candiato->names . ' ' . $datos_candiato->paternal_name.' ' . $datos_candiato->maternal_name}}</label>
                <br>
                <a style="float: right" class="badge badge-pill badge-info"   href="{{route('DetalleCandidato',['id' => $datos_candiato->id])}}">Ver perfil</a>
                <br>
                <label style="float: left;font-size: 10px"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{$datos_candiato->city .','. $datos_candiato->state}}</label>
                <hr>
            </div>
            @endforeach

            <div style="clear: both"></div>

            <h3>Femenino</h3>
            <br>
            <br>

       @php( $femenino = DB::select("SELECT * FROM candidates WHERE gender  = 'Mujer';"))
       @foreach($femenino as $datos_candiato)
                <div style="float: left;height: 100px;width: 100%">
                    <img style="float: left;width:30px" src="{{isset($datos_candiato->profile_picture) ?  asset('storage/app/'. $datos_candiato->profile_picture) : asset('Img/notpictureuser.png')}}" alt="Avatar" class="w3-circle" >
                    <label style="float: left;">&nbsp;{{$nombre_completo =$datos_candiato->names . ' ' . $datos_candiato->paternal_name.' ' . $datos_candiato->maternal_name}}</label>
                    <br>
                    <a style="float: right" class="badge badge-pill badge-info"   href="{{route('DetalleCandidato',['id' => $datos_candiato->id])}}">Ver perfil</a>
                    <br>
                    <label style="float: left;font-size: 10px"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{$datos_candiato->city .','. $datos_candiato->state}}</label>
                   <hr>
                    </div>
        @endforeach

<br>
</div>
</div>
<!-- Fin Contenido Principal  -->

<script>
if(document.getElementById('t1').innerHTML == 0 && document.getElementById('t2').innerHTML == 0){
//window.setTimeout('location.reload()', 1000);
}
</script>

</div>

@endsection
