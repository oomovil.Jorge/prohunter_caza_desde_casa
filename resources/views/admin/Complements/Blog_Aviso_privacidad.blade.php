@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 13;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <!-- Editor -->

    <link rel="stylesheet" href="{{asset('editor/richtext.min.css')}}">

    <script src="{{asset('editor/jquery.richtext.js')}}"></script>



    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp;
            Blog & Aviso de privacidad</label>
        <br>
        <br>


        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente' or isset($_GET['Msg']) &&  $_GET['Msg'] === 'Registado correctamente' )
            <br>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                '
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @endif


        <br>
        <form method="POST" action="{{url('/api/Cambiar_blog_Aviso')}}">

            <div class="form-row">
                <div class="col-6">
                    <label for="inputblog">URL Blog</label>
                    <input
                        type="text"
                        class="form-control"
                        id="inputblog"
                        name="inputblog"
                        placeholder="URL blog"
                        required
                        value="{{ $url_blog  }}">
                </div>
                <div class="page-wrapper box-content">
                    <label for="inputAviso">Aviso de privacidad</label>
                    <textarea
                        disabled
                        class="content"
                        id="inputAviso"
                        name="inputAviso"
                        placeholder="..."
                        required>{{ html_entity_decode($Aviso) }}
                    </textarea>
                </div>

            </div>
            <br>
            <button type="button" class="btn btn-info" name="Btn_Modifica" id="Btn_Modifica">Modificar</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-secondary" name="Btn_Cancelar" id="Btn_Cancelar">Cancelar</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button name="btn_Aceptar" id="btn_Aceptar" type="submit" class="btn btn-info">Aceptar</button>
        </form>

        <br>
    </div>
    </body>


    <script>
        $(document).ready(function () {

            $('.content').richText();

            $('#Btn_Cancelar').prop('disabled', true);
            $('#btn_Aceptar').prop('disabled', true);
            $('#inputblog').prop('disabled', true);
            $('#inputAviso').prop('disabled', true);

            $('#Btn_Modifica').click(function () {
                $('#inputblog').prop('disabled', false);
                $('#inputAviso').prop('disabled', false);
                $('#Btn_Cancelar').prop('disabled', false);
                $('#btn_Aceptar').prop('disabled', false);
            });

            $('#Btn_Cancelar').click(function () {
                $('#inputblog').prop('disabled', true);
                $('#inputAviso').prop('disabled', true);
                $('#Btn_Cancelar').prop('disabled', true);
                $('#btn_Aceptar').prop('disabled', true);
            });

        });


        history.pushState(null, "", "/admin/Complementos/Blog_Aviso_privacidad");

    </script>
@endsection
