@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 15;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>
    <style>

        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>
    <script>
        $(document).ready(function() {
            $('#Tabla').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ bancos ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }

        );

        function Confirmacion(id){
            var ids = id;
            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                    function() {
                        window.location.href = '{{url('/api/eliminar_bancos')}}?id= '+ ids;
                    },function() {/*Cancel*/ });
        }
    </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Bancos</label>
        <br>
        <br>

        <?php
        if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente'  or isset($_GET['Msg']) && $_GET['Msg'] === 'Guardado correctamente'  ){
            echo '<br>';
            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
            echo '<br>';
            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }
        ?>
        <br>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/agregar_bancos')}}">

            <div class="form-row">
                <div class="col-3">
                    <input type="text" class="form-control" id="inputbanco" name="inputbanco" placeholder="Nombre del banco" required>
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-info">Agregar</button>
                </div>
            </div>

        </form>

        <br>
        <table id="Tabla" class="table" style="width: 100%">
            <thead>
            <tr>
                <th>Banco</th>
                <th>Fecha Creación</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <?php
            $content =  \Illuminate\Support\Facades\DB::select('select * from available_banks');?>
            @foreach($content as $result)
                <tr>
                    <td name="nombre">{{ $result->name }}</td>
                    <td>{{  date("d/m/Y", strtotime($result->created_at))}}</td>
                    <!-- Eliminar -->
                    <td align="center">
                        <a style="background-color: #b24d57;padding: 7px 25px;" class="Btn_Eliminar" onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}" alt="x" />
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    </body>
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Complementos/Bancos");
</script>
@endsection