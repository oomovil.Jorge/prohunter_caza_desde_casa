@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 12;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#Tabla').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }
        );

        function Confirmacion(id){
            let ids = id;

            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                    function() {
                        window.location.href = '{{url('/api/eliminar_software')}}?id= '+ ids;
                    },
                    function() {/*Cancel*/ });
        }
    </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Software & categorías</label>
        <br>
        <br>


        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente' or isset($_GET['Msg']) &&  $_GET['Msg'] === 'Registado correctamente' )
            <br>
           <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $_GET['Msg'] }}</strong>
           </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
          <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{  $_GET['Msg'] }}</strong>
            </div>
        @endif

        <br>

        <form method="POST" action="{{url('/api/agregar_software_categoria')}}">

            <div class="form-row">
                <div class="col-3">
                    <input type="text" class="form-control" id="inputSoftware" name="inputSoftware" placeholder="Nombre software" required minlength="3">
                </div>

                <div class="col-3">
                <select id="inputcat" name="inputcat" class="form-control" id="Input_Pais" name="Input_Pais" required>
                    @foreach($categories as $result)
                        <option value="{{$result->id}}">{{$result->name}}</option>
                    @endforeach
                </select> </div>

                <div class="col-2">
                    <button type="submit" class="btn btn-info">Agregar</button>
                </div>

            </div>
        </form>

        <br>
        <br>
        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Software</th>
                <th>Categorías</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

           @foreach($software_categories as $result)
                <tr>
                    <td >{{ $result->id }}</td>
                    <td>{{ $result->software }}</td>
                    <td>{{ $result->categoria }}</td>

                    <!-- Eliminar -->
                    <td  style="text-align: center;vertical-align: middle">
                        <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}">
                        </button>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    </body>
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Complementos/Software_Categorias");
@endsection
