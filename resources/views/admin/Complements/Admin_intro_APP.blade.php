@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 7;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>

    <script>
        $(document).ready(function () {
                $('#Tabla').DataTable({

                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros ",
                        "zeroRecords": "Sin resultados encontrados..",
                        "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                        "infoEmpty": "No resultados disponibles",
                        "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                        "search": "Buscar:",
                        "processing": "Buscando...",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"

                        }
                    }
                })
            }
        );

        function Confirmacion(id) {
            var ids = id;

            alertify.confirm('Eliminar', "¿Estas seguro de querer eliminar?",
                function () {
                    window.location.href = '{{url('/api/eliminar_slider')}}?id= ' + ids;
                },
                function () {/*Cancel*/
                });
        }
    </script>
    <style>
        .thumbnail {
            position: relative;
            z-index: 0;
        }

        .thumbnail:hover {
            background-color: transparent;
            z-index: 50;
        }

        .thumbnail span { /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }

        .thumbnail span img {
            border-width: 0;
            padding: 2px;
        }

        .thumbnail:hover span {
            visibility: visible;
            top: 0;
            left: 10px;
        }

    </style>
    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img
                src="{{url('Img/icn-seccion-complementos.png')}}">&nbsp;&nbsp; Administración intro APP</label>
        <br>
        <br>


        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.' )
            <br>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @endif

        <br>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/agregar_nuevo_slider')}}">

            <div class="form-group col-md-6">
                <label for="inputtitulo">Título *</label>
                <input type="text" class="form-control" id="inputtitulo" name="inputtitulo" minlength="3"
                       placeholder="Nombre completo" required>
            </div>

            <div class="form-group col-md-8">
                <label for="inputContenido">Contenido</label>
                <textarea class="form-control" id="inputContenido" name="inputContenido" rows="3" minlength="3"
                          placeholder="..."></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="inputfile">Imagen de intro *</label>
                <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*" required>
            </div>

            <button type="submit" class="btn btn-info">Agregar</button>
        </form>

        <br>

        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th>Título</th>
                <th>Contenido</th>
                <th>Imagen</th>
                <th>Fecha Creación</th>
                <th>Actualizado última vez</th>
                <th></th>
            </tr>
            </thead>
            <tbody>


            @foreach($introsSlider as $result)
                <tr>
                    <td name="titulo">{{ $result->titulo }}</td>
                    <td name="Contenido">{{ $result->contenido }}</td>

                    @if(isset($result->urlImagen))
                        <td>
                            <a class="thumbnail" href="#thumb"><img src="{{ $result->urlImagen }}" width="35px"
                                                                    height="35px" border="0"/>
                                <span><img width="150px" height="150px" src="{{ $result->urlImagen }}"/></span></a>
                    @else
                        <td>Sin imagen</td>
                    @endif
                    <td>{{  date("d/m/Y", strtotime($result->created_at))}}</td>
                    <td>{{  date("d/m/Y", strtotime($result->updated_at)) }}</td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Eliminar   -->
                        <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"
                                onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}">
                        </button>
                    </td>
                </tr>
            @endforeach

        </table>

    </div>

    </body>
    <script>
        window.onload = function () {
            if (document.getElementsByClassName('dataTables_empty').length) {
                document.getElementById('Tabla_paginate').style.visibility = "hidden";
            }
        };
        history.pushState(null, "", "/admin/Complementos/Admin_intro_app");
    </script>
@endsection
