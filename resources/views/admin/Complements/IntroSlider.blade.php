@extends('admin.layouts.app')
@section('Contenido')

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <style>

        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>
    <script>
        $(document).ready(function() {
            $('#Tabla').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ notificaciónes ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }

        );

        function Confirmacion(id){
            var ids = id;
            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                    function() {
                        window.location.href = '{{url('/api/eliminar_slider')}}?id= '+ ids;
                    },function() {/*Cancel*/ });
        }

        function Modificar(id,titulo,contenido) {
            alert(id);
            alert(titulo);
            alert(contenido);
            document.getElementById("mytext").value = titulo;
            document.getElementById("mytext").value = titulo;
        }



    </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Intro Slider</label>
        <br>
        <br>

        <?php
        if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.' ){
            echo '<br>';
            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
            echo '<br>';
            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }
        ?>
        <br>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/agregar_nuevo_slider')}}">

            <div class="form-group col-md-6">
                <label for="inputtitulo">Titulo *</label>
                <input type="text" class="form-control" id="inputtitulo" name="inputtitulo" placeholder="Nombre completo" required>
            </div>

            <div class="form-group col-md-8">
                <label for="inputContenido">Contenido</label>
                <textarea class="form-control" id="inputContenido" name="inputContenido" rows="3" placeholder="..."></textarea>
            </div>

            <div class="form-group col-md-6">
                <label for="inputfile">Imagen de perfil</label>
                <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*"  required >
            </div>

            <button type="submit" class="btn btn-info">Agregar</button>
        </form>

        <br>
        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th>Titulo</th>
                <th>Contenido</th>
                <th>Imagen</th>
                <th>Fecha Creación</th>
                <th>Actualizado ultima vez</th>
                <th></th>

            </tr>
            </thead>
            <tbody>

            <?php
            $content =  \Illuminate\Support\Facades\DB::select('select * from intro_sliders');?>
            @foreach($content as $result)
                <tr>
                    <td name="titulo">{{ $result->titulo }}</td>
                    <td name="Contenido">{{ $result->contenido }}</td>

                    @if(isset($result->urlImagen))
                        <td>
                            <a class="thumbnail" href="#thumb"><img src="{{ $result->urlImagen }}" width="35px" height="35px" border="0" />
                                <span><img src="{{ $result->urlImagen }}" /></span></a>
                    @else
                        <td>Sin imagen</td>
                    @endif

                    <td>{{ $result->created_at }}</td>
                    <td>{{ $result->updated_at }}</td>

                    <!-- Eliminar -->
                    <td align="center">
                        <a style="background-color: #b24d57;padding: 7px 25px;" class="Btn_Eliminar" onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}" alt="x" />
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
</div>
    </body>
    <?php  echo'<script>history.pushState(null, "", "/admin/Complementos/Intro_Slider");</script>';  ?>
@endsection