@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 9;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>
    <style>

        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>
    <script>
        $(document).ready(function() {
            $('#Tabla').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }

        );

        function Confirmacion(id){
            var ids = id;

            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                    function() {
                        window.location.href = '{{url('/api/eliminar_preguntas')}}?id= '+ ids;
                    },
                    function() {/*Cancel*/ });
        }
    </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Preguntas frecuentes</label>
        <br>
        <br>

        <?php
        if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.' ){
            echo '<br>';
            echo '<div class="alert alert-success">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
            echo '<br>';
            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }
        ?>

        <br>
        <form enctype="multipart/form-data" method="POST" action="{{url('/api/agregar_pregunta')}}">

            <div class="form-group col-md-8">
                <label for="inputtitulo">Pregunta *</label>
                <textarea class="form-control" id="inputPregunta" name="inputPregunta" minlength="3" rows="2" placeholder="Pregunta" required></textarea>
            </div>

            <div class="form-group col-md-8">
                <label for="inputContenido">Respuesta *</label>
                <textarea class="form-control" id="inputRespuesta" name="inputRespuesta" minlength="3" rows="3" placeholder="Respuesta" required></textarea>
            </div>
            <button type="submit" class="btn btn-info">Agregar</button>
        </form>

        <br>
        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th>Pregunta</th>
                <th>Respuesta</th>
                <th>Fecha Creación</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <?php
            $content =  \Illuminate\Support\Facades\DB::select('select * from faq');?>
            @foreach($content as $result)
                <tr>
                    <td name="titulo">{{ $result->question }}</td>
                    <td name="Contenido">{{ $result->answer }}</td>
                    <td>{{ date("d/m/Y", strtotime($result->created_at))}}</td>

                    <!-- Eliminar -->
                    <td  style="text-align: center;vertical-align: middle">
                        <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}">
                        </button>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    </body>
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Complementos/Preguntas_frecuentes");
</script>
@endsection