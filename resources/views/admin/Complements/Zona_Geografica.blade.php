@extends('admin.layouts.app')
@section('Contenido')


    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 8;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <link rel="stylesheet" href="{{ url('datatable/bootstrap.min.css')}}">
    <script src="{{ url('datatable/jquery.min.js')}}"></script>
    <script src="{{ url('datatable/bootstrap.min.js')}}"></script>
    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#Tabla1').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }
        );

        $(document).ready(function() {
            $('#Tabla2').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }
        );

        $(document).ready(function() {
            $('#Tabla3').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"

                    } } })  }
        );
        $(document).ready(function(){
                        //Pais
            $('#InputPais3').change(function(){
                $('#InputPais3 option:selected').each(function () {
                    var id_Pais =  $(this).val();

                    $.get("{{url('/api/load_states_web')}}?id=" + id_Pais,
                            function(data){
                
                                $('#inputEstado3').html(data);
                            });
                });
            });
            //Estado
            $('#inputEstado3').change(function(){
                $('#inputEstado3 option:selected').each(function () {
                    var id_Pais =  $(this).val();
                    $.get("{{url('/api/load_cities_web')}}?id=" + id_Pais,
                            function(data){
                        
                                $('#inputCiudad3').html(data);
                            });
                });
            });

        });

    function Eliminar_pais(id){
            var ids = id;

            alertify.confirm('Eliminar'," ¿Esta seguro de querer eliminar este país?  Esto eliminara a todos los estados/provincias y ciudades que contenga!",
                    function() {
                        window.location.href = '{{url('/api/elimina_pais')}}?id= '+ ids;
                    },
                    function() {/*Cancel*/ });
        }

    function Eliminar_estado(id){
            var ids = id;

            alertify.confirm('Eliminar',"¿Esta seguro de querer eliminar este estado? Esto eliminara todas las ciudades que contenga!",
                    function() {
                        window.location.href = '{{url('/api/elimina_estado')}}?id= '+ ids;
                    },
                    function() {/*Cancel*/ });
        }

    function Eliminar_ciudad(id){
            var ids = id;

            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar esta ciudad?",
                    function() {
                        window.location.href = '{{url('/api/elimina_ciudad')}}?id= '+ ids;
                    },
                    function() {/*Cancel*/ });
        }
        


    </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1); -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1); box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20); padding: 50px; width: 100%">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-complementos.png')}}">&nbsp;&nbsp; Zonas Geográficas</label>
        <br>
        <br>

        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente' )
          <br>
          <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{$_GET['Msg']}}</strong>
          </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
          <br>
          <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{$_GET['Msg']}}</strong>
          </div>
        @endif

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#pais">País</a></li>
            <li><a data-toggle="tab" href="#estado">Estado</a></li>
            <li><a data-toggle="tab" href="#ciudad">Ciudad</a></li>
        </ul>

        <div class="tab-content">

            <br>
            <div id="pais" class="tab-pane fade in active">
                <h3>Agregar País</h3>
                <form  method="POST" action="{{url('/api/agregar_pais')}}">
                    <div class="form-row">
                        <div class="col-3">
                            <input type="te" id="inputPais" name="inputPais" class="form-control" placeholder="País" required>
                        </div>
                        <div class="col-2">
                            <input type="text" id="inputNombrecorto" name="inputNombrecorto" class="form-control" placeholder="Nombre corto" required>
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn btn-info">Agregar pais</button>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <table id="Tabla1" class="table" style="width:100%">
                    <thead>
                    <tr>
                        <th>País</th>
                        <th>Nombre corto</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                   @foreach($country as $result)
                        <tr>

                            <td>{{ $result->name }}</td>
                            <td>{{ $result->short_name }}</td>

                            <!-- Eliminar -->
                            <td  style="text-align: center;vertical-align: middle">
                                <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Eliminar_pais({{$result->id }})">
                                    <img src="{{url('Img/icn-eliminar.png')}}">
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>

            <div id="estado" class="tab-pane fade">
                <h3>Agregar Estado/Provincia</h3>
                <form  method="POST" action="{{url('/api/agregar_estado')}}">
                    <div class="form-row">

                        <div class="col-3">

                            <select id="inputPais" name="inputPais" class="form-control" id="Input_Pais" name="Input_Pais" required>

                                @foreach($country as $result)
                                    <option value="{{$result->id}}">{{$result->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-3">
                            <input type="text" id="inputEstado" name="inputEstado" class="form-control" placeholder="Estado/Provincia" required>
                        </div>

                        <div class="col-2">
                            <input type="text" id="inputNombrecorto" name="inputNombrecorto" class="form-control" placeholder="Nombre corto" required>
                        </div>

                        <div class="col-1">
                            <button type="submit" class="btn btn-info">Agregar Estado/Provincia</button>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <table id="Tabla2" class="table" style="width:100%">
                    <thead>
                    <tr>
                        <th>País</th>
                        <th>Estado/ Provincia</th>
                        <th>Nombre corto</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                   @foreach($Estado_Provincia as $result)
                        <tr>
                            <td>{{ $result->Pais }}</td>
                            <td>{{ $result->Estado }}</td>
                            <td>{{ $result->short_name  }}</td>

                            <!-- Eliminar -->
                            <td  style="text-align: center;vertical-align: middle">
                                <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Eliminar_estado({{$result->id }})">
                                    <img src="{{url('Img/icn-eliminar.png')}}">
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div id="ciudad" class="tab-pane fade">
                <h3>Agregar ciudad</h3>
                <form  method="POST" action="{{url('/api/agregar_ciudad')}}">
                    <div class="form-row">

                        <div class="col-3">

                            <select class="form-control" id="InputPais3" name="InputPais3" required>
                                <option value="">Selecccionar país</option>
                                @foreach($country as $result)
                                    <option value="{{$result->id}}">{{$result->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-3">
                            <select class="form-control" id="inputEstado3" name="inputEstado3" required>
                                <option value=''>Seleccione un Estado</option>
                            </select>
                        </div>

                        <div class="col-2">
                            <input type="text" id="inputCiudad3" name="inputCiudad3" class="form-control" placeholder="Ciudad" required>
                        </div>
                        <div class="col-1">
                            <button type="submit" class="btn btn-info">Agregar ciudad</button>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <table id="Tabla3" class="table" style="width:100%">
                    <thead>
                    <tr>
                        <th>País</th>
                        <th>Estado/ Provincia</th>
                        <th>Ciudad</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($ciudad as $result)
                        <tr>
                            <td>{{ $result->Pais }}</td>
                            <td>{{ $result->Estado }}</td>
                            <td>{{ $result->Ciudad }}</td>

                            <!--  Eliminar   -->
                            <td  style="text-align: center;vertical-align: middle">
                                <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Eliminar_ciudad({{$result->id }})">
                                    <img src="{{url('Img/icn-eliminar.png')}}">
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
        <br>
    </div>

    </body>
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Complementos/Zona_Geografica");
</script>
    <!-- Fin Contenido Principal  -->
@endsection
