@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 14;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <style>

        .thumbnail {
            position: relative;
            z-index: 0;
        }

        .thumbnail:hover {
            background-color: transparent;
            z-index: 50;
        }

        .thumbnail span { /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }

        .thumbnail span img {
            border-width: 0;
            padding: 2px;
        }

        .thumbnail:hover span {
            visibility: visible;
            top: 0;
            left: 10px;
        }

    </style>

    <script>

        $(document).ready(function () {
            $('#Tabla').DataTable({

                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing": "Buscando...",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"

                    }
                }
            })
        });

        function Confirmacion(id) {
            let ids = id;

            alertify.confirm('Eliminar', "¿Estas seguro de querer eliminar?",
                function () {
                    window.location.href = '{{url('/api/eliminar_img_about')}}?id= ' + ids;
                },
                function () {/*Cancel*/
                });
        }
    </script>
    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
       -webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
       -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
       box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
       padding: 50px;align-content: center">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img
                src="{{url('Img/icn-seccion-complementos.png')}}">&nbsp;&nbsp; Acerca de la APP</label>
        <br>
        <br>

        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente' or isset($_GET['Msg']) &&  $_GET['Msg'] === 'Registrado correctamente.' )
            <br>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @elseif(  isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
            </div>
        @endif
        <br>


        <form enctype="multipart/form-data" method="POST" action="{{url('/api/actualizar_about_texto')}}">
            <div class="form-group col-md-6">
                <label for="inputtext">Acerca de la aplicación *</label>
                <textarea type="text" class="form-control" id="inputtext" name="inputtext" rows="4"
                          placeholder="Nombre completo" required>{{$text}}</textarea>
            </div>
            <div class="form-group col-md-2" align="left">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>

        <br> <br>
        <form enctype="multipart/form-data" method="POST" action="{{url('/api/actualizar_about_imgs')}}">

            <div class="form-row">
                <div class="col-6">
                    <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*">
                </div>
                <div align="left" class="col-6">
                    <button type="submit" class="btn btn-info">Agregar</button>
                </div>
            </div>
        </form>

        <br> <br>

        <table id="Tabla" class="table" style="width:50%" align="center">
            <thead>
            <tr>
                <th>Imagen</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @foreach($img_about as $result)
                <tr>
                    <td><img width="150px" height="150px" src="{{$result->image_url}}"/></td>
                    <!-- Eliminar -->
                    <td style="text-align: center;vertical-align: middle">
                        <button class="btn" style="background-color: #b24d57;width: 70px;height: 40px"
                                onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}">
                        </button>
                    </td>
                </tr>
            @endforeach
        </table>
        <br>

    </div>
    </body>
    <script>history.pushState(null, "", "/admin/Complementos/Acerca_de_app");</script>
@endsection
