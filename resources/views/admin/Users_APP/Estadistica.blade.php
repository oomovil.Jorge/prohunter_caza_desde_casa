@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link rel="stylesheet" href="{{url('css/rateyo/jquery.rateyo.min.css')}}">
    <script src="{{asset('css/rateyo/jquery.rateyo.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;overflow: hidden;">
        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp;
            Estadística</label>
        <br>
        <br>
        <?php
        $session = Session::get('session'); // session activa

        function webservice($url, $parametros, $metodo)
        {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metodo);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parametros));
            $response = curl_exec($ch);
            curl_close($ch);
            if (!$response) {
                return false;
            } else {
                return $response;
            }
        }
        if(!isset($_GET['id'])){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{
        $result = \App\User::where('id', $_GET['id'])->first();
        if(is_null($result) ){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{

        //ESTADISTICA
        $estadistica = json_decode(webservice(url('/api/estadistica'), array("email" => $result->correo), 'POST'));
        $estadistica_grafica_fechas = array();
        for ($i = 0; $i < 12; $i++) {
            $estadistica_grafica_fechas[] = 0;
        }

        if ($estadistica != null) {



            foreach ($estadistica->grafica_fechas as $value) {
                $estadistica_grafica_fechas[$value->mes - 1] = $value->registros;
            }

            $star_rating = $estadistica->Estrellas;
            $conteo_postulados = $estadistica->grafica_ultimos_30_dias[0]->conteo_postulados;
            $porcentaje_vacantes_30_Dias = $estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_30_Dias;
            $conteo_contratados = $estadistica->grafica_ultimos_30_dias[0]->conteo_contratados;
            $porcentaje_vacantes_contratados_30_Dias = $estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_contratados_30_Dias;
            $postulados = $estadistica->detalle_grafica_vacantes_vs_postulados->postulados;
            $contratados = $estadistica->detalle_grafica_vacantes_vs_postulados->contratados;
            $porcentaje_femenino = $estadistica->grafica_ultimos_30_dias[0]->porcentaje_femenino;
            $porcentaje_masculinos = $estadistica->grafica_ultimos_30_dias[0]->porcentaje_masculinos;


        } else {

            $star_rating = 0;
            $conteo_postulados = 0;
            $porcentaje_vacantes_30_Dias = 0;
            $conteo_contratados = 0;
            $porcentaje_vacantes_contratados_30_Dias = 0;
            $postulados = [];
            $contratados = [];
            $porcentaje_femenino = 0;
            $porcentaje_masculinos = 0;

        }



        ?>

        <style>
            ::-webkit-scrollbar {
                display: none;
            }
        </style>


        <div name="alerta_guadado" id="alerta_guadado" class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong><label>Calificación guardada</label></strong>
        </div>
        <div style="z-index: 0;width: 160px;" class="star" id="star_rating"></div>
        <br>
        <br>
        <div class="w3-container">
            <h5 class="w3-opacity"><b>Gráfica de fechas</b></h5>
            <canvas id="canvas" width="948" height="474" class="chartjs-render-monitor"
                    style="display: block;  width: 100%"></canvas>
            <hr>
        </div>
        <div class="w3-container">
            <h5 class="w3-opacity"><b>Postulados vs Candidatos últimos 30 días</b></h5>
            <div id="ultimos30">
                <div class="last-30 postulados">
                    <h4>{{$conteo_postulados}} Postulados</h4>
                    <canvas id="canvas_d_1" class="chartjs-render-monitor"
                            style="max-height: 300px;max-width: 300px"></canvas>
                    <h4>{{$porcentaje_vacantes_30_Dias}}%</h4>
                    <br>
                    <div style="text-align:left;overflow: scroll;height: 350px;">
                        @foreach ($postulados as  $postulado)
                            <div class="postulado">
                                <img
                                    src="{{$postulado->profile_picture!=null?asset('storage/app/'.$postulado->profile_picture):asset('Img/notpictureuser.png')}}"
                                    alt="Avatar" class="w3-circle" style="width:10%">
                                <label>&nbsp;{{$postulado->names}} {{$postulado->paternal_name}} {{$postulado->maternal_name}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="last-30 contratados">
                    <h4>{{$conteo_contratados}} Contratados</h4>
                    <canvas id="canvas_d_2" class="chartjs-render-monitor"
                            style="max-height: 300px;max-width: 300px"></canvas>
                    <h4>{{$porcentaje_vacantes_contratados_30_Dias}}%</h4>
                    <br>
                    <div style="text-align:left;overflow: scroll;height: 350px;">
                        @foreach ( $contratados as  $contratado)
                            <div class="contratado">
                                <img
                                    src="{{$contratado->profile_picture!=null?asset('storage/app/'.$contratado->profile_picture):asset('Img/notpictureuser.png')}}"
                                    alt="Avatar" class="w3-circle" style="width:10%">
                                <label>&nbsp;{{$contratado->names}} {{$contratado->paternal_name}} {{$contratado->maternal_name}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="w3-container">
            <h5 class="w3-opacity"><b>Género más registrado</b></h5>
            <div class="monitos label">
                <h3>{{$porcentaje_femenino}}%</h3>
                <h4>Mujeres</h4>
            </div>
            <div class="monitos">
                <div class="arriba"
                     style="height: {{100-$porcentaje_femenino}}%;"></div>
                <div class="mujer"
                     style="height: {{$porcentaje_femenino}}%;"></div>
                <div
                    style="background: url('{{asset('monitos/monita-prohunter.png')}}') no-repeat;height: 100%;width: 100%;z-index: 1;position: relative;left: -1px;"></div>
            </div>
            <div class="monitos label">
                <h4>VS</h4>
            </div>
            <div class="monitos">
                <div class="arriba"
                     style="height: {{100-$porcentaje_masculinos}}%;"></div>
                <div class="hombre"
                     style="height: {{$porcentaje_masculinos}}%;"></div>
                <div
                    style="background: url('{{asset('monitos/monito-prohunter.png')}}') no-repeat;height: 100%;width: 100%;z-index: 1;position: relative;left: -1px;"></div>
            </div>
            <div class="monitos label">
                <h3>{{$porcentaje_masculinos}}%</h3>
                <h4>Hombres</h4>
                <div style="clear: both;"></div>
            </div>
        </div>
        <br>
        <a class="btn btn-info" style="color: white;float: inherit" href="javascript:history.back(-1);">Regresar</a>
    </div>

    <script>

        $(document).ready(function () {
            $('#alerta_guadado').hide();
            $(function () {
                $("#star_rating").rateYo({
                    rating:{{ isset($star_rating) ? $star_rating : 0.0 }}
                });

                $("#star_rating").rateYo().on("rateyo.set", function (e, data) {
                    var rating = data.rating;
                    'user_id', 'starnew', 'admin_id'
                    $.post("{{url('/api/calificar_usuario')}}", {
                        user_id: {{$_GET['id']}} ,
                        starnew: rating,
                        admin_id: {{$session}}
                    }, function (msg) {
                        $('#alerta_guadado').show();
                    });
                });
            });
        });
        var config = {
            type: 'line',
            data: {
                labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic',],
                datasets: [
                    {
                        label: 'Postulados',
                        backgroundColor: '#00B9AE',
                        borderColor: '#00B9AE',
                        data: [
                            @foreach($estadistica_grafica_fechas as $dato)
                            {{$dato}},
                            @endforeach
                        ],
                        fill: false,
                    }]
            },
            options: {
                responsive: true,
                title: {
                    display: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        }
                    }]
                }
            }
        };
        window.onload = function () {
            var ctx = document.getElementById('canvas').getContext('2d');
            window.myLine = new Chart(ctx, config);
            var canvas_d_1 = document.getElementById('canvas_d_1').getContext('2d');
            window.myLine = new Chart(canvas_d_1, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            backgroundColor: ['#2E3D56', 'rgba(255, 159, 64, 0)'],
                            borderColor: ['#2E3D56', 'rgba(255, 159, 64, 0)'],
                            data: [
                                {{  $porcentaje_vacantes_30_Dias}},
                                {{  100-$porcentaje_vacantes_30_Dias }},
                            ],
                            fill: false,
                        }]
                },
                options: {
                    cutoutPercentage: 70,
                    responsive: true,
                    tooltips: {
                        enabled: false,
                    },
                }
            });
            var canvas_d_2 = document.getElementById('canvas_d_2').getContext('2d');
            window.myLine = new Chart(canvas_d_2, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            backgroundColor: ['#00B9AE', 'rgba(255, 159, 64, 0)'],
                            borderColor: ['#00B9AE', 'rgba(255, 159, 64, 0)'],
                            data: [
                                {{ $porcentaje_vacantes_30_Dias }},
                                {{ 100-$porcentaje_vacantes_contratados_30_Dias}},
                            ],
                            fill: false,
                        }]
                },
                options: {
                    cutoutPercentage: 70,
                    responsive: true,
                    tooltips: {
                        enabled: false,
                    },
                }
            });
        };
    </script>
    <style>
        body {
            min-height: 1111px;
        }

        @font-face {
            font-family: 'Harabara';
            src: url('{{asset('Fonts/Harabara.ttf')}}');
        }

        strong, .title-dash, #mis_favoritas, #all_vacantes, .btnRedondo_gris, .btnRedondo_verde {
            font-family: 'Harabara';
        }

        .last-30 {
            width: 45%;
            display: table-cell;
            text-align: center;
        }

        :not(pre) > code, :not(pre) > kbd, :not(pre) > samp {
            background: #19191e;
            white-space: inherit;
        }

        .postulados h4 {
            color: #2E3D56;
        }

        .contratados h4 {
            color: #00B9AE;
        }

        .postulado {
            margin-bottom: 10px;
        }

        .contratado {
            margin-bottom: 10px;
        }

        .monitos {
            width: 155px;
            height: 255px;
            display: inline-block;
        }

        .monitos .arriba {
            background: #c9d0d6;
            width: 98%;
            z-index: 0;
            float: left;
        }

        .monitos .hombre {
            background: #2E3D56;
            width: 98%;
            z-index: 0;
            float: left;
        }

        .monitos .mujer {
            background: #00B9AE;
            width: 98%;
            z-index: 0;
            float: left;
        }

        @media only screen and (max-width: 600px) {
            .monitos.label {
                position: unset !important;
            }
        }

        .monitos.label {
            position: relative;
            height: 128px;
            top: -127px;
            text-align: center;
        }


        .monitos.label h4 {
            color: #c9d0d6;
        }

        .monitos.label:first-of-type h3 {
            color: #00B9AE;;
        }

        .monitos.label:last-of-type h3 {
            color: #2E3D56;;
        }

    </style>
    <?php  } } ?>

    </body>
    <!-- Fin Contenido Principal  -->

@endsection
