@extends('admin.layouts.app')
@section('Contenido')
    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <style>
        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp;Detalle candidato</label>
        <br>
        <br>

        <?php

        if(!isset($_GET['id'])){
            echo '<label><strong>candidato no encontrado</strong></label>';
        }else{
        $result = \App\Candidate::where('id',$_GET['id'])->first();
        if(is_null($result) ){
            echo '<label><strong>candidato no encontrado</strong></label>';
        }else{
        ?>

        <input class="btn btn-info" type="button" onclick="printDiv('DetalleCandidato')" value="Imprimir" />

        <div id="DetalleCandidato" name="DetalleCandidato">
        <hr>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="inputPuesto">Nombre</label>
                    <input type="text" class="form-control" id="inputPuesto" name="inputPuesto" placeholder="Nombre"  value="{{$result->names}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputPuesto">Apellido paterno</label>
                    <input type="text" class="form-control" id="inputPuesto" name="inputPuesto" placeholder="Apellido paterno" value="{{$result->paternal_name}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputPuesto">Apellido materno</label>
                    <input type="text" class="form-control" id="inputPuesto" name="inputPuesto" placeholder="Apellido materno" value="{{$result->maternal_name}}" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="">País</label>
                    <input type="text" class="form-control" id="" name="inputPuesto" placeholder="País"  value="{{$result->country}}" readonly>
                </div>
                <div class="form-group col-md-2">
                    <label for="">Estado</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Estado"  value="{{$result->state}}" readonly>
                </div>
                <div class="form-group col-md-2">
                    <label for="">Ciudad</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Ciudad"  value="{{$result->city}}" readonly>
                </div>
                <div class="form-group col-md-2">
                    <label for="">Colonia</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Colonia"  value="{{$result->district}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label for="">Calle</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Calle"  value="{{$result->street}}" readonly>
                </div>
                <div class="form-group col-md-1">
                    <label for="">Num</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Num"  value="{{$result->house_number}}" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="">CURP</label>
                    <input type="text" class="form-control" id="" name="" placeholder="CURP"  value="{{$result->curp}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label for="">RFC</label>
                    <input type="text" class="form-control" id="" name="" placeholder="RFC"  value="{{$result->rfc}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label for="">NSS</label>
                    <input type="text" class="form-control" id="" name="" placeholder="NSS"  value="{{$result->nss}}" readonly>
                </div>
            </div>

            <div class="form-row">
            <div class="form-group col-md-3">
                <label for="">Celular</label>
                <input type="text" class="form-control" id="" name="" placeholder="Celular "  value="{{$result->mobile_phone}}" readonly>
            </div>

            <div class="form-group col-md-3">
                <label for="">Teléfono casa</label>
                <input type="text" class="form-control" id="" name="" placeholder="Teléfono casa"  value="{{$result->home_phone}}" readonly>
            </div>

                <div class="form-group col-md-3">
                    <label for="">Fecha de cumpleaños</label>
                    <input type="text" class="form-control" id="" name="" placeholder="Cumpleaños"  value="{{!is_null($result->birthday_date)? date("d/m/Y", strtotime($result->birthday_date)) : ''}}" readonly>
                </div>

        </div>

@if(isset($result->curriculum))
            <div class="form-row" id="Curriculum" name="Curriculum">
                <div class="form-group col-md-3">
                    <a href="{{asset('storage/app/'. $result->curriculum.'')}}">Ver Currículum</a>
                </div>
            </div>
@endif
            <hr>


        <?php  $content =  \Illuminate\Support\Facades\DB::select('select * from education_data where candidate_id =  ' .$_GET['id'] );?>
        <strong><label>Educación</label></strong>
        @foreach($content as $result)

        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="">Nivel académico</label>
                <input type="text" class="form-control" id="" name="" placeholder="Nivel académico" value="{{$result->academic_level}}" readonly>
            </div>
            <div class="form-group col-md-3">
                <label for="">Universidad</label>
                <input type="text" class="form-control" id="" name="" placeholder="Universidad" value="{{$result->universidad}}" readonly>
            </div>
            <div class="form-group col-md-2">
                <label for="">Año inicio</label>
                <input type="text" class="form-control" id="" name="" placeholder="Inicio" value="{{$result->start_year}}" readonly>
            </div>
            <div class="form-group col-md-2">
                <label for="">Año termino</label>
                <input type="text" class="form-control" id="" name="" placeholder="termino" value="{{$result->end_year}}" readonly>
            </div>

            <div class="form-group col-md-8">
                <label for="">Información adicional</label>
                <input type="text" class="form-control" id="" name="" placeholder="info" value="{{$result->additional_info}}" readonly>
            </div>
        </div>
            <hr>
        @endforeach

        <?php  $content =  \Illuminate\Support\Facades\DB::select('select (select name from languages l where l.id = cl.language_id) as lengua,cl.percentage_known,cl.certification_type from candidate_language cl where cl.candidate_id = ' .$_GET['id'] );?>
        <strong><label>Lenguajes</label></strong>
        @foreach($content as $result)
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="">Idioma</label>
                <input type="text" class="form-control" id="" name="" placeholder="Idioma" value="{{$result->lengua}}" readonly>
            </div>
            <div class="form-group col-md-2">
                <label for="">Porcentaje</label>
                <input type="text" class="form-control" id="" name="" placeholder="Porcentaje" value="{{$result->percentage_known}} %" readonly>
            </div>
            <div class="form-group col-md-3">
                <label for="">Certificación</label>
                <input type="text" class="form-control" id="" name="" placeholder="Certificación" value="{{$result->certification_type}}" readonly>
            </div>
        </div>
            <hr>
        @endforeach

        <?php  $content =  \Illuminate\Support\Facades\DB::select('select (select name from software s where s.id = cs.software_id)  as software ,cs.percentage_known,cs.certification_type from candidate_software cs where cs.candidate_id = ' .$_GET['id'] );?>
        <strong><label>Software</label></strong>
        @foreach($content as $result)
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="">Software</label>
                <input type="text" class="form-control" id="" name="" placeholder="Software" value="{{$result->software}}" readonly>
            </div>
            <div class="form-group col-md-2">
                <label for="">Porcentaje</label>
                <input type="text" class="form-control" id="" name="" placeholder="Porcentaje" value="{{$result->percentage_known}} %" readonly>
            </div>
            <div class="form-group col-md-3">
                <label for="">Certificación</label>
                <input type="text" class="form-control" id="" name="" placeholder="Certificación" value="{{$result->certification_type}}" readonly>
            </div>
        </div>
            <hr>


        @endforeach
        </div>
        <script>
            function printDiv(nombreDiv) {
                var contenido= document.getElementById(nombreDiv).innerHTML;
                var contenidoOriginal= document.body.innerHTML;

                document.body.innerHTML = contenido;

                var y = document.getElementById("Curriculum");
                if(y != null){
                    y.style=  "display:none";
                }
                window.print();
                document.body.innerHTML = contenidoOriginal;

            }
        </script>

        <?php  } } ?>

        <br>
        <br>
        <a class="btn btn-info"  style="color: white;float: right" href="javascript:history.back(-1);">Regresar</a>
        <br>
        <br>



    </div>
    </body>
    <!-- Fin Contenido Principal  -->

@endsection