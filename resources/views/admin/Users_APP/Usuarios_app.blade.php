@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id', Session::get('session'))->first();

    foreach (json_decode($asignados->modules) as $assigned) {
        if ($id_modulo == $assigned) {
            $permiso = true;
        }
    }
    if (!$permiso) {
        header('Location: /admin/Dashboard');
        exit;
    }



    ?>

    <!-- Contenido Principal  -->
    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#Tabla').DataTable({

                "order": [[0, "desc"]],
                "scrollX": true,
                columnDefs: [{
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ usuarios ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing": "Buscando...",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });

    </script>
    <body style="overflow-x: hidden;background: #F3F7FA; width: 100%">


    <div class="w3-container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">
        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp;Usuarios
            APP</label>
        <br>
        <br>
        <button class="btn btn-info" style="color: white"
                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Nuevo_usuario') }}'">Agregar
        </button>
        <br>


        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente')
            <br>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong> {{ $_GET['Msg'] }}</strong>
            </div>
        @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong> {{ $_GET['Msg'] }}</strong>
            </div>
        @endif


        <script type="text/javascript">
            function Confirmacion(id) {
                let ids = id;
                alertify.confirm('Eliminar', "¿Estas seguro de querer eliminar?",
                    function () {
                        window.location.href = '{{url('/api/eliminar_usuario')}}?id= ' + ids;
                    }, function () {/*Cancel*/
                    });
            }
        </script>
        <!--vertical-align: middle"-->
        <br>

        <table id="Tabla" class="table" name="table" width="100%">
            <thead>
            <tr>
                <th></th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Nombre</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Correo</th>
                <th style="ttext-align: center;vertical-align: middle;align-content: center">Tipo de registro</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Mi cuenta</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Candidatos</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Favoritos</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Estadísticas</th>
                <th style="text-align: center;vertical-align: middle;align-content: center">Ganancias</th>
                <th style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>

            @foreach($table as $result)
                <tr>

                    <td hidden> <!--  fecha oculta   -->
                        {{ $result->created_at }}
                    </td>
                    <td style="text-align: center;vertical-align: middle">
                        @if(isset($result->imagen_perfil))
                            <img style="width: 30px;height: 30px"
                                 src="{{ asset('storage/'. $result->imagen_perfil.'')}}"><br>
                        @endif
                        {{ $result->nombre }}
                    </td>

                    <td style="vertical-align: middle">
                        @if($result->tipo  ==  "facebook")
                            <label>Facebook token</label>
                        @elseif ($result->tipo  ==  "google")
                            <label>Google token</label>
                        @elseif ($result->tipo  ==  "normal")
                            {{ $result->correo }}
                        @endif
                    </td>

                    <td align="center" style="text-align: center;vertical-align: middle;align-content: center">
                        <a style="vertical-align: middle;background-color: transparent;width: 30px;" href="#">

                            @if($result->tipo  ==  "facebook")
                                <img src="{{url('Img/icn-facebook.png')}}" alt="x"/>
                            @elseif ($result->tipo  ==  "google")
                                <img src="{{url('Img/icn-google.png')}}" alt="x"/>
                            @elseif ($result->tipo  ==  "normal")
                                <img src="{{url('Img/icn-correo.png')}}" alt="x"/>
                            @endif
                        </a>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Mi Perfil   -->
                        <button class="btn" style="background-color: #00B9AE;width: 30px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Mi_cuenta?id='. $result->id ) }}'">
                            <img src="{{url('Img/pago-con-tarjetas-de-credito.png')}}">
                        </button>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Postulados   -->
                        <button class="btn" style="background-color: #00B9AE;width: 30px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Ver_Candidatos_usuario?id='. $result->id ) }}'">
                            <img src="{{url('Img/icn-btn-postulados.png')}}">
                        </button>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Favoritos   -->
                        <button class="btn" style="background-color: #00B9AE;width: 30px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Listar_favoritos?id='. $result->id ) }}'">
                            <img src="{{url('Img/icn-btn-vacantes-favoritas.png')}}">
                        </button>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Estadisticas   -->
                        <button class="btn" style="background-color: #00B9AE;width: 30px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Estadistica?id='. $result->id ) }}'">
                            <img src="{{url('Img/icn-btn-estadisticas.png')}}">
                        </button>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Ganacias   -->
                        <button class="btn" style="background-color: #00B9AE;width: 30px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Ganacias?id='. $result->id ) }}'">
                            <img src="{{url('Img/icn-btn-ganancias.png')}}">
                        </button>
                    </td>


                    <td style="text-align: center;vertical-align: middle"> <!--  Eliminar   -->
                        <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"
                                onclick="Confirmacion({{$result->id }})">
                            <img src="{{url('Img/icn-eliminar.png')}}">
                        </button>
                    </td>

                    <td style="text-align: center;vertical-align: middle"> <!--  Modificar   -->
                        <button class="btn" style="background-color: #00B2A6;width: 40px;height: 20px"
                                onClick="top.location.href= '{{ url('/admin/Usuarios_app/Modificar_usuario?correo='. $result->correo ) }}'">
                            <img src="{{url('Img/icn-editar.png')}}">
                        </button>
                    </td>

                </tr>


            @endforeach
        </table>
    </div>
    </body>
    <!-- Fin Contenido Principal  -->
    <script>
        window.onload = function () {
            if (document.getElementsByClassName('dataTables_empty').length) {
                document.getElementById('Tabla_paginate').style.visibility = "hidden";
            }
        };
        history.pushState(null, "", "/admin/Usuarios_app");
    </script>

@endsection
