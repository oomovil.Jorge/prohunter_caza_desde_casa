@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <style>

        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;width: 60%">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Favoritos</label>
        <br>
        <br>

        <?php

        if(!isset($_GET['id'])){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{
        $result = \App\User::where('id',$_GET['id'])->first();
        if(is_null($result) ){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{

        echo '<label><strong>Usuario : '.$result->nombre.'</strong></label>';
        ?>
        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th>Categoría</th>
                <th>Icono</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $content =  \Illuminate\Support\Facades\DB::select('select c.name as categoria,c.icon_url as img from categories c  join category_preference cp on c.id = cp.category_id
                   join preferences p on cp.preference_id = p.id  join users u on p.user_id = u.id where u.id = ' .$_GET['id'] );?>
            @foreach($content as $result)
                <tr>
                    <td>{{ $result->categoria }}</td>

                    @if(isset($result->img))
                        <td>
                            <a class="thumbnail" href="#thumb"><img src="{{ $result->img }}" width="45px" height="45px" border="0" />
                                <span><img  width="150px" height="150px" src="{{ $result->img }}" /></span></a>
                    @else
                        <td>Sin imagen</td>
                    @endif

                </tr>
            @endforeach
        </table>

        <?php  } } ?>
        <br>
        <a class="btn btn-info"  style="color: white;float: right" href="javascript:history.back(-1);">Regresar</a>
        <br>
    </div>
    </body>
    <!-- Fin Contenido Principal  -->

@endsection