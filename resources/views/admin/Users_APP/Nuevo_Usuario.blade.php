@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <link  href="{{url('DataPicker/datepicker.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">

<body style="overflow-x: hidden;background: #F3F7FA;">

<div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Nuevo Usuario APP</label>
    <br>
    <br>

   @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Registrado correctamente.' )
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $_GET['Msg'] }}</strong>
        </div>
    @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
       <div class="alert alert-warning">
       <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>{{ $_GET['Msg'] }}</strong>
       </div>
  @endif

    <form enctype="multipart/form-data" method="POST" action="{{url('/api/registrar_usuario_en_Web')}}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputNombre">Nombre completo *</label>
                <input type="text" class="form-control txtSolo" id="inputNombre" name="inputNombre" minlength="3" placeholder="Nombre completo" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="inputCorreo">Correo Electrónico *</label>
                <input type="email" class="form-control" id="inputCorreo" name="inputCorreo" minlength="3" placeholder="Correo Electrónico" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="inputRFC">RFC</label>
                <input type="text" class="form-control txtSoloMayus" id="inputRFC" name="inputRFC" minlength="3" placeholder="RFC"  maxlength="14">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="inputContrasena">Nueva contraseña *</label>
                <input type="password" class="form-control" id="inputContrasena" name="inputContrasena" minlength="3" placeholder="Nueva contraseña" required>
            </div>
            <div class="form-group col-md-5">
                <label for="inputConfirmar"> Confirmar contraseña *</label>
                <input type="password" class="form-control" id="inputConfirmar" name="inputConfirmar" minlength="3" placeholder="Confirmar contraseña" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="input_fecha_nacimiento">Fecha de nacimiento</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                    </div>
                    <input type="text" class="form-control datepicker" id="input_fecha_nacimiento" name="input_fecha_nacimiento" minlength="3" placeholder="Fecha de nacimiento">
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="input_Lugar_nacimiento">Lugar de nacimiento</label>
                <input type="text" class="form-control" id="input_Lugar_nacimiento" name="input_Lugar_nacimiento" minlength="3" placeholder="Lugar de nacimiento">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCel">Celular *</label>
                <input type="text" class="form-control txtCel" id="inputCel" name="inputCel" placeholder="Celular" minlength="3" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputfile">Imagen de perfil</label>
                <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*" >
            </div>
        </div>
        <button type="submit" class="btn btn-info">Guardar</button>
        <a class="btn btn-secondary" style="color: white" href="javascript:history.back(-1);">Volver</a>

    </form>

</div>
</body>
<!-- Fin Contenido Principal  -->
    <script src="https://unpkg.com/imask"></script>
    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('DataPicker/datepicker.js')}}"></script>
    <script>

        Array.from(document.getElementsByClassName('txtSolo')).forEach(function (item){
         new IMask(item, {
                mask: /^[a-zA-Z_áéíóúñ\s]*$/
            })
        });

        Array.from(document.getElementsByClassName('txtSoloMayus')).forEach( function (item){
            new IMask(item, {
                mask: /^\w+$/,
                prepare: function (str) {
                    return str.toUpperCase();
                },commit: function (value, masked) {
                    masked._value = value.toLowerCase();
                }
            })
        });

        Array.from(document.getElementsByClassName('txtCel')).forEach(function (item){
            new IMask(item, {
                mask: '+(00)0000-0000'
            })
        });

        Array.from(document.getElementsByClassName('datepicker')).forEach(function (item){
            new IMask(item, {
                mask: '00/00/0000'
            });
        });

         //Datapicker
        $(function() {$('.datepicker').datepicker({ format: 'dd/mm/yyyy'});});
            $(function() {
                $('.datepicker').datepicker({
                    format: 'dd/mm/yyyy',
                    language: 'es-ES',
                    trigger:true,
                    autoHide:true,
                    zIndex: 2048
                });
        });


    history.pushState(null, "", "/admin/Usuarios_app/Nuevo_usuario");
</script>
@endsection