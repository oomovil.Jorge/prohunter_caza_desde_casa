@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;width: 40%">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Mi cuenta</label>
        <br>
        <br>

        <?php

        if(!isset($_GET['id'])){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{
        $result = \App\User::where('id',$_GET['id'])->first();
        if(is_null($result) ){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{

        echo '<label><strong>Usuario : '.$result->nombre.'</strong></label>';

            $Cuenta ="";
            $Banco ="";
            $Benedificiario ="";
            $content =  \Illuminate\Support\Facades\DB::select('select * from user_bank_data where id_user =' .$_GET['id'] );
            foreach ($content as $result) {
                $Cuenta =$result->cuenta_clabe;
                $Banco =$result->banco_destino;
                $Benedificiario =$result->beneficiario;
            }
                ?>

            <div class="form-row">
                <div class="col-5">
                    <label for="inputCuenta">Cuenta /Clabe</label>
                    <input type="text" class="form-control" id="inputblog" name="inputblog" style="width: 200%" placeholder="Cuenta /Clabe" required readonly value="{{$Cuenta}}">
                </div>
            </div>
            <div class="form-row">
                <div class="col-5">
                    <label for="inputBanco">Banco destino</label>
                    <input type="text" class="form-control" id="inputblog" name="inputblog"  style="width: 200%" placeholder="Banco destino" required readonly value="{{$Banco}}">
                </div>
            </div>
            <div class="form-row">
                <div class="col-5">
                    <label for="inputBeneficiario">Beneficiario</label>
                    <input type="text" class="form-control" id="inputblog" name="inputblog"  style="width: 200%" placeholder="Beneficiario" required readonly value="{{$Benedificiario}}">
                </div>
            </div>
       <br>
        <?php  }  } ?>
        <br>
        <a class="btn btn-info"  style="color: white;float: right" href="javascript:history.back(-1);">Regresar</a>
        <br>

                    </div>
    </body>
    <!-- Fin Contenido Principal  -->

@endsection