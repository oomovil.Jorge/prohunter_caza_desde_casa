@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 2;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>


        <!-- Contenido Principal  -->
<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>

<script>
        $(document).ready(function() {
            $('#Tabla').DataTable({
                "language": {
                    "lengthMenu": "Mostrar _MENU_ resultados ",
                    "zeroRecords": "Sin resultados encontrados..",
                    "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                    "infoEmpty": "No resultados disponibles",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "processing":  "Buscando...",
                    "paginate": {
                        "first":    "Primero",
                        "last":     "Ultimo",
                        "next":     "Siguiente",
                        "previous": "Anterior"


                    } } });
        });

       function open_modal(a,b) {
           document.getElementById("input_id").value = a;
           var select = document.getElementById("input_pago");
           switch(b) {
               case '0%':
                   select.options[1] = new Option( '50%', '50%');
                   select.options[2] = new Option( '100%', '100%');
                   break;
               case '50%':
                   select.options[1] = new Option( '100%', '100%');
                   break;
           }
       }

 </script>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Actualizado correctamente' )
           <br>
           <div class="alert alert-success">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{$_GET['Msg']}}</strong>
           </div>
            @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
           <div class="alert alert-warning">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{$_GET['Msg']}}</strong>
           </div>
            @endif

    <?php
        if(!isset($_GET['id'])){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{
        $result = \App\User::where('id',$_GET['id'])->first();
        if(is_null($result)){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{

        $content =  \Illuminate\Support\Facades\DB::select('call Ganacias_usuario('.$_GET['id'].')');
        $total_ganado = 0;
        $total_faltante = 0;
        foreach($content as $Result){
            $total_ganado = $Result->total_ganado;
            $total_faltante = $Result->total_faltante;
        }
        ?>

         <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp; Ganancias</label>
        <br>
            <br>
        <label style="font-size: 16px;"><strong>Ganancia total :&nbsp;&nbsp;&nbsp;&nbsp;</strong> ${{$total_ganado}}</label>
        <br>
             <br>
       <label style="font-size: 16px;"> <strong>Faltante a pagar :&nbsp;&nbsp;&nbsp;&nbsp;</strong>${{$total_faltante}}</label>
        <br>
            <br>

        <table align="center" id="Tabla" class="table" style="width:90%">
            <thead>
            <tr>
                <th>Postulación</th>
                <th>Total</th>
                <th>Porcentaje pagado</th>
                <th>Faltante</th>
                <th>Fecha</th>
                <th></th>
            </tr>
            </thead>

            @foreach($content as $Result)
                       <tr>
            <td style="vertical-align: middle;text-align: center;">
             @if(isset($Result->id_postulate))

                <button class="btn" style="color: white;background-color: #00B9AE;width: 30px;height: 20px" onClick="top.location.href= '{{ url('/admin/Postulados/Ver_postulacion?id='.$Result->id_postulate ) }}'">Ver</button>
                       @else
                   <label>Postulación eliminada</label>
                        @endif
             </td>
                    <td style="text-align: center;vertical-align: middle">$ {{ $Result->total_to_pay }}</td>
                    <td style="text-align: center;vertical-align: middle">{{ $Result->paid_percentage }}</td>
                    <td style="text-align: center;vertical-align: middle">$  {{ $Result->faltante }}</td>
                    <td style="text-align: center;vertical-align: middle">{{ $Result->created_at }}</td>
                    <td  style="text-align: center;vertical-align: middle"><!--  Pagar   -->


                @if($Result->paid_percentage == '100%')
                  <label>Pagado</label>
                        @else
                  <button data-toggle="modal" data-target="#myModal" onclick="open_modal('{{$Result->gain_id}}','{{$Result->paid_percentage}}')" class="btn" href="#" style="background-color: #00B9AE;height: 20px;color: white;" >Actualizar pago</button>
                        @endif
                   </td>
             </tr>
            @endforeach
        </table>
        <br>
            <a class="btn btn-info"  style="color: white;float: right" href="javascript:history.back(-1);">Regresar</a>
            <br>
            <br>


    </div>

    <form method="POST" action="{{url('/api/actualizar_ganacias_usuario')}}">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Actualizar pago</h4>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="input_pago">Porcentaje a pagar :</label>
                            </div>
                            <select name="input_pago" id="input_pago" class="custom-select" id="input_pago">
                                <option value="-1" selected>Seleccióna...</option>
                             </select>
                            <input name="input_id_ganancia" id="input_id_ganancia" hidden value="{{ $_GET['id'] }}">
                            <input name="input_id" id="input_id" hidden value="0">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Aceptar" class="btn btn-info">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    </body>
<script>history.pushState(null, "", "/admin/Usuarios_app/Ganacias?id={{$_GET['id']}}");</script>

  <?php } } ?>
<!-- Fin Contenido Principal  -->

@endsection