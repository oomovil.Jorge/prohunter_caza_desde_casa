@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 2;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
    <script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
    <script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
    <script src="{{ url('datatable/semantic.min.js')}}"></script>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <style>

        .thumbnail {position: relative; z-index: 0; }
        .thumbnail:hover{ background-color: transparent; z-index: 50; }
        .thumbnail span{ /* Estilos para la imagen agrandada */
            position: absolute;
            background-color: black;
            padding: 5px;
            left: -100px;
            border: 5px double gray;
            visibility: hidden;
            color: #ffffff;
            text-decoration: none;
        }
        .thumbnail span img{ border-width: 0; padding: 2px; }
        .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

    </style>

    <script>
        function Confirmacion(id,user){
            var ids = id;
            var id_user = user;

            alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                    function() {
                        window.location.href = '{{url('/api/eliminar_candidato_web')}}?id= '+ ids+'&user_id= '+ id_user;
                    },
                    function() {/*Cancel*/ });
        }

    </script>

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-usuarios-header.png')}}">&nbsp;&nbsp;Candidatos</label>
        <br>
        <br>


    @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente')
               <br>
               <div class="alert alert-success">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong> {{ $_GET['Msg'] }}</strong>
               </div>
    @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
                <br>
                <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $_GET['Msg'] }}</strong>
                </div>
            @endif
                <?php
        if(!isset($_GET['id'])){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{
        $result = \App\User::where('id',$_GET['id'])->first();
        if(is_null($result) ){
            echo '<label><strong>Usuario no encontrado</strong></label>';
        }else{

        echo '<label><strong>Usuario : '.$result->nombre.'</strong></label>';
        ?>
        <table id="Tabla" class="table" style="width:100%">
            <thead>
            <tr>
                <th style="text-align: center;vertical-align: middle">Nombre</th>
                <th style="text-align: center;vertical-align: middle">Imagen</th>
                <th style="text-align: center;vertical-align: middle">Género</th>
                <th style="text-align: center;vertical-align: middle">Celular</th>
                <th style="text-align: center;vertical-align: middle">Teléfono hogar</th>
                <th style="text-align: center;vertical-align: middle">CURP</th>
                <th></th>
                <!--     <th></th> -->
            </tr>
            </thead>
            <tbody>

            <?php
            $content =  \Illuminate\Support\Facades\DB::select('select * from candidates where user_id = ' .$_GET['id'] );?>
            @foreach($content as $result)
                <tr>
                    <td>{{ $result->names }}</td>

                    @if(isset($result->profile_picture))
                        <td>
                            <a class="thumbnail" href="#thumb">
                                <img src="{{asset('storage/app/'. $result->profile_picture.'')}}" width="45px" height="45px" border="0" />
                                <span><img  width="350px" height="350px" src="{{asset('storage/app/'. $result->profile_picture.'')}}" /></span></a>
                    @else
                        <td>Sin imagen</td>
                    @endif
                    <td>{{ $result->gender }}</td>
                    <td>{{ $result->mobile_phone }}</td>
                    <td>{{ $result->house_number }}</td>
                    <td>{{ $result->curp }}</td>

                    <!-- Modificar -->
                     <td  style="text-align: center;vertical-align: middle"> <!--  Modificar   -->
                        <button class="btn" style="background-color: #00B2A6;width: 70px;height: 30px"  onClick="top.location.href= '{{ url('/admin/Usuarios_app/Detalle_Candidato?id='. $result->id ) }}'">
                            <img src="{{url('Img/icn-editar.png')}}">
                        </button>
                    </td>
            @endforeach
        </table>

        <?php  } } ?>
        <br>
        <a style="float: right" class="btn btn-info" href="{{url('/admin/Usuarios_app')}}">Regresar</a>
        <br>
    </div>
    </body>
    <!-- Fin Contenido Principal  -->
    <script>history.pushState(null, "", "/admin/Ver_Candidatos_usuario?id="{{  $_GET['id'] }});</script>

@endsection