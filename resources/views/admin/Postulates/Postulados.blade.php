@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 5;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>

        <!-- Contenido Principal  -->
<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>


<script>
    $(document).ready(function() {
        $('#Tabla').DataTable({

            "language": {
                "lengthMenu": "Mostrar _MENU_ postulados ",
                "zeroRecords": "Sin resultados encontrados..",
                "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "No resultados disponibles",
                "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "processing":  "Buscando...",
                "paginate": {
                    "first":    "Primero",
                    "last":     "Ultimo",
                    "next":     "Siguiente",
                    "previous": "Anterior"

                } } })  } );

    function Confirmacion(id){
        var ids = id;

        alertify.confirm('Eliminar',"¿Estas seguro de elimianr este postulado, se podira  perder todo el progreso de una postulación ? ",
                function() {
                    window.location.href = '{{url('/api/eliminar_postulacion_web')}}?id= '+ ids;
                },
                function() {/*Cancel*/ });
    }

</script>
<body style="overflow-x: hidden;background: #F3F7FA;">

<div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;word-wrap: break-word;">
    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-postulados.png')}}">&nbsp;&nbsp;Postulados</label>
    <br>
         <br>
              <br>

    @if(isset($_GET['Msg']))
        <br>
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>{{$_GET['Msg']}}</strong>
       </div>
@elseif(isset($_GET['Error']) && isset($_GET['Msg']))
        <br>
        <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{$_GET['Msg']}}</strong>
        </div>
        @endif

    <?php
    $session =  \Illuminate\Support\Facades\Session::get('session');
    $privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');
     ?>
    <button class="btn btn-info" onclick="window.location.href='{{url('/api/generar_excel?id='.$session)}}'">Generar excel de pagos</button>
    <button class="btn btn-info" onclick="window.location.href='{{route('Rechazados')}}'">Postulaciones rechazadas</button>
    <br>
        <br>
    <table id="Tabla" class="table" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;vertical-align: middle">Usuario</th>
            <th style="text-align: center;vertical-align: middle">Postulado</th>
            <th style="text-align: center;vertical-align: middle">Compañía</th>
            <th style="text-align: center;vertical-align: middle">Vacante</th>
            <th style="text-align: center;vertical-align: middle">Status</th>
            <th style="text-align: center;vertical-align: middle">Vacante</th>
          <!--  <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> -->
        </tr>
        </thead>
        <tbody>

        <?php
        if($privilegios == 'Root'){
            $content =  \Illuminate\Support\Facades\DB::select('select * from tabla_postulados');
        }else{
            $content =  \Illuminate\Support\Facades\DB::select('select * from tabla_postulados where created_by = ?;',[$session]);
        }
?>
        @foreach($content as $result)
            <tr>
                <td>{{ $result->Usuario }}</td>
                <td>{{ $result->names.' '.$result->paternal_name .' '. $result->maternal_name  }} </td>
                <td>{{ $result->Compañia }}</td>
                <td>{{ $result->Vacancy }}</td>
                <td>{{ $result->status }}</td>
                <td  style="text-align: center;vertical-align: middle"> <!--  Modificar a8aab2   -->
                    <a class="btn btnhover" style="background-color: #00B2A6;width: 40px;height: 20px"  href="{{ url('/admin/Postulados/Ver_postulacion?id='. $result->id ) }}">
                        <img src="{{url('Img/icn-editar.png')}}">
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
</div>
</body>
<!-- Fin Contenido Principal  -->
<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Postulados");
</script>
@endsection