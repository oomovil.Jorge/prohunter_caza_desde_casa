@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 5;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>
        <!-- Contenido Principal  -->
<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>





<script>
    $(document).ready(function() {

        $('#Tabla').DataTable({
            "scrollX": true,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros ",
                "zeroRecords": "Sin resultados encontrados..",
                "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "No resultados disponibles",
                "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "processing":  "Buscando...",
                "paginate": {
                    "first":    "Primero",
                    "last":     "Ultimo",
                    "next":     "Siguiente",
                    "previous": "Anterior"

                } } });
    });

</script>

<body style="overflow-x: hidden;background: #F3F7FA;">
<div  style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-vacantes.png')}}">&nbsp;&nbsp;Postulados rechazados</label>
    <br>
    <br>
    <br>
    <table id="Tabla" class="table" width="100%">
        <thead>
        <tr>
            <th style="text-align: center;vertical-align: middle">Correo usuario</th>
            <th style="text-align: center;vertical-align: middle">Candidato</th>
            <th style="text-align: center;vertical-align: middle">Vacante</th>
            <th style="text-align: center;vertical-align: middle">Campañia</th>
            <th style="width:  150px;text-align: center;vertical-align: middle">Motivo</th>
            <th style="text-align: center;vertical-align: middle">Fecha</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $sesion =  \Illuminate\Support\Facades\Session::get('session');
        $privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');
        if($privilegios == 'Root'){
            $content =  \Illuminate\Support\Facades\DB::select("select * from postulados_rechazados;");
        }else{
            $content =  \Illuminate\Support\Facades\DB::select('select  * from postulados_rechazados where created_by = ?;',[$sesion]);
        }
        ?>

        @foreach($content as $result)
            <tr>
                <td style="max-width: 300px"> {{ $result->correo }}</td>
                <td style="max-width: 300px"> {{ $result->candidato }}</td>
                <td style="max-width: 300px"> {{ $result->vacancy_name }}</td>
                <td style="max-width: 300px"> {{ $result->company_name }}</td>
                <td style="max-width: 300px;overflow:hidden;white-space: nowrap;text-overflow: ellipsis;word-wrap: break-word;"  uk-tooltip="{{ $result->reason_rejected }}">{{ $result->reason_rejected }}</td>
                <td style="max-width: 300px"> {{ date("d/m/Y", strtotime($result->date_rejected))  }}</td>

            </tr>
        @endforeach
    </table>
    <br>
    <a class="btn btn-secondary" style="color: white;float: right;margin-right: 10px" href="{{url('/admin/Postulados')}}">Volver</a>

</div>

<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Postulados/Rechazados");
</script>

<!-- Fin Contenido Principal  -->
@endsection
