@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 5;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>

<script type="text/javascript">
    function Confirmacion(id){
        var ids = id;

        alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar esta vacante ? ," +
                " si contiene postulados o esta en las preferencias de alguien  ,esto se eliminaran también",
                function() {
                    window.location.href = '{{url('/api/eliminar_vacante')}}?id= '+ ids;
                },
                function() {/*Cancel*/});
    }
</script>
<style>
    .thumbnail {position: relative; z-index: 0; }
    .thumbnail:hover{ background-color: transparent; z-index: 50; }
    .thumbnail span{ /* Estilos para la imagen agrandada */
        position: absolute;
        background-color: black;
        padding: 5px;
        left: -100px;
        border: 5px double gray;
        visibility: hidden;
        color: #ffffff;
        text-decoration: none;
    }
    .thumbnail span img{ border-width: 0; padding: 2px; }
    .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

</style>
<body style="overflow-x: hidden;background: #F3F7FA;">

<div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">
    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-postulados.png')}}">&nbsp;&nbsp;Postulación</label>
    <br>
    <br>

    <?php
    if(isset($_GET['Msg']) && $_GET['Msg'] === 'Actualizado correctamente' ){
        echo '<br>';
        echo '<div class="alert alert-success">';
        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
        echo '<strong>'.$_GET['Msg'].'</strong>';
        echo '</div>';
    }else  if(isset($_GET['Error']) && isset($_GET['Msg'])){
        echo '<br>';
        echo '<div class="alert alert-warning">';
        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
        echo '<strong>'.$_GET['Msg'].'</strong>';
        echo '</div>';
    }
    if(!isset($_GET['id'])){   echo '<label><strong>Postulación no encontrada</strong></label>';  }else{

    function webservice($url,$parametros,$metodo){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metodo);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($parametros));
        $response = curl_exec($ch);
        curl_close($ch);
        if(!$response) {
            return false;
        }else{
            return $response;
        }
    }


        $content =  \Illuminate\Support\Facades\DB::select("select
        cv.id as id_postulacion,
        CONCAT(c.names,' ', c.maternal_name,' ',c.paternal_name) AS Nombre_completo,
        v.vacancy_name,
        cv.candidate_id,
        v.id,
        v.company_logo,
        v.company_name,
        v.schedule_type,
        v.city,
        v.salary,
        v.vacancy_name,
        cv.status
        FROM vacancies v,candidate_vacancy cv,candidates c WHERE v.id = cv.vacancy_id AND cv.candidate_id = c.id AND cv.id = ? LIMIT 1",[$_GET['id']]);
        $candidato = \Illuminate\Support\Facades\DB::select('select * from candidates where id = ?',[$content[0]->candidate_id]);
        $usuario = \App\User::where('id', $candidato[0]->user_id)->first();

        $ganancia = \Illuminate\Support\Facades\DB::select('select * from payment_users where postulate_id = ?',[$_GET['id']]);
       if(isset($ganancia[0])){
                 $historial_ganancias = json_decode(webservice(url('/api/listar__historial_ganancias'),array("payment_id" => $ganancia[0]->id),'POST'));
       }


    if(is_null($content) ){
        echo '<label><strong>Postulación no encontrada</strong></label>';
    }else{
    ?>
    @foreach($content as $result)

    <br>
    <table id="Tabla" class="table" style="width:100%">
        <thead>
        <tr>
            <th>Logo</th>
            <th>Compañia</th>
            <th>Vacante</th>
            <th>Ciudad</th>
            <th>Horario</th>
            <th>Salario</th>
        </tr>
        </thead>
        <tbody>
                <tr>
                <td>
                    @if(isset($result->company_logo))
                        <a class="thumbnail" href="#thumb">
                            <img style="width: 30px;height: 30px" src="{{ $result->company_logo }}">
                                <span>
                                     <img style="width: 150px;height: 150px" src="{{ $result->company_logo }}">
                                </span>
                        </a>
                    @endif
                </td>
                <td>{{ $result->company_name }}</td>
                <td>{{ $result->vacancy_name }}</td>
                <td>{{ $result->city }}</td>
                <td>{{ $result->schedule_type }}</td>
                <td>{{ $result->salary }}</td>
                    </tr>
    </table>
        @if(isset($ganancia[0]))
            <br>
            <table id="Tabla" class="table" style="width:40%" >
                <thead>
                <tr>
                    <th>Ganado</th>
                    <th>Pagado actualmente %</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>$ {{ $ganancia[0]->total_to_pay }} </td>
                    <td>  {{ $ganancia[0]->paid_percentage }} pagado</td>
                    <td  style="text-align: center;vertical-align: middle">
                        <button class="btn btnhover" style="background-color: #00B2A6;color: white;width: 80px;height: 35px" uk-toggle="target: #modal-detalle_pago">Detalle</button>
                    </td>
                </tr>
            </table>

            <div id="modal-detalle_pago" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" align="center">
                    <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

                    <h1>Historial de pagos</h1>
                    <hr>
                    @foreach($historial_ganancias->data as $GananciasDetalle)
                        <div style="width: 30rem;">
                            <label>Pagado : $ {{$GananciasDetalle->paid}}</label><br>
                            <label>Porcentaje : {{$GananciasDetalle->paid_percentage}}</label><br>
                            <label>Fecha : {{$GananciasDetalle->created_at}}</label><br>
                        </div>
                        <hr>
                    @endforeach
                    <br>
                </div>
            </div>
            <br>
        @endif




    <form  method="POST" action="{{url('/api/editar_estatus_postulacion')}}">

    <div class="row">
        <div class="col-4">
            <label for="inputcandidato">Porsulación por:</label>
            <input readonly name="inputcandidato" id="inputcandidato" type="text" class="form-control" placeholder="Usuario" required value="{{ $usuario->nombre }}">

            <label for="inputcandidato">Candidato :</label>
            <input readonly name="inputcandidato" id="inputcandidato" type="text" class="form-control" placeholder="Candidato" required value="{{$nombreandidato = $result->Nombre_completo}}">
            <input hidden value="{{$result->id_postulacion}}" id="id" name="id">

        </div>

        <div class="col-3">
            <label for="inputEstatus">Estatus :</label>
            <select name="inputEstatus" id="inputEstatus" type="text" class="form-control">
                <?php
                $content1 = \Illuminate\Support\Facades\DB::select('select * from candidate_vacancy where id = ?',[$_GET['id']]);
                 foreach($content1 as $result){  $status_actual = $result->status;  }
                ?>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        var status_actual = '<?php echo $status_actual;?>';
                        if(status_actual == 'Contratado' || status_actual == 'Rechazado'){
                            $("#blnc").hide();
                            $("#inputMotivo").attr('disabled',true);
                        }else{
                            $("#blnc").show();
                        }
                    });
                    document.getElementById('blnc').style.disabled = true;
                    </script>

                    @if($status_actual == 'Contratado' or $status_actual == 'Rechazado' or $status_actual == 'Vacante cubierta' )
                            <option selected disabled>{{$status_actual}}</option>
                    @else
        <?php $content2 = \Illuminate\Support\Facades\DB::select('select * from status_candidate where id != 1 or id != 6;'); ?>
                @foreach($content2 as $result)
                    @if(isset($status_actual))
                            @if($status_actual == $result->name_status)
                                 <option selected value="{{$result->name_status }}">{{$result->name_status}}</option>
                            @else
                                 <option  value="{{$result->name_status }}">{{$result->name_status}}</option>
                            @endif
                     @endif
                @endforeach
            @endif
            </select>

            <button class="btn" style="background-color: #00B2A6;width: 70px;height: 37px;position: relative; top: 32px"  onClick="top.location.href= '{{ url('/admin/Usuarios_app/Detalle_Candidato?id='. $candidato[0]->id ) }}'">
            <label style="color: white">Ver</label>
            </button>

        </div>
            <div class="form-group col-md-6">
                <label for="inputMotivo">Motivo : *</label>
                <?php $content3 = \Illuminate\Support\Facades\DB::select("select case
                                   when status = 'Contratado' then reason_hired
                                   when status = 'Vacante cubierta' then reason_rejected
                                   when status = 'Rechazado' then reason_rejected
                                   when status = 'Psicométrico' then reason_psychometric
                                   when status = 'En entrevista' then reason_interview
                                   when status = 'CV Visto' then reason_cv_sent
                                    end as msg  from candidate_vacancy where id = ?;",[$_GET['id']]);
                foreach($content3 as $result){  $motivo = $result->msg;  } ?>
                <textarea type="text" class="form-control" id="inputMotivo" name="inputMotivo" placeholder="Motivo" required>{{isset($motivo)?$motivo:null}}</textarea>
            </div>
        </div>

        <button name="blnc" id="blnc"  style="float: right" type="submit" class="btn btn-info">Guardar</button>
        <a class="btn btn-secondary" style="color: white;float: right;margin-right: 10px" href="{{url('/admin/Postulados')}}">Volver</a>
             <br>
        <br>

</form>

@endforeach



</div>
<br>






<script>
    history.pushState(null, "", "/admin/Postulados/Ver_postulacion?id={{$_GET['id']}}");
</script>
<?php  } }?>
</body>
@endsection
