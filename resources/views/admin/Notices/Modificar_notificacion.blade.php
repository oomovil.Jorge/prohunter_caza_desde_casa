@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 6;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">
    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-notificaciones.png')}}">&nbsp;&nbsp; Modificar notificación</label>
        <br>
        <br>

       @if(isset($_GET['Msg']) && $_GET['Msg'] === 'Modificado correctamente' )
           <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{  $_GET['Msg'] }}</strong>
           </div>
       @elseif(isset($_GET['Error']) && isset($_GET['Msg']))
           <div class="alert alert-warning">
           <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $_GET['Msg'] }}</strong>
           </div>
       @endif

        @if(!isset($_GET['id']))
            <label><strong>Notificación no encontrada</strong></label>
        @else
        @php($result = \App\Notice::where('id',$_GET['id'])->first())
        @if(is_null($result) )
           <label><strong>Notificación no encontrada</strong></label>
        @else

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/modificar_notificacion')}}">

            <div class="form-group col-md-6">
                <label for="inputtitulo">Titulo *</label>
                <input type="text" class="form-control" id="inputtitulo" name="inputtitulo" minlength="3" placeholder="Nombre completo" required value="{{$result->titulo}}">
            </div>
            <div class="form-group col-md-8">
                <label for="inputContenido">Contenido *</label>
                <textarea class="form-control" id="inputContenido" name="inputContenido" rows="3" minlength="3"  placeholder="..." required>{{$result->contenido}}</textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="inputfile">Imagen *</label>
                <input type="file" class="form-control" id="inputfile" name="inputfile"  accept="image/*"  >
                <br>

                <img src="{{$result->imagen}}"  id="img1" height="50%" width="100%"  style="object-fit: cover;padding-left: 5%;max-height: 600px;max-width: 400px">


                <br>
                <br>
            </div>
            <input hidden id="inputid" name="inputid" value="{{$result->id}}">
            <button type="submit" class="btn btn-info">Guardar</button>
            <a class="btn btn-secondary" style="color: white;" href="{{route('notificacion')}}">Volver</a>

        </form>
       @endif
        @endif
    </div>
    </body>

    <script>
        function init() {
            var inputFile = document.getElementById('inputfile');
            inputFile.addEventListener('change', mostrarImagen, false);
        }

        function mostrarImagen(event) {
            var file = event.target.files[0];
            var reader = new FileReader();
            reader.onload = function(event) {
                var img = document.getElementById('img1');
                img.src= event.target.result;
            };
            reader.readAsDataURL(file);
        }
        window.addEventListener('load', init, false);
        history.pushState(null, "", "/admin/Notificaciones/Modificar_notificacion?id={{$_GET['id']}}");
    </script>

@endsection