@extends('admin.layouts.app')
@section('Contenido')

<?php // Verifica permiso si no redirege a dash
$id_modulo = 6;
$permiso = false;
$asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

foreach(json_decode($asignados->modules) as $assigned){
    if($id_modulo == $assigned){
        $permiso = true;
    }
}
if(!$permiso){
    header('Location: /admin/Dashboard');
    exit;
}
?>
        <!-- Contenido Principal  -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>

<script>
    $(document).ready(function() {
        $('#Tabla').DataTable({

            "language": {
                "lengthMenu": "Mostrar _MENU_ notificaciones ",
                "zeroRecords": "Sin resultados encontrados..",
                "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                "infoEmpty": "No resultados disponibles",
                "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "processing":  "Buscando...",
                "paginate": {
                    "first":    "Primero",
                    "last":     "Ultimo",
                    "next":     "Siguiente",
                    "previous": "Anterior"

                } } })  }
    );

    function Confirmacion(id){
        var ids = id;

        alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                function() {
                    window.location.href = '{{url('/api/eliminar_notificacion')}}?id= '+ ids;
                },
                function() {/*Cancel*/ });
    }

</script>
<style>
    .thumbnail {position: relative; z-index: 0; }
    .thumbnail:hover{ background-color: transparent; z-index: 50; }
    .thumbnail span{ /* Estilos para la imagen agrandada */
        position: absolute;
        background-color: black;
        padding: 5px;
        left: -100px;
        border: 5px double gray;
        visibility: hidden;
        color: #ffffff;
        text-decoration: none;
    }
    .thumbnail span img{ border-width: 0; padding: 2px; }
    .thumbnail:hover span{ visibility: visible; top: 0; left: 10px; }

</style>
<body style="overflow-x: hidden;background: #F3F7FA;">

<div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">
    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-notificaciones.png')}}">&nbsp;&nbsp;Notificaciones</label>
    <br>
    <br>
    <button class="btn btn-info" style="color: white" onClick="top.location.href= '{{ url('/admin/Notificaciones/Nueva_Notificacion') }}'">Nuevo</button>
    <br>

    <?php
    if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente' ){
        echo '<br>';
        echo '<div class="alert alert-success">';
        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
        echo '<strong>'.$_GET['Msg'].'</strong>';
        echo '</div>';
    }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
        echo '<br>';
        echo '<div class="alert alert-warning">';
        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
        echo '<strong>'.$_GET['Msg'].'</strong>';
        echo '</div>';
    }
    ?>
    <br>
    <table id="Tabla" class="table" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;vertical-align: middle">Titulo</th>
            <th style="text-align: center;vertical-align: middle">Contenido</th>
            <th style="text-align: center;vertical-align: middle">Imagen</th>
            <th style="text-align: center;vertical-align: middle">Fecha Creación</th>
            <th style="text-align: center;vertical-align: middle">Actualizado última vez</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php
        $content =  \Illuminate\Support\Facades\DB::select('select * from notices');?>
        @foreach($content as $result)
            <tr>
                <td>{{ $result->titulo }}</td>
                <td>{{ $result->contenido }}</td>

        @if(isset($result->imagen))
              <td>
               <a class="thumbnail" href="#thumb"><img src="{{ $result->imagen }}" width="35px" height="35px" border="0" />
           <span><img src="{{ $result->imagen }}" /></span></a>
            @else
               <td>Sin imagen</td>
        @endif
                <td>{{ $result->created_at }}</td>
                <td>{{ $result->updated_at }}</td>


                <td  style="text-align: center;vertical-align: middle"> <!--  Eliminar   -->
                    <button class="btn" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Confirmacion({{$result->id }})">
                        <img src="{{url('Img/icn-eliminar.png')}}">
                    </button>
                </td>

                <td  style="text-align: center;vertical-align: middle"> <!--  Modificar   -->
                    <button class="btn" style="background-color: #00B2A6;width: 40px;height: 20px"  onClick="top.location.href= '{{ url('/admin/Notificaciones/Modificar_notificacion?id='. $result->id ) }}'">
                        <img src="{{url('Img/icn-editar.png')}}">
                    </button>
                </td>
            </tr>
        @endforeach
    </table>
</div>
</body>
<!-- Fin Contenido Principal  -->

<script>
    window.onload = function() {
        if (document.getElementsByClassName('dataTables_empty').length) {
            document.getElementById('Tabla_paginate').style.visibility = "hidden";
        }
    };
    history.pushState(null, "", "/admin/Notificaciones");</script>
@endsection