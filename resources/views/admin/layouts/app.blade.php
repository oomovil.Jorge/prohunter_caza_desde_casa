<?php

if(!isset($_SESSION['Oomovil']) or !$_SESSION['isAdmin']){
    header('Location: /');
    exit;
}else{
Session::put('session',$_SESSION['Oomovil']);
Session::put('privilegios',$_SESSION['privilegios']);

$session =  \Illuminate\Support\Facades\Session::get('session');
$privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');


$usuario_app = \App\User::where('id',$session)->first();
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"  style="min-width: 1380px;overflow: scroll">

          <head>
                <meta charset="utf-8">
                <meta name="Pro hunter">
                <meta name="Jorge Manzano">
                <meta name="csrf-token" content="{{ csrf_token() }}">

              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <link rel="stylesheet" href=" {{ asset('css/bootstrap.css')}}">
              <link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
              <!--Css -->
              <link href="{{asset('css/Dashboard.css')}}" rel="stylesheet">
              <!--Scripts-->
              <script src="{{asset('jquery-3.2.1.min.js')}}"></script>
              <script src="{{asset('css/Bootstrap/js/bootstrap.min.js')}}"></script>
             <!--Alertify-->
              <link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
              <link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
              <script src="{{asset('alertifyjs/alertify.js')}}"></script>

              <link rel="stylesheet" href="{{asset('css/w3.css')}}">
              <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">


              <link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
              <script src="{{asset('uikit/js/uikit.min.js')}}"></script>
              <script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>
                <style>
                    .clear-botton{
                        clear: both;
                        width: 100%;
                    }
                </style>

              <script type="text/javascript">
       $(function() {
          $('.Close_Session').click(function(event) {
                alertify.confirm('Cerrar sesión',"¿Quieres cerrar sesión?",
                        function() {
                       window.location.href = '{{url('/api/cerrar_sesion_admin')}}';
                          }, function() {/*Cancel*/});
                      });
                  });
              </script>

              <title>Pro Hunter</title>
            </head>
     <body>





 <div id="wrapper" class="toggled">
                <!-- Inicio barra  -->
<?php  $asignados = \App\administrator_modules_assigned::where('user_id',$session)->first(); ?>
 @if(!is_null($asignados))
        <!-- no existe la 11 -->
     @foreach(json_decode($asignados->modules) as $assigned)

             @switch($assigned)
             @case(1) <?php $mod_admin = true ?>  @break
             @case(2) <?php  $mod_usuarios_app = true ?>   @break
             @case(3) <?php  $mod_categorias = true ?>  @break
             @case(4) <?php  $mod_vacantes = true ?>  @break
             @case(5) <?php  $mod_postulados = true ?>  @break
             @case(6) <?php  $mod_notificaciones = true ?>  @break
             @case(7) <?php $mod_introapp = true ?> @break
             @case(8) <?php $mod_zona_geo = true ?> @break
             @case(9) <?php $mod_preguntas_frec = true ?> @break
             @case(10) <?php $mod_idiomas = true ?>  @break
             @case(12) <?php $mod_soft_cat = true ?> @break
             @case(13) <?php $mod_blog_aviso = true ?>  @break
             @case(14) <?php $mod_acerca_app = true ?> @break
             @case(15) <?php $mod_bancos = true ?> @break

             @endswitch

         @endforeach
     @endif


     <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">
                        <li>
                            <div style="background: #213B56" align="center">
                                <div id="Espacio" style="height: 10px"></div>
                                <img src="{{ url('Img/logo-prohunter-dashboard.png') }}">
                                <label style="font-size: 75%;color: #ffffff;">PERFIL DE ADMINISTRADOR </label>
                            </div>
                        </li>
                        <li>
                            <label style="color: #ffffff">Menú</label>
                        </li>
                        <li>
                            <a href="{{url('/admin/Dashboard')}}" style="font-size: 95%"><img src="{{ url('Img/icn-dashboard.png')}}">&nbsp;&nbsp;Dashboard</a>
                        </li>

                        @if($privilegios == 'Root' || isset($mod_admin) )
                        <li>
                            <a href="{{url('/admin/Administradores')}}" style="font-size: 95%"><img src="{{ url('Img/icn-administradores.png')}}">&nbsp;&nbsp;Administradores</a>
                        </li>
                        @endif
                        @if($privilegios == 'Root' || isset($mod_usuarios_app))
                        <li>
                            <a href="{{url('/admin/Usuarios_app')}}" style="font-size: 95%"><img src="{{ url('Img/icn-usuarios.png')}}">&nbsp;&nbsp;Usuarios APP</a>
                        </li>
                        @endif
                        @if($privilegios == 'Root' || isset($mod_categorias) && $mod_categorias )
                        <li>
                            <a href="{{url('/admin/Categorias')}}" style="font-size: 95%"><img src="{{ url('Img/icn-categorias.png')}}">&nbsp;&nbsp;Categorías</a>
                        </li>
                        @endif
                        @if($privilegios == 'Root' || isset($mod_vacantes))
                        <li>
                            <a href="{{url('/admin/Vacantes')}}" style="font-size: 95%"><img src="{{ url('Img/icn-vacantes.png')}}">&nbsp;&nbsp;Vacantes</a>
                        </li>
                        @endif
                        @if($privilegios == 'Root' || isset($mod_postulados))
                        <li>
                            <a href="{{url('/admin/Postulados')}}" style="font-size: 95%"><img src="{{ url('Img/icn-postulados.png')}}">&nbsp;&nbsp;Postulados</a>
                        </li>
                        @endif
                        @if($privilegios == 'Root' || isset($mod_notificaciones))
                        <li>
                            <a href="{{url('/admin/Notificaciones')}}" style="font-size: 95%"><img src="{{ url('Img/icn-notificaciones.png')}}">&nbsp;&nbsp;Notificaciones</a>
                        </li>
                        @endif
               @if(isset($mod_introapp) ||  isset($mod_zona_geo) || isset($mod_preguntas_frec) || isset($mod_idiomas) || isset($mod_bancos) || isset($mod_soft_cat) || isset($mod_soft_cat) || isset($mod_blog_aviso) || isset($mod_acerca_app))
                        <li class="active">
                            <a href="#ComplemetosSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <img src="{{ url('Img/icn-complementos.png')}}">&nbsp;&nbsp;Complementos</a>
                            <ul class="collapse list-unstyled" id="ComplemetosSubmenu">

                                @if($privilegios == 'Root' || isset($mod_introapp))
                                <li>
                                    <a href="{{url('/admin/Complementos/Admin_intro_app')}}" style="font-size: 80%"> - Admin. Intro APPS</a>
                                </li>
                                @endif
                                    @if($privilegios == 'Root'  || isset($mod_zona_geo))
                                <li>
                                    <a href="{{url('/admin/Complementos/Zona_Geografica')}}" style="font-size: 80%"> - Zona geográfica</a>
                                </li>
                                    @endif
                                 @if($privilegios == 'Root'  || isset($mod_preguntas_frec))
                                <li>
                                    <a href="{{url('/admin/Complementos/Preguntas_frecuentes')}}" style="font-size: 80%"> - Preguntas frecuentes</a>
                                </li>
                                 @endif
                                @if($privilegios == 'Root'  || isset($mod_idiomas))
                                <li>
                                    <a href="{{url('/admin/Complementos/Idiomas')}}" style="font-size: 80%"> - Idiomas</a>
                                </li>
                                    @endif
                                        @if($privilegios == 'Root' || isset($mod_bancos))
                                <li>
                                    <a href="{{url('/admin/Complementos/Bancos')}}" style="font-size: 80%"> - Bancos</a>
                                </li>
                                    @endif
                                            @if($privilegios == 'Root'  || isset($mod_soft_cat))
                                <li>
                                    <a href="{{url('/admin/Complementos/Software_Categorias')}}" style="font-size: 80%"> - Software y categorías</a>
                                </li>
                                    @endif
                                  @if($privilegios == 'Root'  || isset($mod_blog_aviso))
                                        <li>
                                    <a href="{{url('/admin/Complementos/Blog_Aviso_privacidad')}}" style="font-size: 80%"> - Blog y Aviso privacidad</a>
                                </li>
                                  @endif
                                  @if($privilegios == 'Root'  || isset($mod_acerca_app))
                                <li>
                                    <a href="{{url('/admin/Complementos/Acerca_de_app')}}" style="font-size: 80%"> - Acerca de APPS</a>
                                </li>
                                  @endif
                        @endif
                            </ul>
                        </li>
                    </ul>
                </div>


             <!-- Fin barra  -->
     <style>
         button:hover {
             box-shadow: 0 4px 16px rgb(164, 165, 169);
             transition: all 0.2s ease;
         }
     </style>
                <div class="menu">

                    <ul>
                <!--        <a style="float: left;color: white;padding: 5px" href="#menu-toggle"  id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>-->
                        <li>
                            <a style="font-size: 14px;color: white">

                 @if($usuario_app->imagen_perfil != null)
         <img class="w3-circle" style="float:left; margin:-1px;width: 26px;height: 26px" src="{{asset('storage/'. $usuario_app->imagen_perfil.'')}}">&nbsp;&nbsp;
                       {{ $usuario_app->correo }}
                    @else
                    <img style="float:left; margin:-1px;width: 26px;height: 26px" class="w3-circle" src="{{asset('Img/icn-perfil-.png')}}">&nbsp;&nbsp;{{ $usuario_app->correo }}
              @endif
                      </a>
                            <ul class="Close_Session">
                                <li><img style="float:left;" src="{{asset('Img/icn-cerrar-sesion.png')}}">
                                    <a>&nbsp;&nbsp;Cerrar sesión</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="page-content-wrapper">
                    <div class="container-fluid">
                        <br>
              @yield('Contenido')
                    </div>
                </div>
            </div>
 <script>
     $("#menu-toggle").click(function(e) {
         e.preventDefault();
         $("#wrapper").toggleClass("toggled");
     });
     localStorage.setItem('admprohunter','{{$usuario_app->correo}}');

 </script>

<!-- firebase  -->
<script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
<script src="{{asset('js/firebaseAdmin.js')}}"></script>

     </body>
</html>
<?php  }  ?>

