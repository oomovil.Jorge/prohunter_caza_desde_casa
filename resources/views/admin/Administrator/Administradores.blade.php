@extends('admin.layouts.app')
@section('Contenido')

      <?php // Verifica permiso si no redirege a dash
          $id_modulo = 1;
          $permiso = false;
          $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();


          if(!$asignados){
              header('Location: /admin/Dashboard');
              exit;
          }
          foreach(json_decode($asignados->modules) as $assigned){
              if($id_modulo == $assigned){
                  $permiso = true;
              }
          }
          if(!$permiso){
              header('Location: /admin/Dashboard');
              exit;
          }
           ?>



        <!-- Contenido Principal  -->
<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#Tabla').DataTable({

            "language": {
                "lengthMenu": "Mostrar _MENU_ usuarios ",
                "zeroRecords": "Sin resultados encontrados..",
                "info": "Mostrando _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "No resultados disponibles",
                "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "processing":  "Buscando...",
                "paginate": {
                    "first":    "Primero",
                    "last":     "Ultimo",
                    "next":     "Siguiente",
                    "previous": "Anterior"

                } } })  } );


    function Confirmacion(id){
        let ids = id;

        alertify.confirm('Eliminar',"¿Estas seguro de querer eliminar?",
                function() {
                    window.location.href = '{{url('/api/eliminar_Admin?id=')}}'+ ids;
                },
                function() {/*Cancel*/ });
    }
</script>
<body style="overflow-x: hidden;background: #F3F7FA;">

<style>
    button:hover {
        color: rgba(255, 255, 255, 1) !important;
        box-shadow: 0 4px 16px rgb(173, 174, 178);
        transition: all 0.2s ease;
    }
</style>

<div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">
    <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-administradores.png')}}">&nbsp;&nbsp;Administradores</label>
    <br>
    <br>
    <button class="btn btn-info" style="color: white" onClick="top.location.href= '{{ url('admin/Administradores/Nuevo_administrador') }}'">Agregar</button>
    <br>

    <?php if(isset($_GET['Msg']) && $_GET['Msg'] === 'Eliminado correctamente' ){
        echo '<br>';
        echo '<div class="alert alert-success">';
        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
        echo '<strong>'.$_GET['Msg'].'</strong>';
        echo '</div>';
    }else  if(isset($_GET['Error']) && isset($_GET['Msg'])){
            echo '<br>';
            echo '<div class="alert alert-warning">';
            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        } ?>
        <br>
    <table id="Tabla" class="table" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;vertical-align: middle">Nombre</th>
            <th style="text-align: center;vertical-align: middle">Correo</th>
            <th style="text-align: center;vertical-align: middle">Celular</th>
            <th style="text-align: center;vertical-align: middle">Privilegios</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>


      @foreach($table as $result)
          <tr>
              <td>
              @if(isset($result->imagen_perfil))
            <img style="width: 30px;height: 30px" src="{{asset('storage/'. $result->imagen_perfil.'')}}">
              @endif
              &nbsp;{{ $result->nombre }}</td>
              <td>{{ $result->correo }}</td>
              <td>{{ $result->celular }}</td>
              <td>{{ $result->tipo }}</td>

              <td  style="text-align: center;vertical-align: middle"> <!--  Eliminar   -->
                  <button class="btn btnhover" style="background-color: #b24d57;width: 40px;height: 20px"  onclick="Confirmacion({{$result->id }})">
                      <img src="{{url('Img/icn-eliminar.png')}}">
                  </button>
              </td>

              <td  style="text-align: center;vertical-align: middle"> <!--  Modificar a8aab2   -->
                  <button class="btn btnhover" style="background-color: #00B2A6;width: 40px;height: 20px"  onClick="top.location.href= '{{ url('/admin/Administradores/Modificar_administrador?correo='. $result->correo ) }}'">
                      <img src="{{url('Img/icn-editar.png')}}">
                  </button>
              </td>
          </tr>
        @endforeach
    </table>
</div>
</body>
<!-- Fin Contenido Principal  -->
@endsection
