@extends('admin.layouts.app')
@section('Contenido')

    <?php // Verifica permiso si no redirege a dash
    $id_modulo = 1;
    $permiso = false;
    $asignados = \App\administrator_modules_assigned::where('user_id',Session::get('session'))->first();

    foreach(json_decode($asignados->modules) as $assigned){
        if($id_modulo == $assigned){
            $permiso = true;
        }
    }
    if(!$permiso){
        header('Location: /admin/Dashboard');
        exit;
    }
    ?>

    <body style="overflow-x: hidden;background: #F3F7FA;">

    <div class="container" style="background: white;
-webkit-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 -moz-box-shadow: 5px 3px 5px -3px rgba(148,148,148,1);
 box-shadow: 5px 3px 5px -3px  rgba(0,0,0,0.20);
 padding: 50px;">

        <label style="font-size: 25px;font-family: OpenSans-Semibold"><img src="{{url('Img/icn-seccion-administradores.png')}}">&nbsp;&nbsp; Modificar Administrador</label>
        <br>
        <br>

        <?php
        if(isset($_GET['Msg']) && $_GET['Msg'] === 'Modificado correctamente.' ){
                echo '<div class="alert alert-success">';
                echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>'.$_GET['Msg'].'</strong>';
                echo '</div>';

        }else if(isset($_GET['Error']) && isset($_GET['Msg'])){
                echo '<div class="alert alert-warning">';
                echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>'.$_GET['Msg'].'</strong>';
                echo '</div>';
            }

        $user_edit = \App\User::where('correo', $_GET['correo'] )
            ->where('tipo','=' ,'Administrador')
            ->orWhere('tipo','=' ,'Root')
            ->where('correo','=',$_GET['correo'])->first();


        if(!isset($_GET['correo']) || is_null($user_edit)){
                echo '<label><strong>Usuario no encontrado</strong></label>';
            }else{

                ?>

        <form enctype="multipart/form-data" method="POST" action="{{url('/api/modificar_Admin')}}">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputNombre">Nombre completo *</label>
                    <input type="text" class="form-control txtSolo" id="inputNombre" name="inputNombre" placeholder="Nombre completo" minlength="3" required value="{{ $user_edit->nombre }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputCorreo">Correo Electrónico *</label>
                    <input readonly type="email" class="form-control txtEmail" id="inputCorreo" name="inputCorreo" placeholder="Correo Electrónico"  minlength="6" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required value="{{ $user_edit->correo }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputContrasena">Cambiar Contraseña</label>
                    <input type="password" class="form-control" id="inputContrasena" name="inputContrasena" minlength="3" placeholder="Cambiar Contraseña">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputConfirmar"> Confirmar contraseña</label>
                    <input type="password" class="form-control" id="inputConfirmar" name="inputConfirmar" minlength="3" placeholder="Confirmar contraseña">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputCel">Celular *</label>
                    <input type="text" class="form-control txtCel" id="inputCel" name="inputCel" placeholder="Celular" minlength="3" maxlength="11" required value="{{ $user_edit->celular }}" >
                </div>
                <div class="form-group col-md-4">
                    <label for="inputfile">Imagen de perfil</label>
                    <input type="file" class="form-control" id="inputfile" name="inputfile" accept="image/*" >
                </div>
                <div class="form-group col-md-2">

                    <label for="inputPrivilegio">Privilegios *</label>
                    <select class="form-control" id="inputPrivilegio" name="inputPrivilegio" required>
                        @if($user_edit->tipo == 'Administrador')
                            <option value="Administrador" selected>Administrador</option>
                            <option value="Root">Root</option>
                        @else
                            <option value="Administrador" >Administrador</option>
                            <option value="Root" selected>Root</option>
                        @endif
                    </select>
                </div>
            </div>
            <br>
            <div id="Modulos">
            <label for="inputPrivilegio">Modulos </label>

            @php
                $modules = \App\modules_admin::all();
                $asignados = \App\administrator_modules_assigned::where('user_id',$user_edit->id)->first();
            @endphp

            @foreach($modules as $module)
                 <div class="custom-control custom-checkbox">
                   <input
                           @if(!is_null($asignados))
                           @foreach(json_decode($asignados->modules) as $assigned) {{ $assigned == $module->id ? 'checked' : ''}} @endforeach
                           @endif
                           type="checkbox" class="custom-control-input" id="{{ str_replace(" ", "",$module->name) }}" name="{{ str_replace(" ", "",$module->name)}}" value="{{$module->id}}">
                   <label class="custom-control-label" for="{{ str_replace(" ", "",$module->name) }}">{{$module->name}}</label>
                </div>
                @endforeach
            </div>
            <br>
            <br>
            <button type="submit" class="btn btn-info">Guardar</button>
            <a class="btn btn-secondary" style="color: white" onClick="top.location.href= '{{ route('administradores') }}'">Volver</a>

        </form>
        <?php  }  ?>
    </div>
    </body>
    <!-- Fin Contenido Principal  -->
    <script src="https://unpkg.com/imask"></script>
    <script>
        history.pushState(null, "", "/admin/Administradores/Modificar_administrador?correo={{ $user_edit->correo }}");

        if($('#inputPrivilegio').val() == 'Root'){
            $('#Modulos').hide();
        }else{
            $('#Modulos').show();
        }

        $('#inputPrivilegio').change(function (){
          if($('#inputPrivilegio').val() == 'Root'){
              $('#Modulos').hide();
          }else{
              $('#Modulos').show();
          }
        });




        window.onload = function() {
            if (document.getElementsByClassName('dataTables_empty').length) {
                document.getElementById('Tabla_paginate').style.visibility = "hidden";
            }
        };

        Array.from(document.getElementsByClassName('txtSolo')).forEach(function (item){
            var mask = new IMask(item, {
                mask: /^[a-zA-Z_áéíóúñ\s]*$/
            })
        });

        Array.from(document.getElementsByClassName('txtCel')).forEach(function (item){
            var mask = new IMask(item, {
                mask: '0000000000'
            })
        });
    </script>
@endsection
