<?php


//OBTENER DATOS DEL USUARIO
$email    =   array();
$correo   =   array();
$vacio    =   array();
$miscandidatos_array = array();

$service = new \GuzzleHttp\Client(['verify' => false]);


// OBTENER VACANTES

$response = $service->request('POST', url('api/listado_vacantes'),$email);
$vacantes = json_decode($response->getBody()->getContents());
$json_vacantes = json_encode($vacantes->data);

// OBTENER CANDIDATOS
$response = $service->request('POST', url('api/listado_candidatos'),$email);
$candidatos = json_decode($response->getBody()->getContents());

// LISTADO DE VACANTES FAVORITAS
$response = $service->post(url('api/listado_vacantes_favoritas'), $email);
$vacantes_favoritas = json_decode($response->getBody()->getContents());

// LISTADO DE POSTULADOS
$response = $service->post(url('api/listado_postulados'), $email);
$listado_postulados = json_decode($response->getBody()->getContents());

// PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());
// BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());
// ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());
// AVISO DE PRIVACIDAD
$response = $service->post(url('api/aviso_privacidad'), $vacio);
$aviso_privacidad = json_decode($response->getBody()->getContents());




?>

        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale())}}" style="min-width: 1280px;overflow: scroll;">

<head>

    <title>Pro Hunter</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Pro hunter">
    <meta name="Oomovil">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="{{asset('css/toolbar.css')}}">
    <link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/w3.css')}}">
    <link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

    <script src="{{asset('jquery-3.2.1.min.js')}}"></script>
    <!--Alertify-->
    <link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
    <script src="{{asset('alertifyjs/alertify.js')}}"></script>
    <!-- uikit -->
    <link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
    <script src="{{asset('uikit/js/uikit.min.js')}}"></script>
    <script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

    <!--lightbox -->
    <link href="{{asset('lightbox/css/lightbox.css')}}" rel="stylesheet">
    <script src="{{asset('lightbox/js/lightbox.js')}}"></script>

    <style>


        body{
            height: 1111px;
        }
        @font-face {
            font-family: 'Harabara';
            src: url('{{asset('Fonts/Harabara.ttf')}}');
        }
        strong, .title-dash,#mis_favoritas,#all_vacantes,.btnRedondo_gris,.btnRedondo_verde{
            font-family:  'Harabara';
        }
        ::-webkit-scrollbar {
            display: none;
        }
        .w3-dropdown-content{
            right: 0px;
            margin-right: 10px;
            border-radius: 5px;
            /*padding: 10px 0px;*/
            color: #c2c9d1;
        }

        .w3-button:hover{
            background: #41636f !important;
            color: white !important;
            text-decoration:none !important;
        }

    </style>
    <link rel="stylesheet" href="{{asset('css/web-dash.css')}}">

<body class="w3-theme-l5">


<!-- Navbar -->
<div class="w3-top ph-nav">
    <div class="w3-bar w3-theme-d2 w3-left-align w3-large">

        <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>


        <div class="ph-search w3-dropdown-hover w3-hide-small">

            <form class="uk-search uk-search-default" id="buscar" style="width: 300px;">
                <button class="uk-search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
                <input class="uk-search-input" type="search" placeholder="Buscar" name="q">
            </form>

        </div>

        <div class="w3-dropdown-hover w3-hide-small" style="float: right">
            <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-user"></i><span id="perfil" name="perfil"  class="w3-badge w3-right w3-small w3-green"></span></button>
            <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block" >
                <div style="padding: 10px 0px;">
                    <img src="{{asset('storage/app/triangle.svg')}}" style="position: absolute;top: -16px;height: 21px;right: 2px;">
                    <a href="{{route('Login_web') }}" class="w3-bar-item w3-button">Iniciar sesión</a>
                    <a href="{{route('registro') }}" class="w3-bar-item w3-button">Registrarse</a>
                    <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>


                </div>
            </div>
        </div>

    </div>
</div>
<!-- Fin Navbar -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top: 50px;padding-top: 10px;">
    <div class="w3-row">
        <!-- Left Column -->
        <div class="w3-col m3">
            <div class="title-dash"></div></div>
        <!-- End Left Column -->

        <!-- Middle Column -->
        <div class="w3-col m6" id="col_central" name="col_centrar">

            <div>
                <ul class="uk-child-width-expand" uk-tab>
                    <li><a id="all_vacantes" href="#">Vacantes</a></li>
                </ul>
            </div>

            <!-- Vacantes -->
            <div id="vacantes_div">

            </div>
        </div>
        <!-- End Middle Column -->


    </div>
</div>

<!-- End Page Container -->
<br>

<script src="{{asset('js/dashboard.js')}}"></script>
<script>


    formBuscarFetch('');
    // Accordion
    function myFunction(id) {
        var x = document.getElementById(id);
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
            x.previousElementSibling.className += " w3-theme-d1";
        } else {
            x.className = x.className.replace("w3-show", "");
            x.previousElementSibling.className =
                    x.previousElementSibling.className.replace(" w3-theme-d1", "");
        }
    }
    // Used to toggle the menu on smaller screens when clicking on the menu button
    function openNav() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }


 try {
    //Galeria de img
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }


     function showDivs(n) {
         var i;
         var x = document.getElementsByClassName("mySlides");
         if (n > x.length) {
             slideIndex = 1
         }
         if (n < 1) {
             slideIndex = x.length
         }
         for (i = 0; i < x.length; i++) {
             x[i].style.display = "none";
         }
         x[slideIndex - 1].style.display = "block";
     }

     // fin galeria
 }catch (err){

 }
    formBuscarCandidato('');


    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async function invitado() {
        let no_entra = true;
        while(entra){
        await sleep(500);
        for(let a = 0; a < document.getElementById('vacantes_div').childElementCount ; a++) {
            document.getElementsByClassName('btnRedondo_gris fav')[a].style.display = 'none';
            document.getElementsByClassName('btnRedondo_verde')[a].style.display = 'none';
            no_entra = false;
          }
        }
    } invitado();


</script>


</body>
</html>
