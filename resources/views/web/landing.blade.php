<!doctype html>
<html lang="en">

<head>
  <title>Landing</title>
  <base href="/">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script>
    if (window.location.protocol !== "https:") {
        //window.location.href='https://'+window.location.host+window.location.pathname;
    }
  </script>
  <style>
  </style>
<link rel="stylesheet" href="styles.05784a219d555f300849.css"></head>

<body>
  <app-root></app-root>
  <script>
    $('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
      &&
      location.hostname === this.hostname
    ) {
      // Figure out element to scroll to
      let target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          let $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        });
      }
    }
  });
    // Modal Image Gallery
    function onClick(element) {
      document.getElementById("img01").src = element.src;
      document.getElementById("modal01").style.display = "block";
      let captionText = document.getElementById("caption");
      captionText.innerHTML = element.alt;
    }

    window.onscroll = function() {myFunction()};
    function myFunction() {

        if (document.body.scrollTop > document.querySelector("header").clientHeight-59 || document.documentElement.scrollTop > document.querySelector("header").clientHeight-59) {
            document.getElementById("myNavbar").className = "w3-bar w3-text-white menu-blue";
        } else {
            document.getElementById("myNavbar").className = "w3-bar w3-text-white";
        }
    }
/*
   window.onload = function (){
    $.post( 'https://porvenirapps.com/api/listado_preguntas_frecuentes', function( response ) {

      console.log(response);
        console.log(document.getElementById('nodeFAQS'));
      });
   }*/
  </script>

<script type="text/javascript" src="runtime.a66f828dca56eeb90e02.js"></script><script type="text/javascript" src="polyfills.7fb637d055581aa28d51.js"></script><script type="text/javascript" src="main.becabb45f4cacc10022f.js"></script></body>

</html>
