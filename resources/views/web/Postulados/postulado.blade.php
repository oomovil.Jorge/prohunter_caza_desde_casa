<?php

if(!isset($_SESSION['Oomovil']) or $_SESSION['isAdmin'] == true){
  header('Location: /');
  exit;
}else{

Session::put('session',$_SESSION['Oomovil']);
Session::put('privilegios',$_SESSION['privilegios']);
$session =  \Illuminate\Support\Facades\Session::get('session');
$privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');


//OBTENER DATOS DEL USUARIO
$usuario_app = \App\User::where('id',$session)->first();
$usuario_app_info_bancaria = \App\UserBankData::where('id_user',$session)->first();

$email = ['form_params' => ['email' => $usuario_app->correo]];
$parametros_postulacion = ['form_params' => [
    "email" => $usuario_app->correo ,
     "id_postulacion" => isset($_GET['id']) ? $_GET['id'] : 0
]];

$correo = ['form_params' =>['correo' => $usuario_app->correo]];
$vacio    =   array();

$service = new \GuzzleHttp\Client(['verify' => false]);

// PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());
// BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());

// ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());
// AVISO DE PRIVACIDAD
$response = $service->post(url('api/aviso_privacidad'), $vacio);
$aviso_privacidad = json_decode($response->getBody()->getContents());

// LISTADO DE NOTIFICACIONES DEL USUARIO
$response = $service->post(url('api/listado_notificacion'), $vacio);
$notificaciones = json_decode($response->getBody()->getContents());
// LISTADO DE AVISOS DEL USUARIO
$response = $service->post(url('api/listar_avisos_usuarios'), $vacio);
$avisos = json_decode($response->getBody()->getContents());
// LISTADO DE AVISOS DEL USUARIO
$response = $service->post(url('api/buscar_postulacion'), $vacio);
$postulacion = json_decode($response->getBody()->getContents());

     if($postulacion->msg != 'No encontro la postulacion'){
       $info = null;
         foreach($postulacion as $indice => $info_postulacion){

           if(is_object($info_postulacion)){
            $info =  $info_postulacion;
            }
         }

       $candidato = \App\Candidate::where('id',$info->candidate_id)->first();
       $candidato_educacion = \App\EducationData::where('candidate_id',$info->vacancy_id)->first();

       $titulo = $candidato_educacion!=null?$candidato_educacion->degree_obtained:'Sin titulo.';
       $vacante = \App\Vacancy::where('id',$info->vacancy_id)->first();

               }else{
       header('Location: '.route('Web'));
      }
?>
        @if(isset($info))

        <!DOCTYPE html>
<html>
<title>Pro Hunter</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Pro hunter">
<meta name="Oomovil">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/toolbar.css')}}">
<link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

<script src="{{asset('jquery-3.2.1.min.js')}}"></script>
<!--Alertify-->
<link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
<link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
<script src="{{asset('alertifyjs/alertify.js')}}"></script>
<!-- uikit -->
<link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
<script src="{{asset('uikit/js/uikit.min.js')}}"></script>
<script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('css/w3.css')}}">
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">

<style>
  html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}

</style>
<body class="w3-light-grey">

    <style>
        ::-webkit-scrollbar {
            display: none;
        }
          .w3-dropdown-content{
            right: 0px;
            margin-right: 10px;
            border-radius: 5px;

            color: #c2c9d1;
          }

          .w3-button:hover{
              background: #41636f !important;
              color: white !important;
              text-decoration:none !important;
          }
      </style>
    <div id="modal-acerca" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          <div class="w3-content w3-display-container" align="center">
              @foreach($about->image_urls as $img)
              <img class="mySlides" src="{{$img}}" style="height: 250px">
              @endforeach
              <img src="{{asset('Img/logo-prohunter-color.png')}}" style="height: 100px;">
              <button class="w3-button w3-white w3-display-left"  onclick="plusDivs(-1)">&#10094;</button>
              <button class="w3-button w3-white w3-display-right" onclick="plusDivs(1)">&#10095;</button>
          </div>
          <br>
          <p>{{$about->text}}</p>

          </p>
      </div>
    </div>

    <div id="modal-aviso" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          {!!html_entity_decode($aviso_privacidad->text)!!}

          </p>
      </div>
    </div>

    <div id="modal-ayuda" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          <h2 style="text-align: center;">Ayuda</h2>
          <!-- Mis vacantes favorias -->
          <ul style="list-style-type: none;">
                  @foreach($preguntas_frecuentes->data as $preguntas)
                      <li>
                          <h3><b>{{ $preguntas->question}}</b></h3>
                          <p>{{ $preguntas->answer}}</p>
                      </li>
                      @endforeach
          </ul>

      </div>
    </div>
    <!-- Navbar -->
    <div class="w3-top ph-nav">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">

            <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>



            <div class="w3-dropdown-hover w3-hide-small Close_Session" style="float: right;" >
                    <button class="w3-button w3-padding-large" title="Cerrar_sesion"><i class="fa fa-sign-out"></i><span id="Cerrar_sesion" name="Cerrar_sesion" class="w3-badge w3-right w3-small w3-green"></span></button>
                    <div>
                        <script type="text/javascript">
                            $(function() {
                                $('.Close_Session').click(function(e) {
                                    alertify.confirm('Cerrar sesión',"¿Quieres cerrar sesión?",
                                            function() {
                                                localStorage.removeItem('prohunter');
                                                localStorage.removeItem('firebase');
                                                window.location.href = '{{url('/api/cerrar_sesion_user')}}'; window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                            }, function() {/*Cancel*/});
                                });
                            });
                        </script>
                    </div>
                </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="notificaciones"><i class="fa fa-globe"></i><span id="notificaciones" name="notificaciones" class="w3-badge w3-right w3-small w3-green"></span></button>
                    <div id="noticias-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:250px;max-height: 500px;overflow-y: scroll;">
                    </div>
            </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-bell"></i><span id="avisos" name="avisos"></span></button>
                <div id="avisos-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:300px;max-height: 500px;overflow-y: scroll;">

                </div>
            </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-user"></i><span id="perfil" name="perfil"  class="w3-badge w3-right w3-small w3-green"></span></button>
                <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block" >
                    <div style="padding: 10px 0px;">
                        <img src="{{asset('storage/app/triangle.svg')}}" style="position: absolute;top: -16px;height: 21px;left: 45px;">
                            <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

                            <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

                            <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

                            <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

                            <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

                    </div>
                </div>
            </div>
            <div class="w3-dropdown-hover w3-hide-small" style="float: right;" >
                <a href="{{route('Web')}}" class="w3-button w3-padding-large" title="inicio"><i class="fa fa-home"></i></a>

            </div>
        </div>
    </div>
    <!-- Fin Navbar -->

    <script>
      //Galeria de img
      var slideIndex = 1;
      showDivs(slideIndex);

      function plusDivs(n) {
          showDivs(slideIndex += n);
      }

      function showDivs(n) {
          var i;
          var x = document.getElementsByClassName("mySlides");
          if (n > x.length) {slideIndex = 1}
          if (n < 1) {slideIndex = x.length}
          for (i = 0; i < x.length; i++) {
              x[i].style.display = "none";
          }
          x[slideIndex-1].style.display = "block";
      }
      // fin galeria
    </script>

<!-- Page Container -->
<div class="w3-content" style="max-width:1400px;margin-top: 50px !important;">

  <!-- The Grid -->
  <div class="w3-row-padding" style="padding: 8px 8px;">

    <!-- Left Column -->
    <div class="">
       <div class="w3-white w3-text-grey w3-card-4">
          <div class="w3-display-container" align="center">

               <img  src="{{isset($candidato->profile_picture)?asset('storage/app/'. $candidato->profile_picture.''):asset('Img/notpictureuser.png')}}" class="w3-circle" style="height:106px;width:106px;margin-top: 10px" alt="Avatar">
           <br>
              <h5>{{$candidato->names.' '.$candidato->paternal_name.' '.$candidato->maternal_name}}</h5>

              <h5 style="color: #00B9AE">{{$titulo }}</h5>

               <div class="w3-display-bottomleft w3-container w3-text-black">
            </div>
          </div>
       <div class="w3-container" style="padding-bottom: 150px;padding-left:50px">
          <br>
         <p>
          <a class="w3-large"><b></i>Candidato para:</b></a>
         </p>
         <article class="uk-comment">
           <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
             <div class="uk-width-auto">
               <img class="w3-circle" src="{{isset($vacante->company_logo)? $vacante->company_logo :asset('Img/notpictureuser.png')}}" width="50" height="50" alt="">
             </div>
             <div class="uk-width-expand">
               <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">{{isset($vacante) ? $vacante->company_name : '- -' }}</a></h4>
               <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                 <li><a href="#">{{isset($vacante) ? $vacante->vacancy_name: '- -' }}</a></li>
               </ul>
             </div>
           </header>
           <div class="uk-comment-body">
             <br>
             <ul class="uk-subnav uk-subnav-divider" uk-margin >
               <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">CV Enviado</a></li>
               <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_cv_enviado.png')}}" style="height: 30px;"/></a></li>
               <li><a href="#">{{$info->date_postulate != null ?  explode(" ",$info->date_postulate)[0] : '- -' }}</a></li>
             </ul>
             <br>

             <ul class="uk-subnav uk-subnav-divider" uk-margin >
               <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">CV Visto</a></li>
               <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_cv_enviado.png')}}" style="height: 30px;"/></a></li>
               <li style="width: 150px"><a href="#">{{ is_null($info->date_cv_sent) ? '- -' : explode(" " , $info->date_cv_sent)[0]  }}</a></li>
               <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_cv_sent: '- -' }}</div>
             </ul>
             <br>



             <ul class="uk-subnav uk-subnav-divider" uk-margin >
               <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">En entrevista</a></li>
               <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_entrevista.png')}}" style="height: 28px;"/></a></li>
               <li style="width: 150px"><a href="#">{{ is_null($info->date_interview) ? '- -' : explode(" " , $info->date_interview)[0] }}</a></li>
               <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_interview: '- -' }}</div>
             </ul>
             <br>

             <ul class="uk-subnav uk-subnav-divider" uk-margin >
               <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">Psicometrico</a></li>
               <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_psicometrico.png')}}" style="height: 30px;"/></a></li>
               <li style="width: 150px"><a href="#">{{  is_null($info->date_psychometric) ? '- -' : explode(" " , $info->date_psychometric)[0]  }}</a></li>
               <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_psychometric: '- -' }}</div>
             </ul>
             <br>

             <ul class="uk-subnav uk-subnav-divider" uk-margin >
             @if($info->status == "Contratado")
               <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">Contratado</a></li>
               <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_contratado.png')}}" style="height: 30px;"/></a></li>
               <li style="width: 150px"><a href="#">{{ is_null($info->date_hired) ? '- -' : explode(" " , $info->date_hired)[0]  }}</a></li>
               <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_hired: '- -' }}</div>

             @endif
             @if($info->status == "Vacante cubierta")

                 <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">Vacante cubierta</a></li>
                 <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_vac_cubierta.png')}}" style="height: 30px;"/></a></li>
                 <li style="width: 150px"><a href="#">{{ is_null($info->date_rejected) ? '- -' : explode(" " , $info->date_rejected)[0]  }}</a></li>
                 <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_rejected: '- -' }}</div>

             @endif
             @if($info->status == "Rechazado")


                 <li style="margin-left: 20px"><a href="#" style="text-transform: capitalize;width: 100px">Rechazado</a></li>
                 <li style="width: 75px"><a href="#"><img src="{{url('Img/Desgrane/mipmap/icn_vac_cubierta.png')}}" style="height: 30px;"/></a></li>
                 <li style="width: 150px"><a href="#">{{is_null($info->date_rejected) ? '- -' : explode(" " , $info->date_rejected)[0]  }}</a></li>
                 <li><div uk-alert style="text-transform: capitalize;width: 500px;max-width: 500px">{{isset($info) ? $info->reason_rejected: '- -' }}</div>

             @endif
             </ul>
           </div>

         </article>
       </div>
       </div>
    </div>
    <!-- End Left Column -->


  <!-- End Page Container -->
</div>


</div>
<!-- firebase  -->
<script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
<script> let url = '../'; </script><script src="{{asset('js/firebase.js')}}"></script>
</body>
</html>
@endif

<?php  }  ?>
