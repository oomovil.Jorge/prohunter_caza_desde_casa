<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ProHunter">
    <meta name="Oomovil">
    <meta name="google-signin-client_id" content="134592454019-57nk01oa26md7a4nk4hre0a9io7ihpi0.apps.googleusercontent.com">

    <title>Pro Hunter</title>
    <link rel="shortcut icon" href="{{ asset('/Img/icn-favicon.png') }}">
    <link href="{{ url('/css/login.css') }}" rel="stylesheet">
    <link href="{{ url('/css/Bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{asset('/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('/css/Bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/bodymovin/js/bodymovin.min.js')}}" type="text/javascript"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
    <script src="{{asset('/js/social.js')}}"></script>

    <script>
        if (window.location.protocol != "https:") {
          //  window.location.href='https://'+window.location.host+window.location.pathname;
        }
        if(!!localStorage.getItem('prohunter')){
            window.location.href =  window.location.href+"web";
        }

    </script>
    <link rel="stylesheet" href="{{asset('/font-awesome-4.7.0/css/font-awesome.css')}}">
    <style>
        .fa {
          padding: 20px;
          font-size: 30px;
          width: 70px;
          text-align: center;
          text-decoration: none; !important;
          margin: 5px 2px;
          border-radius: 50%;
          cursor: pointer;
        }
        .fa:hover {
            box-shadow: inset 0px 0px 13px #20335a;
        }
        .fa-facebook {
          background: #3B5998;
          color: white;
        }
        .fa-google {
          background: #dd4b39;
          color: white;
        }
      </style>
</head>

<body class="FondoProhunter-usuario">
    <form id="regis" method="POST" >
        <div id="lotiFB" style="z-index: 1; position: relative; display: none; height: 539px; background-color: rgba(0,0,0,.85);"></div>
        <div id="lotiG" style="z-index: 1; position: relative; display: none; height: 539px; background-color: rgba(0,0,0,.85);"></div>

        <div class="centrar" align="Center" style="margin-top: -290px;">


            @if(isset($msg))
                <input type="text" class='form-control'> <span>{{ $msg }}</span>
            @endif

            <img src="{{ url('/Img/logo-prohunter-login.png') }}"/>
            <br>
            <br>
                <label class="LabeLogin">Registro</label>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="text" name="name" placeholder="Nombre de usuario *" required="" />
                <br>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="email" name="correo" placeholder="Correo *" required="" />
                <br>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="email" name="confirma_correo" placeholder="Confirmar correo *" required="" />
                <br>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="Password" name="contrasena" placeholder="Contraseña *" required="" />
                <br>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="Password" name="re_contrasena" placeholder="Confirmar Contraseña *" required="" />
                <br>
                <br>
                <input class="BordesRedondosInput" style="width: 100%" type="tel" name="celular" placeholder="Telefono/Celular" required="" />
                <br>
                <br>
                <input type="submit" class="Boton_Login" style="width: 100%;cursor: pointer"  value="Registrar" />
                <br>
                <div id=error>
                            <?php
                            if(isset($_GET['Error']) && isset($_GET['Msg'])){
                                echo '<br>';
                                echo '<br>';
                                echo '<div class="alert alert-danger">';
                                echo '<button  type="button" class="close" data-dismiss="alert">×</button>';
                                echo '<strong>'.$_GET['Msg'].'</strong>';
                                echo '</div>';
                            }
                            ?>
                </div>

            <label style="color: white;">¿Ya tienes una cuenta?</label>
            <a  style="text-decoration: underline;color: white;padding-bottom: 5px" href="{{url('/login')}}">Inicia sesión aquí</a>
            <br>


            <a class="fa fa-facebook" id="fb_btn"   style="color: white" onclick="iniciofb()"></a>
            <a class="fa fa-google"  id="google_btn" style="color: white" onclick="iniciog()"></a>
        <br>
                <br>
                <br>
        <br>
        </div>


    </form>
    <br>
    <script>
        let lottieAnimationFB = bodymovin.loadAnimation({
            container: lotiFB, // ID del div
            path: '{{asset("/bodymovin/json/fbicon.json")}}', // Ruta fichero .json de la animación
            renderer: 'svg', // Requerido
            loop: true, // Opcional
            autoplay: true, // Opcional
            name: "Hello World", // Opcional
        });
        let lottieAnimationG = bodymovin.loadAnimation({
            container: lotiG, // ID del div
            path: '{{asset("/bodymovin/json/loading.json")}}', // Ruta fichero .json de la animación
            renderer: 'svg', // Requerido
            loop: true, // Opcional
            autoplay: true, // Opcional
            name: "Hello World", // Opcional
        });
        const formReg = document.querySelector('#regis');

        formReg.addEventListener('submit',async (event)=>{

            event.preventDefault();
            let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            let ps = /[0-9](6)/;
            if(!re.test(formReg.correo.value)||!re.test(formReg.confirma_correo.value)){
                if(!re.test(formReg.correo.value)){
                    formReg.correo.focus();
                }
                else if(!re.test(formReg.confirma_correo.value)){
                    formReg.confirma_correo.focus();
                }
                return error_social(['El correo ingresado no cumple el formato']);

            }
            if(!ps.test(formReg.contrasena.value)||!ps.test(formReg.re_contrasena.value)){
                if(!ps.test(formReg.contrasena.value)){
                    formReg.contrasena.focus();
                }
                else if(!ps.test(formReg.re_contrasena.value)){
                    formReg.re_contrasena.focus();
                }
                return error_social(['la contraseña ingresado no tienes una longitud minima de 6 caracteres']);
            }
            if(formReg.correo.value != formReg.confirma_correo.value){
                formReg.confirma_correo.focus();
                return error_social(['Los correos no coinciden entre ellos']);

            }

            if(formReg.contrasena.value != formReg.re_contrasena.value ){
                formReg.re_contrasena.focus();
                return error_social(['Las contraseñas no coinciden entre ellas']);
            }
            let reg ={
                nombre:formReg.name.value,
                correo:formReg.correo.value,
                contrasena:formReg.contrasena.value,
                tipo:'normal',
                info_dispositivo:navigator.appVersion,
                celular: formReg.celular.value
            };
            let log ={
                correo:formReg.correo.value,
                contrasena:formReg.contrasena.value,
                info_dispositivo:navigator.appVersion
            };
            await loginReg(log,reg);
        });
        function error_social(array){
            document.getElementById('error').innerHTML = "";
            array.forEach(function (element) {
                document.getElementById('error').innerHTML += "<br>                <br>                <div class=\"alert alert-danger\">                <button  type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>                <strong>"+element+"</strong>                </div>";
            });

        }

    </script>
    <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    <script src="{{asset('/js/social.js')}}" type="text/javascript"></script>

    <script>history.pushState(null, "", "/registro");</script>

</body>
</html>


