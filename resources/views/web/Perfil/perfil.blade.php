<?php
if (!isset($_SESSION['Oomovil']) or $_SESSION['isAdmin'] == true) {
    header('Location: /');
    exit;
}

Session::put('session', $_SESSION['Oomovil']);
Session::put('privilegios', $_SESSION['privilegios']);
$session = Session::get('session');
$privilegios = Session::get('privilegios');

$service = new \GuzzleHttp\Client(['verify' => false]);

//OBTENER DATOS DEL USUARIO
$usuario_app = \App\User::where('id', $session)->first();
$usuario_app_info_bancaria = \App\UserBankData::where('id_user', $session)->first();

$email = ['form_params' => ['email' => $usuario_app->correo]];
$correo = ['form_params' =>['correo' => $usuario_app->correo]];
$vacio = array();


// OBTENER CANDIDATOS
$response = $service->post(url('api/listado_candidatos'), $email);
$candidatos = json_decode($response->getBody()->getContents());

// PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());

// BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());

// ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());


// LISTADO DE NOTIFICACIONES DEL USUARIO
$response = $service->post(url('api/listado_notificacion'), $vacio);
$notificaciones = json_decode($response->getBody()->getContents());

// LISTADO DE AVISOS DEL USUARIO
$response = $service->post(url('api/listar_avisos_usuarios'), $vacio);
$avisos = json_decode($response->getBody()->getContents());

//MI CUENTA
$response = $service->post(url('api/listar_bancos'), $vacio);
$listar_bancos = json_decode($response->getBody()->getContents());
//ESTADISTICA
//$estadistica = json_decode(webservice(url('/api/estadistica'), $email, 'POST'));
$response = $service->post(url('api/estadistica'), $vacio);
$estadistica = json_decode($response->getBody()->getContents());
//LISTAR GANANCIAS
//$listar_ganancias = json_decode(webservice(url('/api/listar_ganancias'), $correo, 'POST'));
$response = $service->post(url('api/listar_ganancias'), $vacio);
$listar_ganancias = json_decode($response->getBody()->getContents());
/*
foreach ($listar_ganancias->data as $key => $value) {
    //LISTAR HISTORIAL GANANCIAS
    $listar_ganancias->data[$key]->data =  json_decode(webservice(url('/api/listar__historial_ganancias'),array('payment_id' => $listar_ganancias->data[$key]->gain_id),'POST'))->data; // EN MODAL ? payment_id QUE VIENE DE LISTAR GANANCIAS
}
*/
$response = $service->post(url('api/listado_categorias'), $vacio);
$listar_preferencias = json_decode($response->getBody()->getContents());
//$listar_preferencias = json_decode(webservice(url('/api/listado_categorias'), array(), 'POST'));
//$listar_ganancias = json_decode(webservice(url('/api/listar_ganancias'), $email, 'POST'));
$response = $service->post(url('api/listar_ganancias'), $vacio);
$listar_ganancias = json_decode($response->getBody()->getContents());

foreach ($listar_preferencias->data as $preferecias) {
    $preferecias->check = false;
    foreach ($listar_mis_preferencias->data as $mis_preferecias) {
        if ($mis_preferecias->id == $preferecias->id) {
            $preferecias->check = true;
            break;
        }
    }
    unset($preferecias->created_at);
    unset($preferecias->updated_at);
    $listar_div[] = $preferecias;
}
$estadistica_grafica_fechas = array();
for ($i = 0; $i < 12; $i++) {
    $estadistica_grafica_fechas[] = 0;
}
foreach ($estadistica->grafica_fechas as $value) {
    $estadistica_grafica_fechas[$value->mes - 1] = $value->registros;
}

?>

    <!DOCTYPE html>
<html>
<title>Pro Hunter</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Pro hunter">
<meta name="Oomovil">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/toolbar.css')}}">
<link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

<script src="{{asset('jquery-3.2.1.min.js')}}"></script>
<!--Alertify-->
<link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
<link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
<script src="{{asset('alertifyjs/alertify.js')}}"></script>
<!-- uikit -->
<link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
<script src="{{asset('uikit/js/uikit.min.js')}}"></script>
<script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('css/w3.css')}}">
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/web-dash.css')}}">
<link rel="stylesheet" href="{{asset('css/rateyo/jquery.rateyo.css')}}">

<script src="{{ url('datatable/jquery-3.3.1.js')}}"></script>
<script src="{{ url('datatable/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="{{ url('datatable/dataTables.semanticui.min.css')}}">
<link rel="stylesheet" href="{{ url('datatable/semantic.min.css')}}">
<script src="{{ url('datatable/dataTables.semanticui.min.js')}}"></script>
<script src="{{ url('datatable/semantic.min.js')}}"></script>

<link rel="stylesheet" href=" {{ asset('css/bootstrap.css')}}">
<script src="{{asset('css/Bootstrap/js/bootstrap.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<!-- firebase  -->
<script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
<script> let url = '../'; </script>
<script src="{{asset('js/firebase.js')}}"></script>


<style>
    html, body, h1, h2, h3, h4, h5, h6 {
        font-family: "Roboto", sans-serif
    }

    .preferencias {
        float: left;
        width: 210px;
        padding: 10px 10px 1px 10px;
        box-shadow: 0px 8px 20px 0px black;
        margin: 15px;
        height: 200px;
        font-size: 14px;
        text-align: center;
        overflow-y: hidden;

    }

    .footer-modal {
        clear: both;
    }

    .last-30 {
        width: 45%;
        display: table-cell;
        text-align: center;
    }

    :not(pre) > code, :not(pre) > kbd, :not(pre) > samp {
        background: #19191e;
        white-space: inherit;
    }

    .postulados h4 {
        color: #2E3D56;
    }

    .contratados h4 {
        color: #00B9AE;
    }

    .postulado {
        margin-bottom: 10px;
    }

    .contratado {
        margin-bottom: 10px;
    }

    .monitos {
        width: 145px;
        height: 255px;
        display: inline-block;
    }

    .monitos .arriba {
        background: #c9d0d6;
        width: 98%;
        z-index: 0;
        float: left;
    }

    .monitos .hombre {
        background: #2E3D56;
        width: 98%;
        z-index: 0;
        float: left;
    }

    .monitos .mujer {
        background: #00B9AE;
        width: 98%;
        z-index: 0;
        float: left;
    }

    @media only screen and (max-width: 600px) {
        .monitos.label {
            position: unset !important;
        }
    }

    .monitos.label {
        position: relative;
        height: 128px;
        top: -127px;
        text-align: center;
    }

    .monitos.label h4 {
        color: #c9d0d6;
    }

    .monitos.label:first-of-type h3 {
        color: #00B9AE;;
    }

    .monitos.label:last-of-type h3 {
        color: #2E3D56;;
    }

    .ganancia {
        text-align: center;
        width: 90%;
        margin: auto;
        height: 190px;
    }


    .btn_hover:hover {
        border-radius: 60px 60px 60px 60px;
        -moz-border-radius: 60px 60px 60px 60px;
        -webkit-border-radius: 60px 60px 60px 60px;
        box-shadow: 0 4px 16px rgb(164, 165, 169);
        transition: all 0.3s ease;
    }
</style>
<body class="w3-light-grey">

<style>
    body {
        min-height: 1111px;
    }

    @font-face {
        font-family: 'Harabara';
        src: url('{{asset('Fonts/Harabara.ttf')}}');
    }

    strong, .title-dash, #mis_favoritas, #all_vacantes, .btnRedondo_gris, .btnRedondo_verde {
        font-family: 'Harabara';
    }

    ::-webkit-scrollbar {
        display: none;
    }

    .w3-dropdown-content {
        right: 0px;
        margin-right: 10px;
        border-radius: 5px;
        /*padding: 10px 0px;*/
        color: #c2c9d1;
    }

    .w3-button:hover, #navDemo_perfil a:hover {
        background: #41636f !important;
        color: white !important;
        text-decoration: none !important;
    }

    #navDemo {
        position: fixed;
        width: 100%;
        z-index: 2;
    }

    .uk-close {
        background-color: transparent;
    }
</style>
<div id="modal-acerca" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div class="w3-content w3-display-container" align="center">
            @foreach($about->image_urls as $img)
                <img class="mySlides" src="{{$img}}" style="height: 250px">
            @endforeach
            <img src="{{asset('Img/logo-prohunter-color.png')}}" style="height: 100px;">
            <button class="w3-button w3-white w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
            <button class="w3-button w3-white w3-display-right" onclick="plusDivs(1)">&#10095;</button>
        </div>
        <br>
        <p>{{$about->text}}</p>

        </p>
    </div>
</div>

<div id="modal-aviso" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        {!!html_entity_decode($aviso_privacidad->text)!!}

        </p>
    </div>
</div>

<div id="modal-ayuda" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <h2 style="text-align: center;">Ayuda</h2>
        <!-- Mis vacantes favorias -->
        <ul style="list-style-type: none;">
            @foreach($preguntas_frecuentes->data as $preguntas)
                <li>
                    <h3><b>{{ $preguntas->question}}</b></h3>
                    <p>{{ $preguntas->answer}}</p>
                </li>
            @endforeach
        </ul>

    </div>
</div>

<!-- Navbar -->
<div class="w3-top ph-nav">
    <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
           href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" style="    margin: 0px 12px;"></i></a>
        <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img
                style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>


        <div class="w3-dropdown-hover w3-hide-small Close_Session" style="float: right;">
            <button class="w3-button w3-padding-large" title="Cerrar sesion"><i class="fa fa-sign-out"></i><span
                    id="Cerrar_sesion" name="Cerrar_sesion" class="w3-badge w3-right w3-small w3-green"></span></button>
            <div>
                <script type="text/javascript">
                    $(function () {
                        $('.Close_Session').click(function (e) {
                            alertify.confirm('Cerrar sesión', "¿Quieres cerrar sesión?",
                                function () {
                                    localStorage.removeItem('prohunter');
                                    localStorage.removeItem('firebase');
                                    window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                    window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                }, function () {/*Cancel*/
                                });
                        });
                    });
                </script>
            </div>
        </div>

        <div class="w3-dropdown-hover " style="float: right">
            <button class="w3-button w3-padding-large" title="notificaciones"><i class="fa fa-globe"></i><span
                    id="notificaciones" name="notificaciones" class="w3-badge w3-right w3-small w3-green"></span>
            </button>
            <div id="noticias-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block"
                 style="width:250px;max-height: 500px;overflow-y: scroll;">
            </div>
        </div>

        <div class="w3-dropdown-hover " style="float: right">
            <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-bell"></i><span id="avisos"
                                                                                                      name="avisos"></span>
            </button>
            <div id="avisos-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block"
                 style="width:300px;max-height: 500px;overflow-y: scroll;">

            </div>
        </div>

        <div class="w3-dropdown-hover w3-hide-small" style="float: right">
            <button class="w3-button w3-padding-large" title="perfil"><i class="fa fa-user"></i><span id="perfil"
                                                                                                      name="perfil"
                                                                                                      class="w3-badge w3-right w3-small w3-green"></span>
            </button>
            <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block">
                <div style="padding: 10px 0px;">
                    <img src="{{asset('storage/app/triangle.svg')}}"
                         style="position: absolute;top: -16px;height: 21px;left: 45px;">
                    <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

                    <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

                    <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

                    <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

                    <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

                </div>
            </div>
        </div>
        <div class="w3-dropdown-hover w3-hide-small" style="float: right;">
            <a href="{{route('Web')}}" class="w3-button w3-padding-large" title="inicio"><i class="fa fa-home"></i></a>

        </div>
    </div>
</div>
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large" style="top: 50px">

    <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

    <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

    <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

    <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

    <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

    <a href="#" class="w3-bar-item w3-button w3-padding-large Close_Session"><i class="fa fa-sign-out"></i></a>
</div>
<!-- Fin Navbar -->

<script>
    // Used to toggle the menu on smaller screens when clicking on the menu button
    function openNav() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }

    //Galeria de img
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = x.length
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex - 1].style.display = "block";
    }

    // fin galeria
</script>

<!-- Page Container -->
<div class="w3-content" style="max-width:1400px;margin-top: 50px !important;">

    <!-- The Grid -->
    <div class="w3-row-padding" style="padding: 8px 8px;">
        <!-- Left Column -->
        <div class="w3-third">

            <div class="w3-white w3-text-grey w3-card-4">

                <div class="w3-display-container">

                    <img
                        src="{{isset($usuario_app->imagen_perfil)?asset('storage/app/'. $usuario_app->imagen_perfil.''):asset('Img/notpictureuser.png')}}"
                        style="width:100%;max-height: 325px" alt="Avatar">
                    <div class="w3-display-bottomleft w3-container w3-text-black">
                    </div>

                </div>

                <div class="js-upload uk-form-custom btn_hover" uk-form-custom="">
                    <input type="file" id="profile_picture" name="profile_picture" accept="image/*">
                    <button tabindex="-1" class="uk-button uk-button-default"
                            style="background-color: transparent; border: 0;float: right;color: black;"><i
                            class="fa fa-camera" aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Editar
                    </button>
                </div>

                <div class="w3-container">
                    <br>
                    <a class="w3-large"><b><i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>Perfil</b></a>
                    <div>  <!-- perfil -->
                        <div id="label-perfil">
                            <button id="btn-perfil" style="background-color: transparent; border: 0;float: right;"><i
                                    class="fa fa-pencil" aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Editar
                            </button>
                            <br>
                            <p>
                                <i class="fa fa-user-circle-o fa-fw w3-margin-right w3-text-theme"></i>Nombre: {{$usuario_app->nombre}}
                            </p>
                            <p>
                                <i class="fa fa-phone fa-fw w3-margin-right w3-text-theme"></i>Teléfono: {{ $usuario_app->celular != '0000000000'?$usuario_app->celular:'- -'}}
                            </p>
                            <p>
                                <i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i>Correo: {{$usuario_app->correo}}
                            </p>
                            <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i>Fecha
                                cumpleaños: {{isset($usuario_app->fecha_cumpleaños)? date("d/m/Y", strtotime($usuario_app->fecha_cumpleaños)):'- -'}}
                            </p>
                            <p><i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i>Lugar
                                nacimiento: {{isset($usuario_app->lugar_nacimiento)?$usuario_app->lugar_nacimiento:'- -'}}
                            </p>
                            <p>
                                <i class="fa fa-address-card-o fa-fw w3-margin-right w3-text-theme"></i>RFC: {{isset($usuario_app->RFC)?$usuario_app->RFC:'- -'}}
                            </p>
                        </div>
                        <form id="form-perfil" style="display: none;">
                            <button style="background-color: transparent; border: 0;float: right;"><i
                                    class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Guardar
                            </button>
                            <br>
                            <p><i class="fa fa-user-circle-o fa-fw w3-margin-right w3-text-theme"></i>Nombre:<input
                                    class="w3-input txtSolo" name="nombre" type="text" value="{{$usuario_app->nombre}}">
                            </p>
                            <p><i class="fa fa-phone fa-fw w3-margin-right w3-text-theme"></i>Teléfono:<input
                                    class="w3-input txtCel" name="celular" type="text" maxlength="10"
                                    value="{{ $usuario_app->celular != '0000000000'?$usuario_app->celular:'- -'}}"></p>
                            @if($usuario_app->tipo == 'normal')
                                <p><i class="fa fa-key fa-fw w3-margin-right w3-text-theme"></i>Contraseña: <input
                                        class="w3-input" name="contrasena" type="password"></p>
                            @endif
                            <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i>Fecha
                                cumpleaños:<input class="w3-input" name="fecha_cumpleaños" type="date"
                                                  value="{{isset($usuario_app->fecha_cumpleaños)?$usuario_app->fecha_cumpleaños:'- -'}}">
                            </p>
                            <p><i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i>Lugar
                                nacimiento:<input class="w3-input" name="lugar_nacimiento" type="text"
                                                  value="{{isset($usuario_app->lugar_nacimiento)?$usuario_app->lugar_nacimiento:'- -'}}">
                            </p>
                            <p><i class="fa fa-address-card-o fa-fw w3-margin-right w3-text-theme"></i>RFC:<input
                                    class="w3-input" name="RFC" type="text" maxlength="13"
                                    value="{{isset($usuario_app->RFC)?$usuario_app->RFC:'- -'}}"
                                    style="text-transform: uppercase"></p>

                        </form>
                    </div> <!-- perfil -->
                    <hr>

                    <a class="w3-large"><b><i class="fa fa-credit-card-alt fa-fw w3-margin-right w3-text-teal"></i>Mi
                            cuenta</b></a>
                    <div> <!-- mi cuenta -->
                        <div id="label-cuenta">
                            <button id="btn-cuenta" style="background-color: transparent; border: 0px;float: right;"><i
                                    class="fa fa-pencil" aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Editar
                            </button>
                            <br>
                            <p><i class="fa fa-user-circle-o fa-fw w3-margin-right w3-text-theme"></i><strong>Número de
                                    cuenta /
                                    Clabe: </strong> {{isset($usuario_app_info_bancaria->cuenta_clabe)?$usuario_app_info_bancaria->cuenta_clabe:'- -'}}
                            </p>
                            <p>
                                <i class="fa fa-university fa-fw w3-margin-right w3-text-theme"></i><strong>Banco: </strong> {{ isset($usuario_app_info_bancaria->banco_destino)?$usuario_app_info_bancaria->banco_destino:'- -'}}
                            </p>
                            <p><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i><strong>Titular de la
                                    cuenta: </strong> {{ isset($usuario_app_info_bancaria->beneficiario)?$usuario_app_info_bancaria->beneficiario:'- -'}}
                            </p>
                        </div>
                        <form id="form-cuenta" style="display: none;">
                            <button style="background-color: transparent; border: 0px;float: right;"><i
                                    class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Guardar
                            </button>
                            <br>
                            <p><i class="fa fa-user-circle-o fa-fw w3-margin-right w3-text-theme"></i>Número de cuenta /
                                Clabe:<input class="w3-input" name="cuenta" type=""
                                             value="{{ isset($usuario_app_info_bancaria->cuenta_clabe)?$usuario_app_info_bancaria->cuenta_clabe:'- -'}}">
                            </p>
                            <p><i class="fa fa-university fa-fw w3-margin-right w3-text-theme"></i>Banco:<select
                                    class="w3-input" name="banco"> @foreach($listar_bancos->data as $data)
                                        <option>{{$data->name}}</option> @endforeach </select></p>
                            <p><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i>Titular de la
                                cuenta:<input class="w3-input" name="beneficiario" type=""
                                              value="{{ isset($usuario_app_info_bancaria->beneficiario)?$usuario_app_info_bancaria->beneficiario:'- -'}}">
                            </p>
                        </form>
                    </div><!-- mi cuenta -->
                    <hr>

                    <a class="w3-large"><b><i class="fa fa-sliders fa-rotate-90 fa-fw w3-margin-right w3-text-teal"></i>Preferencias</b></a>
                    <button uk-toggle="target: #mis-preferencias"
                            style="background-color: transparent; border: 0px;float: right;"><i class="fa fa-pencil"
                                                                                                aria-hidden="true">&nbsp;&nbsp;&nbsp;</i>Editar
                    </button>
                </div>
                <br>
            </div>
            <br>

            <!-- End Left Column -->
        </div>
        <!-- Right Column -->
        <div class="w3-twothird">

            <div class="w3-container w3-card w3-white w3-margin-bottom">
                <h2 class="w3-text-grey w3-padding-16" style="display: inline-block;"><i
                        class="fa fa-pie-chart fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Gráfica de fechas</h2>
                <div id="rateYo" style="z-index: 0;width: 160px;"></div>

                <div class="w3-container">
                    <canvas id="canvas" width="948" height="474" class="chartjs-render-monitor"
                            style="display: block; width: 948px; height: 474px;"></canvas>
                    <hr>
                </div>
            </div>

            <div class="w3-container w3-card w3-white w3-margin-bottom">
                <h2 class="w3-text-grey w3-padding-16" style="display: inline-block;"><i
                        class="fa fa-pie-chart fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Postulados vs
                    Candidatos últimos 30 días</h2>
                <div class="w3-container">
                    <div id="ultimos30">
                        <div class="last-30 postulados">
                            <h4>{{$estadistica->grafica_ultimos_30_dias[0]->conteo_postulados}} Postulados</h4>
                            <canvas id="canvas_d_1" width="948" height="474" class="chartjs-render-monitor"
                                    style="display: block; width: 948px; height: 474px;"></canvas>
                            <h4>{{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_30_Dias}}%</h4>
                            <div style="text-align:left;overflow-y: scroll;max-height: 500px;">
                                @foreach ($estadistica->detalle_grafica_vacantes_vs_postulados->postulados as  $postulados)
                                    <div class="postulado">
                                        <img
                                            src="{{$postulados->profile_picture!=null?asset('storage/app/'.$postulados->profile_picture):asset('Img/notpictureuser.png')}}"
                                            alt="Avatar" class="w3-circle" style="width:10%">
                                        <label>&nbsp;{{$postulados->names}} {{$postulados->paternal_name}} {{$postulados->maternal_name}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="last-30 contratados">
                            <h4>{{$estadistica->grafica_ultimos_30_dias[0]->conteo_contratados}} Contratados</h4>
                            <canvas id="canvas_d_2" width="948" height="474" class="chartjs-render-monitor"
                                    style="display: block; width: 948px; height: 474px;"></canvas>
                            <h4>{{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_contratados_30_Dias}}
                                %</h4>
                            <div style="text-align:left;overflow-y: scroll;max-height: 500px;">
                                @foreach ($estadistica->detalle_grafica_vacantes_vs_postulados->contratados as  $contratados)
                                    <div class="contratado">
                                        <img
                                            src="{{$contratados->profile_picture!=null?asset('storage/app/'.$contratados->profile_picture):asset('Img/notpictureuser.png')}}"
                                            alt="Avatar" class="w3-circle" style="width:10%">
                                        <label>&nbsp;{{$contratados->names}} {{$contratados->paternal_name}} {{$contratados->maternal_name}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>

                    <hr>
                </div>
            </div>

            <div class="w3-container w3-card w3-white w3-margin-bottom">
                <h2 class="w3-text-grey w3-padding-16" style="display: inline-block;"><i
                        class="fa fa-pie-chart fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Género más registrado
                </h2>
                <div class="w3-container">
                    <div style="clear: both;"></div>
                    <div class="monitos label">
                        <h3>{{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_femenino}}%</h3>
                        <h4>Mujeres</h4>
                    </div>
                    <div class="monitos">
                        <div class="arriba"
                             style="height: {{100-$estadistica->grafica_ultimos_30_dias[0]->porcentaje_femenino}}%;"></div>
                        <div class="mujer"
                             style="height: {{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_femenino}}%;"></div>
                        <div
                            style="background: url('{{asset('monitos/monita-prohunter.png')}}') no-repeat;height: 100%;width: 100%;position: relative;left: -1px;"></div>
                    </div>
                    <div class="monitos label">
                        <h4>VS</h4>
                    </div>
                    <div class="monitos">
                        <div class="arriba"
                             style="height:{{100-$estadistica->grafica_ultimos_30_dias[0]->porcentaje_masculinos}}%;"></div>
                        <div class="hombre"
                             style="height:{{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_masculinos}}%;"></div>
                        <div
                            style="background: url('{{asset('monitos/monito-prohunter.png')}}') no-repeat;height: 100%;width: 100%;position: relative;left: -1px;"></div>
                    </div>
                    <div class="monitos label">
                        <h3>{{$estadistica->grafica_ultimos_30_dias[0]->porcentaje_masculinos}}%</h3>
                        <h4>Hombres</h4>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <div class="w3-container w3-card w3-white">
                <h2 class="w3-text-grey w3-padding-16"><i
                        class="fa fa-money fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Mis ganancias</h2>
                <div class="w3-container">

                    <table align="center" id="Tabla" class="table" style="width:100%">
                        <thead>
                        <tr>
                            <th>Total a pagar</th>
                            <th>Porcentaje pagado</th>
                            <th>Faltante</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th>Detalle</th>
                        </tr>
                        </thead>
                        @foreach($listar_ganancias->data as $key => $Result)
                            <tr>
                                <td style="text-align: center;vertical-align: middle">$ {{ $Result->total_to_pay }}</td>
                                <td style="text-align: center;vertical-align: middle">{{ $Result->paid_percentage }}</td>
                                <td style="text-align: center;vertical-align: middle">$ {{ $Result->faltante }}</td>
                                <td style="text-align: center;vertical-align: middle">{{ $Result->created_at }}</td>
                                <td style="text-align: center;vertical-align: middle">
                                    <label>{{ $Result->paid_percentage == '100%' ? 'Pagado' : 'Pendiente' }}</label>
                                <td style="text-align: center;vertical-align: middle">
                                    <button class="btn btn-outline-info"
                                            onclick="Modal_Ganancias('{{$Result->gain_id}}','{{$Result->vacancy_name}}','{{$Result->company_logo}}','{{ $Result->full_name }}')">
                                        Detalles
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <br>
                    <br>
                </div>
            </div>

            <!-- End Right Column -->
        </div>
        <!-- End Grid -->
    </div>
    <br>

    <!-- This is the modal  prefencias -->
    <div id="mis-ganancias" uk-modal align="center">
        <div class="uk-modal-dialog uk-modal-body" style="width: 500px;">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <h3 class="uk-modal-title">Detalle Ganancia</h3>
            <br>
            <div id="historial_Ganancias" name="historial_Ganancias"></div>

        </div>
    </div>

    <!-- This is the modal  prefencias -->
    <div id="mis-preferencias" uk-modal align="center">
        <div class="uk-modal-dialog uk-modal-body" style="width: 900px;">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>

            <h2 class="uk-modal-title">Elige tus preferencias</h2>
            <form id="form-cat">
                <div class="body-modal" style="margin-left: 60px;">
                    @foreach($listar_div as $data)
                        <div class="preferencias">
                            <input id="cat-{{$data->id}}" type="checkbox" name="category_ids[category_ids]"
                                   class="category_ids" value="{{$data->id}}"
                                   style="float: right;" {{($data->check) ? "checked" : ""}} >
                            <label for="cat-{{$data->id}}">
                                <img src="{{$data->icon_url}}" style="width: 100px;">
                                <p>{{$data->name}}</p>
                            </label>
                        </div>
                    @endforeach
                </div>
                <div class="footer-modal">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="checkCategorias" name="checkCategorias">
                        <label class="custom-control-label" for="checkCategorias">Seleccionar todas</label>
                    </div>
                    <button class="btnRedondo_verde">Guardar cambios</button>
                </div>
            </form>

        </div>
    </div>

    <!-- End Page Container -->
</div>
<script src="https://unpkg.com/imask"></script>
<script src="{{asset('js/ValidatorForm.js')}}"></script>
<script src="{{asset('js/perfil.js')}}"></script>
<script src="{{asset('css/rateyo/jquery.rateyo.js')}}"></script>
<script>

    // Formato de telefono
    Array.from(document.getElementsByClassName('txtCel')).forEach(function (item) {
        var mask = new IMask(item, {
            mask: /^\d+$/,
        })
    });

    $(function () {
        $("#rateYo").rateYo({
            rating:{{$estadistica->Estrellas}},
            readOnly: true
        });

    });
    var config = {
        type: 'line',
        data: {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic',],
            datasets: [
                {
                    label: 'Postulados',
                    backgroundColor: '#00B9AE',
                    borderColor: '#00B9AE',
                    data: [
                        @foreach($estadistica_grafica_fechas as $dato)
                        {{$dato}},
                        @endforeach
                    ],
                    fill: false,
                }]
        },
        options: {
            responsive: true,
            title: {
                display: true
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    };
    window.onload = function () {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);
        var canvas_d_1 = document.getElementById('canvas_d_1').getContext('2d');
        window.myLine = new Chart(canvas_d_1, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        backgroundColor: ['#2E3D56', 'rgba(255, 159, 64, 0)'],
                        borderColor: ['#2E3D56', 'rgba(255, 159, 64, 0)'],
                        data: [
                            {{  $estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_30_Dias }},
                            {{  100-$estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_30_Dias }},
                        ],
                        fill: false,
                    }]
            },
            options: {
                cutoutPercentage: 70,
                responsive: true,
                tooltips: {
                    enabled: false,
                },
            }
        });
        var canvas_d_2 = document.getElementById('canvas_d_2').getContext('2d');
        window.myLine = new Chart(canvas_d_2, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        backgroundColor: ['#00B9AE', 'rgba(255, 159, 64, 0)'],
                        borderColor: ['#00B9AE', 'rgba(255, 159, 64, 0)'],
                        data: [
                            {{ $estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_contratados_30_Dias }},
                            {{ 100-$estadistica->grafica_ultimos_30_dias[0]->porcentaje_vacantes_contratados_30_Dias }},
                        ],
                        fill: false,
                    }]
            },
            options: {
                cutoutPercentage: 70,
                responsive: true,
                tooltips: {
                    enabled: false,
                },
            }
        });
    };
</script>
</body>
</html>
