<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ProHunter">
    <meta name="Oomovil">

    <title>Pro Hunter</title>
    <link rel="shortcut icon" href="{{ asset('/Img/icn-favicon.png') }}">
    <link href="{{ url('/css/login.css') }}" rel="stylesheet">
    <link href="{{ url('/css/Bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{asset('/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('/css/Bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/bodymovin/js/bodymovin.min.js')}}" type="text/javascript"></script>

    <script>
        if (window.location.protocol !== "https:") {
            //window.location.href='https://'+window.location.host+window.location.pathname;
        }
       if(!!localStorage.getItem('prohunter')){
            window.location.href =  window.location.href+"web";
        }
    </script>
</head>

<body class="FondoProhunter-usuario">
<form id="recuperar">

    <div class="centrar" align="Center" style="margin-top: -200px;">
        @if(isset($msg))
            <input type="text" class='form-control'> <span>{{ $msg }}</span>
        @endif
        <img src="{{ url('/Img/logo-prohunter-login.png') }}"/>
        <br>
        <br>
        <label class="LabeLogin">Recuperar contraseña</label>
        <br>
        <input class="BordesRedondosInput" style="width: 100%" type="text" name="correo" placeholder="Ingresa tu correo"/>
        <br>
        <br>
        <button type="submit" class="Boton_Login" style="width: 100%;cursor: pointer">Recuperar contraseña</button>
        <br><br>
        <a  style="text-decoration: underline;color: white" href="{{url('/login')}}">Volver</a>
        <br>
       <?php
        if(isset($_GET['Error']) && isset($_GET['Msg'])){
            echo '<br>';
            echo '<br>';
            echo '<div class="alert alert-danger">';
            echo '<button  type="button" class="close" data-dismiss="alert">×</button>';
            echo '<strong>'.$_GET['Msg'].'</strong>';
            echo '</div>';
        }
        ?>

    </div>
</form>

<br>
<script>
    let formRecuperar = document.querySelector("#recuperar");
    formRecuperar.addEventListener('submit',async event =>{
        
        event.preventDefault();
        let response = await fetch('api/recuperar_password',{
            method: 'POST', // or 'PUT'
            body: `{"correo": "${formRecuperar.correo.value}"}`, // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res=>res.json());
        console.log(response);
        if(!!response.exito){
            window.location += "/..";
        }
    })

</script>
<script>history.pushState(null, "", "/recuperar");</script>
</body>
</html>
