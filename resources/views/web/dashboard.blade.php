<?php
if(!isset($_SESSION['Oomovil']) or $_SESSION['isAdmin'] == true){
    header('Location: /');
    exit;
}else{

Session::put('session', $_SESSION['Oomovil']);
Session::put('privilegios', $_SESSION['privilegios']);
$session = \Illuminate\Support\Facades\Session::get('session');
$privilegios = \Illuminate\Support\Facades\Session::get('privilegios');

//OBTENER DATOS DEL USUARIO
$usuario_app = \App\User::where('id', $session)->first();

$email = ['form_params' => ['email' => $usuario_app->correo]];
$correo = ['form_params' =>['correo' => $usuario_app->correo]];
$vacio = array();
$miscandidatos_array = array();



$service = new \GuzzleHttp\Client(['verify' => false]);

// OBTENER VACANTES
$response = $service->request('POST', url('api/listado_vacantes'));
$json_vacantes = json_decode($response->getBody()->getContents());


// OBTENER CANDIDATOS
$response = $service->request('POST', 'https://prohunter.test/api/listado_candidatos', $email);
$candidatos = json_decode($response->getBody()->getContents());


// LISTADO DE VACANTES FAVORITAS
$response = $service->post(url('api/listado_vacantes_favoritas'), $email);
$vacantes_favoritas = json_decode($response->getBody()->getContents());

// LISTADO DE POSTULADOS
$response = $service->post(url('api/listado_postulados'), $email);
$listado_postulados = json_decode($response->getBody()->getContents());

// PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());

// BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());

// ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());
// AVISO DE PRIVACIDAD
$response = $service->post(url('api/aviso_privacidad'), $vacio);
$aviso_privacidad = json_decode($response->getBody()->getContents());


// OBTENER VACANTES
/*$vacantes = json_decode(webservice(url('/api/listado_vacantes'), $email, 'POST'));
$json_vacantes = json_encode($vacantes->data);
// OBTENER CANDIDATOS
$candidatos = json_decode(webservice(url('/api/listado_candidatos'), $email, 'POST'));
// LISTADO DE VACANTES FAVORITAS
$vacantes_favoritas = json_decode(webservice(url('/api/listado_vacantes_favoritas'), $email, 'POST'));
// LISTADO DE POSTULADOS
$listado_postulados = json_decode(webservice(url('/api/listado_postulados'), $email, 'POST'));
// PREGUNTAS FRECUENTES
$preguntas_frecuentes = json_decode(webservice(url('/api/listado_preguntas_frecuentes'), $vacio, 'POST'));
// BLOG
$blog = json_decode(webservice(url('/api/blog'), $vacio, 'POST'));
// ABOUT
$about = json_decode(webservice(url('/api/about'), $vacio, 'POST'));
// AVISO DE PRIVACIDAD
$aviso_privacidad = json_decode(webservice(url('/api/aviso_privacidad'), $vacio, 'POST'));
*/

?>

    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale())}}">

<head>

    <title>Pro Hunter</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Pro hunter">
    <meta name="Oomovil">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="{{asset('css/toolbar.css')}}">
    <link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/w3.css')}}">
    <link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

    <script src="{{asset('jquery-3.2.1.min.js')}}"></script>
    <!--Alertify-->
    <link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
    <script src="{{asset('alertifyjs/alertify.js')}}"></script>
    <!-- uikit -->
    <link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
    <script src="{{asset('uikit/js/uikit.min.js')}}"></script>
    <script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

    <!--lightbox -->
    <link href="{{asset('lightbox/css/lightbox.css')}}" rel="stylesheet">
    <script src="{{asset('lightbox/js/lightbox.js')}}"></script>

    <link rel="stylesheet" href="{{asset('css/web-dash.css')}}">

    <style>


        body {
            min-height: 1111px;
        }

        @font-face {
            font-family: 'Harabara';
            src: url('{{asset('Fonts/Harabara.ttf')}}');
        }

        strong, .title-dash, #mis_favoritas, #all_vacantes, .btnRedondo_gris, .btnRedondo_verde {
            font-family: 'Harabara';
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .w3-dropdown-content {
            right: 0px;
            margin-right: 10px;
            border-radius: 5px;
            /*padding: 10px 0px;*/
            color: #c2c9d1;
        }

        .w3-button:hover, #navDemo_perfil a:hover {
            background: #41636f !important;
            color: white !important;
            text-decoration: none !important;
        }

        #navDemo {
            position: fixed;
            width: 100%;
            z-index: 2;
        }

        .uk-close {
            background-color: transparent;
        }

    </style>
    <link rel="stylesheet" href="{{asset('css/web-dash.css')}}">

<body class="w3-theme-l5">


<div id="modal-acerca" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div class="w3-content w3-display-container" align="center">
            @foreach($about->image_urls as $img)
                <img class="mySlides" src="{{$img}}" style="height: 250px">
            @endforeach
            <img src="{{asset('Img/logo-prohunter-color.png')}}" style="height: 100px;">
            <button class="w3-button w3-white w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
            <button class="w3-button w3-white w3-display-right" onclick="plusDivs(1)">&#10095;</button>
        </div>
        <br>
        <p>{{$about->text}}</p>

        </p>
    </div>
</div>

<div id="modal-aviso" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        {!!html_entity_decode($aviso_privacidad->text)!!}
    </div>
</div>

<div id="modal-ayuda" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <h2 style="text-align: center;">Ayuda</h2>
        <!-- Mis vacantes favorias -->
        <ul style="list-style-type: none;">
            @foreach($preguntas_frecuentes->data as $preguntas)
                <li>
                    <h3><b>{{ $preguntas->question}}</b></h3>
                    <p>{{ $preguntas->answer}}</p>
                </li>
            @endforeach
        </ul>

    </div>
</div>
<!-- Navbar -->
<div class="w3-top ph-nav">
    <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
           href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" style="    margin: 0px 12px;"></i></a>
        <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img
                style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>


        <div class="ph-search w3-dropdown-hover w3-hide-small">

            <form class="uk-search uk-search-default" id="buscar" style="width: 300px;">
                <button class="uk-search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
                <input class="uk-search-input" type="search" placeholder="Buscar" name="q">
            </form>

        </div>
        <div class="w3-dropdown-hover w3-hide-small Close_Session" style="float: right;">
            <button class="w3-button w3-padding-large" title="Cerrar sesion"><i class="fa fa-sign-out"></i><span
                    id="Cerrar_sesion" name="Cerrar_sesion" class="w3-badge w3-right w3-small w3-green"></span></button>
            <div>
                <script type="text/javascript">
                    $(function () {
                        $('.Close_Session').click(function (e) {
                            alertify.confirm('Cerrar sesión', "¿Quieres cerrar sesión?",
                                function () {
                                    localStorage.removeItem('prohunter');
                                    localStorage.removeItem('firebase');
                                    window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                    window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                }, function () {/*Cancel*/
                                });
                        });
                    });
                </script>
            </div>
        </div>

        <div class="w3-dropdown-hover " style="float: right">
            <button class="w3-button w3-padding-large" title="notificaciones"><i class="fa fa-globe"></i><span
                    id="notificaciones" name="notificaciones" class="w3-badge w3-right w3-small w3-green"></span>
            </button>
            <div id="noticias-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block">

            </div>
        </div>

        <div class="w3-dropdown-hover " style="float: right">
            <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-bell"></i><span id="avisos"
                                                                                                      name="avisos"></span>
            </button>
            <div id="avisos-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block">

            </div>
        </div>

        <div class="w3-dropdown-hover w3-hide-small" style="float: right">
            <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-user"></i><span id="avisos"
                                                                                                      name="perfil"
                                                                                                      class="w3-badge w3-right w3-small w3-green"></span>
            </button>
            <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block">
                <div style="padding: 10px 0px;">
                    <img src="{{asset('storage/app/triangle.svg')}}"
                         style="position: absolute;top: -16px;height: 21px;left: 45px;">
                    <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

                    <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

                    <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

                    <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

                    <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

                </div>
            </div>
        </div>
        <div class="w3-dropdown-hover w3-hide-small" style="float: right;">
            <a href="{{route('Web')}}" class="w3-button w3-padding-large" title="inicio"><i class="fa fa-home"></i></a>

        </div>
    </div>
</div>
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="#" class="ph-search w3-bar-item w3-button w3-padding-large ">
        <form class="uk-search uk-search-default" id="buscar_m" style="width: 300px;">
            <button class="uk-search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
            <input class="uk-search-input" type="search" placeholder="Buscar" name="q">
        </form>
    </a>
    <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

    <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

    <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

    <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

    <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

    <a href="#" class="w3-bar-item w3-button w3-padding-large Close_Session"><i class="fa fa-sign-out"></i></a>
</div>
<!-- Fin Navbar -->


<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top: 50px;padding-top: 10px;">
    <div class="w3-row">
        <!-- Left Column -->
        <div class="w3-col m3">
            <div class="title-dash">
                Mis candidatos

                <form class="uk-search uk-search-default" id="candidatos_">
                    <button uk-search-icon></button>
                    <input name="q" class="uk-search-input" type="search" placeholder="Buscar">
                </form>


                <a href="{{route('Registro_candidato')}}">Agregar</a>


            </div>
            <div class="misCandidatos" id="mis_candidatos_list"></div>


        </div>
        <!-- End Left Column -->

        <!-- Middle Column -->
        <div class="w3-col m6" id="col_central" name="col_centrar">

            <div>
                <ul class="uk-child-width-expand" uk-tab>
                    <li><a id="all_vacantes" href="#">Vacantes</a></li>
                    <li><a id="mis_favoritas" href="#">Mis favoritas</a></li>
                </ul>
            </div>

            <!-- Vacantes -->
            <div id="vacantes_div">

            </div>
        </div>
        <!-- End Middle Column -->

        <!-- Right Column -->
        <div class="w3-col m3" style="padding-left: 5px; float: right">
            <div class="title-dash">
                Mis postulados

                <form class="uk-search uk-search-default" id="postulados_">
                    <button uk-search-icon></button>
                    <input name="q" class="uk-search-input" type="search" placeholder="Buscar">
                </form>


                <button class="etapas_post" type="button"><i class="fa fa-filter" aria-hidden="true"></i>
                </button>
                <div uk-dropdown>
                    <ul class="uk-nav uk-dropdown-nav">
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <i class="fa fa-globe fa-2x" aria-hidden="true"></i>
                                    <label>Todos</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_cv_enviado.png" style="height: 30px;">
                                    <label>Postulado</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_cv_enviado.png" style="height: 30px;">
                                    <label>CV Visto</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_entrevista.png" style="height: 30px;">
                                    <label>En entrevista</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_psicometrico.png" style="height: 30px;">
                                    <label>Psicométrico</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_contratado.png" style="height: 30px;">
                                    <label>Contratado</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_vac_cubierta.png" style="height: 30px;">
                                    <label>Vacante cubierta</label>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="li_etapas">
                                <div class="boton">
                                    <img src="Img/Desgrane/mipmap/icn_vac_cubierta.png" style="height: 30px;">
                                    <label>Rechazado</label>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>


            </div>
            <div class="misPostulados">
                <div id="postulados-div" class="w3-container"></div>
            </div>


        </div>
        <!-- End Right Column -->

    </div>
</div>

<!-- End Page Container -->
<br>

<script src="{{asset('js/dashboard.js')}}"></script>
<script>
    formBuscarFetch('');

    // Accordion
    function myFunction(id) {
        var x = document.getElementById(id);
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
            x.previousElementSibling.className += " w3-theme-d1";
        } else {
            x.className = x.className.replace("w3-show", "");
            x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" w3-theme-d1", "");
        }
    }

    // Used to toggle the menu on smaller screens when clicking on the menu button
    function openNav() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }

    //Galeria de img
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = x.length
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex - 1].style.display = "block";
    }

    // fin galeria


    let vacantes_arr =  @php echo json_encode($json_vacantes); @endphp;

    let candidatos_arr = @php echo json_encode($candidatos->exito != 0 ?  $candidatos->candidates : '')@endphp;
    formBuscarCandidato('');
    localStorage.setItem('prohunter', '{{$usuario_app->correo}}');

</script>
<!-- firebase  -->
<script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
<script> let url = ''; </script>
<script src="{{asset('js/firebase.js')}}"></script>
</body>
</html>

<?php
}
?>
