<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prohunter</title>
    <style type="text/css">

        @font-face {
           font-family: 'Harabara';
           src: url('{{asset('Fonts/Harabara.ttf')}}');
        }
        .btn-and , .btn-ios{
            position: absolute;
            left: 30%;
        }
        .btn-and img , .btn-ios img{
            width:55%;
            height: 55%;
        }
        .btn-ios{
            top: 35%;
        }
        .btn-and{
            top: 35%;
        }
        .st0{
           fill:none;
           enable-background:new;
        }
        .st1{
           fill:#FEFEFE;
        }
        .st2{
           font-family: 'Harabara';
        }
        .st3{
           font-size:83.8755px;
        }
        .st4{
           font-family:'Harabara';
        }
        </style>
</head>
<body>
    
<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 1242 2208" style="enable-background:new 0 0 1242 2208;" xml:space="preserve">

<image style="overflow:visible;" width="1242" height="2208" xlink:href="{{asset('storage/app/movil/D8E3410EC89EA1.png')}}" >
</image>
<path class="st0" />
<image style="overflow:visible;" width="1242" height="2208" xlink:href="{{asset('storage/app/movil/D8E3410EC89EA3.png')}}" >
</image>
<path class="st0" />
<image style="overflow:visible;" width="758" height="1260" xlink:href="{{asset('storage/app/movil/D8E3410EC89EA6.png')}}"  transform="matrix(1 0 0 1 315 948)">
</image>
<path class="st0" />
<image style="overflow:visible;" width="414" height="769" xlink:href="{{asset('storage/app/movil/D8E3410EC89EA7.png')}}"  transform="matrix(1 0 0 1 587 1048)">
</image>
<path class="st0" />
<text transform="matrix(1 0 0 1 317.375 407.6172)" class="st1 st2 st3">Descarga la app</text>
<text transform="matrix(1 0 0 1 424.7578 508.2695)" class="st1 st2 st3">Prohunter</text>
<text transform="matrix(1 0 0 1 257.6016 255.6172)" class="st1 st4 st3">¡Caza desde casa!</text>


</svg>

    


<?php
 
 // android
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
  ?>
    <a class="btn-and" href="https://play.google.com/store/apps/details?id=com.oomovil.cam" >
        <img style="overflow:visible;" width="564" height="168" src="{{asset('storage/app/movil/D8E3410EC89EAD.png')}}"  transform="matrix(1 0 0 1 329 557)">
    </a>
  <?php
}


// ipad


// iphone/ipod
if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod')||strpos($_SERVER['HTTP_USER_AGENT'],'iPad'))
{
    ?>
    <a class="btn-ios" href="https://itunes.apple.com/mx/app/huitr%C3%B3n-flash-cards/id1261082991" >
        <img style="overflow:visible;" width="565" height="178" src="{{asset('storage/app/movil/D8E3410EC89EA2.png')}}"  transform="matrix(1 0 0 1 329 755)">
    </a>
    <?php
}
 
?>



</body>
</html>