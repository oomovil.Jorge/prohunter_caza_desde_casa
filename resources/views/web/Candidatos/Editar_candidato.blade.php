<?php
if(!isset($_SESSION['Oomovil']) or $_SESSION['isAdmin'] == true){
    header('Location: /');
    exit;
}else{

Session::put('session',$_SESSION['Oomovil']);
Session::put('privilegios',$_SESSION['privilegios']);
$session =  \Illuminate\Support\Facades\Session::get('session');
$privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');


//OBTENER DATOS DEL USUARIO
$usuario_app = \App\User::where('id',$session)->first();
$usuario_app_info_bancaria = \App\UserBankData::where('id_user',$session)->first();

$email = ['form_params' => ['email' => $usuario_app->correo]];
$correo = ['form_params' =>['correo' => $usuario_app->correo]];
$id_candidato  =  array("id" => isset($_GET['id']) ? $_GET['id'] : 0);
$vacio    =   array();

$service = new \GuzzleHttp\Client(['verify' => false]);

  // PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());
// BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());

// ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());

// AVISO DE PRIVACIDAD
$response = $service->post(url('api/aviso_privacidad'), $vacio);
$aviso_privacidad = json_decode($response->getBody()->getContents());

// datos del candidato
$response = $service->post(url('api/datos_candidato'), $id_candidato);
$Candidato = json_decode($response->getBody()->getContents());
// lista software e idiomas
$response = $service->post(url('api/load_languages_softwares'), array());
$lista_idioma_software = json_decode($response->getBody()->getContents());



if($Candidato->msg == 'Candidato no encontrado'){
    header('Location: '.route('Web'));
    exit;
}else{

    $countries =  \Illuminate\Support\Facades\DB::select('select * from countries');
    $personal_data = $Candidato->candidato->personal_data;
    $education_data  = array();

    foreach($Candidato->candidato->education_data  as $education){
        $education_data = $education;
    }

}

?>

        <!DOCTYPE html>
<html>
<title>Pro Hunter</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Pro hunter">
<meta name="Oomovil">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

<link rel="stylesheet" href="{{asset('css/toolbar.css')}}">

<script src="{{asset('jquery-3.2.1.min.js')}}"></script>
<!--Alertify-->
<link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
<link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
<script src="{{asset('alertifyjs/alertify.js')}}"></script>
<!-- uikit -->
<link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
<script src="{{asset('uikit/js/uikit.min.js')}}"></script>
<script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

<link rel="stylesheet" href=" {{ asset('css/bootstrap.css')}}">
<script src="{{asset('css/Bootstrap/js/bootstrap.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('css/w3.css')}}">
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
<link  href="{{url('DataPicker/datepicker.css')}}" rel="stylesheet">

<style>
    html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
    .uk-button{
        padding: 0px 10px;
    }

    ::-webkit-scrollbar {
        display: none;
    }
    .w3-dropdown-content{
        right: 0px;
        margin-right: 10px;
        border-radius: 5px;

        color: #c2c9d1;
    }
    .w3-button:hover{
        background: #41636f !important;
        color: white !important;
        text-decoration:none !important;
    }

     .btn_agregar{
         border-radius: 140px 140px 140px 140px;
         -moz-border-radius: 140px 140px 140px 140px;
         -webkit-border-radius: 140px 140px 140px 140px;
         border: 0px solid #115a8a;background-color: #115a8a;color: white;padding: 7px;margin-top: 7px
     }
    .btm_eliminar{
        border-radius: 140px 140px 140px 140px;
        -moz-border-radius: 140px 140px 140px 140px;
        -webkit-border-radius: 140px 140px 140px 140px;
        border: 0px solid #b22034;background-color: #b22034;color: white;padding: 7px;margin-bottom: 7px
    }

    .range-slider {
        margin: 15px 0 15px 0%;
        width: 80%;
    }


    .range-slider__range {
        -webkit-appearance: none;
        width: calc(100% - (73px));
        height: 10px;
        border-radius: 5px;
        background: #d7dcdf;
        outline: none;
        padding: 0;
        margin: 0;
    }
    .range-slider__range::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        background: #2c3e50;
        cursor: pointer;
        transition: background .15s ease-in-out;
    }
    .range-slider__range::-webkit-slider-thumb:hover {
        background: #1abc9c;
    }
    .range-slider__range:active::-webkit-slider-thumb {
        background: #1abc9c;
    }
    .range-slider__range::-moz-range-thumb {
        width: 20px;
        height: 20px;
        border: 0;
        border-radius: 50%;
        background: #2c3e50;
        cursor: pointer;
        transition: background .15s ease-in-out;
    }
    .range-slider__range::-moz-range-thumb:hover {
        background: #1abc9c;
    }
    .range-slider__range:active::-moz-range-thumb {
        background: #1abc9c;
    }
    .range-slider__range:focus::-webkit-slider-thumb {
        box-shadow: 0 0 0 3px #fff, 0 0 0 6px #1abc9c;
    }

    .range-slider__value {
        display: inline-block;
        position: relative;
        width: 60px;
        color: #fff;
        line-height: 20px;
        text-align: center;
        border-radius: 3px;
        background: #2c3e50;
        padding: 5px 10px;
        margin-left: 8px;
    }
    .range-slider__value:after {
        position: absolute;
        top: 8px;
        left: -7px;
        width: 0;
        height: 0;
        border-top: 7px solid transparent;
        border-right: 7px solid #2c3e50;
        border-bottom: 7px solid transparent;
        content: '';
    }

    ::-moz-range-track {
        background: #d7dcdf;
        border: 0;
    }

    input::-moz-focus-inner,
    input::-moz-focus-outer {
        border: 0;
    }


    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#0C9;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
        font-size: 18px;
    }

    .btn_hover:hover {
        border-radius: 60px 60px 60px 60px;
        -moz-border-radius: 60px 60px 60px 60px;
        -webkit-border-radius: 60px 60px 60px 60px;
        box-shadow: 0 4px 16px rgb(164, 165, 169);,
    transition: all 0.3s ease;
    }
    .btn_hover_sinborde:hover {
        box-shadow: 0 4px 16px rgb(164, 165, 169);,
    transition: all 0.3s ease;
    }
</style>
<body class="w3-light-grey">

            <div id="modal-acerca" uk-modal>
              <div class="uk-modal-dialog uk-modal-body">
                      <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                  <div class="w3-content w3-display-container" align="center">
                      @foreach($about->image_urls as $img)
                      <img class="mySlides" src="{{$img}}" style="height: 250px">
                      @endforeach
                      <img src="{{asset('Img/logo-prohunter-color.png')}}" style="height: 100px;">
                      <button class="w3-button w3-white w3-display-left"  onclick="plusDivs(-1)">&#10094;</button>
                      <button class="w3-button w3-white w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                  </div>
                  <br>
                  <p>{{$about->text}}</p>
                  </p>
              </div>
            </div>

            <div id="modal-aviso" uk-modal>
              <div class="uk-modal-dialog uk-modal-body">
                      <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                  {!!html_entity_decode($aviso_privacidad->text)!!}
                  </p>
              </div>
            </div>

            <div id="modal-ayuda" uk-modal>
              <div class="uk-modal-dialog uk-modal-body">
                      <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                  <h2 style="text-align: center;">Ayuda</h2>
                  <!-- Mis vacantes favorias -->
                  <ul style="list-style-type: none;">
                          @foreach($preguntas_frecuentes->data as $preguntas)
                              <li>
                                  <h3><b>{{ $preguntas->question}}</b></h3>
                                  <p>{{ $preguntas->answer}}</p>
                              </li>
                              @endforeach
                  </ul>

              </div>
            </div>
            <!-- Navbar -->
            <div class="w3-top ph-nav">
                <div class="w3-bar w3-theme-d2 w3-left-align w3-large">

                    <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>



                    <div class="w3-dropdown-hover w3-hide-small Close_Session" style="float: right;" >
                            <button class="w3-button w3-padding-large" title="Cerrar_sesion"><i class="fa fa-sign-out"></i><span id="Cerrar_sesion" name="Cerrar_sesion" class="w3-badge w3-right w3-small w3-green"></span></button>
                            <div>
                                <script type="text/javascript">
                                    $(function() {
                                        $('.Close_Session').click(function(e) {
                                            alertify.confirm('Cerrar sesión',"¿Quieres cerrar sesión?",
                                                    function() {
                                                        localStorage.removeItem('prohunter');
                                                        localStorage.removeItem('firebase');
                                                        window.location.href = '{{url('/api/cerrar_sesion_user')}}'; window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                                    }, function() {/*Cancel*/});
                                        });
                                    });
                                </script>
                            </div>
                        </div>

                    <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                        <button class="w3-button w3-padding-large" title="notificaciones"><i class="fa fa-globe"></i><span id="notificaciones" name="notificaciones" class="w3-badge w3-right w3-small w3-green"></span></button>
                            <div id="noticias-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:250px;max-height: 500px;overflow-y: scroll;">
                            </div>
                    </div>

                    <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                        <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-bell"></i><span id="avisos" name="avisos"></span></button>
                        <div id="avisos-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:300px;max-height: 500px;overflow-y: scroll;">

                        </div>
                    </div>

                    <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                        <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-user"></i><span id="perfil" name="perfil"  class="w3-badge w3-right w3-small w3-green"></span></button>
                        <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block" >
                                <div style="padding: 10px 0px;">
                                        <img src="{{asset('storage/app/triangle.svg')}}" style="position: absolute;top: -16px;height: 21px;left: 45px;">
                                        <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

                                        <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

                                        <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

                                        <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

                                        <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

                                </div>
                            </div>
                    </div>
                    <div class="w3-dropdown-hover w3-hide-small" style="float: right;" >
                        <a href="{{route('Web')}}" class="w3-button w3-padding-large" title="inicio"><i class="fa fa-home"></i></a>

                    </div>
                </div>
            </div>
            <!-- Fin Navbar -->
            <script>
              //Galeria de img
              var slideIndex = 1;
              showDivs(slideIndex);

              function plusDivs(n) {
                  showDivs(slideIndex += n);
              }

              function showDivs(n) {
                  var i;
                  var x = document.getElementsByClassName("mySlides");
                  if (n > x.length) {slideIndex = 1}
                  if (n < 1) {slideIndex = x.length}
                  for (i = 0; i < x.length; i++) {
                      x[i].style.display = "none";
                  }
                  x[slideIndex-1].style.display = "block";
              }
              // fin galeria
            </script>

            <!-- Modificar candidato -->
            <div  class="w3-content" style="max-width:800px;margin-top: 50px;">
    <div class="w3-row-padding" style="padding-top: 15px;">

        <form method="POST" id="formCandidato" name="formCandidato" onsubmit="return formularioCandidato()">
           <input type="text" name="id_candidato" id="id_candidato" value="{{$_GET['id']}}" hidden>

                <div  style="top: -12px;position: relative;max-width:100%" id="nodos" >


                <!-- form  personal info-->
                <div class="w3-margin-bottom" id="personal-nodos" name="personal-nodos">
                    <div class="w3-white w3-text-grey w3-card-4">
                        <div class="w3-display-container">
                            <h2  style="padding: 15px;" class="w3-text-grey w3-padding-16"><i class="fa fa-user fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Modificar candidato</h2>
                            <div class="w3-display-bottomleft w3-container w3-text-black">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="w3-display-container" align="center">
                            <img src="{{ isset($personal_data->profile_picture) ? $personal_data->profile_picture  : asset('Img/usuarioxl.png')}}" class="w3-circle" style="height: 300px;width: 300px" alt="Avatar">
                        </div>
                        <div class="w3-container">
                            <br>
                            <div class="uk-margin">

                                <div class="js-upload btn_hover_sinborde" uk-form-custom>
                                    <input type="file" id="profile_picture" name="profile_picture" accept="image/*">
                                    <button class="uk-button uk-button-default" type="button" tabindex="-1">Agregar imagen</button>
                                </div>

                                <div class="js-upload btn_hover_sinborde" uk-form-custom>
                                    <input type="file" id="curriculum" name="curriculum" >
                                    <button class="uk-button uk-button-default" type="button" tabindex="-1">Agregar currículum</button>
                                </div>
                                <br>
                                <label class="uk-form-label"> * Nombres :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSolo"  minlength="3" id="names" name="names" type="text" placeholder="Nombres" required value="{{$personal_data->names}}">
                                </div>

                                <label class="uk-form-label">* Apellido paterno :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSolo" id="paternal_name"  minlength="3" name="paternal_name" type="text" placeholder="Apellido paterno" required value="{{$personal_data->paternal_name}}">
                                </div>

                                <label class="uk-form-label">* Apellido materno :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSolo"  id="maternal_name"  minlength="3" name="maternal_name" type="text" placeholder="Apellido materno " required value="{{$personal_data->maternal_name}}">
                                </div>

                                <label class="uk-form-label">* Fecha de nacimiento :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input datepicker"  minlength="10" maxlength="10"  id="birthday_date" name="birthday_date" type="text" placeholder="Fecha de nacimiento" required value="{{ date("d/m/Y", strtotime($personal_data->birthday_date))}}">
                                </div>

                                <label class="uk-form-label" for="country"> * Pais :</label>
                                <select class="uk-select" id="country" name="country" required>
                                    <option value="" disabled>Selecccionar Pais</option>
                                    @foreach($countries as $result)
                                        <option value="{{ $result->id}}" @if($result->name == $personal_data->country)selected @endif>{{$result->name}}</option>
                                    @endforeach
                                </select>

                                <label class="uk-form-label" for="state"> * Estado :</label>
                                <select class="uk-select" id="state" name="state" required>

                                    <option value='' disabled>Seleccione un Estado</option>
                                    <?php  $states =  \Illuminate\Support\Facades\DB::select('select * from states where country_id = (select id from countries where name = ?)',[$personal_data->country]); ?>
                                    @foreach($states as $result)
                                        <option value="{{$result->id}}" @if($result->name == $personal_data->state)selected @endif>{{$result->name}}</option>
                                    @endforeach
                                </select>

                                <label class="uk-form-label" for="city"> * Ciudad :</label>
                                <select class="uk-select" id="city" name="city" required>
                                    <option value='' disabled>Seleccione una ciudad</option>
                                    <?php  $citys =  \Illuminate\Support\Facades\DB::select('select * from cities where state_id = (select id from states where name = ?)',[$personal_data->state]);?>
                                    @foreach($citys as $result)
                                        <option value="{{$result->id}}" @if($result->name == $personal_data->city) selected @endif>{{$result->name}}</option>
                                    @endforeach

                                </select>

                                <label class="uk-form-label">* Colonia :</label>
                                <div class="uk-form-controls" for="district"I>
                                    <input class="uk-input txtSolo" id="district" name="district" type="text" placeholder="Colonia" required value="{{$personal_data->district}}">
                                </div>

                                <label class="uk-form-label" >* Calle :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSolo" id="street" name="street" type="text" placeholder="Calle" required value="{{$personal_data->street}}">
                                </div>

                                <label class="uk-form-label" >* Número ext :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="house_number"  minlength="10" maxlength="11" name="house_number" type="text" placeholder="Número ext" required value="{{$personal_data->house_number}}">
                                </div>

                                <label class="uk-form-label"> Teléfono casa :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtCel" id="home_phone" minlength="10" maxlength="11" name="home_phone"  type="text" placeholder="Teléfono casa" value="{{$personal_data->home_phone}}">
                                </div>

                                <label class="uk-form-label">* Teléfono celular :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtCel" id="mobile_phone" name="mobile_phone" type="text" placeholder="Teléfono celular" value="{{$personal_data->mobile_phone}}">
                                </div>

                                <label class="uk-form-label">* Correo :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="email" name="email" type="email" placeholder="Correo" required value="{{$personal_data->email}}">
                                </div>

                                <label class="uk-form-label">* CURP :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="curp" name="curp" type="text" placeholder="CURP" required value="{{$personal_data->curp}}">
                                </div>

                                <label class="uk-form-label"> RFC :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSoloMayus" id="rfc" name="rfc" type="text" placeholder="RFC" value="{{$personal_data->rfc}}">
                                </div>

                                <label class="uk-form-label">NSS :</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input txtSoloMayus" id="nss" name="nss" type="text" placeholder="NSS" value="{{$personal_data->nss}}">
                                </div>

                                <div class="uk-margin">
                                    <div class="uk-form-label">Género</div>
                                    <div class="uk-form-controls uk-form-controls-text">
                                        <label style="padding-right: 25px">
                                            <input class="uk-radio" type="radio"  id="gender" name="gender" @if($personal_data->gender == "Hombre") checked @endif value="Hombre"> Hombre</label>
                                        <label><input class="uk-radio" type="radio"  id="gender" name="gender" @if($personal_data->gender == "Mujer") checked @endif value="Mujer"> Mujer</label>
                                    </div>
                                </div>
                                <div class="uk-margin">
                                    <div class="uk-form-label">Busca trabajo</div>
                                    <div class="uk-form-controls uk-form-controls-text">

                                        <label style="padding-right: 25px">
                                            <input class="uk-checkbox" type="checkbox"  id="seeking_work" name="gender" @if($personal_data->seeking_work == "1") checked @endif  > Si</label>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                </div>
                <!-- form  personan info-->

                    <br>
          <!-- form  educacion-->

                <div id="Edu-nodos" name="Edu-nodos">
                    @php $edu = 0; @endphp
                    @foreach($Candidato->candidato->education_data  as $education)
                    <div class="w3-container w3-card w3-white w3-margin-bottom" >
                        @php $edu++; @endphp

                        <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                        class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Educación</h2>
                        <div class="w3-container" id="Edu00" name="Edu00">

                        <label class="uk-form-label"> * Universidad / Instituto :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="Edu-universidad"  name="Edu-universidad" type="text" placeholder="Universidad / Instituto" required value="{{$education->universidad}}">

                            <br><br>

                            <label class="uk-form-label"> * Desde :</label>
                            <select class="uk-select uk-input uk-form-width-medium" id="Edu-desdeMes"  name="Edu-desdeMes" required>
                                <option value='0'>Seleccióna un mes</option>
                                <option value='1' @if($education->start_month == '1') selected @endif >Enero</option>
                                <option value='2' @if($education->start_month == '2') selected @endif>Febrero</option>
                                <option value='3' @if($education->start_month == '3') selected @endif>Marzo</option>
                                <option value='4' @if($education->start_month == '4') selected @endif>Abril</option>
                                <option value='5' @if($education->start_month == '5') selected @endif>Mayo</option>
                                <option value='6' @if($education->start_month == '6') selected @endif>Junio</option>
                                <option value='7' @if($education->start_month == '7') selected @endif>Julio</option>
                                <option value='8' @if($education->start_month == '8') selected @endif>Agosto</option>
                                <option value='9' @if($education->start_month == '9') selected @endif>Septiembre</option>
                                <option value='10' @if($education->start_month == '10') selected @endif >Octubre</option>
                                <option value='11' @if($education->start_month == '11') selected @endif>Noviembre</option>
                                <option value='12' @if($education->start_month == '12') selected @endif>Diciembre</option>
                            </select>
                            -

                            <select class="uk-select uk-input uk-form-width-medium" id="Edu-desdeAnio"  name="Edu-desdeAnio" required>
                              @for($i = date("Y"); $i >= 1960; $i--)
                                <option @if($i == $education->start_year)selected @endif value='{{$i}}'>{{$i}}</option>
                               @endfor
                            </select>

                            <br><br>

                            <label class="uk-form-label">* Hasta :</label>
                            <select class="uk-select uk-input uk-form-width-medium" id="Edu-hastaMes"  name="Edu-hastaMes" required>
                                <option value='0'>Seleccióna un mes</option>
                                <option value='1' @if($education->end_month == '1') selected @endif >Enero</option>
                                <option value='2' @if($education->end_month == '2') selected @endif>Febrero</option>
                                <option value='3' @if($education->end_month == '3') selected @endif>Marzo</option>
                                <option value='4' @if($education->end_month == '4') selected @endif>Abril</option>
                                <option value='5' @if($education->end_month == '5') selected @endif>Mayo</option>
                                <option value='6' @if($education->end_month == '6') selected @endif>Junio</option>
                                <option value='7' @if($education->end_month == '7') selected @endif>Julio</option>
                                <option value='8' @if($education->end_month == '8') selected @endif>Agosto</option>
                                <option value='9' @if($education->end_month == '9') selected @endif>Septiembre</option>
                                <option value='10' @if($education->end_month == '10') selected @endif >Octubre</option>
                                <option value='11' @if($education->end_month == '11') selected @endif>Noviembre</option>
                                <option value='12' @if($education->end_month == '12') selected @endif>Diciembre</option>
                            </select>
                            -
                            <select class="uk-select uk-input uk-form-width-medium" id="Edu-hastaAnio"  name="Edu-hastaAnio" required>
                                @for($i = date("Y"); $i >= 1960; $i--)
                                    <option @if($i == $education->end_year)selected @endif value='{{$i}}'>{{$i}}</option>
                                @endfor
                            </select>

                            <br><br>

                            <label class="uk-form-label">Grado académico :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="Edu-grado"  name="Edu-grado" type="text" placeholder="Grado académico" value="{{$education->academic_level}}">

                            <br><br>

                            <label class="uk-form-label">Titulo obtenido :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input class="uk-input uk-input uk-form-width-large" id="Edu-titulo"  name="Edu-titulo" type="text" placeholder="Titulo obtenido" value="{{$education->degree_obtained}}">

                            <br><br>

                            <label class="uk-form-label">Información adicional:</label>
                            <br><br>
                            <textarea class="uk-textarea" rows="3" placeholder="Textarea" id="Edu-info_adi"  name="Edu-info_adi">{{$education->additional_info}}</textarea>
                            <br><br>
                        </div>
                    </div>
                   @endforeach
                </div>
           <!-- fin form educacion -->
                        <br>
                <!-- Idiomas -->

              <div id="Idi-nodos" name="Idi-nodos">
                    @php $Idi = 0; @endphp
                    @foreach($Candidato->candidato->languages  as $lenguajes)
                    <div class="w3-container w3-card w3-white w3-margin-bottom" >

                    <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                    class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Idiomas</h2>
                        <div class="w3-container" id="Idi00" name="Idi00">

                            <label class="uk-form-label"> * Idioma :</label>
                            <select class="uk-select uk-input uk-form-width-medium" id="Idi-idioma" name="Idi-idioma" required>
                                <option value='0'>Seleccione un idioma</option>
                                @foreach($lista_idioma_software->languages as $lenguaje)
                                    <option value='{{$lenguaje->id}}' @if($lenguaje->name == $lenguajes->name) selected @endif>{{$lenguaje->name}}</option>
                                @endforeach
                            </select>

                            <div class="range-slider" id="range" name="range">
                                <input class="range-slider__range" type="range" min="1" max="100" step="1" id="Idi-idioma_porcentaje" name="Idi-idioma_porcentaje" value="{{$lenguajes->percentage_known}}">
                                <span class="range-slider__value">{{$lenguajes->percentage_known}}</span>
                            </div>

                            <label class="uk-form-label">Tipo certificación :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="Idi0-idioma_certificacion" name="Idi-idioma_certificacion" type="text" placeholder="Tipo certificación"  value="{{$lenguajes->certification_type}}">

                            <br><br>
                        </div>
                    </div>
                    @endforeach
                     </div>
                <!--  software -->
              <div id="soft-nodos" name="soft-nodos">
                    @php $softc = 0; @endphp@foreach($Candidato->candidato->softwares as $software)
                    <div class="w3-container w3-card w3-white w3-margin-bottom" >

                        <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                        class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Software</h2>
                        <div class="w3-container" id="soft00">

                            <label class="uk-form-label"> * software :</label>

                            <select class="uk-select uk-input uk-form-width-medium" id="soft0-software" name="soft0-software" required>
                                <option value='0'>Seleccione un software</option>
                                @foreach($lista_idioma_software->softwares as $softwares)
                                    <option value='{{$softwares->id}}' @if($softwares->name == $software->name) selected @endif >{{$softwares->name}}</option>
                                @endforeach
                            </select>

                            <div class="range-slider" id="range" name="range">
                                <input class="range-slider__range" type="range"  min="1" max="100" step="1" id="soft0-software_porcentaje" name="soft0-software_porcentaje" value="{{$software->percentage_known}}">
                                <span class="range-slider__value">{{$software->percentage_known}}</span>
                            </div>
                            <label class="uk-form-label">Tipo certificación :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="soft0-software_certificacion" name="soft0-software_certificacion" type="text" placeholder="Tipo certificación" value="{{$software->certification_type}}">
                            <br><br>
                        </div>
                    </div>
                    @endforeach
                </div>

          <!-- form laboral -->

                <div id="xp-nodos" name="xp-nodos">
                    @php $softc = 0; @endphp  @foreach($Candidato->candidato->work_experience  as $work_experience)
                    <div class="w3-container w3-card w3-white w3-margin-bottom" >

                        <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                        class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Experiencia laboral</h2>
                        <div class="w3-container" id="xp00">

                            <label class="uk-form-label"> * Puesto de la empresa : &nbsp;</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-puesto" name="xp0-puesto" type="text" placeholder="Puesto de la empresa" required value="{{$work_experience->position}}">
                            <br>
                            <br>
                            <label class="uk-form-label">* Nombre de la empresa :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-Nombre_empresa" name="xp0-Nombre_empresa" type="text" placeholder="Nombre de la empresa" required value="{{$work_experience->company_name}}">
                            <br>
                            <br>
                            <label class="uk-form-label"> * Giro de la empresa :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-giro" name="xp0-giro" type="text" placeholder="Giro de la empresa" required value="{{$work_experience->role}}">
                            <br><br>
                            <label class="uk-form-label">Actividades y/o funciones:</label>
                            <textarea class="uk-textarea" rows="3" placeholder="Textarea" id="xp0-actividades"  name="xp0-actividades" style="width: 70%">{{$work_experience->role}}</textarea>
                            <br><br>

                            <label class="uk-form-label" for="" > * Desde :</label>
                            <select class="uk-select uk-input uk-form-width-medium" id="xp0-desdeMes" name="xp0-desdeMes" required>
                                <option value='0'>Seleccióna un mes</option>
                                <option value='1' @if($work_experience->start_month == '1') selected @endif>Enero</option>
                                <option value='2' @if($work_experience->start_month == '2') selected @endif>Febrero</option>
                                <option value='3' @if($work_experience->start_month == '3') selected @endif>Marzo</option>
                                <option value='4' @if($work_experience->start_month == '4') selected @endif>Abril</option>
                                <option value='5' @if($work_experience->start_month == '5') selected @endif>Mayo</option>
                                <option value='6' @if($work_experience->start_month == '6') selected @endif>Junio</option>
                                <option value='7' @if($work_experience->start_month == '7') selected @endif>Julio</option>
                                <option value='8' @if($work_experience->start_month == '8') selected @endif>Agosto</option>
                                <option value='9' @if($work_experience->start_month == '9') selected @endif>Septiembre</option>
                                <option value='10' @if($work_experience->start_month == '10') selected @endif>Octubre</option>
                                <option value='11' @if($work_experience->start_month == '11') selected @endif>Noviembre</option>
                                <option value='12' @if($work_experience->start_month == '12') selected @endif>Diciembre</option>
                            </select>
                            -
                            <select class="uk-select uk-input uk-form-width-medium" id="xp0-desdeAnio" name="xp0-desdeAnio" required>
                                @for($i = date("Y"); $i >= 1960; $i--)
                                    <option @if($i == $work_experience->start_year)selected @endif value='{{$i}}'>{{$i}}</option>
                                @endfor
                            </select>

                            <br><br>
                            <label style="padding-left: 20%;" id="lbl_check" name="lbl_check" >
                                <input class="uk-checkbox" type="checkbox" id="xp0-check" name="xp0-check"  @if($work_experience->end_month != '1') checked  @endif> Actualmente trabajando</label>
                            <br><br>

                           <label class="uk-form-label" name="xp0-lbl_hasta" id="xp0-lbl_hasta">* Hasta :</label>
                           <select class="uk-select uk-input uk-form-width-medium" id="xp0-hastaMes" name="xp0-hastaMes" required>
                                <option value='0'>Seleccióna un mes</option>
                                <option value='1' @if($work_experience->end_month == '1') selected @endif>Enero</option>
                                <option value='2' @if($work_experience->end_month == '2') selected @endif>Febrero</option>
                                <option value='3' @if($work_experience->end_month == '3') selected @endif>Marzo</option>
                                <option value='4' @if($work_experience->end_month == '4') selected @endif>Abril</option>
                                <option value='5' @if($work_experience->end_month == '5') selected @endif>Mayo</option>
                                <option value='6' @if($work_experience->end_month == '6') selected @endif>Junio</option>
                                <option value='7' @if($work_experience->end_month == '7') selected @endif>Julio</option>
                                <option value='8' @if($work_experience->end_month == '8') selected @endif>Agosto</option>
                                <option value='9' @if($work_experience->end_month == '9') selected @endif>Septiembre</option>
                                <option value='10' @if($work_experience->end_month == '10') selected @endif>Octubre</option>
                                <option value='11' @if($work_experience->end_month == '11') selected @endif>Noviembre</option>
                                <option value='12' @if($work_experience->end_month == '12') selected @endif>Diciembre</option>
                            </select>

                            <label class="uk-form-label" name="xp0-separador" id="xp0-separador"> - </label>
                            <select class="uk-select uk-input uk-form-width-medium" id="xp0-hastaAnio" name="xp0-hastaAnio" required>
                                @for($i = date("Y"); $i >= 1960; $i--)
                                    <option @if($i == $work_experience->end_year)selected @endif value='{{$i}}'>{{$i}}</option>
                                @endfor
                            </select>

                            <br><br>

                            <label class="uk-form-label">Link :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-link1" name="xp0-link1" type="text" placeholder="Link a red social 1 / portafolio de trabajo"  value="{{$work_experience->work_portfolio_url_1}}">
                            <br><br>
                            <label class="uk-form-label">Link :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-link2" name="xp0-link2" type="text" placeholder="Link a red social 2 / portafolio de trabajo"  value="{{$work_experience->work_portfolio_url_1}}">
                            <br><br>
                            <label class="uk-form-label">Link :</label>
                            <input class="uk-input uk-input uk-form-width-large" id="xp0-link3" name="xp0-link3" type="text" placeholder="Link a red social 3 / portafolio de trabajo"  value="{{$work_experience->work_portfolio_url_1}}">

                            <br><br>
                        </div>
                    </div>
                    @endforeach
                </div>
                    <input type="button" class="float btn_hover" uk-toggle="target: #modal-Cartas" value="+"/>
                    <div id="modal-Cartas" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                            <h2 class="uk-modal-title">¿Deseas agregar mas información?</h2>
                            <table class="uk-table uk-table-justify uk-table-divider">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle;align-content: center;">
                                        <i class="fa fa-graduation-cap fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Educación</td>
                                    <td><button style="float: right;" class="uk-button uk-button-default uk-modal-close" onclick="VisibleEdu()" type="button">Agregar</button></td>
                                </tr>
                                <tr>
                                    <td  style="vertical-align: middle;align-content: center">
                                        <i class="fa fa-globe fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Idioma</td>
                                    <td><button style="float: right;" class="uk-button uk-button-default uk-modal-close" onclick="VisibleIdi()" type="button">Agregar</button></td>
                                </tr>
                                <tr>
                                    <td  style="vertical-align: middle;align-content: center">
                                        <i class="fa fa-desktop fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Software</td>
                                    <td><button style="float: right;" class="uk-button uk-button-default uk-modal-close" onclick="VisibleSoft()" type="button">Agregar</button></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;align-content: center">
                                        <i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Experiencia laboral</td>
                                    <td><button style="float: right;" class="uk-button uk-button-default uk-modal-close" onclick="Visiblexp()" type="button">Agregar</button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
            </div>
            </div>
                <input type="submit" class="btn btn-info btn-lg btn-block uk-modal-close"  id="btnGuardar" name="btnGuardar" value="Terminar y guardar"/>
                <br>
                <br>
                <br>
        </form>

    <div style="display:none">

            <div class="w3-container w3-card w3-white w3-margin-bottom" id="edu_">

                    <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                       class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>

                    <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Educación</h2>


                    <div class="w3-container" id="Edu00" name="Edu00">
                        <label class="uk-form-label"> * Universidad / Instituto :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="Edu-universidad"  name="Edu-universidad" type="text" placeholder="Universidad / Instituto" >

                        <br><br>

                        <label class="uk-form-label"> * Desde :</label>
                        <select class="uk-select uk-input uk-form-width-medium" id="Edu-desdeMes"  name="Edu-desdeMes" >
                            <option value=''>Seleccióna un mes</option>
                            <option value='1'>Enero</option>
                            <option value='2'>Febrero</option>
                            <option value='3'>Marzo</option>
                            <option value='4'>Abril</option>
                            <option value='5'>Mayo</option>
                            <option value='6'>Junio</option>
                            <option value='7'>Julio</option>
                            <option value='8'>Agosto</option>
                            <option value='9'>Septiembre</option>
                            <option value='10'>Octubre</option>
                            <option value='11'>Noviembre</option>
                            <option value='12'>Diciembre</option>
                        </select>
                        -
                        <select class="uk-select uk-input uk-form-width-medium" id="Edu-desdeAnio"  name="Edu-desdeAnio" >
                            <option value=''>Seleccióna un año</option>
                            @for($i = date("Y"); $i >= 1960; $i--)
                                <option value='{{$i}}'>{{$i}}</option>
                            @endfor
                        </select>

                        <br><br>

                        <label class="uk-form-label">* Hasta :</label>
                        <select class="uk-select uk-input uk-form-width-medium" id="Edu-hastaMes"  name="Edu-hastaMes" >
                            <option value=''>Seleccióna un mes</option>
                            <option value='1'>Enero</option>
                            <option value='2'>Febrero</option>
                            <option value='3'>Marzo</option>
                            <option value='4'>Abril</option>
                            <option value='5'>Mayo</option>
                            <option value='6'>Junio</option>
                            <option value='7'>Julio</option>
                            <option value='8'>Agosto</option>
                            <option value='9'>Septiembre</option>
                            <option value='10'>Octubre</option>
                            <option value='11'>Noviembre</option>
                            <option value='12'>Diciembre</option>
                        </select>
                        -
                        <select class="uk-select uk-input uk-form-width-medium" id="Edu-hastaAnio"  name="Edu-hastaAnio" >
                            <option value=''>Seleccióna un año</option>
                        @for($i = date("Y"); $i >= 1960; $i--)
                                <option value='{{$i}}'>{{$i}}</option>
                            @endfor
                        </select>
                        <br><br>
                        <label class="uk-form-label">Grado académico :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="Edu-grado"  name="Edu-grado" type="text" placeholder="Grado académico">
                        <br><br>
                        <label class="uk-form-label">Titulo obtenido :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input class="uk-input uk-input uk-form-width-large" id="Edu-titulo"  name="Edu-titulo" type="text" placeholder="Titulo obtenido">
                        <br><br>
                        <label class="uk-form-label">Información adicional:</label>
                        <br><br>
                        <textarea class="uk-textarea" rows="3" placeholder="..." id="Edu-info_adi"  name="Edu-info_adi"></textarea>
                        <br><br>

                        <br><br>

                    </div>
            </div>
            <div class="w3-container w3-card w3-white w3-margin-bottom" id="idi_">

                <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                        class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>


                <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Idiomas</h2>

                <div class="w3-container" id="Idi00" name="Idi00">


                    <label class="uk-form-label"> * Idioma :</label>


                    <select class="uk-select uk-input uk-form-width-medium" id="Idi-idioma" name="Idi-idioma">
                        <option value=''>Seleccione un idioma</option>
                        @foreach($lista_idioma_software->languages as $lenguaje)
                            <option value='{{$lenguaje->id}}'>{{$lenguaje->name}}</option>
                        @endforeach
                    </select>

                    <div class="range-slider" id="range" name="range">
                        <input class="range-slider__range" type="range" min="1" max="100" step="1" id="Idi-idioma_porcentaje" name="Idi-idioma_porcentaje">
                        <span class="range-slider__value">0</span>
                    </div>

                    <label class="uk-form-label">Tipo certificación :</label>
                    <input class="uk-input uk-input uk-form-width-large" id="Idi0-idioma_certificacion" name="Idi-idioma_certificacion" type="text" placeholder="Tipo certificación">
                    <br><br>


                    <br><br>
                </div>
            </div>
            <div class="w3-container w3-card w3-white w3-margin-bottom" id="soft_">
                    <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                       class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>
                    <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Software</h2>
                    <div class="w3-container" id="soft00">
                        <label class="uk-form-label"> * software :</label>

                        <select class="uk-select uk-input uk-form-width-medium" id="soft0-software" name="soft0-software">
                            <option value=''>Seleccione un software</option>
                            @foreach($lista_idioma_software->softwares as $softwares)
                                <option value='{{$softwares->id}}'>{{$softwares->name}}</option>
                            @endforeach
                        </select>

                        <div class="range-slider" id="range" name="range">
                            <input class="range-slider__range" type="range"  min="1" max="100" step="1" id="soft0-software_porcentaje" name="soft0-software_porcentaje">
                            <span class="range-slider__value">0</span>
                        </div>
                        <label class="uk-form-label">Tipo certificación :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="soft0-software_certificacion" name="soft0-software_certificacion" type="text" placeholder="Tipo certificación">
                        <br><br>


                        <br><br>
                    </div>
            </div>
            <div class="w3-container w3-card w3-white w3-margin-bottom" id="xp_">

                    <a id="Elimina_nodo" name="Elimina_nodo" style="float: right;cursor: pointer;padding-top: 10px;"
                       class="uk-close-large" uk-close  onclick="Elimina_nodo($(this).parent())"></a>

                    <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Experiencia laboral</h2>
                    <div class="w3-container" id="xp00">

                        <label class="uk-form-label"> * Puesto de la empresa : &nbsp;</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-puesto" name="xp0-puesto" type="text" placeholder="Puesto de la empresa" >
                        <br>
                        <br>
                        <label class="uk-form-label">* Nombre de la empresa :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-Nombre_empresa" name="xp0-Nombre_empresa" type="text" placeholder="Nombre de la empresa" >
                        <br>
                        <br>
                        <label class="uk-form-label"> * Giro de la empresa :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-giro" name="xp0-giro" type="text" placeholder="Giro de la empresa" >
                        <br><br>
                        <label class="uk-form-label">Actividades y/o funciones:</label>
                        <textarea class="uk-textarea" rows="3" placeholder="..." id="xp0-actividades"  name="xp0-actividades" style="width: 70%"></textarea>
                        <br><br>

                        <label class="uk-form-label" for="" > * Desde :</label>
                        <select class="uk-select uk-input uk-form-width-medium" id="xp0-desdeMes" name="xp0-desdeMes" >
                            <option value='0'>Seleccióna un mes</option>
                            <option value='1'>Enero</option>
                            <option value='2'>Febrero</option>
                            <option value='3'>Marzo</option>
                            <option value='4'>Abril</option>
                            <option value='5'>Mayo</option>
                            <option value='6'>Junio</option>
                            <option value='7'>Julio</option>
                            <option value='8'>Agosto</option>
                            <option value='9'>Septiembre</option>
                            <option value='10'>Octubre</option>
                            <option value='11'>Noviembre</option>
                            <option value='12'>Diciembre</option>
                        </select>
                        -
                        <select class="uk-select uk-input uk-form-width-medium" id="xp0-desdeAnio" name="xp0-desdeAnio" >
                            @for($i = date("Y"); $i >= 1960; $i--)
                                <option value='{{$i}}'>{{$i}}</option>
                            @endfor
                        </select>
                        <br><br>
                        <label style="padding-left: 20%;" id="lbl_check" name="lbl_check"><input class="uk-checkbox" type="checkbox" id="xp0-check" name="xp0-check"> Actualmente trabajando</label>

                        <br><br>

                        <label class="uk-form-label" name="xp0-lbl_hasta" id="xp0-lbl_hasta">* Hasta :</label>
                        <select class="uk-select uk-input uk-form-width-medium"id="xp0-hastaMes" name="xp0-hastaMes" >
                            <option value='0'>Seleccióna un mes</option>
                            <option value='1'>Enero</option>
                            <option value='2'>Febrero</option>
                            <option value='3'>Marzo</option>
                            <option value='4'>Abril</option>
                            <option value='5'>Mayo</option>
                            <option value='6'>Junio</option>
                            <option value='7'>Julio</option>
                            <option value='8'>Agosto</option>
                            <option value='9'>Septiembre</option>
                            <option value='10'>Octubre</option>
                            <option value='11'>Noviembre</option>
                            <option value='12'>Diciembre</option>
                        </select>
                        <label class="uk-form-label" name="xp0-separador" id="xp0-separador"> - </label>

                        <select class="uk-select uk-input uk-form-width-medium" id="xp0-hastaAnio" name="xp0-hastaAnio" >
                            @for($i = date("Y"); $i >= 1960; $i--)
                                <option value='{{$i}}'>{{$i}}</option>
                            @endfor
                        </select>
                        <br><br>
                        <label class="uk-form-label">Link :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-link1" name="xp0-link1" type="text" placeholder="Link a red social 1 / portafolio de trabajo">
                        <br><br>
                        <label class="uk-form-label">Link :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-link2" name="xp0-link2" type="text" placeholder="Link a red social 2 / portafolio de trabajo">
                        <br><br>
                        <label class="uk-form-label">Link :</label>
                        <input class="uk-input uk-input uk-form-width-large" id="xp0-link3" name="xp0-link3" type="text" placeholder="Link a red social 3 / portafolio de trabajo">
                        <br><br>
                        <br><br>
                    </div>
            </div>

    </div>
        <script src="{{asset('js/Candidato.js')}}"></script>
        <script src="https://unpkg.com/imask"></script>
        <script src="{{ url('DataPicker/datepicker.js')}}"></script>
        <script>

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {

                        $('#img_perfil').attr('src', e.target.result);
                        $('#img_perfil').attr('class', 'w3-circle');
                        $('#img_perfil').attr('name', 'img_perfil');
                        $('#img_perfil').attr('id','img_perfil');
                        $('#img_perfil').attr('alt', 'Avatar');
                        $('#img_perfil').css('height', '300px');
                        $('#img_perfil').css('witdh', '200px');

                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#profile_picture").change(function() {
                readURL(this);
            });


            Array.from(document.getElementsByClassName('txtSolo')).forEach(function (item){
                new IMask(item, {
                    mask: /^[a-zA-Z_áéíóúñ\s]*$/
                })
            });

            Array.from(document.getElementsByClassName('txtSoloMayus')).forEach( function (item){
                new IMask(item, {
                    mask: /^\w+$/,
                    prepare: function (str) {
                        return str.toUpperCase();
                    },commit: function (value, masked) {
                        masked._value = value.toLowerCase();
                    }
                })
            });

            Array.from(document.getElementsByClassName('txtCel')).forEach(function (item){
                new IMask(item, {
                    mask: /^\d+$/,
                })
            });

            Array.from(document.getElementsByClassName('datepicker')).forEach(function (item){
                new IMask(item, {
                    mask: '00/00/0000'
                });
            });

            /*      //Datapicker
             $(function() {$('.datepicker').datepicker({ format: 'dd/mm/yyyy'});});
             $(function() {
             $('.datepicker').datepicker({
             format: 'dd/mm/yyyy',
             language: 'es-ES',
             trigger:true,
             autoHide:true,
             zIndex: 2048
             });
             });
             */




            ///Actualmente trabaja
            $(function(){
                $('#xp0-check').on('change',function(){
                    if(!$(this).prop('checked')){
                        $('#xp0-lbl_hasta').show();
                        $('#xp0-hastaAnio').show();
                        $('#xp0-hastaMes').show();
                        $('#xp0-separador').show();
                    }else{
                        $('#xp0-lbl_hasta').hide();
                        $('#xp0-hastaAnio').hide();
                        $('#xp0-hastaMes').hide();
                        $('#xp0-separador').hide();
                    }
                });
            });

            // paises,estados,ciudades
            $(document).ready(function(){
               //Pais
                $('#country').change(function(){
                    $('#country option:selected').each(function () {
                        var id_Pais =  $(this).val();
                        $.get("{{url('/api/load_states_web')}}?id=" + id_Pais,
                                function(data){
                                    $('#state').attr('disabled',false);
                                    $('#state').html(data);
                                });
                    });
                });
                //Estado
                $('#state').change(function(){
                    $('#state option:selected').each(function () {
                        var id_Pais =  $(this).val();
                        $.get("{{url('/api/load_cities_web')}}?id=" + id_Pais,
                                function(data){
                                    $('#city').attr('disabled',false);
                                    $('#city').html(data);
                                });
                    });
                });

            });
        </script>
<!-- firebase  -->
<script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
<script> let url = '../../'; </script><script src="{{asset('js/firebase.js')}}"></script>

</div>
</div>
</body>
</html>
<?php  }  ?>
