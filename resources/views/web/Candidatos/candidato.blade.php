<?php
if(!isset($_SESSION['Oomovil']) or $_SESSION['isAdmin'] == true){
  header('Location: /');
  exit;
}else{

Session::put('session',$_SESSION['Oomovil']);
Session::put('privilegios',$_SESSION['privilegios']);
$session =  \Illuminate\Support\Facades\Session::get('session');
$privilegios =  \Illuminate\Support\Facades\Session::get('privilegios');


//OBTENER DATOS DEL USUARIO
$usuario_app = \App\User::where('id',$session)->first();
$usuario_app_info_bancaria = \App\UserBankData::where('id_user',$session)->first();

$email = ['form_params' => ['email' => $usuario_app->correo]];
$correo = ['form_params' =>['correo' => $usuario_app->correo]];
$id_candidato  =  array("id" => isset($_GET['id']) ? $_GET['id'] : 0);
$vacio    =   array();

$service = new \GuzzleHttp\Client(['verify' => false]);


// PREGUNTAS FRECUENTES
$response = $service->post(url('api/listado_preguntas_frecuentes'), $vacio);
$preguntas_frecuentes = json_decode($response->getBody()->getContents());

  // BLOG
$response = $service->post(url('api/blog'), $vacio);
$blog = json_decode($response->getBody()->getContents());

  // ABOUT
$response = $service->post(url('api/about'), $vacio);
$about = json_decode($response->getBody()->getContents());

  // AVISO DE PRIVACIDAD
$response = $service->post(url('api/aviso_privacidad'), $vacio);
$aviso_privacidad = json_decode($response->getBody()->getContents());

  // datos del candidato
$response = $service->post(url('api/datos_candidato'), $vacio);
$Candidato = json_decode($response->getBody()->getContents());


  // lista software e idiomas
$response = $service->post(url('api/load_languages_softwares'), $vacio);
$lista_idioma_software = json_decode($response->getBody()->getContents());

if($Candidato->msg == 'Candidato no encontrado'){
       header('Location: '.route('Web'));
       exit;
}else{

    $personal_data = $Candidato->candidato->personal_data;
    $education_data  = array();

    foreach($Candidato->candidato->education_data  as $education){
      $education_data = $education;
    }

  }

?>

<!DOCTYPE html>
<html>
<title>Pro Hunter</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Pro hunter">
<meta name="Oomovil">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('Img/icn-favicon.png') }}">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="{{asset('css/w3-theme-blue-grey.css')}}">

<link rel="stylesheet" href="{{asset('css/toolbar.css')}}">


<script src="{{asset('jquery-3.2.1.min.js')}}"></script>
<link rel="stylesheet" href=" {{ asset('css/bootstrap.css')}}">
<script src="{{asset('css/Bootstrap/js/bootstrap.min.js')}}"></script>
<!--Alertify-->
<link rel="stylesheet" href="{{asset('alertifyjs/css/alertify.css')}}">
<link rel="stylesheet" href="{{asset('alertifyjs/css/themes/default.css')}}">
<script src="{{asset('alertifyjs/alertify.js')}}"></script>
<!-- uikit -->
<link rel="stylesheet" href="{{asset('uikit/css/uikit.min.css')}}"/>
<script src="{{asset('uikit/js/uikit.min.js')}}"></script>
<script src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('css/w3.css')}}">
<link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">

<style>
  html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
  .uk-button{
    padding: 0px 10px;
  }
  .btn_hover:hover {
      border-radius: 60px 60px 60px 60px;
      -moz-border-radius: 60px 60px 60px 60px;
      -webkit-border-radius: 60px 60px 60px 60px;
      box-shadow: 0 4px 16px rgb(164, 165, 169);,
  transition: all 0.3s ease;
  }
  .btn_hover_sinborde:hover {
      box-shadow: 0 4px 16px rgb(164, 165, 169);,
  transition: all 0.3s ease;
  }


</style>
<body class="w3-light-grey">

    <style>
        ::-webkit-scrollbar {
            display: none;
        }
          .w3-dropdown-content{
            right: 0px;
            margin-right: 10px;
            border-radius: 5px;
            color: #c2c9d1;
          }

          .w3-button:hover{
              background: #41636f !important;
              color: white !important;
              text-decoration:none !important;
          }
      </style>
    <div id="modal-acerca" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          <div class="w3-content w3-display-container" align="center">
              @foreach($about->image_urls as $img)
              <img class="mySlides" src="{{$img}}" style="height: 250px">
              @endforeach
              <img src="{{asset('Img/logo-prohunter-color.png')}}" style="height: 100px;">
              <button class="w3-button w3-white w3-display-left"  onclick="plusDivs(-1)">&#10094;</button>
              <button class="w3-button w3-white w3-display-right" onclick="plusDivs(1)">&#10095;</button>
          </div>
          <br>
          <p>{{$about->text}}</p>

          </p>
      </div>
    </div>

    <div id="modal-aviso" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          {!!html_entity_decode($aviso_privacidad->text)!!}
          </p>
      </div>
    </div>

    <div id="modal-ayuda" uk-modal>
      <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
          <h2 style="text-align: center;">Ayuda</h2>
          <!-- Mis vacantes favorias -->
          <ul style="list-style-type: none;">
                  @foreach($preguntas_frecuentes->data as $preguntas)
                      <li>
                          <h3><b>{{ $preguntas->question}}</b></h3>
                          <p>{{ $preguntas->answer}}</p>
                      </li>
                      @endforeach
          </ul>

      </div>
    </div>
    <!-- Navbar -->
    <div class="w3-top ph-nav">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">

            <a href="{{route('Web')}}" class="w3-bar-item w3-button w3-theme-d4" style="padding: 6px 10px;"><img style="height: 39px;" src="{{ url('Img/logo-prohunter-login.png') }}"/></a>



            <div class="w3-dropdown-hover w3-hide-small Close_Session" style="float: right;" >
                    <button class="w3-button w3-padding-large" title="Cerrar_sesion"><i class="fa fa-sign-out"></i><span id="Cerrar_sesion" name="Cerrar_sesion" class="w3-badge w3-right w3-small w3-green"></span></button>
                    <div>
                        <script type="text/javascript">
                            $(function() {
                                $('.Close_Session').click(function(e) {
                                    alertify.confirm('Cerrar sesión',"¿Quieres cerrar sesión?",
                                            function() {
                                                localStorage.removeItem('prohunter');
                                                localStorage.removeItem('firebase');
                                                window.location.href = '{{url('/api/cerrar_sesion_user')}}'; window.location.href = '{{url('/api/cerrar_sesion_user')}}';
                                            }, function() {/*Cancel*/});
                                });
                            });
                        </script>
                    </div>
                </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="notificaciones"><i class="fa fa-globe"></i><span id="notificaciones" name="notificaciones" class="w3-badge w3-right w3-small w3-green"></span></button>
                    <div id="noticias-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:250px;max-height: 500px;overflow-y: scroll;">
                    </div>
            </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-bell"></i><span id="avisos" name="avisos"></span></button>
                <div id="avisos-firebase" class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:300px;max-height: 500px;overflow-y: scroll;">

                </div>
            </div>

            <div class="w3-dropdown-hover w3-hide-small" style="float: right">
                <button class="w3-button w3-padding-large" title="avisos"><i class="fa fa-user"></i><span id="perfil" name="perfil"  class="w3-badge w3-right w3-small w3-green"></span></button>
                    <div id="perfil-nav" class="w3-dropdown-content w3-card-4 w3-bar-block" >
                        <div style="padding: 10px 0px;">
                                <img src="{{asset('storage/app/triangle.svg')}}" style="position: absolute;top: -16px;height: 21px;left: 45px;">
                                <a href="{{route('perfil') }}" class="w3-bar-item w3-button">Perfil</a>

                                <a href="{{$blog->url}}" class="w3-bar-item w3-button">Blog</a>

                                <a uk-toggle="target: #modal-ayuda" class="w3-bar-item w3-button">Ayuda</a>

                                <a uk-toggle="target: #modal-acerca" class="w3-bar-item w3-button">Acerca de</a>

                                <a uk-toggle="target: #modal-aviso" class="w3-bar-item w3-button">Aviso de privacidad</a>

                        </div>
                    </div>
            </div>
            <div class="w3-dropdown-hover w3-hide-small" style="float: right;" >
                <a href="{{route('Web')}}" class="w3-button w3-padding-large" title="inicio"><i class="fa fa-home"></i></a>

            </div>
        </div>
    </div>
    <!-- Fin Navbar -->
    <script>
      //Galeria de img
      var slideIndex = 1;
      showDivs(slideIndex);

      function plusDivs(n) {
          showDivs(slideIndex += n);
      }

      function showDivs(n) {
          var i;
          var x = document.getElementsByClassName("mySlides");
          if (n > x.length) {slideIndex = 1}
          if (n < 1) {slideIndex = x.length}
          for (i = 0; i < x.length; i++) {
              x[i].style.display = "none";
          }
          x[slideIndex-1].style.display = "block";
      }
      // fin galeria
    </script>


   <div class="w3-content" style="max-width:1400px;margin-top: 50px;">
  <div class="w3-row-padding" style="padding-top: 15px;">

      <!-- Candidato perfil -->
      <div class="w3-container w3-card w3-white w3-margin-bottom">
          <br>
          <h2 style="display: unset;" class="w3-text-grey w3-padding-16"><i class="fa fa-user fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Candidato</h2>

          <div align="container">
              <div class="form-row">
                  <div class="col" align="center">
                      <div class="w3-display-container">
                          <img src="{{ isset($personal_data->profile_picture) ? $personal_data->profile_picture : asset('Img/usuarioxl.png')}}" class="w3-circle" style="max-height: 200px;border: 1px;margin-top: 15px" alt="Avatar">
                      </div>
                  </div>
                  <div class="col" align="left">
                      <button class="btn_hover" onClick="delCandidato({{$_GET['id']}})" id="btn-perfil" style="background-color: transparent; border: 0;float: left; margin-right: 50px"><i class="fa fa-trash" aria-hidden="true" >&nbsp;&nbsp;&nbsp;</i>Eliminar</button>
                      <button class="btn_hover" onClick="top.location.href= '{{ url('/web/editar/candidato').'?id='.$_GET['id'] }}'" id="btn-perfil" style="background-color: transparent; border: 0px;" ><i class="fa fa-pencil" aria-hidden="true" >&nbsp;&nbsp;&nbsp;</i>Editar</button>
                      <br>
                      <br>
                      <p><i class="fa fa-user fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Nombre: </strong> {{ $personal_data->names .' '. $personal_data->paternal_name . ' ' . $personal_data->maternal_name }}</p>
                      <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Correo:</strong> {{ $personal_data->email }}</p>
                      <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Teléfono:</strong> {{ $personal_data->home_phone }}</p>
                      <p><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Celular:</strong> {{ $personal_data->mobile_phone }}</p>
                      </div>
                  <div class="col" align="left">
                      <p><i class="fa fa-genderless fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Genero:</strong> {{ $personal_data->gender }}</p>
                      <p><i class="fa fa-globe fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>Ubicación:</strong> {{ $personal_data->state .','. $personal_data->city}}</p>
                      <p><i class="fa fa-address-card fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>CURP:</strong> {{  $personal_data->curp }}</p>
                      <p><i class="fa fa-address-card fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>RFC:</strong> {{  $personal_data->rfc   }}</p>
                      <p><i class="fa fa-address-card fa-fw w3-margin-right w3-large w3-text-teal"></i><strong>NSS: </strong> {{  $personal_data->nss   }}</p>
                      <p>
                          <i class="fa fa-file-o fa-fw w3-margin-right w3-large w3-text-teal"></i><strong style="margin-right: 20px">Curriculum: </strong>
                          @if(is_null($personal_data->curriculum))
                              <label class="badge badge-pill badge-light">Sin agregar</label>
                          @else
                              <a class="btn btn-outline-info" href="{{$personal_data->curriculum}}">{{ is_null($personal_data->curriculum)  ? ' Sin agregar' : ' Descargar'}}</a>
                          @endif
                      </p>
                      </div>

              </div>
          </div>


          </div>
      </div>

     <!-- Experiencia laboral -->
    <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 100%;float: left;margin: 5px">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Experiencia laboral</h2>
        <div class="w3-container">
          @foreach($Candidato->candidato->work_experience  as $work_experience)
          <h5 class="w3-opacity"><b>{{$work_experience->position . ' - ' . $work_experience->company_name}}</b></h5>
          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>{{$work_experience->start_year}} - <span class="w3-tag w3-teal w3-round">{{$work_experience->end_year == 0 ? 'Actualmente': $work_experience->end_year}}</span></h6>
          <p>{{ $work_experience->activities }}</p>
          <hr>
          @endforeach
        </div>
      </div>

     <!-- Educación -->
    <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 100%;float: left;margin: 5px">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-graduation-cap fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Educación</h2>
        <div class="w3-container">
          @foreach($Candidato->candidato->education_data  as $education)
            <h5 class="w3-opacity"><b>{{$education->degree_obtained}}</b></h5>
            <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>{{$education->start_year .' - '. $education->end_year}}</h6>
            <p>{{$education->universidad}}</p>
            <hr>
            @endforeach
        </div>
      </div>

     <!-- Software -->
    <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 100%;float: left;margin: 5px" >
    <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-desktop fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Software</h2>
    <div class="w3-container">
      @foreach($Candidato->candidato->softwares as $softwares)
        <h5 class="w3-opacity"><b>{{$softwares->name}}</b></h5>
        <div class="w3-light-grey w3-round-xlarge w3-small">
          <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:{{$softwares->percentage_known}}%">{{$softwares->percentage_known}}%</div>
        </div>
        <h6>Certificación : {{ $softwares->certification_type}}</h6>
        <hr>
      @endforeach
    </div>
  </div>

     <!-- Idiomas -->
    <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 100%;float: left;margin: 5px">
            <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-globe fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Idiomas</h2>
            <div class="w3-container">
                @foreach($Candidato->candidato->languages  as $lenguajes)
                    <h5 class="w3-opacity"><b>{{$lenguajes->name}}</b></h5>
                    <div class="w3-light-grey w3-round-xlarge w3-small">
                        <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:{{$lenguajes->percentage_known}}%">{{$lenguajes->percentage_known}}%</div>
                    </div>
                    <h6>Certificación : {{ $lenguajes->certification_type}}</h6>
                    <hr>
                @endforeach
            </div>
        </div>
       <div style="clear: both"></div>
        <br>
        <br>
        <br>
</div>


<script>
//Eliminar candidato
async function delCandidato(id){
    //eliminar_candidato
    let response = await fetch('../api/eliminar_candidato',{
        method: 'POST', // or 'PUT'
        body: `{"candidate_id": ${id}}`, // data can be `string` or {object}!
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(res=>res.json());
    console.log(response);
    if(!!response.exito){
        window.location = `${window.location.origin}${window.location.pathname}/..`;
    }
}

</script>
     <!-- firebase  -->
     <script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
     <script> let url = '../'; </script><script src="{{asset('js/firebase.js')}}"></script>

  </body>
</html>
<?php  }  ?>
