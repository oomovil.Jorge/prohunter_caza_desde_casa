<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ProHunter">
    <meta name="Oomovil">
    <meta name="google-signin-client_id"
          content="134592454019-57nk01oa26md7a4nk4hre0a9io7ihpi0.apps.googleusercontent.com">

    <title>Pro Hunter</title>
    <link rel="shortcut icon" href="{{ asset('/Img/icn-favicon.png') }}">
    <link href="{{ url('/css/login.css') }}" rel="stylesheet">
    <link href="{{ url('/css/Bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{asset('/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('/css/Bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/bodymovin/js/bodymovin.min.js')}}" type="text/javascript"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.0/firebase.js"></script>
    <script src="{{asset('/js/social.js')}}"></script>
    <script>
        /* if (window.location.protocol != "https:") {
            // window.location.href='https://'+window.location.host+window.location.pathname;
         }*/
        if (!!localStorage.getItem('prohunter')) {
            window.location.href = window.location.href + "web";
        }

        if (!!localStorage.getItem('admprohunter')) {
            window.location.href = window.location.href + "admin";
        }

        localStorage.clear();
    </script>
    <link rel="stylesheet" href="{{asset('/font-awesome-4.7.0/css/font-awesome.css')}}">

    <style>
        .fa {
            padding: 20px;
            font-size: 30px;
            width: 70px;
            text-align: center;
            text-decoration: none;
        !important;
            margin: 5px 2px;
            border-radius: 50%;
            cursor: pointer;
        }

        .fa:hover {
            box-shadow: inset 0px 0px 13px #20335a;
        }

        .fa-facebook {
            background: #3B5998;
            color: white;

        }

        .fa-google {
            background: #dd4b39;
            color: white;
        }
    </style>

</head>
<body class="FondoProhunter-usuario">
<form id="login" method="POST" action="{{url('/api/login_web')}}">
    <div id="lotiFB"
         style="z-index: 1; position: relative; display: none; height: 539px; background-color: rgba(0,0,0,.85);"></div>
    <div id="lotiG"
         style="z-index: 1; position: relative; display: none; height: 539px; background-color: rgba(0,0,0,.85);"></div>
    <div class="centrar" align="Center" style="margin-top: -200px;">
        <img src="{{url('/Img/logo-prohunter-login.png')}}"/>
        <br>
        <br>
        <label class="LabeLogin">Inicio de sesión usuarios</label>
        <br>
        <input class="BordesRedondosInput" style="width: 100%" type="text" name="correo" placeholder="Correo"/>
        <br>
        <br>
        <input class="BordesRedondosInput" style="width: 100%" type="Password" name="contrasena"
               placeholder="Contraseña"/>
        <br>
        <br>
        <input type="submit" class="Boton_Login" style="width: 100%;cursor: pointer;letter-spacing: 0.1em; "
               value="INICIAR SESIÓN"/>
        <br>

        @if(isset($_GET['Error']) && isset($_GET['Msg']))
            <br>
            <br>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{$_GET['Msg'] }}</strong>
            </div>
        @endif
        <br>
        <label style="color: white;">¿Aún no tienes una cuenta?</label>
        <a style="text-decoration: underline;color: white" href="{{url('/registro')}}"> Regístrate aquí</a>
        <br>
        <a style="text-decoration: underline;color: white" href="{{url('/recuperar')}}">¿Olvidaste tu contraseña?</a>
        <br>
        <a style="text-decoration: underline;color: white" href="{{url('/admin')}}">¿Eres administrador?</a>
        <br>

        <a class="fa fa-facebook" id="fb_btn" style="color: white" onclick="iniciofb()"></a>
        <a class="fa fa-google" id="google_btn" style="color: white" onclick="iniciog()"></a>
        <br>
        <br>
    </div>
</form>

<script>
    let lottieAnimationFB = bodymovin.loadAnimation({
        container: lotiFB, // ID del div
        path: '{{asset("/bodymovin/json/fbicon.json")}}', // Ruta fichero .json de la animación
        renderer: 'svg', // Requerido
        loop: true, // Opcional
        autoplay: true, // Opcional
        name: "Hello World", // Opcional
    });
    let lottieAnimationG = bodymovin.loadAnimation({
        container: lotiG, // ID del div
        path: '{{asset("/bodymovin/json/loading.json")}}', // Ruta fichero .json de la animación
        renderer: 'svg', // Requerido
        loop: true, // Opcional
        autoplay: true, // Opcional
        name: "Hello World" // Opcional
    });

</script>
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
<script>
    //  history.pushState(null, "", "/login");
</script>

</body>
</html>
