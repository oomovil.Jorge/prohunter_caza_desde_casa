//Hace visible la primera cartas





// Elimina los nodos

function Elimina_nodo(nodo_) {
    
    let nodo = nodo_[0];
    nodo.parentNode.removeChild(nodo);
  
}

function VisibleEdu(){
    Clonar_Edu();
}
//Educación
function Clonar_Edu() {
    var original = document.getElementById('edu_');
    var clone = original.cloneNode(true);
    
    //clone.onclick = Clonar_Edu;
    document.getElementById('Edu-nodos').appendChild(clone);

    clone.querySelector('#Edu-universidad').setAttribute("required", "true");
    clone.querySelector('#Edu-universidad').value = "";
    clone.querySelector('#Edu-desdeMes').setAttribute("required", "true");
    clone.querySelector('#Edu-desdeAnio').setAttribute("required", "true");
    clone.querySelector('#Edu-desdeAnio').setAttribute("required", "true");
    clone.querySelector('#Edu-hastaMes').setAttribute("required", "true");
    clone.querySelector('#Edu-hastaMes').setAttribute("required", "true");
    clone.querySelector('#Edu-hastaAnio').setAttribute("required", "true");

    clone.querySelector('#Edu-universidad').focus();

}

function VisibleIdi(){
    Clonar_Idi();
}

//Idioma
function Clonar_Idi() {
    var original = document.getElementById('idi_');
    var clone = original.cloneNode(true);
    
    clone.onclick = rangeSlider;
    document.getElementById('Idi-nodos').appendChild(clone);
    rangeSlider();

    clone.querySelector('#Idi-idioma').setAttribute("required","true");
    clone.querySelector('#Idi-idioma_porcentaje').setAttribute("required","true");

    clone.querySelector('#Idi-idioma').focus();
}


function VisibleSoft(){

    Clonar_Soft();
}

// software
function Clonar_Soft() {
    var original = document.getElementById('soft_');
    var clone = original.cloneNode(true);
    
    clone.onclick = rangeSlider;
    document.getElementById('soft-nodos').appendChild(clone);
    rangeSlider();

    clone.querySelector('#soft0-software').setAttribute("required","true");
    clone.querySelector('#soft0-software_porcentaje').setAttribute("required","true");

    clone.querySelector('#soft0-software').focus();

}



function Visiblexp(){

    Clonar_xp();

}

// Experiencia laboral
function Clonar_xp() {
    var original = document.getElementById('xp_');
    var clone = original.cloneNode(true);
    
    document.getElementById('xp-nodos').appendChild(clone);

    clone.querySelector('#xp0-puesto').setAttribute("required","true");
    clone.querySelector('#xp0-Nombre_empresa').setAttribute("required","true");
    clone.querySelector('#xp0-giro').setAttribute("required","true");

    clone.querySelector('#xp0-desdeMes').setAttribute("required","true");
    clone.querySelector('#xp0-desdeAnio').setAttribute("required","true");

    clone.querySelector('#xp0-hastaMes').setAttribute("required","true");
    clone.querySelector('#xp0-hastaAnio').setAttribute("required","true");

 clone.querySelector('#xp0-check').addEventListener('change',function(){
     if(!$(this).prop('checked')){
         clone.querySelector('#xp0-lbl_hasta').style.display =  "";
         clone.querySelector('#xp0-hastaAnio').style.display =  "";
         clone.querySelector('#xp0-hastaMes').style.display =   "";
         clone.querySelector('#xp0-separador').style.display =  "";
           }else{
         clone.querySelector('#xp0-lbl_hasta').style.display = "none";
         clone.querySelector('#xp0-hastaAnio').style.display = "none";
         clone.querySelector('#xp0-hastaMes').style.display =  "none";
         clone.querySelector('#xp0-separador').style.display = "none";
     }
 });
    clone.querySelector('#xp0-puesto').focus();
}

function RecorreInformacionPersonal(){
    let a_birth_date = document.getElementById('birthday_date').value.split('/');
    let birth_date = a_birth_date[2]+'-'+a_birth_date[1]+'-'+a_birth_date[0];

    var objeto = {
        "names"          : document.getElementById('names').value,
        "maternal_name"  : document.getElementById('maternal_name').value,
        "paternal_name"  : document.getElementById('paternal_name').value,
        "country"        : document.getElementById('country').options[document.getElementById('country').selectedIndex].text,
        "state"          : document.getElementById('state').options[document.getElementById('state').selectedIndex].text,
        "city"           : document.getElementById('city').options[document.getElementById('city').selectedIndex].text,
        "district"       : document.getElementById('district').value,
        "street"         : document.getElementById('street').value,
        "house_number"   : document.getElementById('house_number').value,
        "home_phone"     : document.getElementById('home_phone').value,
        "mobile_phone"   : document.getElementById('mobile_phone').value,
        "email"          : document.getElementById('email').value,
        "gender"         : document.getElementById('gender').value,
        "curp"           : document.getElementById('curp').value,
        "rfc"            : document.getElementById('rfc').value,
        "birthday_date"     : birth_date,
        "nss"            : document.getElementById('nss').value
    };
    console.log(objeto);
    return JSON.stringify(objeto);
}

function RecorreEducacion(){
    var nodos = document.getElementById('Edu-nodos');
    var datos  = [];
    var objeto = {};

    for(let a = 0; a < nodos.childElementCount ; a++){
        let elemento =  nodos.children[a];

        if(elemento.children['Edu00'].children['Edu-universidad'].value == ""){
           datos  = [];
        }else {
            datos.push({
                "universidad": elemento.children['Edu00'].children['Edu-universidad'].value,
                "start_month": elemento.children['Edu00'].children['Edu-desdeMes'].value,
                "start_year": elemento.children['Edu00'].children['Edu-desdeAnio'].value,
                "end_month": elemento.children['Edu00'].children['Edu-hastaMes'].value,
                "end_year": elemento.children['Edu00'].children['Edu-hastaAnio'].value,
                "academic_level": elemento.children['Edu00'].children['Edu-titulo'].value,
                "degree_obtained": elemento.children['Edu00'].children['Edu-grado'].value,
                "additional_info": elemento.children['Edu00'].children['Edu-info_adi'].value
            });
        }
    }
    objeto = datos;
    return JSON.stringify(objeto);
    //  console.log(JSON.stringify(objeto));
}

function RecorreIdioma(){
    var nodo = document.getElementById('Idi-nodos');
    var datos  = [];
    var objeto = {};
    for(let a = 0; a < nodo.childElementCount ; a++){
        let elemento =  nodo.children[a];
        let subelemento =  elemento.children['Idi00'];

        if(subelemento.children['Idi-idioma'].value == ""){
            datos  = [];
        }else {
            datos.push({
                "language_id": subelemento.children['Idi-idioma'].value,
                "percentage_known": subelemento.children['range'].children['Idi-idioma_porcentaje'].value,
                "certification_type": subelemento.children['Idi-idioma_certificacion'].value
            });
        }
    }
    objeto = datos;
    return JSON.stringify(objeto);
    //  console.log(JSON.stringify(objeto));
}

function RecorreSoftware(){
    var nodo = document.getElementById('soft-nodos');
    var datos  = [];
    var objeto = {};

    for(let a = 0; a < nodo.childElementCount ; a++){
        let elemento =  nodo.children[a];
        let subelemento =  elemento.children['soft00'];
        if(subelemento.children['soft0-software'].value == ""){
            datos  = [];
        }else {
            datos.push({
                "software_id": subelemento.children['soft0-software'].value,
                "percentage_known": subelemento.children['range'].children['soft0-software_porcentaje'].value,
                "certification_type": subelemento.children['soft0-software_certificacion'].value
            });
        }
    }
    objeto = datos;
    return JSON.stringify(objeto);
}

function RecorreExperiencia(){
    var nodo = document.getElementById('xp-nodos');
    var datos  = [];
    var objeto = {};

    for(let a = 0; a < nodo.childElementCount ; a++){
        let elemento =  nodo.children[a];
        let subelemento =  elemento.children['xp00'];

        if(subelemento.children['xp0-puesto'].value == ""){
           datos  = [];
        }else{
            datos.push({
                "position": subelemento.children['xp0-puesto'].value,
                "active": subelemento.children['lbl_check'].children['xp0-check'].value,
                "company_name": subelemento.children['xp0-Nombre_empresa'].value,
                "role": subelemento.children['xp0-giro'].value,
                "activities": subelemento.children['xp0-actividades'].value,
                "start_month": subelemento.children['xp0-desdeMes'].value,
                "start_year": subelemento.children['xp0-desdeAnio'].value,
                "end_month": subelemento.children['xp0-hastaMes'].value,
                "end_year": subelemento.children['xp0-hastaAnio'].value,
                "work_portfolio_url_1" : subelemento.children['xp0-link1'].value,
                "work_portfolio_url_2" : subelemento.children['xp0-link1'].value,
                "work_portfolio_url_3" : subelemento.children['xp0-link1'].value
            });
        }
    }
    objeto = datos;
    return JSON.stringify(objeto);
}

function formularioCandidato(){

    var personal     =  RecorreInformacionPersonal();
    var educacion    =  RecorreEducacion();
    var idioma       =  RecorreIdioma();
    var software     =  RecorreSoftware();
    var experiencia  =  RecorreExperiencia();

    let id_candidato = "";
    try{
        id_candidato = document.getElementById('id_candidato').value;
        url = '../../api/agregar_editar_candidato';
    }
    catch(e){
        console.error();
    }

    var picture = document.getElementById('profile_picture').files[0];
    if (picture == null){
        picture = null;
    }
    var curriculum = document.getElementById('curriculum').files[0];
    if (curriculum == null){
        curriculum = null;
    }

    let form_data = new FormData();
    form_data.append("email",localStorage.getItem('prohunter'));
    form_data.append("candidate_id",id_candidato);
    form_data.append("seeking_work",Number(document.getElementById('seeking_work').checked));
    form_data.append("profile_picture",picture);
    form_data.append("curriculum",curriculum);
    form_data.append("personal_data",personal);
    form_data.append("education_data",educacion);
    form_data.append("languages",idioma);
    form_data.append("software",software);
    form_data.append("experience",experiencia);

    var urlService= "";
    if(id_candidato == ""){
        urlService = url+'api/agregar_editar_candidato ';
    }else{
        urlService = url;
    }


    fetch (urlService,{
        method: 'POST', // or 'PUT'
        body: form_data,
    })
    .then(function(res){
        return res.json();
    })
    .then(function(response){
        if(!!response.exito){
            if(!id_candidato){
                window.location = window.location.origin+window.location.pathname+'/../../candidato?id='+response.id;
            }else{
                window.location = window.location.origin+window.location.pathname+'/../../candidato?id='+response.id;
            }
        }
    });



  //  Aqui enviar

    return false;
}

//Range slider
var rangeSlider = function(){
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');

    slider.each(function(){

        value.each(function(){
            var value = $(this).prev().attr('value');
            $(this).html(value);
        });

        range.on('input', function(){
            $(this).next(value).html(this.value);
        });
    });
};rangeSlider();

function delCandidato(id){
    //eliminar_candidato
    fetch('../api/eliminar_candidato',{
        method: 'POST', // or 'PUT'
        body: '{"candidate_id": '+id+'}', // data can be `string` or {object}!
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(res=>res.json())
    .then(function(response){
        console.log(response);
        if(!!response.exito){
            window.location = window.location.origin+window.location.pathname+'/..';
        }
    });
}