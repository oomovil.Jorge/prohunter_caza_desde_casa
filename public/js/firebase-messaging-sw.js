// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
var config = {
    messagingSenderId: "134592454019"
  };

firebase.initializeApp(config);


// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler( (payload) => {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    console.log(payload);
    var notificationTitle = 'Background Message Title';
    var notificationOptions = {
      body: payload.data.status,
      icon: payload.notification.icon
    };
  
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
  });