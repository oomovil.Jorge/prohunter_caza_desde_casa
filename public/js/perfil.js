document.getElementById('profile_picture').addEventListener('change',function(event){
    if(document.getElementById('profile_picture').files[0]){
        UIkit.modal.confirm('Esta seguro que quieres cambiar la foto').then(
        function () {
            console.log('Confirmed.');
            let form_data = new FormData();
            form_data.append("correo",window.localStorage.getItem('prohunter'));
            form_data.append("imagen_perfil",document.getElementById('profile_picture').files[0]);
            fetch('../api/actualizar_imagen_perfil',{
                method: 'POST', // or 'PUT'
                body: form_data, 
                
            })
            .then(function(res){return res.json()})
            .then(function(response){
                if(!!response.exito){
                    window.location.reload();
                }
            })
        }, 
        function () {
            console.log('Rejected.');
        });
    }
});

document.getElementById('btn-perfil').addEventListener('click',function(event){
    $('#label-perfil').hide();
    $('#form-perfil').show();
});



let formPerfil = document.querySelector('#form-perfil');

formPerfil.addEventListener('submit', function(event){
    
    event.preventDefault();
    let formData = new FormData(formPerfil);
    formData.append('correo', localStorage.getItem('prohunter'));
    var object = {};
    formData.forEach(function(value, key){
        object[key] = value;
    });
    var json = JSON.stringify(object);
    fetch('../api/actualizar_perfil',{
        method: 'POST', // or 'PUT'
        body: json, 
        headers:{
            'Content-Type': 'application/json'
        }
        })
        .then(function(res){ return res.json()})
        .then(function(response){
            console.log(response);
            UIkit.notification({message: response.msg, status: !!response.exito?'success':'danger'});
            if(!!response.exito){
                location.reload();
            }
        });
        })

document.getElementById('btn-cuenta').addEventListener('click',function(event){
    $('#label-cuenta').hide();
    $('#form-cuenta').show();
})
let formCuenta = document.querySelector('#form-cuenta');

formCuenta.addEventListener('submit', function(event){
    
    event.preventDefault();
    let formData = new FormData(formCuenta);
    formData.append('correo', localStorage.getItem('prohunter'));
    var object = {};
    formData.forEach(function(value, key){
        object[key] = value;
    });
    var json = JSON.stringify(object);
   
    fetch('../api/agregar_modificar_datos_bancarios',{
        method: 'POST', // or 'PUT'
        body: json, 
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(function(res){return res.json()})
    .then(function(response){
        console.log(response);
        UIkit.notification({message: response.msg, status: !!response.exito?'success':'danger'})
        if(!!response.exito){
            location.reload();
        }
    });
    
})
let formCategoria = document.querySelector('#form-cat');

formCategoria.addEventListener('submit', function(event){
    
    event.preventDefault();
    let formData = new FormData(formCategoria);
    var object = {
        email:localStorage.getItem('prohunter'),
        category_ids:''
    };

    let category_ids = {
        category_ids:[]
    }
    formData.forEach(function(value, key){
        category_ids.category_ids.push(Number(value));
        console.log(value);
    });
    object.category_ids = JSON.stringify(category_ids);
    var json = JSON.stringify(object);
    console.log(object);
    console.log(json);
    fetch('../api/seleccionar_preferencias',{
        method: 'POST', // or 'PUT'
        body: json, 
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(function(res){return res.json()})
    .then(function(response){
        console.log(response);
        UIkit.notification({message: response.msg, status: !!response.exito?'success':'danger'})
        if(!!response.exito){
            location.reload();
        }
    });
    
})

