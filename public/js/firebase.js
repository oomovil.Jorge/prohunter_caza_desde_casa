// Initialize Firebase
var config = {
  apiKey: "AIzaSyBLd_huWYpW5Pita3-9vAx3ukaJGxg1_tE",
  authDomain: "prohunter-42f92.firebaseapp.com",
  databaseURL: "https://prohunter-42f92.firebaseio.com",
  projectId: "prohunter-42f92",
  storageBucket: "prohunter-42f92.appspot.com",
  messagingSenderId: "134592454019"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

var flagReadAnnouncements = false;

messaging.requestPermission()
.then( function() {
 // console.log('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
  return messaging.getToken();
})
.then(function(token){
  //  console.log(token);
    if(token!==localStorage.getItem('firebase')){
      localStorage.setItem('firebase',token);
      fetch('./api/agregar_token_firebase',{
        method: 'POST', // or 'PUT'
        body: '{"email": "'+localStorage.getItem('prohunter')+'", "token": "'+token+'"}',
          headers:{
              'Content-Type': 'application/json'
          }
      })
      .then(function(res){ return res.json();})
      .then(function(response){
         // console.log(response);
      })
    }
})
.catch(function(err){
 // console.log('Unable to get permission to notify.', err);
});


messaging.onMessage(function(payload){
//  console.log('Message received. ', payload);

/*  body: "3"
    icon: "http://porvenirapps.com/prohunter/storage/app/"
    title: "test"
  */
    var img ;
    switch (payload.notification.icon) {
        case "1":
             img = 'public/Img/Desgrane/mipmap/icn_cv_enviado.png';
            break;
        case "2":
             img = 'public/Img/Desgrane/mipmap/icn_cv_visto.png';
            break;
        case "3":
             img  = 'public/Img/Desgrane/mipmap/icn_entrevista.png';
            break;
        case "4":
             img = 'public/Img/Desgrane/mipmap/icn_psicometrico.png';
            break;
        case "5":
             img  = 'public/Img/Desgrane/mipmap/icn_contratado.png';
            break;
        case "6":
             img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
            break;
        case "7":
             img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
            break;
        case "8":
             img = 'public/Img/Desgrane/mipmap/icn_info_menu_b.png';
            break;

        default: img = payload.notification.icon||'public/Img/Desgrane/mipmap/icn_contratado.png';
    }
  UIkit.notification({message: '<div style="width: 300px">    <img src="'+img+'" style="float: left;width: 10%;">    <label style="float: right; margin-right: 30px;width: 70%;">'+payload.notification.title+'</label><div class="clear-botton"></div></div>', pos: 'bottom-right'});
  try{
    reloadForNotificacion();
  } catch(e){
   // console.log(e);
  }
  // ...
});


function reloadForNotificacion(){

  fetch(url+'api/listar_avisos_usuarios',{
    method: 'POST', // or 'PUT'
    body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
    headers:{
      'Content-Type': 'application/json'
    }
  })
  .then(function(res){return res.json();})
  .then(function(response){
 //   console.log('listar_avisos_usuarios',response.data);
    let data = response.data.sort(function(a, b) { return parseFloat(b.id) - parseFloat(a.id)});
    flagReadAnnouncements = !response.count;
    if(response.count>0){
        document.getElementById('avisos').parentNode.querySelector('i').style.color ="#2E3D56";
    }
    //  console.log('listar_avisos_usuarios',data);
      document.getElementById('avisos-firebase').innerHTML = "";
      let _div_ = document.createElement('div');
      _div_.setAttribute('style','padding: 10px 0px;overflow-y: scroll;width: 300px;max-height: 500px;');
      if(data.length>0){
        let flecha = document.createElement('img');
        flecha.src = url+"storage/app/public/triangle.svg";
        flecha.setAttribute('style','position: absolute;top: -16px;height: 21px;right: 85px;')
        document.getElementById('avisos-firebase').appendChild(flecha);
        document.getElementById('avisos-firebase').appendChild(_div_);
      }
      data.forEach(function(item)  {
       var _img ;

        switch (item.icon) {
            case "1":
                _img = 'public/Img/Desgrane/mipmap/icn_cv_enviado.png';
                break;
            case "2":
                _img = 'public/Img/Desgrane/mipmap/icn_cv_visto.png';
                break;
            case "3":
                _img  = 'public/Img/Desgrane/mipmap/icn_entrevista.png';
                break;
            case "4":
                _img = 'public/Img/Desgrane/mipmap/icn_psicometrico.png';
                break;
            case "5":
                _img  = 'public/Img/Desgrane/mipmap/icn_contratado.png';
                break;
            case "6":
                _img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
                break;
            case "7":
                _img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
                break;
            case "8":
                _img = 'public/Img/Desgrane/mipmap/icn_info_menu_b.png';
                break;

            default: _img = item.icon||'public/Img/Desgrane/mipmap/icn_contratado.png';
        }
        let a = document.createElement('a');
        a.setAttribute('href',"#");
        a.setAttribute('class',"w3-bar-item w3-button");
        let img = document.createElement('img');
        img.setAttribute('src',url+_img);
        img.setAttribute('style','margin-right: 10px;height: 30px;');
        a.appendChild(img);
        a.appendChild(document.createTextNode(item.title+' - '+item.body));
        _div_.appendChild(a);
        _div_.appendChild(document.createElement('hr'));
      })
  });

   fetch(url+'api/listado_notificacion',{
    method: 'POST', // or 'PUT'
    body: '{"email": '+localStorage.getItem('prohunter')+'}',
    headers:{
      'Content-Type': 'application/json'
    }
  })
  .then(function(res){return res.json();})
  .then(function(response){
    let data = response.data.sort(function(a, b) {return parseFloat(b.id) - parseFloat(a.id)});
   //   console.log('listar_avisos_usuarios',data);
      document.getElementById('noticias-firebase').innerHTML = "";

      let _div_ = document.createElement('div');
      _div_.setAttribute('style','padding: 10px 0px;overflow-y: scroll;width: 300px;max-height: 500px;');
      if(data.length>0){
        let flecha = document.createElement('img');
        flecha.src = url+"storage/app/public/triangle.svg";
        flecha.setAttribute('style','position: absolute;top: -16px;height: 21px;right: 45px;')
        document.getElementById('noticias-firebase').appendChild(flecha);
        document.getElementById('noticias-firebase').appendChild(_div_);
      }

      data.forEach(function(item) {
        let a = document.createElement('a');
        a.setAttribute('href',"#");
        a.addEventListener('click',function(event){noti_modal(item.imagen,item.titulo,item.contenido)});
        a.setAttribute('class',"w3-bar-item w3-button");
        let img = document.createElement('img');
        img.setAttribute("src",item.imagen);
        img.setAttribute("style","float: left;width: 30%;");
        a.appendChild(img);
        let span = document.createElement('span');
        span.appendChild(document.createTextNode(item.titulo+' - '+item.contenido));
        span.setAttribute("style","float: right;");
        a.appendChild(span);
        _div_.appendChild(a);
        _div_.appendChild(document.createElement('hr'));
      })
  });

fetch(url+'api/listado_postulados',{
    method: 'POST', // or 'PUT'
    body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
    headers:{
      'Content-Type': 'application/json'
    }
  })
   .then(function(res){ return res.json();})
   .then(function(response){
    //  console.log('listado_postulados',response);



   try {
    formBuscarPostulado(response.candidates);
   }catch(e){

    }

  });
}
reloadForNotificacion();

function noti_modal(img,titule,text){

UIkit.modal.dialog('<div style="text-align: center; padding-bottom: 20px;">'+
              '<button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>'+
              '<div class="w3-content w3-display-container" align="center">'+
              '<img src="'+img+'" style="height: 100px;">'+
              '</div>'+
              '<br>'+
              '<h2>'+titule+'</h2>'+
              '<p>'+text+'</p>'+
          '</p>'+
      '</div>');
}

window.onload = function () {
    document.getElementById('avisos').parentNode.addEventListener("mouseover",event=>{
        if(!flagReadAnnouncements){
            flagReadAnnouncements= true;
            fetch(url+'api/marcar_avisos_usuarios',{
                method: 'POST', // or 'PUT'
                body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
                headers:{
                'Content-Type': 'application/json'
                }
            })
            .then(function(res){return res.json();})
            .then(function(response){
                flagReadAnnouncements= !!response.exito;
                document.getElementById('avisos').parentNode.querySelector('i').style.color ="unset"
            });
        }
    });
}
