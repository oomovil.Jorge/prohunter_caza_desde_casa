var vacantes_q="";
var candidato_q="";
var postulado_q="";
var postulado_qt="";
var vacantes_lis=[];

var li_etapas = document.getElementsByClassName("li_etapas");


Array.from(li_etapas).forEach( function(element) {
        element.addEventListener('click', function(event){
            let categori = this.getElementsByTagName("label")[0].innerHTML;
            //console.log(categori);
            if(categori == "Todos"){
                postulado_qt= "";
            }
            else{
                postulado_qt=categori;
            }

            fetch('api/listado_postulados',{
                method: 'POST', // or 'PUT'
                body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
                headers:{
                  'Content-Type': 'application/json'
                }
              })
               .then(function(res){ return res.json();})
               .then(function(response){
                  //console.log('listado_postulados',response);



                try {
                    formBuscarPostulado(response.candidates);
                }catch(e){
                }

              });
        });
});

document.getElementById('all_vacantes').addEventListener('click',function(event){
    //console.log(vacantes_q);
    formBuscarFetch(vacantes_q);
});
document.getElementById('mis_favoritas').addEventListener('click',function(event){
    inflar_mis_vacantes();
});
function inflar_mis_vacantes(){
    fetch('api/listado_vacantes_favoritas',{
        method: 'POST', // or 'PUT'
        body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(function(res){return res.json()})
    .then(function(vacantes_favoritas){

        if(vacantes_favoritas.data.length==0){
            UIkit.notification({message: 'No tienes ninguna vacante en favoritos', status: 'warning'});
        }
        inflar_vacantes(vacantes_favoritas.data);
    });
}
function refres(){
    var fav = document.getElementsByClassName("fav");

    Array.from(fav).forEach( function(element) {
        element.addEventListener('click', function(event) {
            actFav(event.path[0]);
        });
    });

    var postl_vacnt = document.getElementsByClassName("postular");


    Array.from(postl_vacnt).forEach( function(element) {
            element.addEventListener('click', function(event){return postlVacnt(event.path[0])});
    });

    var del_fav = document.getElementsByClassName("del-fav");


    Array.from(del_fav).forEach( function(element) {
            element.addEventListener('click', function(event){return delFav(event.path[0].dataset)});
    });


}
refres();
var formBuscar = document.querySelector('#buscar');

formBuscar.addEventListener('submit',  function(event){
    event.preventDefault();
    //console.log(formBuscar.q.value);
    vacantes_q = formBuscar.q.value;
    formBuscarFetch(vacantes_q);
    window.location = window.location.origin+window.location.pathname+"#vacantes_div";
})
var formBuscar_m = document.querySelector('#buscar_m');

formBuscar_m.addEventListener('submit',  function(event){
    event.preventDefault();
    //console.log(formBuscar_m.q.value);
    vacantes_q = formBuscar_m.q.value;
    formBuscarFetch(vacantes_q);
    window.location = window.location.origin+window.location.pathname+"#vacantes_div"
})

var mis_candidatos_form = document.querySelector('#candidatos_');

mis_candidatos_form.addEventListener('submit',  function(event){
    event.preventDefault();
    //console.log(mis_candidatos_form.q.value);

    candidato_q = mis_candidatos_form.q.value;
    formBuscarCandidato(candidato_q);
})
var mis_postulados_form = document.querySelector('#postulados_');

function formBuscarCandidato(q){
    if(!q){
        inflar_candidatos(candidatos_arr);
    } else {
        let q_candidatos_arr = [];
        for(let a = 0; a< candidatos_arr.length; a++){
            let nombre = candidatos_arr[a].personal_data.names + " " +candidatos_arr[a].personal_data.paternal_name + " " +candidatos_arr[a].personal_data.maternal_name;
            let ubicacion = candidatos_arr[a].personal_data.city + " " +candidatos_arr[a].personal_data.state;
            let titulo_obj = candidatos_arr[a].education_data.length>0? candidatos_arr[a].education_data[0]:{'degree_obtained':'Sin titulo.'};
            for(let b = 0; b<candidatos_arr[a].education_data.length;b++){
                if(titulo_obj.end_year<candidatos_arr[a].education_data.end_year){
                    titulo_obj = candidatos_arr[a].education_data;
                }
            }
            let titulo = titulo_obj.degree_obtained;


            if( nombre.toLowerCase().indexOf(q.toLowerCase())>-1||
                titulo.toLowerCase().indexOf(q.toLowerCase())>-1||
                ubicacion.toLowerCase().indexOf(q.toLowerCase())>-1
            ){
                q_candidatos_arr.push(candidatos_arr[a]);
            }
        }


        inflar_candidatos(q_candidatos_arr);
    }
}
function inflar_candidatos(data){

    document.getElementById('mis_candidatos_list').innerHTML = "";
    let pristino = 1;

    data.forEach(function(item){

        let datos_candiato = item.personal_data;
        let nombre = item.personal_data.names + " " +item.personal_data.paternal_name + " " +item.personal_data.maternal_name;
        let titulo_obj = item.education_data.length>0? item.education_data[0]:{'degree_obtained':'Sin titulo.'};
            for(let b = 0; b<item.education_data.length;b++){
                if(titulo_obj.end_year<item.education_data.end_year){
                    titulo_obj = item.education_data;
                }
            }
            pristino=0;
        var img_perfil = (datos_candiato.profile_picture != null) ? datos_candiato.profile_picture : "public/Img/notpictureuser.png";
        document.getElementById('mis_candidatos_list').innerHTML +=
                    '<div class="aCandidato">'+

                            '<div class="w3-circle uk-cover-container uk-height-medium foto">'+
                            '<img uk-cover class="" src="'+img_perfil+'" alt="">'+
                            '</div>'+
                            '<div class="info">'+
                            '<label>'+nombre+'</label><br>'+
                            '<label class="span_green">'+titulo_obj.degree_obtained+'</label><br>'+
                            '<label style="font-size: 10px "><i class="fa fa-map-marker span_green" aria-hidden="true"></i>&nbsp;'+datos_candiato.city +', '+datos_candiato.state+'</label>'+
                            '</div>'+
                            '<div class="boton">'+
                            '<a href="web/candidato?id='+datos_candiato.id+'" class="btn">Ver&nbsp;perfil</a>'+
                            '</div>'+
                            '<div class="clear-botton"></div>'+
                    '</div>';
    });

    if(pristino){
        UIkit.notification({message: 'Ups.. No tienes candidatos.', status: 'warning'})
    }
}

mis_postulados_form.addEventListener('submit',  function(event){
    event.preventDefault();
    //console.log(mis_postulados_form.q.value);
    postulado_q = mis_postulados_form.q.value;
    fetch('api/listado_postulados',{
        method: 'POST', // or 'PUT'
        body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
        headers:{
          'Content-Type': 'application/json'
        }
      })
       .then(function(res){ return res.json();})
       .then(function(response){
          //console.log('listado_postulados',response);



        try {
            formBuscarPostulado(response.candidates);
        }catch(e){
        }

      });
})

function formBuscarPostulado(data){
    let q = postulado_q;
    let qt = postulado_qt;
    if(!q&&!qt){
        inflar_postulado(data);
    } else {

        let q_data = [];
        let n = data.length;
        for(let a = 0; a< data.length; a++){
            let nombre = data[a].personal_data.names + " " +data[a].personal_data.paternal_name + " " +data[a].personal_data.maternal_name;
            let ubicacion = data[a].personal_data.city + " " +data[a].personal_data.state;
            let titulo_obj = data[a].education_data.length>0? data[a].education_data[0]:{'degree_obtained':'Sin titulo.'};
            for(let b = 0; b<data[a].education_data.length;b++){
                if(titulo_obj.end_year<data[a].education_data.end_year){
                    titulo_obj = data[a].education_data;
                }
            }
            let titulo = titulo_obj.degree_obtained;


            if( nombre.toLowerCase().indexOf(q.toLowerCase())>-1||
                titulo.toLowerCase().indexOf(q.toLowerCase())>-1||
                ubicacion.toLowerCase().indexOf(q.toLowerCase())>-1
            ){
                q_data.push(data[a]);
            }
        }
        inflar_postulado(q_data);
    }
}

function inflar_postulado(response){

        document.getElementById('postulados-div').innerHTML="";

        let pristino  = 1;
        response.forEach(function(item) {

            //console.log(item);
            var nombre_completo = item.personal_data.names +" "+ item.personal_data.paternal_name +" "+ item.personal_data.maternal_name;
            var titulo = (item.education_data.length)?item.education_data[0]:{'degree_obtained':'Sin titulo.'} ;
            for(let a = 0; a<item.education_data.length; a++){
                if(titulo.end_year<item.education_data[a].end_year){
                    titulo=item.education_data[a];
                }
            }
            var img_perfil = (item.personal_data.profile_picture != null) ? item.personal_data.profile_picture : "public/Img/notpictureuser.png";
            var localizacion = item.personal_data.city + "," + item.personal_data.state;
            item.postulated_vacancies.forEach(function(postulacion){

                //console.log(postulacion);
                var status = postulacion.pivot.status;
                var porcentaje;
                var img_status;
                var id = postulacion.pivot.id;
                switch (status) {
                    case "Postulado":
                        porcentaje =  20;
                        img_status = 'public/Img/Desgrane/mipmap/icn_cv_enviado.png';
                        break;
                    case "CV Visto":
                        porcentaje =  40;
                        img_status = 'public/Img/Desgrane/mipmap/icn_cv_enviado.png';
                        break;
                    case "En entrevista":
                        porcentaje =  60;
                        img_status = 'public/Img/Desgrane/mipmap/icn_entrevista.png';
                        break;
                    case "Psicométrico":
                        porcentaje =  80;
                        img_status = 'public/Img/Desgrane/mipmap/icn_psicometrico.png';
                        break;
                    case "Contratado":
                        porcentaje =  100;
                        img_status = 'public/Img/Desgrane/mipmap/icn_contratado.png';
                        break;
                    case "Vacante cubierta":
                        porcentaje =  100;
                        img_status = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
                        break;
                    case "Rechazado":
                        porcentaje =  100;
                        img_status = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
                        break;
                }
                if(postulado_qt==""||postulado_qt==status){
                    pristino = 0;
                    document.getElementById('postulados-div').innerHTML +=
                    '<a href="./web/postulado?id='+id+'" uk-tooltip="title: Da click en la postulacion para ver detalle; pos: left">'+
                    '<div class="aCandidato">'+
                    '<div class="w3-circle uk-cover-container uk-height-medium foto">'+
                    '<img uk-cover class="" src="'+img_perfil+'" alt="">'+
                    '</div>'+

                    '<div class="info">'+
                    '<label>'+
                    nombre_completo +
                    '</label><br>'+
                    '<label class="span_green">'+
                    titulo.degree_obtained +
                    '</label><br>'+
                    '<label style="font-size: 10px "><i class="fa fa-map-marker span_green" aria-hidden="true"></i>'+localizacion+'</label>'+
                    '</div>'+
                    '<div class="boton">'+
                    '<img src="'+img_status +'" style="height: 30px;">'+
                    '<label>'+status+'</label>'+
                    '</div>'+
                    '<div class="clear-botton"></div>'+
                    '<div class="bar">'+
                    '<div style="width: '+porcentaje +'%">'+porcentaje +'%</div>'+
                    '</div>'+
                    '</div>'+
                    '</a>';
                }
            });
        });
        if(pristino){
            UIkit.notification({message: 'Ups.. No tienes postulados.', status: 'warning'})
        }

}

function formBuscarFetch(q){
    if(!!q){
        fetch('api/buscar_vacante', {
            method: 'POST', // or 'PUT'
            body: '{"email": "'+localStorage.getItem('prohunter')+'","search_string":"'+q+'"}',
            headers:{
                'Content-Type': 'application/json'
            }
        }
        )
        .then(function(res){return res.json()})
        .then(function(respo){inflar_vacantes(respo.data)});
    }
    else {
         fetch('api/listado_vacantes', {
            method: 'POST', // or 'PUT'
            body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
            headers:{
                'Content-Type': 'application/json'
            }
        }
        ).then(function(res){return res.json()})
        .then(function(respo){inflar_vacantes(respo.data)});
    }
}

function inflar_vacantes(respo_data){
    vacantes_lis=respo_data;
    fetch('api/listado_vacantes_favoritas',{
        method: 'POST', // or 'PUT'
        body: '{"email": "'+localStorage.getItem('prohunter')+'"}',
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(function(res){return res.json()})
    .then(function(vacantes_favoritas){
        document.getElementById('vacantes_div').innerHTML="";
        respo_data.forEach(function(element){
            //console.log(element);
            vacantes_favoritas.data.forEach(function(item){
                if(item.id==element.id){
                    element.fav = 1;
                    element.fav_fa = "fa-heart";
                    element.fav_text = "  Quitar de favoritos";
                }
            })
            element.fav = element.fav||'';
            element.fav_fa = element.fav_fa||'fa-heart-o';
            element.fav_text = element.fav_text||' Agregar a favoritos';
            unaVacante(element);
        });
        refres();
    })
    .catch(function(e){
        console.error(e);
    });

}

function unaVacante(data){
    //console.log(data);
    let div_vacante = document.createElement('div');
    div_vacante.setAttribute("class","w3-container w3-card w3-white w3-round w3-margin");
    div_vacante.setAttribute("id",'vacante-'+data.id);
    div_vacante.appendChild(document.createElement('br'));

    let div_top = document.createElement('div');
    div_top.setAttribute('style','min-height: 55px');

    let div_left = document.createElement('div');
    div_left.setAttribute('style','float:left;width:15%');
    let img = document.createElement('img');


    img.setAttribute('src',data.company_logo);
    img.setAttribute('alt',"Avatar");
    img.setAttribute('class',"w3-left w3-margin-right");
    img.setAttribute('style',"max-height:55px");

    div_left.appendChild(img);
    div_top.appendChild(div_left);

    let div_center = document.createElement('div');
    div_center.setAttribute('style','float:left;width:78%;');
    let span_ = document.createElement('span');

    span_.setAttribute('class',"encabezado_vacante");
    span_.appendChild(document.createTextNode(data.company_name));
    div_center.appendChild(span_);
    span_.appendChild(document.createElement('br'));
    span_ = document.createElement('span');
    span_.appendChild(document.createTextNode(data.vacancy_name));
    div_center.appendChild(span_);
    div_top.appendChild(div_center);

    let div_right = document.createElement('div');
    div_right.setAttribute('style','float:left;width:7%');
    let btn_share = document.createElement('button');
    btn_share.setAttribute('class','btn_share')
    let i = document.createElement('i');
    i.setAttribute("class","fa fa-share-alt");
    i.setAttribute("aria-hidden","true");
    btn_share.appendChild(i);
    btn_share.addEventListener('click',function(event){sharedFunction(data.id)});
    div_right.appendChild(btn_share);
    div_top.appendChild(div_right);
    div_vacante.appendChild(div_top);

    let div_boot_ = document.createElement('div');
    div_boot_.setAttribute('style','clear: both;');
    div_top.appendChild(div_boot_);


    let hr = document.createElement('hr');
    hr.setAttribute('class',"w3-clear");
    div_vacante.appendChild(hr);

    let div_mid = document.createElement('div');
    div_mid.setAttribute('style','width: 100%');

    let div_l = document.createElement('div');
    div_l.setAttribute('style','width: 50%');
    div_l.setAttribute('class','div_mid');

    let span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    let glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-calendar span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0 Publicada : '+data.updated_at.split(' ')[0]));


    div_l.appendChild(span);
    div_l.appendChild(document.createElement('br'));

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-calendar");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0 Limite : '+data.limit_date));

    div_l.appendChild(span);
    div_l.appendChild(document.createElement('br'));

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-money span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0$'+data.salary+' MXN'));

    div_l.appendChild(span);
    div_l.appendChild(document.createElement('br'));

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-percent span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0 Ganancia : '+data.contract_profit_percentage+'%'));

    div_l.appendChild(span);
    div_l.appendChild(document.createElement('br'));


    div_mid.appendChild(div_l);


    let div_r = document.createElement('div');
    div_r.setAttribute('style','width: 50%');
    div_r.setAttribute('class','div_mid');

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-map-marker span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0'+data.city+', '+data.state));

    div_r.appendChild(span);
    div_r.appendChild(document.createElement('br'));

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-users span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0'+data.total_registred+' Postulados'));

    div_r.appendChild(span);
    div_r.appendChild(document.createElement('br'));

    span = document.createElement('span');
    span.setAttribute('class',"w3-left w3-opacity");
    glicon = document.createElement('i');
    glicon.setAttribute('class',"fa fa-clock-o span_green");
    glicon.setAttribute('aria-hidden',"true");
    span.appendChild(glicon);
    span.appendChild(document.createTextNode('\u00A0'+data.schedule_type));

    div_r.appendChild(span);
    div_r.appendChild(document.createElement('br'));


    div_mid.appendChild(div_r);


    div_vacante.append(div_mid);

    let h4 = document.createElement('h4');
    let strong = document.createElement('strong');
    strong.style.color = "#2E3D56";
    strong.appendChild(document.createTextNode('Acerca de la empresa'));
    h4.appendChild(strong); div_vacante.appendChild(h4);
    let p = document.createElement('p');
    p.appendChild(document.createTextNode(data.company_info));
    div_vacante.appendChild(p);
    h4 = document.createElement('h4');
    strong = document.createElement('strong');
    strong.style.color = "#2E3D56";
    strong.appendChild(document.createTextNode('Detalle de la vacante'));
    h4.appendChild(strong);
    div_vacante.appendChild(h4);
    p = document.createElement('p');
    p.innerHTML=data.details.replace(/(?:\r\n|\r|\n)/g, '<br>');
    div_vacante.appendChild(p);

    let div = document.createElement('div');
    div.setAttribute('class',"w3-row-padding");
    div.setAttribute('style',"margin:0 -16px");
    div_vacante.appendChild(div);

    let button = document.createElement('button');
    button.setAttribute("type","button");
    button.setAttribute("class","btnRedondo_gris fav");
    button.setAttribute("id",'act-'+data.id);
    button.setAttribute("data-id",data.id);
    button.setAttribute("data-fav",data.fav);
    glicon = document.createElement('i')
    glicon.setAttribute("class",'fa '+data.fav_fa);
    button.appendChild(glicon);
    button.appendChild(document.createTextNode(data.fav_text));
    div_vacante.appendChild(button);


    button = document.createElement('button');
    button.setAttribute("type","button");
    button.setAttribute("class","btnRedondo_verde postular");
    button.setAttribute("data-id",data.id);
    glicon = document.createElement('i')
    glicon.setAttribute("class","fa fa-dot-circle-o");
    button.appendChild(glicon);
    button.appendChild(document.createTextNode(' Postular candidato'));
    div_vacante.appendChild(button);

    div_vacante.appendChild(document.createElement('br'));
    div_vacante.appendChild(document.createElement('br'));
    let textarea = document.createElement('textarea');
    textarea.setAttribute('style','display:none;width: 100%;');
    let shared =
"¡Hola! En ProHunter te hemos seleccionado como el mejor candidato para la vacante: "+data.vacancy_name+" \n\
Te compartimos un poco de la informacion de la vacante: \n\
Compañia: "+data.company_name+" \n\
Sueldo: "+data.salary+" MXN \n\
Ubicación: "+data.city+", "+data.state+" \n\
Detalle: "+data.details+"\n\
";
    textarea.value = shared;
    div_vacante.appendChild(textarea);

    document.getElementById('vacantes_div').appendChild(div_vacante);

}



 function postlVacnt(data){
    //console.log(data.dataset);
    UIkit.modal.dialog('<div class="uk-modal-body">        <h2 class="uk-modal-title">Selecciona un candidatos</h2>        <button class="uk-modal-close-default uk-close uk-icon" type="button" uk-close=""><svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"><line fill="none" stroke="#000" stroke-width="1.1" x1="1" y1="1" x2="13" y2="13"></line><line fill="none" stroke="#000" stroke-width="1.1" x1="13" y1="1" x2="1" y2="13"></line></svg></button>        <div class="scroll-candidatos" id="candicatos-sel">        </div>            </div>');

    candidatos_arr.forEach(function(item_){
        /*
            <img src="https://porvenirapps.com/prohunter/public/Img/notpictureuser.png" alt="Avatar" style="">
            <label>&nbsp;Jorge Octavio Manzano González </label>
        */
        let item = item_.personal_data;
        item.img = (item.profile_picture != null) ? item.profile_picture : "public/Img/notpictureuser.png";
        item.nombre = item.names + " " +item.paternal_name + " " +item.maternal_name;


        let div = document.createElement('div');
        div.setAttribute('class','item-candidatos');
        div.setAttribute('data-candidate_id',item.id);
        div.setAttribute('data-vacancy_id',data.dataset.id);
        let img = document.createElement('img');
        img.setAttribute('src',item.img);
        img.setAttribute('style','width:10%');
        div.appendChild(img);
        let label = document.createElement('label');
        label.appendChild(document.createTextNode(' '+item.nombre))
        div.appendChild(label)
        div.addEventListener('click', function(event){return api_postular(div.dataset)})
        document.getElementById('candicatos-sel').appendChild(div);

    });
}

 function actFav(element){
    let glicon = document.createElement('i');
    let nodo;
    if(element.dataset.fav == 1){
        delFav(element.dataset)
        .then(function(){
            element.dataset.fav=0;
            glicon.setAttribute('class','fa fa-heart-o');
            nodo = document.createTextNode('  Agregar a favoritos');
            element.innerHTML = "";
            element.appendChild(glicon);
            element.appendChild(nodo);
        });
    }
    else{
        addFav(element.dataset).then(function(){
            element.dataset.fav=1;
            glicon.setAttribute('class','fa fa-heart');
            nodo = document.createTextNode('  Quitar de favoritos');
            element.innerHTML = "";
            element.appendChild(glicon);
            element.appendChild(nodo);
        })
    }

    //json_decode(webservice(url('/api/listado_vacantes_favoritas'),$email,'POST'));
}

 function api_postular(data){
    //console.log(data);
    data.status="";
    fetch('api/postular_candidato', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
        })
    .then(function(res){return res.json();})
    .then(function(postular){
        //console.log(postular);
        UIkit.notification({message: postular.msg, status: !!postular.exito?'success':'danger'})
    });


}

 function delFav(data){
    //console.log(data);

    return fetch('api/modificar_status_vacante_favorita', {
    method: 'POST', // or 'PUT'
    body: '{"email": "'+localStorage.getItem('prohunter')+'","vacancy_id":"'+data.id+'","status":"0"}',
    headers:{
        'Content-Type': 'application/json'
    }
    })
    .then(function(res){return res.json();})
    .then(function(addFav){
        //console.log(addFav);
        reMisFav();
    });

    //json_decode(webservice(url('/api/listado_vacantes_favoritas'),$email,'POST'));
}
 function addFav(data){
    //console.log(data);
    //console.log(document.getElementById('vacante-'+data.id).children)

    return fetch('api/modificar_status_vacante_favorita', {
    method: 'POST', // or 'PUT'
    body: '{"email": "'+localStorage.getItem('prohunter')+'","vacancy_id":"'+data.id+'","status":"1"}',
    headers:{
        'Content-Type': 'application/json'
    }
    })
    .then(function(res){return res.json();})
    .then(function(addFav){
        //console.log(addFav);
        reMisFav();
    });

    //json_decode(webservice(url('/api/listado_vacantes_favoritas'),$email,'POST'));
}

function reMisFav(){
    if(document.getElementById('mis_favoritas').className.indexOf('uk-active')>-1){
        inflar_mis_vacantes();
    }
}

function sharedFunction(id){
    //console.log(document.getElementById('vacante-'+id).getElementsByTagName('textarea')[0]);

    var copy = document.getElementById('vacante-'+id).getElementsByTagName('textarea')[0];
    copy.style.display = "block";
    copy.select();
    let is_copy=document.execCommand("copy");
    if(is_copy){
        UIkit.notification({message: 'Listo. ¡Ya solo pegalo en donde gustes!', status: 'success'});
    }
    else {
        UIkit.notification({message: 'Ups.. algo salio mal. Intentemos mas adelante.', status: 'danger'})
    }


    copy.style.display = "none";


}
