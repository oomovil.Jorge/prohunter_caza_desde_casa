// Valida Que solo puedas usar numeros
Array.from(document.getElementsByClassName('txtSolo')).forEach(item => {
    var mask = new IMask(item, {
        mask: /^[a-zA-Z_áéíóúñ\s]*$/
    })
});

// formato de pagos

var currencyMask = new IMask(
    document.getElementById('currency-mask'),{
        mask: '$num',
        blocks: {
            num: {
                mask: Number,
                thousandsSeparator: ' '
            }
        }
    });

// Formato de telefono
 Array.from(document.getElementsByClassName('txtCel')).forEach(function (item){
 var mask = new IMask(item, {
 mask: '+(00)0000-0000'
 })
 });

// formato de  solo mayusculas
Array.from(document.getElementsByClassName('txtSoloMayus')).forEach( item => {
    var mask = new IMask(item, {
        mask: /^\w+$/,
        prepare: function (str) {
            return str.toUpperCase();
        },commit: function (value, masked) {
            masked._value = value.toLowerCase();
        }
    })
});


//Datapicker
$(function() {$('.datepicker').datepicker();});
$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    language: 'es-ES',
    trigger:true,
    autoHide:true,
    zIndex: 2048,
    endDate: date()
});
