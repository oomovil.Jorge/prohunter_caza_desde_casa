// Initialize Firebase
var config = {
    apiKey: "AIzaSyBLd_huWYpW5Pita3-9vAx3ukaJGxg1_tE",
    authDomain: "prohunter-42f92.firebaseapp.com",
    databaseURL: "https://prohunter-42f92.firebaseio.com",
    projectId: "prohunter-42f92",
    storageBucket: "prohunter-42f92.appspot.com",
    messagingSenderId: "134592454019"
  };
  firebase.initializeApp(config);

  const messaging = firebase.messaging();


  messaging.requestPermission()
  .then( function() {
   // console.log('Notification permission granted.');
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // ...
    return messaging.getToken();
  })
  .then(function(token){
    //  console.log(token);
      if(token!==localStorage.getItem('firebase')){
        localStorage.setItem('firebase',token);
        fetch(window.location.href.split("admin")[0]+'api/agregar_token_firebase',{
          method: 'POST', // or 'PUT'
          body: '{"email": "'+localStorage.getItem('admprohunter')+'", "token": "'+token+'"}',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(function(res){ return res.json();})
        .then(function(response){
           // console.log(response);
        })
      }
  })
  .catch(function(err){
   // console.log('Unable to get permission to notify.', err);
  });


  messaging.onMessage(function(payload){
  //  console.log('Message received. ', payload);

  /*  body: "3"
      icon: "http://porvenirapps.com/prohunter/storage/app/"
      title: "test"
    */
      var img ;
      switch (payload.notification.icon) {
          case "1":
               img = 'public/Img/Desgrane/mipmap/icn_cv_enviado.png';
              break;
          case "2":
               img = 'public/Img/Desgrane/mipmap/icn_cv_visto.png';
              break;
          case "3":
               img  = 'public/Img/Desgrane/mipmap/icn_entrevista.png';
              break;
          case "4":
               img = 'public/Img/Desgrane/mipmap/icn_psicometrico.png';
              break;
          case "5":
               img  = 'public/Img/Desgrane/mipmap/icn_contratado.png';
              break;
          case "6":
               img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
              break;
          case "7":
               img = 'public/Img/Desgrane/mipmap/icn_vac_cubierta.png';
              break;
          case "8":
               img = 'public/Img/Desgrane/mipmap/icn_info_menu_b.png';
              break;

          default: img = payload.notification.icon||'public/Img/Desgrane/mipmap/icn_contratado.png';
      }

      UIkit.notification({message: '<div style="width: 300px">    <img src="'+window.location.href.split("admin")[0]+img+'" style="float: left;width: 10%;">    <label style="float: right; margin-right: 30px;width: 70%;">'+payload.notification.title+'</label><div class="clear-botton"></div></div>', pos: 'top-center'});

      // ...
  });

