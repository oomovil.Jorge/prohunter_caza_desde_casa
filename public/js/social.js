console.log(navigator);


var config = {
    apiKey: "AIzaSyBLd_huWYpW5Pita3-9vAx3ukaJGxg1_tE",
    authDomain: "prohunter-42f92.firebaseapp.com",
    databaseURL: "https://prohunter-42f92.firebaseio.com",
    projectId: "prohunter-42f92",
    storageBucket: "prohunter-42f92.appspot.com",
    messagingSenderId: "134592454019"
};
firebase.initializeApp(config);

function  iniciofb(){
    var provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().languageCode = 'ES';
    firebase.auth().signInWithPopup(provider)
    .then( function(result) {

        document.getElementById('lotiFB').style.height =window.innerHeight+"px";
        document.getElementById('lotiFB').style.display = "block";
        lottieAnimationG.play();



        var user = result.user;
        var correog = user.uid + '@' + user.providerData[0].providerId;
        var nombre  = user.providerData[0].displayName;


        fetch(result.user.photoURL+'?type=square&height=1500',{method:'GET'})
        .then(function (res){return res.blob()})
        .then(function (img_){
            let reg ={
                nombre            : nombre,
                correo            : correog,
                contrasena        : user.uid,
                tipo:'facebook',
                info_dispositivo:navigator.appVersion,
                celular: user.providerData[0].phoneNumber||'0000000000',
                imagen_perfil: img_||''
            };
    
            let log ={
                correo            : correog,
                contrasena        : user.uid,
                info_dispositivo:navigator.appVersion
            };
    
            loginReg(log,reg);
        });
        
    })
    .catch(function(error) {
        console.log('error :' + error);
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
    });

}



function  iniciog(){

    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().languageCode = 'ES';
    firebase.auth().signInWithPopup(provider)
    .then( function (result) {

        document.getElementById('lotiG').style.height =window.innerHeight+"px";
        document.getElementById('lotiG').style.display = "block";
        lottieAnimationG.play();


        console.log(result.user.providerData[0]);
        
        var user = result.user;
        var correog = user.uid + '@' + user.providerData[0].providerId;
        var nombre  = user.providerData[0].displayName;

        fetch(result.user.photoURL,{method:'GET'})
        .then(function (res){return res.blob()})
        .then(function (img_){
        let reg ={
            nombre            : nombre,
            correo            : correog,
            contrasena        : user.uid,
            tipo:'google',
            info_dispositivo:navigator.appVersion,
            celular: user.providerData[0].phoneNumber||'0000000000',
            imagen_perfil: img_||''
        };

        let log ={
            correo            : correog,
            contrasena        : user.uid,
            info_dispositivo:navigator.appVersion
        };

         loginReg(log,reg);
        });

    })
    .catch(function(error) {
        console.log('error :' + error);
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
    });

}









function loginReg(log,reg){
    console.log('entra');
    console.log(document.getElementById('login'));
    fetch('api/login',{ method: 'POST',headers: {'Content-Type': 'application/json'},body: JSON.stringify(log)})
    .then(function(res){return res.json()})
    .then(function(log_){
        console.log(log_)
        if(log_.exito == 0){
            var form_data = new FormData();
            for ( var key in reg ) {
                form_data.append(key, reg[key]);
            }
            fetch('api/registro',{ method: 'POST',body: form_data})
            .then(function(res){return res.json()})
            .then(function(reg){ 
                console.log(reg);

                loginEnd(log);
            });
        }
        else{
            loginEnd(log);
        }
    });
                

                
}

function loginEnd(log){
    let form = document.createElement('form');
                form.method='POST';
                form.action="api/login_web";

                let input = document.createElement('INPUT');
                input.name = 'correo';
                form.appendChild(input);
                input = document.createElement('INPUT');
                input.name = 'contrasena';
                form.appendChild(input);
                document.getElementsByTagName('body')[0].appendChild(form)

                form.correo.value=log.correo;
                form.contrasena.value=log.contrasena;
                form.submit();
}