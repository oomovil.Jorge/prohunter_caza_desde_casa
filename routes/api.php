<?php

use Illuminate\Http\Request;


//Introducction controller
Route::post('introduccion', 'IntroSlidersController@introduccion');
//User controller
Route::post('registro', 'UserController@registro');
Route::post('login', 'UserController@login');
Route::post('recuperar_password', 'UserController@recuperar_password');
Route::post('actualizar_perfil', 'UserController@actualizar_perfil');
Route::post('actualizar_imagen_perfil', 'UserController@actualizar_imagen_perfil');
Route::post('agregar_token_firebase', 'UserController@agregar_token_firebase');
Route::post('notificacion_estado_postulado', 'UserController@notificacion_estado_postulado');
Route::post('agregar_modificar_datos_bancarios', 'UserController@agregar_modificar_datos_bancarios');
Route::post('listar_datos_bancarios', 'UserController@listar_datos_bancarios');
Route::post('listar_ganancias', 'UserController@listar_ganancias');
Route::post('listar__historial_ganancias', 'UserController@listar__historial_ganancias');
Route::post('listar_avisos_usuarios', 'UserController@listar_avisos_usuarios');
Route::post('marcar_avisos_usuarios', 'UserController@marcar_avisos_usuarios');
Route::post('mostrar_calificacion', 'UserController@mostrar_calificacion');
Route::post('estadistica', 'UserController@estadistica');
Route::post('listar_bancos', 'UserController@listar_bancos');

//user  en web
Route::post('login_web', 'UserController@login_web');
Route::get('cerrar_sesion_admin', 'UserController@cerrar_sesion_admin');
Route::get('cerrar_sesion_user', 'UserController@cerrar_sesion_user');
Route::post('registrar_administrador', 'UserController@registrar_administrador');
Route::get('eliminar_Admin', 'UserController@eliminar_Admin');
Route::post('modificar_Admin', 'UserController@modificar_Admin');
Route::post('registrar_usuario_en_Web', 'UserController@registrar_usuario_en_Web');
Route::get('eliminar_usuario', 'UserController@eliminar_usuario');
Route::post('modificar_Usuario_APP_Web', 'UserController@modificar_Usuario_APP_Web');
Route::post('actualizar_ganacias_usuario', 'UserController@actualizar_ganacias_usuario');
Route::post('calificar_usuario', 'UserController@calificar_usuario');
Route::get('eliminar_bancos', 'UserController@eliminar_bancos');
Route::post('agregar_bancos', 'UserController@agregar_bancos');

//Categorias controller
Route::post('listado_categorias', 'CategoriesController@listado_categorias');
Route::post('listado_preferencias_usuario', 'CategoriesController@listado_preferencias_usuario');
Route::post('seleccionar_preferencias', 'CategoriesController@seleccionar_preferencias');
Route::post('nueva_categoria', 'CategoriesController@nueva_categoria');
Route::get('eliminar_categoria', 'CategoriesController@eliminar_categoria');
Route::post('modificar_categoria', 'CategoriesController@modificar_categoria');

//Vacantes controller
Route::post('listado_vacantes', 'VacantesController@listado_vacantes');
Route::post('listado_vacantes_favoritas', 'VacantesController@listado_vacantes_favoritas');
Route::post('listado_vacantes_favoritas', 'VacantesController@listado_vacantes_favoritas');
Route::post('modificar_status_vacante_favorita', 'VacantesController@modificar_status_vacante_favorita');
Route::post('buscar_vacante', 'VacantesController@buscar_vacante');
Route::post('registrar_vacante', 'VacantesController@registrar_vacante');
Route::get('eliminar_vacante', 'VacantesController@eliminar_vacante');
Route::post('modificar_vacante', 'VacantesController@modificar_vacante');
Route::post('listar_postulados_vacante', 'VacantesController@listar_postulados_vacante');
Route::post('listar_detalle_vacante_cubierta', 'VacantesController@listar_detalle_vacante_cubierta');

//Candidate controller
Route::post('agregar_editar_candidato', 'CandidateController@agregar_editar_candidato');
Route::post('listado_candidatos', 'CandidateController@listado_candidatos');
Route::post('listado_candidatos_activos', 'CandidateController@listado_candidatos_activos');
Route::post('postular_candidato', 'CandidateController@postular_candidato');
Route::post('listado_postulados', 'CandidateController@listado_postulados');
Route::post('eliminar_candidato', 'CandidateController@eliminar_candidato');
Route::get('eliminar_candidato_web', 'CandidateController@eliminar_candidato_web');
Route::post('editar_estatus_postulacion', 'CandidateController@editar_estatus_postulacion');
Route::get('eliminar_postulacion_web', 'CandidateController@eliminar_postulacion_web');
Route::get('generar_excel', 'CandidateController@generar_excel');
Route::post('buscar_candidato', 'CandidateController@buscar_candidato');
Route::post('buscar_postulacion', 'CandidateController@buscar_postulacion');
Route::post('datos_candidato', 'CandidateController@datos_candidato');
Route::post('agregar_archivos_candidato', 'CandidateController@agregar_archivos_candidato');
Route::post('email_administrador_postulacion', 'CandidateController@email_administrador_postulacion');

//Faq controller
Route::post('listado_preguntas_frecuentes', 'FaqController@listado_preguntas_frecuentes');
Route::post('agregar_pregunta', 'FaqController@agregar_pregunta');
Route::get('eliminar_preguntas', 'FaqController@eliminar_preguntas');
//Notices controller
Route::post('listado_notificacion', 'NoticesController@listado_notificacion');
Route::get('eliminar_notificacion', 'NoticesController@eliminar_notificacion');
Route::post('registrar_notificacion', 'NoticesController@registrar_notificacion');
Route::post('modificar_notificacion', 'NoticesController@modificar_notificacion');
//Lenguajes controller
Route::post('load_languages_softwares', 'LanguagesController@load_languages_softwares');
Route::post('agregar_lenguaje', 'LanguagesController@agregar_lenguaje');
Route::get('eliminar_lenguaje', 'LanguagesController@eliminar_lenguaje');

//Location controller
Route::post('load_countries', 'LocationController@load_countries');
Route::post('load_states', 'LocationController@load_states');
Route::post('load_cities', 'LocationController@load_cities');
Route::get('load_states_web', 'LocationController@load_states_web');
Route::get('load_cities_web', 'LocationController@load_cities_web');
Route::post('agregar_pais', 'LocationController@agregar_pais');
Route::post('agregar_estado', 'LocationController@agregar_estado');
Route::post('agregar_ciudad', 'LocationController@agregar_ciudad');
Route::get('elimina_pais', 'LocationController@elimina_pais');
Route::get('elimina_estado', 'LocationController@elimina_estado');
Route::get('elimina_ciudad', 'LocationController@elimina_ciudad');

//Complement
Route::post('agregar_software_categoria', 'ComplementsController@agregar_software_categoria');
Route::get('eliminar_software', 'ComplementsController@eliminar_software');

//Blog controller
Route::post('blog', 'BlogController@blog');
Route::post('aviso_privacidad', 'BlogController@aviso_privacidad');
Route::post('Cambiar_blog_Aviso', 'BlogController@Cambiar_blog_Aviso');
Route::post('about', 'BlogController@about');
Route::post('actualizar_about_imgs', 'BlogController@actualizar_about_imgs');
Route::post('actualizar_about_texto', 'BlogController@actualizar_about_texto');
Route::get('eliminar_img_about', 'BlogController@eliminar_img_about');

//IntroSlider
Route::post('agregar_nuevo_slider', 'IntroSlidersController@agregar_nuevo_slider');
Route::get('eliminar_slider', 'IntroSlidersController@eliminar_slider');

//Notificacion firebase
Route::post('notificacion_manual', 'UserController@notificacion_manual');


