<?php

/*Backend*/

Route::prefix('admin')->group(function () {

    /*Login*/

    Route::get('/', 'Backend\AdministratorController@view_login')->name('Login');

    Route::get('Dashboard', 'Backend\DashboardController@view')->name('Dashboard');

    Route::prefix('Administradores')->group(function () {

        $controller = "Backend\AdministratorController@";

        Route::get('/', $controller . 'view_table')->name('administradores');
        Route::get('/Nuevo_administrador', $controller . 'view_add')->name('administradores_add');
        Route::get('/Modificar_administrador', $controller . 'view_edit')->name('administradores_edit');

    });


    Route::prefix('Usuarios_app')->group(function () {

        $controller = "Backend\UsuariosAppController@";

        Route::get('/', $controller . 'view_show')->name('usuariosApp');
        Route::get('/Nuevo_usuario', $controller . 'view_add')->name('usuariosApp_add');
        Route::get('/Modificar_usuario', $controller . 'view_edit')->name('usuariosApp_edit');
        Route::get('/Mi_cuenta', $controller . 'view_Mi_cuenta')->name('usuariosApp_Mi_cuenta');
        Route::get('/Ganacias', $controller . 'view_Ganancias')->name('Ganancias');
        Route::get('/Estadistica', $controller . 'view_Estadistica')->name('usuariosApp_Estadistica');
        Route::get('/Listar_favoritos', $controller . 'view_Listar_favoritos')->name('usuariosApp_view_Listar_favoritos');
        Route::get('/Detalle_Candidato', $controller . 'view_Detalle_Candidato')->name('DetalleCandidato');
        Route::get('/Ver_Candidatos_usuario', $controller . 'view_candidato_usuario');
        Route::get('/Detalle_Candidato/Imprimir_Candidato', $controller . 'view_imprimir_candidato')->name('Imprimir_Candidato');

    });


    Route::prefix('Categorias')->group(function () {
        $controller = "Backend\CategoriasController@";

        Route::get('/',  $controller . 'view_show')->name('categoria');
        Route::get('/Nueva_categoria',  $controller . 'view_add')->name('administradores_add');
        Route::get('/Modificar_categoria',  $controller . 'view_edit')->name('administradores_edit');

    });

    Route::prefix('Vacantes')->group(function () {

        $controller = "Backend\VacantesController@";

        Route::get('/', $controller . 'view_show')->name('vacantes');
        Route::get('/Nueva_Vacante', $controller . 'view_add');
        Route::get('/Modificar_Vacante', $controller . 'view_edit');
        Route::get('/Vacantes_cubiertas', $controller . 'view_Vacantes_cubiertas')->name('vacantes_cubiertas');

    });

    Route::prefix('Postulados')->group(function () {

        $controller = "Backend\PostuladosController@";

        Route::get('/', $controller . 'view_show')->name('postulados');
        Route::get('/Nuevo_Postulado', $controller . 'view_add');
        Route::get('/Ver_postulacion', $controller . 'view_Ver_postulacion')->name('ver_Postulacion');
        Route::get('/Rechazados', $controller . 'view_Rechazados')->name('Rechazados');

    });

    Route::prefix('Notificaciones')->group(function () {

        $controller = "Backend\NotificacionesController@";

        Route::get('/', $controller . 'view_show')->name('notificacion');
        Route::get('/Nueva_Notificacion',$controller . 'view_add');
        Route::get('/Modificar_notificacion', $controller . 'view_edit');

    });

    Route::prefix('Complementos')->group(function () {

        $controller = "Backend\ComplementosController@";


        Route::get('/Admin_intro_app', $controller . 'Admin_intro_app');
        Route::get('/Zona_Geografica', $controller . 'Zona_Geografica');
        Route::get('/Blog_Aviso_privacidad', $controller . 'Blog_Aviso_privacidad');
        Route::get('/Idiomas', $controller . 'Idiomas');
        Route::get('/Intro_Slider', $controller . 'Intro_Slider');
        Route::get('/Software_Categorias', $controller . 'Software_Categorias');
        Route::get('/Acerca_de_app', $controller . 'Acerca_de_app');
        Route::get('/Preguntas_frecuentes', $controller . 'Preguntas_frecuentes');
        Route::get('/Bancos', $controller . 'Bancos');

    });


});

/* APP WEB */

Route::get('/', 'Backend\landingController@view_landing')->name('Landing_web');
Route::get('/login', 'Backend\landingController@view_login')->name('Login_web');
Route::get('/web', 'Backend\landingController@view_web')->name('Web');
Route::get('/recuperar', 'Backend\landingController@view_recuperar');
Route::get('/registro', 'Backend\landingController@view_registro')->name('registro');

Route::get('/web/perfil', 'Backend\landingController@view_perfil')->name('perfil');

Route::get('/web/perfil', 'Backend\landingController@view_perfil')->name('perfil');
Route::get('/web/candidato', 'Backend\landingController@view_Candidato')->name('Candidato');
Route::get('/web/editar/candidato/', 'Backend\landingController@view_editar_Candidato')->name('Modificar_candidato');
Route::get('/web/registro/candidato/', 'Backend\landingController@view_registro_Candidato')->name('Registro_candidato');
Route::get('/web/postulado/', 'Backend\landingController@view_Postulado')->name('Postulado');

Route::get('/Invitado', 'Backend\landingController@view_Invitado')->name('Invitado');
Route::get('/Movil', 'Backend\landingController@view_Movil')->name('Movil');
